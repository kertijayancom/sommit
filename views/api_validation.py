from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework import status


@api_view(['POST'])
def user_validation(request):
    if request.method == 'POST':
        username = request.data['username']
        password = request.data['password']
        query = User.objects.filter(username=username,password=password)

        if(len(query)==1):
            return Response(status.HTTP_200_OK)
        else:
            return Response(status.HTTP_204_NO_CONTENT)
    else:
        return Response(status.HTTP_403_FORBIDDEN)
