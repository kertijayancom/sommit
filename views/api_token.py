from rest_framework import generics
from rest_framework.decorators import api_view
from ..serializer.token_serializers import TokenSerializer
from django.contrib.auth.models import User, Group
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from ..extras.news.detik import detik_api
from ..extras.tokenizer.tokenizer_news import NewsTokenizer
from ..extras.algorithm.naivebayes import NaiveBayes

@api_view(['POST'])
def get_user_token(request):

    if request.method == 'POST':
        username = request.data['username']
        password = request.data['password']

        if len(User.objects.filter(username= username, password=password))==1 :
            token = Token.objects.filter(user = User.objects.filter(username= username, password=password)).first()
            return Response({"token": token.key})
        else:
            return Response(status.HTTP_204_NO_CONTENT)

    return Response(status.HTTP_403_FORBIDDEN)


@api_view(['GET'])
def get_user_information(request):
    if request.method == 'GET':
        information = {}
        information['token'] = request.META.get('HTTP_AUTHORIZATION').split()[1]
        token = Token.objects.filter(key = information['token']).first()
        information['user'] = {
            "id" : token.user.id,
            "username" : token.user.username,
            "email" : token.user.email,
            "password" : token.user.password
        }
        return Response(information)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def get_berita_detik(request):
    if request.method == 'POST':
        query = request.data['query']
        # obj = detik_api.DetikApi(10)get_news(query=query)
        obj = {
                "file_name": "_2017_11_07__16_07_42.576005_detik.json",
                "sumber": "detik",
                "date_created": "2017_11_07__16_07_42.576005",
                "feed": [
                    {
                        "content": "Jakarta - Peserta aksi damai 2 Desember mulai berdatangan ke Masjid Istiqlal, Jakarta Pusat. Dua posko logistik sudah disiapkan di pelataran Masjid Istiqlal.Pantauan detikcom pukul 17.24 WIB, Kamis (1/12/2016), relawan untuk aksi damai juga sudah berdatangan dari berbagi daerah dan memadati sisi pelataran Masjid Istiqlal di Jl Taman Wijaya Kusuma, Jakpus.Foto: Ahmad Ziaul Fitrahudin/detikcom Para peserta aksi dan relawan aksi damai 2 Desember di Masjid Istiqlal, Kamis (1/12/2016)<!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->\"Sekarang diberi kemudahan, umat sudah berdatangan. Alhamdulillah berkah, sumbangan ini perseorangan sukarela saja bahkan berapa pun kami terima. Dari Sabtu sumbangan sudah mengalir tapi kami tidak bisa menampung,\" ujar relawan Moslem Care Community, Imanul Haq.Sore ini, terlihat mobil yang membawa logistik terus berdatangan membawa berbagai macam bantuan seperti air mineral, nasi bungkus, makanan ringan, alat kebersihan.Foto: Ahmad Ziaul Fitrahudin/detikcomPara peserta aksi dan relawan aksi damai 2 Desember di Masjid Istiqlal, Kamis (1/12/2016)Logistik yang terkumpul langsung dibagikan pada saat yang sama. Peserta pun mengantre tertib dan relawan bersiaga di posko untuk membagikan logistik.\"Logistik langsung dibagi. Apalagi kawan-kawan baru datang. Begitu dateng kita juga langsung bagikan,\" kata Imanul.Salah satu peserta aksi asal Batam, Azka mengaku bersyukur dengan kehadiran para relawan yang menyiapkan logistik. Para relawan sangat membantu peserta aksi.\"Alhamdulillah dapet nasi sama air mineral dari posko, gratis kok,\" ujar Azka.Foto: Ahmad Ziaul Fitrahudin/detikcom Para peserta aksi dan relawan aksi damai 2 Desember di Masjid Istiqlal, Kamis (1/12/2016)(azf/fdn)<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong.create({ target: 'thepolong', id: 55 }) ",
                        "location": "Jakarta",
                        "title": "Peserta Aksi 2 Desember Berdatangan, Posko Logistik Disiapkan di Istiqlal"
                    },
                    {
                        "content": "Jakarta - Ledakan terjadi dini hari pada Sabtu di area kota tua Dhaka sekitar pukul 02:00 pagi.Serangan bom yang menyasar warga Syiah di Bangladesh menewaskan satu orang dan melukai 50 lainnya.  Polisi mengatakan beberapa bom buatan sendiri dilempar ke tengah ribuan orang yang tengah menghadiri perayaan Asyura di Dhaka, ibu kota Bangladesh. <!--// <![CDATA[OA_show('parallaxindetail'); // ]]> --> Pemimpin kelompok Syiah yang merupakan minoritas kecil di Bangladesh mengatakan bahwa ini adalah pertama kalinya peringatan tersebut menjadi sasaran serangan.  Di Pakistan, setidaknya 22 tewas pada Jumat (23/10) saat seorang pengebom bunuh diri menyerang upacara Asyura di Jacobabad.  Ledakan tersebut terjadi dini hari pada Sabtu di area kota tua Dhaka sekitar pukul 02:00 pagi.  Warga Syiah saat itu sedang berkumpul di Hussaini Dalan, situs Syiah utama dan bersejarah di kota tersebut, untuk mulai pawai tahunan untuk memperingati Asyura.  Polisi menahan tiga orang di lokasi, tapi kemudian tak memberi informasi tentang identitas orang yang diduga adalah kelompok penyerang.  \"Sepengetahuan kami, ledakan dimaksudkan untuk membuat kepanikan di antara kerumunan dan menciptakan situasi yang kacau,\" kata Kolonel Ziaul Ahsan dari Batalyon Pasukan Gerak Cepat. \"Kami menemukan dua bom yang belum meledak. Bentuknya seperti alat eksplosif dan hampir seperti granat dan dilengkapi batere,\" kata kepala polisi setempat Azizul Haq pada kantor berita AFP.  Korban yang dibawa ke rumah sakit dilaporkan tidak dalam kondisi kritis.  Pada Hari Asyura, warga Syiah memperingati meninggalnya Imam Hussein, cucu Nabi Muhammad. (ita/ita)<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong.create({ target: 'thepolong', id: 55 }) ",
                        "location": "Jakarta",
                        "title": "Bom Serang Warga Syiah Dhaka Sebabkan Korban Tewas"
                    },
                    {
                        "content": "Jakarta - Bung Tomo adalah sosok Pejuang Islam yang sangat fenomenal. Pekikan takbir disertai dengan semboyan yang begitu masyur \\\\\\\"Merdeka atau Mati Syahid\\\\\\\" berhasil ia gunakan untuk membakar semangat jihad arek-arek Surabaya kala itu. Melalui corong-corong radio Bung Tomo mampu menggerakkan perlawanan rakyat untuk menghadapi penjajah pada saat peristiwa 10 November 1945 atas dukungan ulama-ulama setempat dengan fatwa Jihad (Resolusi Jihad) para ulama tersebut.Dengan semangat yang berkobar-kobar dan dilandasi dengan keimanan yang mendalam dalam petikan pidatonya Bung Tomo mengatakan, \\\\\\\"Dan kita jakin, saoedara-saoedara, pada akhirnja pastilah kemenangan akan djatuh ke tangan kita, sebab Allah selaloe berada di pihak jang benar, pertjajalah saoedara-saoedara, Toehan akan melindungi kita sekalian, Allahu Akbar..! Allahu Akbar..! Allahu Akbar..!\\\\\\\"Perjuangan yang dilakukan Bung Tomo dan arek-arek Surabaya yang sebagian besar merupakan santri-santri dari Pondok Pesanten yang berada di wilayah Surabaya dan sekitarnya waktu itu sungguh luar biasa. Ribuan kiai dan santri dari berbagai daerah sebagaimana ditulis oleh MC Ricklefs (1991) berbondong-bondong ke Surabaya guna memenuhi seruan Jihad itu.Sedemikian dahsyatnya sampai-sampai salah seorang komandan pasukan (Netherlands Indies Civil Administration) NICA sekutu, yakni Ziaul Haq dari India merasa heran menyaksikan kiai dan santri bertakbir sambil mengacungkan senjata. Sebagai muslim, hatinya pun tersentuh, dia akhirnya menarik diri dari medan perang. Sikapnya itu sempat membuat pasukan sekutu panik.Pada peristiwa lain sejarah juga mencatat bahwa Bung Tomo berhasil membentuk pasukan berani mati, yang dikenal dengan Pasukan Bom Syahid. Suatu hari Pasukan Bom Syahid itu melahirkan syuhada bom syahid pertama, seorang pemuda yang tak dikenal namanya menubrukkan dirinya ke tank Belanda. Akhirnya tank Belanda itu pun hancur. Allahu Akbar!Perjuangan rakyat Indonesia dalam mengusir penjajah Belanda telah banyak melahirkan sosok-sosok para pahlawan yang sangat gigih dan begitu gagah berani menerjang musuh-musuhnya. Atas berkat rahmat Allah yang maha kuasa mereka berhasil mengusir tentara-tentara penjajah dari bumi Indonesia. Hal ini tentunya bisa menjadi pelajaran berharga bagi kita semua sebagai penerus generasi bangsa.Kini tentara-tentara para penjajah itu telah pergi dari bumi pertiwi, dan Bung Tomo pun telah perpulang ke rahmat Illahi ketika beliau sedang menjalankan wukuf di Arafah dalam rangkaian menunaikan Ibadah haji. Semoga Allah SWT menempatkan tempat yang mulia di sisi-Nya.Pekerjaan besar masih menanti generasi sekarang, ketika negeri yang telah dinyatakan merdeka lebih dari lebih dari 60 tahun ini ternyata kondisi sebenarnya masih terjajah. Bagaimana tidak. Sistem ekonominya masih sistem ekonomi penjajah yakni kapitalisme. Alhasil kekayaan alam yang begitu melimpah ruah ini sekarang sebagian besar masih di eksploitasi pihak asing. Hal ini berimbas pada kesejahteraan masyarakat yang jauh dari harapan. Tercatat jika menggunakan standard Bank Dunia hampir sekitar 49% masyarakat Indonesia hidup dengan kurang dari US$ 2 per hari alias miskin. Ditambah lagi menggelembungnya hutang negara yang semakin menghabiskan energi negara ini.Di sektor politik negeri ini juga masih menggunakan sistem penjajah yakni demokrasi sekuler yang salah satu ritual 5 tahunannya yang baru saja dilaksanakan kemarin nyata-nyata telah menguras harta milik rakyat. Namun, tidak menghasilkan apa-apa. Apalagi sistem kufur ini bertentangan dengan Aqidah dan Syariah penduduknya yang mayoritas beragama Islam.Penjajahan juga berlangsung di sektor budaya. Ketika budaya hedonisme merengsek masuk ke tengah-tengah masyarakat. Bahkan, baru-baru ini diinformasikan bahwa Indonesia dinobatkan sebagai jawara kedua sebagai pengklik terbanyak situs-situs porno di Internet. Sungguh memprihatinkan. Penjajahan juga masih berlangsung di sektor-sektor yang lain. Inilah sedikit gambaran bahwa Indonesia sebenarnya masih terjajah.Maka sudah selayaknya kita kobarkan semangat perlawanan terhadap penjajahan ini. Mari kita bebaskan Indonesia dari cengkraman penjajah. Dengan berjuang secara sungguh-sungguh untuk menerapkan Syariah Islam secara kaffah dalam bingkai Khilafah Islam. Karena hanya dengan inilah Indonesia bisa benar-benar merdeka.Meminjam sedikit pidato dari Bung Tomo untuk mengobarkan semangat perjuangan bagi diri saya pribadi dan semuanya:\\\\\\\"Bismilahirrahmanirrahim ... marilah kita berjuang dengan sungguh-sungguh menegakkan syariah dan Khilafah untuk Indonesia yang lebih baik. Dan kita yakin, saudara-saudara, pada akhirnya pastilah kemenangan akan jatuh ke tangan kita, sebab Allah selalu berada di pihak yang benar, percayalah saudara-saudara, Tuhan akan melindungi kita sekalian, Allahu Akbar..! Allahu Akbar..! Allahu Akbar..!\\\\\\\"Ali MustofaGang Nusa Indah Cemani Grogol Sukoharjo Surakartaalie_jawi@yahoo.com02719272791<\\/strong><!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->(msh/msh)<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong.create({ target: 'thepolong', id: 55 }) ",
                        "location": "Jakarta",
                        "title": "Meneruskan Semangat Juang Bung Tomo"
                    }
                ]
             }
        tokenizer = NewsTokenizer("file").clean_detik_2(obj)
        tokenizer = {
                        "_file": {
                            "feed": [
                                {
                                    "title": "Peserta Aksi 2 Desember Berdatangan, Posko Logistik Disiapkan di Istiqlal",
                                    "location": "Jakarta",
                                    "content": "Jakarta - Peserta aksi damai 2 Desember mulai berdatangan ke Masjid Istiqlal, Jakarta Pusat. Dua posko logistik sudah disiapkan di pelataran Masjid Istiqlal.Pantauan detikcom pukul 17.24 WIB, Kamis (1/12/2016), relawan untuk aksi damai juga sudah berdatangan dari berbagi daerah dan memadati sisi pelataran Masjid Istiqlal di Jl Taman Wijaya Kusuma, Jakpus.Foto: Ahmad Ziaul Fitrahudin/detikcom Para peserta aksi dan relawan aksi damai 2 Desember di Masjid Istiqlal, Kamis (1/12/2016)<!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->\"Sekarang diberi kemudahan, umat sudah berdatangan. Alhamdulillah berkah, sumbangan ini perseorangan sukarela saja bahkan berapa pun kami terima. Dari Sabtu sumbangan sudah mengalir tapi kami tidak bisa menampung,\" ujar relawan Moslem Care Community, Imanul Haq.Sore ini, terlihat mobil yang membawa logistik terus berdatangan membawa berbagai macam bantuan seperti air mineral, nasi bungkus, makanan ringan, alat kebersihan.Foto: Ahmad Ziaul Fitrahudin/detikcomPara peserta aksi dan relawan aksi damai 2 Desember di Masjid Istiqlal, Kamis (1/12/2016)Logistik yang terkumpul langsung dibagikan pada saat yang sama. Peserta pun mengantre tertib dan relawan bersiaga di posko untuk membagikan logistik.\"Logistik langsung dibagi. Apalagi kawan-kawan baru datang. Begitu dateng kita juga langsung bagikan,\" kata Imanul.Salah satu peserta aksi asal Batam, Azka mengaku bersyukur dengan kehadiran para relawan yang menyiapkan logistik. Para relawan sangat membantu peserta aksi.\"Alhamdulillah dapet nasi sama air mineral dari posko, gratis kok,\" ujar Azka.Foto: Ahmad Ziaul Fitrahudin/detikcom Para peserta aksi dan relawan aksi damai 2 Desember di Masjid Istiqlal, Kamis (1/12/2016)(azf/fdn)<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong.create({ target: 'thepolong', id: 55 }) "
                                },
                                {
                                    "title": "Bom Serang Warga Syiah Dhaka Sebabkan Korban Tewas",
                                    "location": "Jakarta",
                                    "content": "Jakarta - Ledakan terjadi dini hari pada Sabtu di area kota tua Dhaka sekitar pukul 02:00 pagi.Serangan bom yang menyasar warga Syiah di Bangladesh menewaskan satu orang dan melukai 50 lainnya.  Polisi mengatakan beberapa bom buatan sendiri dilempar ke tengah ribuan orang yang tengah menghadiri perayaan Asyura di Dhaka, ibu kota Bangladesh. <!--// <![CDATA[OA_show('parallaxindetail'); // ]]> --> Pemimpin kelompok Syiah yang merupakan minoritas kecil di Bangladesh mengatakan bahwa ini adalah pertama kalinya peringatan tersebut menjadi sasaran serangan.  Di Pakistan, setidaknya 22 tewas pada Jumat (23/10) saat seorang pengebom bunuh diri menyerang upacara Asyura di Jacobabad.  Ledakan tersebut terjadi dini hari pada Sabtu di area kota tua Dhaka sekitar pukul 02:00 pagi.  Warga Syiah saat itu sedang berkumpul di Hussaini Dalan, situs Syiah utama dan bersejarah di kota tersebut, untuk mulai pawai tahunan untuk memperingati Asyura.  Polisi menahan tiga orang di lokasi, tapi kemudian tak memberi informasi tentang identitas orang yang diduga adalah kelompok penyerang.  \"Sepengetahuan kami, ledakan dimaksudkan untuk membuat kepanikan di antara kerumunan dan menciptakan situasi yang kacau,\" kata Kolonel Ziaul Ahsan dari Batalyon Pasukan Gerak Cepat. \"Kami menemukan dua bom yang belum meledak. Bentuknya seperti alat eksplosif dan hampir seperti granat dan dilengkapi batere,\" kata kepala polisi setempat Azizul Haq pada kantor berita AFP.  Korban yang dibawa ke rumah sakit dilaporkan tidak dalam kondisi kritis.  Pada Hari Asyura, warga Syiah memperingati meninggalnya Imam Hussein, cucu Nabi Muhammad. (ita/ita)<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong.create({ target: 'thepolong', id: 55 }) "
                                },
                                {
                                    "title": "Meneruskan Semangat Juang Bung Tomo",
                                    "location": "Jakarta",
                                    "content": "Jakarta - Bung Tomo adalah sosok Pejuang Islam yang sangat fenomenal. Pekikan takbir disertai dengan semboyan yang begitu masyur \\\\\\\"Merdeka atau Mati Syahid\\\\\\\" berhasil ia gunakan untuk membakar semangat jihad arek-arek Surabaya kala itu. Melalui corong-corong radio Bung Tomo mampu menggerakkan perlawanan rakyat untuk menghadapi penjajah pada saat peristiwa 10 November 1945 atas dukungan ulama-ulama setempat dengan fatwa Jihad (Resolusi Jihad) para ulama tersebut.Dengan semangat yang berkobar-kobar dan dilandasi dengan keimanan yang mendalam dalam petikan pidatonya Bung Tomo mengatakan, \\\\\\\"Dan kita jakin, saoedara-saoedara, pada akhirnja pastilah kemenangan akan djatuh ke tangan kita, sebab Allah selaloe berada di pihak jang benar, pertjajalah saoedara-saoedara, Toehan akan melindungi kita sekalian, Allahu Akbar..! Allahu Akbar..! Allahu Akbar..!\\\\\\\"Perjuangan yang dilakukan Bung Tomo dan arek-arek Surabaya yang sebagian besar merupakan santri-santri dari Pondok Pesanten yang berada di wilayah Surabaya dan sekitarnya waktu itu sungguh luar biasa. Ribuan kiai dan santri dari berbagai daerah sebagaimana ditulis oleh MC Ricklefs (1991) berbondong-bondong ke Surabaya guna memenuhi seruan Jihad itu.Sedemikian dahsyatnya sampai-sampai salah seorang komandan pasukan (Netherlands Indies Civil Administration) NICA sekutu, yakni Ziaul Haq dari India merasa heran menyaksikan kiai dan santri bertakbir sambil mengacungkan senjata. Sebagai muslim, hatinya pun tersentuh, dia akhirnya menarik diri dari medan perang. Sikapnya itu sempat membuat pasukan sekutu panik.Pada peristiwa lain sejarah juga mencatat bahwa Bung Tomo berhasil membentuk pasukan berani mati, yang dikenal dengan Pasukan Bom Syahid. Suatu hari Pasukan Bom Syahid itu melahirkan syuhada bom syahid pertama, seorang pemuda yang tak dikenal namanya menubrukkan dirinya ke tank Belanda. Akhirnya tank Belanda itu pun hancur. Allahu Akbar!Perjuangan rakyat Indonesia dalam mengusir penjajah Belanda telah banyak melahirkan sosok-sosok para pahlawan yang sangat gigih dan begitu gagah berani menerjang musuh-musuhnya. Atas berkat rahmat Allah yang maha kuasa mereka berhasil mengusir tentara-tentara penjajah dari bumi Indonesia. Hal ini tentunya bisa menjadi pelajaran berharga bagi kita semua sebagai penerus generasi bangsa.Kini tentara-tentara para penjajah itu telah pergi dari bumi pertiwi, dan Bung Tomo pun telah perpulang ke rahmat Illahi ketika beliau sedang menjalankan wukuf di Arafah dalam rangkaian menunaikan Ibadah haji. Semoga Allah SWT menempatkan tempat yang mulia di sisi-Nya.Pekerjaan besar masih menanti generasi sekarang, ketika negeri yang telah dinyatakan merdeka lebih dari lebih dari 60 tahun ini ternyata kondisi sebenarnya masih terjajah. Bagaimana tidak. Sistem ekonominya masih sistem ekonomi penjajah yakni kapitalisme. Alhasil kekayaan alam yang begitu melimpah ruah ini sekarang sebagian besar masih di eksploitasi pihak asing. Hal ini berimbas pada kesejahteraan masyarakat yang jauh dari harapan. Tercatat jika menggunakan standard Bank Dunia hampir sekitar 49% masyarakat Indonesia hidup dengan kurang dari US$ 2 per hari alias miskin. Ditambah lagi menggelembungnya hutang negara yang semakin menghabiskan energi negara ini.Di sektor politik negeri ini juga masih menggunakan sistem penjajah yakni demokrasi sekuler yang salah satu ritual 5 tahunannya yang baru saja dilaksanakan kemarin nyata-nyata telah menguras harta milik rakyat. Namun, tidak menghasilkan apa-apa. Apalagi sistem kufur ini bertentangan dengan Aqidah dan Syariah penduduknya yang mayoritas beragama Islam.Penjajahan juga berlangsung di sektor budaya. Ketika budaya hedonisme merengsek masuk ke tengah-tengah masyarakat. Bahkan, baru-baru ini diinformasikan bahwa Indonesia dinobatkan sebagai jawara kedua sebagai pengklik terbanyak situs-situs porno di Internet. Sungguh memprihatinkan. Penjajahan juga masih berlangsung di sektor-sektor yang lain. Inilah sedikit gambaran bahwa Indonesia sebenarnya masih terjajah.Maka sudah selayaknya kita kobarkan semangat perlawanan terhadap penjajahan ini. Mari kita bebaskan Indonesia dari cengkraman penjajah. Dengan berjuang secara sungguh-sungguh untuk menerapkan Syariah Islam secara kaffah dalam bingkai Khilafah Islam. Karena hanya dengan inilah Indonesia bisa benar-benar merdeka.Meminjam sedikit pidato dari Bung Tomo untuk mengobarkan semangat perjuangan bagi diri saya pribadi dan semuanya:\\\\\\\"Bismilahirrahmanirrahim ... marilah kita berjuang dengan sungguh-sungguh menegakkan syariah dan Khilafah untuk Indonesia yang lebih baik. Dan kita yakin, saudara-saudara, pada akhirnya pastilah kemenangan akan jatuh ke tangan kita, sebab Allah selalu berada di pihak yang benar, percayalah saudara-saudara, Tuhan akan melindungi kita sekalian, Allahu Akbar..! Allahu Akbar..! Allahu Akbar..!\\\\\\\"Ali MustofaGang Nusa Indah Cemani Grogol Sukoharjo Surakartaalie_jawi@yahoo.com02719272791<\\/strong><!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->(msh/msh)<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong.create({ target: 'thepolong', id: 55 }) "
                                }
                            ],
                            "date_created": "2017_11_07__16_07_42.576005",
                            "file_name": "_2017_11_07__16_07_42.576005_detik.json",
                            "sumber": "detik"
                        },
                        "source": "detik",
                        "feed": [
                            {
                                "title": "Peserta Aksi 2 Desember Berdatangan, Posko Logistik Disiapkan di Istiqlal",
                                "location": "Jakarta",
                                "result_content": [
                                    {
                                        "oldSentence": "Jakarta - Peserta aksi damai 2 Desember mulai berdatangan ke Masjid Istiqlal, Jakarta Pusat",
                                        "resultSentence": "jakarta peserta aksi damai 2 desember masjid istiqlal jakarta pusat",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal0",
                                        "splitSentence": [
                                            "jakarta",
                                            "peserta",
                                            "aksi",
                                            "damai",
                                            "2",
                                            "desember",
                                            "masjid",
                                            "istiqlal",
                                            "jakarta",
                                            "pusat"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Dua posko logistik sudah disiapkan di pelataran Masjid Istiqlal",
                                        "resultSentence": "posko logistik disiapkan pelataran masjid istiqlal",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal1",
                                        "splitSentence": [
                                            "posko",
                                            "logistik",
                                            "disiapkan",
                                            "pelataran",
                                            "masjid",
                                            "istiqlal"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Pantauan detikcom pukul 17",
                                        "resultSentence": "pantauan detikcom 17",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal2",
                                        "splitSentence": [
                                            "pantauan",
                                            "detikcom",
                                            "17"
                                        ]
                                    },
                                    {
                                        "oldSentence": "24 WIB, Kamis (1/12/2016), relawan untuk aksi damai juga sudah berdatangan dari berbagi daerah dan memadati sisi pelataran Masjid Istiqlal di Jl Taman Wijaya Kusuma, Jakpus",
                                        "resultSentence": "24 wib kamis 1/12 2016 relawan aksi damai berbagi daerah memadati sisi pelataran masjid istiqlal jl taman wijaya kusuma jakpus",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal3",
                                        "splitSentence": [
                                            "24",
                                            "wib",
                                            "kamis",
                                            "1/12",
                                            "2016",
                                            "relawan",
                                            "aksi",
                                            "damai",
                                            "berbagi",
                                            "daerah",
                                            "memadati",
                                            "sisi",
                                            "pelataran",
                                            "masjid",
                                            "istiqlal",
                                            "jl",
                                            "taman",
                                            "wijaya",
                                            "kusuma",
                                            "jakpus"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Foto: Ahmad Ziaul Fitrahudin/detikcom Para peserta aksi dan relawan aksi damai 2 Desember di Masjid Istiqlal, Kamis (1/12/2016)<!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->\"Sekarang diberi kemudahan, umat sudah berdatangan",
                                        "resultSentence": "foto ahmad ziaul fitrahudin detikcom peserta aksi relawan aksi damai 2 desember masjid istiqlal kamis 1/12 2016 parallaxindetail kemudahan umat",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal4",
                                        "splitSentence": [
                                            "foto",
                                            "ahmad",
                                            "ziaul",
                                            "fitrahudin",
                                            "detikcom",
                                            "peserta",
                                            "aksi",
                                            "relawan",
                                            "aksi",
                                            "damai",
                                            "2",
                                            "desember",
                                            "masjid",
                                            "istiqlal",
                                            "kamis",
                                            "1/12",
                                            "2016",
                                            "parallaxindetail",
                                            "kemudahan",
                                            "umat"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Alhamdulillah berkah, sumbangan ini perseorangan sukarela saja bahkan berapa pun kami terima",
                                        "resultSentence": "alhamdulillah berkah sumbangan perseorangan sukarela terima",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal5",
                                        "splitSentence": [
                                            "alhamdulillah",
                                            "berkah",
                                            "sumbangan",
                                            "perseorangan",
                                            "sukarela",
                                            "terima"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Dari Sabtu sumbangan sudah mengalir tapi kami tidak bisa menampung,\" ujar relawan Moslem Care Community, Imanul Haq",
                                        "resultSentence": "sabtu sumbangan mengalir menampung relawan moslem care community imanul haq",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal6",
                                        "splitSentence": [
                                            "sabtu",
                                            "sumbangan",
                                            "mengalir",
                                            "menampung",
                                            "relawan",
                                            "moslem",
                                            "care",
                                            "community",
                                            "imanul",
                                            "haq"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Sore ini, terlihat mobil yang membawa logistik terus berdatangan membawa berbagai macam bantuan seperti air mineral, nasi bungkus, makanan ringan, alat kebersihan",
                                        "resultSentence": "sore mobil yang membawa logistik membawa bantuan air mineral nasi bungkus makanan ringan alat kebersihan",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal7",
                                        "splitSentence": [
                                            "sore",
                                            "mobil",
                                            "yang",
                                            "membawa",
                                            "logistik",
                                            "membawa",
                                            "bantuan",
                                            "air",
                                            "mineral",
                                            "nasi",
                                            "bungkus",
                                            "makanan",
                                            "ringan",
                                            "alat",
                                            "kebersihan"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Foto: Ahmad Ziaul Fitrahudin/detikcomPara peserta aksi dan relawan aksi damai 2 Desember di Masjid Istiqlal, Kamis (1/12/2016)Logistik yang terkumpul langsung dibagikan pada saat yang sama",
                                        "resultSentence": "foto ahmad ziaul fitrahudin detikcompara peserta aksi relawan aksi damai 2 desember masjid istiqlal kamis 1/12 2016 logistik yang terkumpul langsung dibagikan yang",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal8",
                                        "splitSentence": [
                                            "foto",
                                            "ahmad",
                                            "ziaul",
                                            "fitrahudin",
                                            "detikcompara",
                                            "peserta",
                                            "aksi",
                                            "relawan",
                                            "aksi",
                                            "damai",
                                            "2",
                                            "desember",
                                            "masjid",
                                            "istiqlal",
                                            "kamis",
                                            "1/12",
                                            "2016",
                                            "logistik",
                                            "yang",
                                            "terkumpul",
                                            "langsung",
                                            "dibagikan",
                                            "yang"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Peserta pun mengantre tertib dan relawan bersiaga di posko untuk membagikan logistik",
                                        "resultSentence": "peserta mengantre tertib relawan bersiaga posko membagikan logistik",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal9",
                                        "splitSentence": [
                                            "peserta",
                                            "mengantre",
                                            "tertib",
                                            "relawan",
                                            "bersiaga",
                                            "posko",
                                            "membagikan",
                                            "logistik"
                                        ]
                                    },
                                    {
                                        "oldSentence": "\"Logistik langsung dibagi",
                                        "resultSentence": "logistik langsung dibagi",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal10",
                                        "splitSentence": [
                                            "logistik",
                                            "langsung",
                                            "dibagi"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Apalagi kawan-kawan baru datang",
                                        "resultSentence": "kawan-kawan",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal11",
                                        "splitSentence": [
                                            "kawan-kawan"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Begitu dateng kita juga langsung bagikan,\" kata Imanul",
                                        "resultSentence": "dateng langsung bagikan imanul",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal12",
                                        "splitSentence": [
                                            "dateng",
                                            "langsung",
                                            "bagikan",
                                            "imanul"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Salah satu peserta aksi asal Batam, Azka mengaku bersyukur dengan kehadiran para relawan yang menyiapkan logistik",
                                        "resultSentence": "salah peserta aksi batam azka mengaku bersyukur kehadiran relawan yang logistik",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal13",
                                        "splitSentence": [
                                            "salah",
                                            "peserta",
                                            "aksi",
                                            "batam",
                                            "azka",
                                            "mengaku",
                                            "bersyukur",
                                            "kehadiran",
                                            "relawan",
                                            "yang",
                                            "logistik"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Para relawan sangat membantu peserta aksi",
                                        "resultSentence": "relawan membantu peserta aksi",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal14",
                                        "splitSentence": [
                                            "relawan",
                                            "membantu",
                                            "peserta",
                                            "aksi"
                                        ]
                                    },
                                    {
                                        "oldSentence": "\"Alhamdulillah dapet nasi sama air mineral dari posko, gratis kok,\" ujar Azka",
                                        "resultSentence": "alhamdulillah dapet nasi air mineral posko gratis azka",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal15",
                                        "splitSentence": [
                                            "alhamdulillah",
                                            "dapet",
                                            "nasi",
                                            "air",
                                            "mineral",
                                            "posko",
                                            "gratis",
                                            "azka"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Foto: Ahmad Ziaul Fitrahudin/detikcom Para peserta aksi dan relawan aksi damai 2 Desember di Masjid Istiqlal, Kamis (1/12/2016)(azf/fdn)<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong",
                                        "resultSentence": "foto ahmad ziaul fitrahudin detikcom peserta aksi relawan aksi damai 2 desember masjid istiqlal kamis 1/12 2016 azf fdn polong",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal16",
                                        "splitSentence": [
                                            "foto",
                                            "ahmad",
                                            "ziaul",
                                            "fitrahudin",
                                            "detikcom",
                                            "peserta",
                                            "aksi",
                                            "relawan",
                                            "aksi",
                                            "damai",
                                            "2",
                                            "desember",
                                            "masjid",
                                            "istiqlal",
                                            "kamis",
                                            "1/12",
                                            "2016",
                                            "azf",
                                            "fdn",
                                            "polong"
                                        ]
                                    },
                                    {
                                        "oldSentence": "create({ target: 'thepolong', id: 55 }) ",
                                        "resultSentence": "create target id 55",
                                        "id": "pesertaaksi2desemberberdatanganposkologistikdisiapkandiistiqlal17",
                                        "splitSentence": [
                                            "create",
                                            "target",
                                            "id",
                                            "55"
                                        ]
                                    }
                                ]
                            },
                            {
                                "title": "Bom Serang Warga Syiah Dhaka Sebabkan Korban Tewas",
                                "location": "Jakarta",
                                "result_content": [
                                    {
                                        "oldSentence": "Jakarta - Ledakan terjadi dini hari pada Sabtu di area kota tua Dhaka sekitar pukul 02:00 pagi",
                                        "resultSentence": "jakarta ledakan sabtu area kota tua dhaka 02:00 pagi",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas0",
                                        "splitSentence": [
                                            "jakarta",
                                            "ledakan",
                                            "sabtu",
                                            "area",
                                            "kota",
                                            "tua",
                                            "dhaka",
                                            "02:00",
                                            "pagi"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Serangan bom yang menyasar warga Syiah di Bangladesh menewaskan satu orang dan melukai 50 lainnya",
                                        "resultSentence": "serangan bom yang menyasar warga syiah bangladesh menewaskan orang melukai 50",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas1",
                                        "splitSentence": [
                                            "serangan",
                                            "bom",
                                            "yang",
                                            "menyasar",
                                            "warga",
                                            "syiah",
                                            "bangladesh",
                                            "menewaskan",
                                            "orang",
                                            "melukai",
                                            "50"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Polisi mengatakan beberapa bom buatan sendiri dilempar ke tengah ribuan orang yang tengah menghadiri perayaan Asyura di Dhaka, ibu kota Bangladesh",
                                        "resultSentence": "polisi bom buatan dilempar ribuan orang yang menghadiri perayaan asyura dhaka kota bangladesh",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas2",
                                        "splitSentence": [
                                            "polisi",
                                            "bom",
                                            "buatan",
                                            "dilempar",
                                            "ribuan",
                                            "orang",
                                            "yang",
                                            "menghadiri",
                                            "perayaan",
                                            "asyura",
                                            "dhaka",
                                            "kota",
                                            "bangladesh"
                                        ]
                                    },
                                    {
                                        "oldSentence": "<!--// <![CDATA[OA_show('parallaxindetail'); // ]]> --> Pemimpin kelompok Syiah yang merupakan minoritas kecil di Bangladesh mengatakan bahwa ini adalah pertama kalinya peringatan tersebut menjadi sasaran serangan",
                                        "resultSentence": "parallaxindetail pemimpin kelompok syiah yang minoritas bangladesh kalinya peringatan sasaran serangan",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas3",
                                        "splitSentence": [
                                            "parallaxindetail",
                                            "pemimpin",
                                            "kelompok",
                                            "syiah",
                                            "yang",
                                            "minoritas",
                                            "bangladesh",
                                            "kalinya",
                                            "peringatan",
                                            "sasaran",
                                            "serangan"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Di Pakistan, setidaknya 22 tewas pada Jumat (23/10) saat seorang pengebom bunuh diri menyerang upacara Asyura di Jacobabad",
                                        "resultSentence": "pakistan 22 tewas jumat 23/10 pengebom bunuh menyerang upacara asyura jacobabad",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas4",
                                        "splitSentence": [
                                            "pakistan",
                                            "22",
                                            "tewas",
                                            "jumat",
                                            "23/10",
                                            "pengebom",
                                            "bunuh",
                                            "menyerang",
                                            "upacara",
                                            "asyura",
                                            "jacobabad"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Ledakan tersebut terjadi dini hari pada Sabtu di area kota tua Dhaka sekitar pukul 02:00 pagi",
                                        "resultSentence": "ledakan sabtu area kota tua dhaka 02:00 pagi",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas5",
                                        "splitSentence": [
                                            "ledakan",
                                            "sabtu",
                                            "area",
                                            "kota",
                                            "tua",
                                            "dhaka",
                                            "02:00",
                                            "pagi"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Warga Syiah saat itu sedang berkumpul di Hussaini Dalan, situs Syiah utama dan bersejarah di kota tersebut, untuk mulai pawai tahunan untuk memperingati Asyura",
                                        "resultSentence": "warga syiah berkumpul hussaini dalan situs syiah utama bersejarah kota pawai tahunan memperingati asyura",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas6",
                                        "splitSentence": [
                                            "warga",
                                            "syiah",
                                            "berkumpul",
                                            "hussaini",
                                            "dalan",
                                            "situs",
                                            "syiah",
                                            "utama",
                                            "bersejarah",
                                            "kota",
                                            "pawai",
                                            "tahunan",
                                            "memperingati",
                                            "asyura"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Polisi menahan tiga orang di lokasi, tapi kemudian tak memberi informasi tentang identitas orang yang diduga adalah kelompok penyerang",
                                        "resultSentence": "polisi menahan orang lokasi informasi identitas orang yang diduga kelompok penyerang",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas7",
                                        "splitSentence": [
                                            "polisi",
                                            "menahan",
                                            "orang",
                                            "lokasi",
                                            "informasi",
                                            "identitas",
                                            "orang",
                                            "yang",
                                            "diduga",
                                            "kelompok",
                                            "penyerang"
                                        ]
                                    },
                                    {
                                        "oldSentence": "\"Sepengetahuan kami, ledakan dimaksudkan untuk membuat kepanikan di antara kerumunan dan menciptakan situasi yang kacau,\" kata Kolonel Ziaul Ahsan dari Batalyon Pasukan Gerak Cepat",
                                        "resultSentence": "sepengetahuan ledakan kepanikan kerumunan menciptakan situasi yang kacau kolonel ziaul ahsan batalyon pasukan gerak cepat",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas8",
                                        "splitSentence": [
                                            "sepengetahuan",
                                            "ledakan",
                                            "kepanikan",
                                            "kerumunan",
                                            "menciptakan",
                                            "situasi",
                                            "yang",
                                            "kacau",
                                            "kolonel",
                                            "ziaul",
                                            "ahsan",
                                            "batalyon",
                                            "pasukan",
                                            "gerak",
                                            "cepat"
                                        ]
                                    },
                                    {
                                        "oldSentence": "\"Kami menemukan dua bom yang belum meledak",
                                        "resultSentence": "menemukan bom yang meledak",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas9",
                                        "splitSentence": [
                                            "menemukan",
                                            "bom",
                                            "yang",
                                            "meledak"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Bentuknya seperti alat eksplosif dan hampir seperti granat dan dilengkapi batere,\" kata kepala polisi setempat Azizul Haq pada kantor berita AFP",
                                        "resultSentence": "bentuknya alat eksplosif granat dilengkapi batere kepala polisi azizul haq kantor berita afp",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas10",
                                        "splitSentence": [
                                            "bentuknya",
                                            "alat",
                                            "eksplosif",
                                            "granat",
                                            "dilengkapi",
                                            "batere",
                                            "kepala",
                                            "polisi",
                                            "azizul",
                                            "haq",
                                            "kantor",
                                            "berita",
                                            "afp"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Korban yang dibawa ke rumah sakit dilaporkan tidak dalam kondisi kritis",
                                        "resultSentence": "korban yang dibawa rumah sakit dilaporkan kondisi kritis",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas11",
                                        "splitSentence": [
                                            "korban",
                                            "yang",
                                            "dibawa",
                                            "rumah",
                                            "sakit",
                                            "dilaporkan",
                                            "kondisi",
                                            "kritis"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Pada Hari Asyura, warga Syiah memperingati meninggalnya Imam Hussein, cucu Nabi Muhammad",
                                        "resultSentence": "asyura warga syiah memperingati meninggalnya imam hussein cucu nabi muhammad",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas12",
                                        "splitSentence": [
                                            "asyura",
                                            "warga",
                                            "syiah",
                                            "memperingati",
                                            "meninggalnya",
                                            "imam",
                                            "hussein",
                                            "cucu",
                                            "nabi",
                                            "muhammad"
                                        ]
                                    },
                                    {
                                        "oldSentence": "(ita/ita)<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong",
                                        "resultSentence": "ita ita polong",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas13",
                                        "splitSentence": [
                                            "ita",
                                            "ita",
                                            "polong"
                                        ]
                                    },
                                    {
                                        "oldSentence": "create({ target: 'thepolong', id: 55 }) ",
                                        "resultSentence": "create target id 55",
                                        "id": "bomserangwargasyiahdhakasebabkankorbantewas14",
                                        "splitSentence": [
                                            "create",
                                            "target",
                                            "id",
                                            "55"
                                        ]
                                    }
                                ]
                            },
                            {
                                "title": "Meneruskan Semangat Juang Bung Tomo",
                                "location": "Jakarta",
                                "result_content": [
                                    {
                                        "oldSentence": "Jakarta - Bung Tomo adalah sosok Pejuang Islam yang sangat fenomenal",
                                        "resultSentence": "jakarta tomo sosok pejuang islam yang fenomenal",
                                        "id": "meneruskansemangatjuangbungtomo0",
                                        "splitSentence": [
                                            "jakarta",
                                            "tomo",
                                            "sosok",
                                            "pejuang",
                                            "islam",
                                            "yang",
                                            "fenomenal"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Pekikan takbir disertai dengan semboyan yang begitu masyur \\\\\\\"Merdeka atau Mati Syahid\\\\\\\" berhasil ia gunakan untuk membakar semangat jihad arek-arek Surabaya kala itu",
                                        "resultSentence": "pekikan takbir disertai semboyan yang masyur \\ \\ \\ merdeka mati syahid \\ \\ \\ berhasil membakar semangat jihad arek-arek surabaya",
                                        "id": "meneruskansemangatjuangbungtomo1",
                                        "splitSentence": [
                                            "pekikan",
                                            "takbir",
                                            "disertai",
                                            "semboyan",
                                            "yang",
                                            "masyur",
                                            "\\",
                                            "\\",
                                            "\\",
                                            "merdeka",
                                            "mati",
                                            "syahid",
                                            "\\",
                                            "\\",
                                            "\\",
                                            "berhasil",
                                            "membakar",
                                            "semangat",
                                            "jihad",
                                            "arek-arek",
                                            "surabaya"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Melalui corong-corong radio Bung Tomo mampu menggerakkan perlawanan rakyat untuk menghadapi penjajah pada saat peristiwa 10 November 1945 atas dukungan ulama-ulama setempat dengan fatwa Jihad (Resolusi Jihad) para ulama tersebut",
                                        "resultSentence": "corong-corong radio tomo menggerakkan perlawanan rakyat menghadapi penjajah peristiwa 10 november 1945 dukungan ulama-ulama fatwa jihad resolusi jihad ulama",
                                        "id": "meneruskansemangatjuangbungtomo2",
                                        "splitSentence": [
                                            "corong-corong",
                                            "radio",
                                            "tomo",
                                            "menggerakkan",
                                            "perlawanan",
                                            "rakyat",
                                            "menghadapi",
                                            "penjajah",
                                            "peristiwa",
                                            "10",
                                            "november",
                                            "1945",
                                            "dukungan",
                                            "ulama-ulama",
                                            "fatwa",
                                            "jihad",
                                            "resolusi",
                                            "jihad",
                                            "ulama"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Dengan semangat yang berkobar-kobar dan dilandasi dengan keimanan yang mendalam dalam petikan pidatonya Bung Tomo mengatakan, \\\\\\\"Dan kita jakin, saoedara-saoedara, pada akhirnja pastilah kemenangan akan djatuh ke tangan kita, sebab Allah selaloe berada di pihak jang benar, pertjajalah saoedara-saoedara, Toehan akan melindungi kita sekalian, Allahu Akbar",
                                        "resultSentence": "semangat yang berkobar-kobar dilandasi keimanan yang mendalam petikan pidatonya tomo \\ \\ \\ jakin saoedara-saoedara akhirnja kemenangan djatuh tangan allah selaloe jang pertjajalah saoedara-saoedara toehan melindungi allahu akbar",
                                        "id": "meneruskansemangatjuangbungtomo3",
                                        "splitSentence": [
                                            "semangat",
                                            "yang",
                                            "berkobar-kobar",
                                            "dilandasi",
                                            "keimanan",
                                            "yang",
                                            "mendalam",
                                            "petikan",
                                            "pidatonya",
                                            "tomo",
                                            "\\",
                                            "\\",
                                            "\\",
                                            "jakin",
                                            "saoedara-saoedara",
                                            "akhirnja",
                                            "kemenangan",
                                            "djatuh",
                                            "tangan",
                                            "allah",
                                            "selaloe",
                                            "jang",
                                            "pertjajalah",
                                            "saoedara-saoedara",
                                            "toehan",
                                            "melindungi",
                                            "allahu",
                                            "akbar"
                                        ]
                                    },
                                    {
                                        "oldSentence": "",
                                        "resultSentence": "",
                                        "id": "meneruskansemangatjuangbungtomo4",
                                        "splitSentence": []
                                    },
                                    {
                                        "oldSentence": "! Allahu Akbar",
                                        "resultSentence": "allahu akbar",
                                        "id": "meneruskansemangatjuangbungtomo5",
                                        "splitSentence": [
                                            "allahu",
                                            "akbar"
                                        ]
                                    },
                                    {
                                        "oldSentence": "",
                                        "resultSentence": "",
                                        "id": "meneruskansemangatjuangbungtomo6",
                                        "splitSentence": []
                                    },
                                    {
                                        "oldSentence": "! Allahu Akbar",
                                        "resultSentence": "allahu akbar",
                                        "id": "meneruskansemangatjuangbungtomo7",
                                        "splitSentence": [
                                            "allahu",
                                            "akbar"
                                        ]
                                    },
                                    {
                                        "oldSentence": "",
                                        "resultSentence": "",
                                        "id": "meneruskansemangatjuangbungtomo8",
                                        "splitSentence": []
                                    },
                                    {
                                        "oldSentence": "!\\\\\\\"Perjuangan yang dilakukan Bung Tomo dan arek-arek Surabaya yang sebagian besar merupakan santri-santri dari Pondok Pesanten yang berada di wilayah Surabaya dan sekitarnya waktu itu sungguh luar biasa",
                                        "resultSentence": "\\ \\ \\ perjuangan yang tomo arek-arek surabaya yang santri-santri pondok pesanten yang wilayah surabaya sungguh",
                                        "id": "meneruskansemangatjuangbungtomo9",
                                        "splitSentence": [
                                            "\\",
                                            "\\",
                                            "\\",
                                            "perjuangan",
                                            "yang",
                                            "tomo",
                                            "arek-arek",
                                            "surabaya",
                                            "yang",
                                            "santri-santri",
                                            "pondok",
                                            "pesanten",
                                            "yang",
                                            "wilayah",
                                            "surabaya",
                                            "sungguh"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Ribuan kiai dan santri dari berbagai daerah sebagaimana ditulis oleh MC Ricklefs (1991) berbondong-bondong ke Surabaya guna memenuhi seruan Jihad itu",
                                        "resultSentence": "ribuan kiai santri daerah ditulis mc ricklefs 1991 berbondong-bondong surabaya memenuhi seruan jihad",
                                        "id": "meneruskansemangatjuangbungtomo10",
                                        "splitSentence": [
                                            "ribuan",
                                            "kiai",
                                            "santri",
                                            "daerah",
                                            "ditulis",
                                            "mc",
                                            "ricklefs",
                                            "1991",
                                            "berbondong-bondong",
                                            "surabaya",
                                            "memenuhi",
                                            "seruan",
                                            "jihad"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Sedemikian dahsyatnya sampai-sampai salah seorang komandan pasukan (Netherlands Indies Civil Administration) NICA sekutu, yakni Ziaul Haq dari India merasa heran menyaksikan kiai dan santri bertakbir sambil mengacungkan senjata",
                                        "resultSentence": "dahsyatnya salah komandan pasukan netherlands indies civil administration nica sekutu ziaul haq india heran menyaksikan kiai santri bertakbir mengacungkan senjata",
                                        "id": "meneruskansemangatjuangbungtomo11",
                                        "splitSentence": [
                                            "dahsyatnya",
                                            "salah",
                                            "komandan",
                                            "pasukan",
                                            "netherlands",
                                            "indies",
                                            "civil",
                                            "administration",
                                            "nica",
                                            "sekutu",
                                            "ziaul",
                                            "haq",
                                            "india",
                                            "heran",
                                            "menyaksikan",
                                            "kiai",
                                            "santri",
                                            "bertakbir",
                                            "mengacungkan",
                                            "senjata"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Sebagai muslim, hatinya pun tersentuh, dia akhirnya menarik diri dari medan perang",
                                        "resultSentence": "muslim hatinya tersentuh menarik medan perang",
                                        "id": "meneruskansemangatjuangbungtomo12",
                                        "splitSentence": [
                                            "muslim",
                                            "hatinya",
                                            "tersentuh",
                                            "menarik",
                                            "medan",
                                            "perang"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Sikapnya itu sempat membuat pasukan sekutu panik",
                                        "resultSentence": "sikapnya pasukan sekutu panik",
                                        "id": "meneruskansemangatjuangbungtomo13",
                                        "splitSentence": [
                                            "sikapnya",
                                            "pasukan",
                                            "sekutu",
                                            "panik"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Pada peristiwa lain sejarah juga mencatat bahwa Bung Tomo berhasil membentuk pasukan berani mati, yang dikenal dengan Pasukan Bom Syahid",
                                        "resultSentence": "peristiwa sejarah mencatat tomo berhasil membentuk pasukan berani mati yang dikenal pasukan bom syahid",
                                        "id": "meneruskansemangatjuangbungtomo14",
                                        "splitSentence": [
                                            "peristiwa",
                                            "sejarah",
                                            "mencatat",
                                            "tomo",
                                            "berhasil",
                                            "membentuk",
                                            "pasukan",
                                            "berani",
                                            "mati",
                                            "yang",
                                            "dikenal",
                                            "pasukan",
                                            "bom",
                                            "syahid"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Suatu hari Pasukan Bom Syahid itu melahirkan syuhada bom syahid pertama, seorang pemuda yang tak dikenal namanya menubrukkan dirinya ke tank Belanda",
                                        "resultSentence": "pasukan bom syahid melahirkan syuhada bom syahid pemuda yang dikenal namanya menubrukkan tank belanda",
                                        "id": "meneruskansemangatjuangbungtomo15",
                                        "splitSentence": [
                                            "pasukan",
                                            "bom",
                                            "syahid",
                                            "melahirkan",
                                            "syuhada",
                                            "bom",
                                            "syahid",
                                            "pemuda",
                                            "yang",
                                            "dikenal",
                                            "namanya",
                                            "menubrukkan",
                                            "tank",
                                            "belanda"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Akhirnya tank Belanda itu pun hancur",
                                        "resultSentence": "tank belanda hancur",
                                        "id": "meneruskansemangatjuangbungtomo16",
                                        "splitSentence": [
                                            "tank",
                                            "belanda",
                                            "hancur"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Allahu Akbar!Perjuangan rakyat Indonesia dalam mengusir penjajah Belanda telah banyak melahirkan sosok-sosok para pahlawan yang sangat gigih dan begitu gagah berani menerjang musuh-musuhnya",
                                        "resultSentence": "allahu akbar perjuangan rakyat indonesia mengusir penjajah belanda melahirkan sosok-sosok pahlawan yang gigih gagah berani menerjang musuh-musuhnya",
                                        "id": "meneruskansemangatjuangbungtomo17",
                                        "splitSentence": [
                                            "allahu",
                                            "akbar",
                                            "perjuangan",
                                            "rakyat",
                                            "indonesia",
                                            "mengusir",
                                            "penjajah",
                                            "belanda",
                                            "melahirkan",
                                            "sosok-sosok",
                                            "pahlawan",
                                            "yang",
                                            "gigih",
                                            "gagah",
                                            "berani",
                                            "menerjang",
                                            "musuh-musuhnya"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Atas berkat rahmat Allah yang maha kuasa mereka berhasil mengusir tentara-tentara penjajah dari bumi Indonesia",
                                        "resultSentence": "berkat rahmat allah yang maha kuasa berhasil mengusir tentara-tentara penjajah bumi indonesia",
                                        "id": "meneruskansemangatjuangbungtomo18",
                                        "splitSentence": [
                                            "berkat",
                                            "rahmat",
                                            "allah",
                                            "yang",
                                            "maha",
                                            "kuasa",
                                            "berhasil",
                                            "mengusir",
                                            "tentara-tentara",
                                            "penjajah",
                                            "bumi",
                                            "indonesia"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Hal ini tentunya bisa menjadi pelajaran berharga bagi kita semua sebagai penerus generasi bangsa",
                                        "resultSentence": "pelajaran berharga penerus generasi bangsa",
                                        "id": "meneruskansemangatjuangbungtomo19",
                                        "splitSentence": [
                                            "pelajaran",
                                            "berharga",
                                            "penerus",
                                            "generasi",
                                            "bangsa"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Kini tentara-tentara para penjajah itu telah pergi dari bumi pertiwi, dan Bung Tomo pun telah perpulang ke rahmat Illahi ketika beliau sedang menjalankan wukuf di Arafah dalam rangkaian menunaikan Ibadah haji",
                                        "resultSentence": "tentara-tentara penjajah pergi bumi pertiwi tomo perpulang rahmat illahi beliau menjalankan wukuf arafah rangkaian menunaikan ibadah haji",
                                        "id": "meneruskansemangatjuangbungtomo20",
                                        "splitSentence": [
                                            "tentara-tentara",
                                            "penjajah",
                                            "pergi",
                                            "bumi",
                                            "pertiwi",
                                            "tomo",
                                            "perpulang",
                                            "rahmat",
                                            "illahi",
                                            "beliau",
                                            "menjalankan",
                                            "wukuf",
                                            "arafah",
                                            "rangkaian",
                                            "menunaikan",
                                            "ibadah",
                                            "haji"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Semoga Allah SWT menempatkan tempat yang mulia di sisi-Nya",
                                        "resultSentence": "semoga allah swt menempatkan yang mulia sisi-nya",
                                        "id": "meneruskansemangatjuangbungtomo21",
                                        "splitSentence": [
                                            "semoga",
                                            "allah",
                                            "swt",
                                            "menempatkan",
                                            "yang",
                                            "mulia",
                                            "sisi-nya"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Pekerjaan besar masih menanti generasi sekarang, ketika negeri yang telah dinyatakan merdeka lebih dari lebih dari 60 tahun ini ternyata kondisi sebenarnya masih terjajah",
                                        "resultSentence": "pekerjaan generasi negeri yang dinyatakan merdeka 60 kondisi terjajah",
                                        "id": "meneruskansemangatjuangbungtomo22",
                                        "splitSentence": [
                                            "pekerjaan",
                                            "generasi",
                                            "negeri",
                                            "yang",
                                            "dinyatakan",
                                            "merdeka",
                                            "60",
                                            "kondisi",
                                            "terjajah"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Bagaimana tidak",
                                        "resultSentence": "",
                                        "id": "meneruskansemangatjuangbungtomo23",
                                        "splitSentence": []
                                    },
                                    {
                                        "oldSentence": "Sistem ekonominya masih sistem ekonomi penjajah yakni kapitalisme",
                                        "resultSentence": "sistem ekonominya sistem ekonomi penjajah kapitalisme",
                                        "id": "meneruskansemangatjuangbungtomo24",
                                        "splitSentence": [
                                            "sistem",
                                            "ekonominya",
                                            "sistem",
                                            "ekonomi",
                                            "penjajah",
                                            "kapitalisme"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Alhasil kekayaan alam yang begitu melimpah ruah ini sekarang sebagian besar masih di eksploitasi pihak asing",
                                        "resultSentence": "alhasil kekayaan alam yang melimpah ruah eksploitasi asing",
                                        "id": "meneruskansemangatjuangbungtomo25",
                                        "splitSentence": [
                                            "alhasil",
                                            "kekayaan",
                                            "alam",
                                            "yang",
                                            "melimpah",
                                            "ruah",
                                            "eksploitasi",
                                            "asing"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Hal ini berimbas pada kesejahteraan masyarakat yang jauh dari harapan",
                                        "resultSentence": "berimbas kesejahteraan masyarakat yang harapan",
                                        "id": "meneruskansemangatjuangbungtomo26",
                                        "splitSentence": [
                                            "berimbas",
                                            "kesejahteraan",
                                            "masyarakat",
                                            "yang",
                                            "harapan"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Tercatat jika menggunakan standard Bank Dunia hampir sekitar 49% masyarakat Indonesia hidup dengan kurang dari US$ 2 per hari alias miskin",
                                        "resultSentence": "tercatat standard bank dunia 49 masyarakat indonesia hidup us 2 alias miskin",
                                        "id": "meneruskansemangatjuangbungtomo27",
                                        "splitSentence": [
                                            "tercatat",
                                            "standard",
                                            "bank",
                                            "dunia",
                                            "49",
                                            "masyarakat",
                                            "indonesia",
                                            "hidup",
                                            "us",
                                            "2",
                                            "alias",
                                            "miskin"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Ditambah lagi menggelembungnya hutang negara yang semakin menghabiskan energi negara ini",
                                        "resultSentence": "ditambah menggelembungnya hutang negara yang menghabiskan energi negara",
                                        "id": "meneruskansemangatjuangbungtomo28",
                                        "splitSentence": [
                                            "ditambah",
                                            "menggelembungnya",
                                            "hutang",
                                            "negara",
                                            "yang",
                                            "menghabiskan",
                                            "energi",
                                            "negara"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Di sektor politik negeri ini juga masih menggunakan sistem penjajah yakni demokrasi sekuler yang salah satu ritual 5 tahunannya yang baru saja dilaksanakan kemarin nyata-nyata telah menguras harta milik rakyat",
                                        "resultSentence": "sektor politik negeri sistem penjajah demokrasi sekuler yang salah ritual 5 tahunannya yang dilaksanakan kemarin nyata-nyata menguras harta milik rakyat",
                                        "id": "meneruskansemangatjuangbungtomo29",
                                        "splitSentence": [
                                            "sektor",
                                            "politik",
                                            "negeri",
                                            "sistem",
                                            "penjajah",
                                            "demokrasi",
                                            "sekuler",
                                            "yang",
                                            "salah",
                                            "ritual",
                                            "5",
                                            "tahunannya",
                                            "yang",
                                            "dilaksanakan",
                                            "kemarin",
                                            "nyata-nyata",
                                            "menguras",
                                            "harta",
                                            "milik",
                                            "rakyat"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Namun, tidak menghasilkan apa-apa",
                                        "resultSentence": "menghasilkan apa-apa",
                                        "id": "meneruskansemangatjuangbungtomo30",
                                        "splitSentence": [
                                            "menghasilkan",
                                            "apa-apa"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Apalagi sistem kufur ini bertentangan dengan Aqidah dan Syariah penduduknya yang mayoritas beragama Islam",
                                        "resultSentence": "sistem kufur bertentangan aqidah syariah penduduknya yang mayoritas beragama islam",
                                        "id": "meneruskansemangatjuangbungtomo31",
                                        "splitSentence": [
                                            "sistem",
                                            "kufur",
                                            "bertentangan",
                                            "aqidah",
                                            "syariah",
                                            "penduduknya",
                                            "yang",
                                            "mayoritas",
                                            "beragama",
                                            "islam"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Penjajahan juga berlangsung di sektor budaya",
                                        "resultSentence": "penjajahan sektor budaya",
                                        "id": "meneruskansemangatjuangbungtomo32",
                                        "splitSentence": [
                                            "penjajahan",
                                            "sektor",
                                            "budaya"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Ketika budaya hedonisme merengsek masuk ke tengah-tengah masyarakat",
                                        "resultSentence": "budaya hedonisme merengsek masuk tengah-tengah masyarakat",
                                        "id": "meneruskansemangatjuangbungtomo33",
                                        "splitSentence": [
                                            "budaya",
                                            "hedonisme",
                                            "merengsek",
                                            "masuk",
                                            "tengah-tengah",
                                            "masyarakat"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Bahkan, baru-baru ini diinformasikan bahwa Indonesia dinobatkan sebagai jawara kedua sebagai pengklik terbanyak situs-situs porno di Internet",
                                        "resultSentence": "baru-baru diinformasikan indonesia dinobatkan jawara pengklik situs-situs porno internet",
                                        "id": "meneruskansemangatjuangbungtomo34",
                                        "splitSentence": [
                                            "baru-baru",
                                            "diinformasikan",
                                            "indonesia",
                                            "dinobatkan",
                                            "jawara",
                                            "pengklik",
                                            "situs-situs",
                                            "porno",
                                            "internet"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Sungguh memprihatinkan",
                                        "resultSentence": "sungguh memprihatinkan",
                                        "id": "meneruskansemangatjuangbungtomo35",
                                        "splitSentence": [
                                            "sungguh",
                                            "memprihatinkan"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Penjajahan juga masih berlangsung di sektor-sektor yang lain",
                                        "resultSentence": "penjajahan sektor-sektor yang",
                                        "id": "meneruskansemangatjuangbungtomo36",
                                        "splitSentence": [
                                            "penjajahan",
                                            "sektor-sektor",
                                            "yang"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Inilah sedikit gambaran bahwa Indonesia sebenarnya masih terjajah",
                                        "resultSentence": "gambaran indonesia terjajah",
                                        "id": "meneruskansemangatjuangbungtomo37",
                                        "splitSentence": [
                                            "gambaran",
                                            "indonesia",
                                            "terjajah"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Maka sudah selayaknya kita kobarkan semangat perlawanan terhadap penjajahan ini",
                                        "resultSentence": "selayaknya kobarkan semangat perlawanan penjajahan",
                                        "id": "meneruskansemangatjuangbungtomo38",
                                        "splitSentence": [
                                            "selayaknya",
                                            "kobarkan",
                                            "semangat",
                                            "perlawanan",
                                            "penjajahan"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Mari kita bebaskan Indonesia dari cengkraman penjajah",
                                        "resultSentence": "mari bebaskan indonesia cengkraman penjajah",
                                        "id": "meneruskansemangatjuangbungtomo39",
                                        "splitSentence": [
                                            "mari",
                                            "bebaskan",
                                            "indonesia",
                                            "cengkraman",
                                            "penjajah"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Dengan berjuang secara sungguh-sungguh untuk menerapkan Syariah Islam secara kaffah dalam bingkai Khilafah Islam",
                                        "resultSentence": "berjuang sungguh-sungguh menerapkan syariah islam kaffah bingkai khilafah islam",
                                        "id": "meneruskansemangatjuangbungtomo40",
                                        "splitSentence": [
                                            "berjuang",
                                            "sungguh-sungguh",
                                            "menerapkan",
                                            "syariah",
                                            "islam",
                                            "kaffah",
                                            "bingkai",
                                            "khilafah",
                                            "islam"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Karena hanya dengan inilah Indonesia bisa benar-benar merdeka",
                                        "resultSentence": "indonesia benar-benar merdeka",
                                        "id": "meneruskansemangatjuangbungtomo41",
                                        "splitSentence": [
                                            "indonesia",
                                            "benar-benar",
                                            "merdeka"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Meminjam sedikit pidato dari Bung Tomo untuk mengobarkan semangat perjuangan bagi diri saya pribadi dan semuanya:\\\\\\\"Bismilahirrahmanirrahim ",
                                        "resultSentence": "meminjam pidato tomo mengobarkan semangat perjuangan pribadi :\\ \\ \\ bismilahirrahmanirrahim",
                                        "id": "meneruskansemangatjuangbungtomo42",
                                        "splitSentence": [
                                            "meminjam",
                                            "pidato",
                                            "tomo",
                                            "mengobarkan",
                                            "semangat",
                                            "perjuangan",
                                            "pribadi",
                                            ":\\",
                                            "\\",
                                            "\\",
                                            "bismilahirrahmanirrahim"
                                        ]
                                    },
                                    {
                                        "oldSentence": "",
                                        "resultSentence": "",
                                        "id": "meneruskansemangatjuangbungtomo43",
                                        "splitSentence": []
                                    },
                                    {
                                        "oldSentence": "",
                                        "resultSentence": "",
                                        "id": "meneruskansemangatjuangbungtomo44",
                                        "splitSentence": []
                                    },
                                    {
                                        "oldSentence": "marilah kita berjuang dengan sungguh-sungguh menegakkan syariah dan Khilafah untuk Indonesia yang lebih baik",
                                        "resultSentence": "marilah berjuang sungguh-sungguh menegakkan syariah khilafah indonesia yang",
                                        "id": "meneruskansemangatjuangbungtomo45",
                                        "splitSentence": [
                                            "marilah",
                                            "berjuang",
                                            "sungguh-sungguh",
                                            "menegakkan",
                                            "syariah",
                                            "khilafah",
                                            "indonesia",
                                            "yang"
                                        ]
                                    },
                                    {
                                        "oldSentence": "Dan kita yakin, saudara-saudara, pada akhirnya pastilah kemenangan akan jatuh ke tangan kita, sebab Allah selalu berada di pihak yang benar, percayalah saudara-saudara, Tuhan akan melindungi kita sekalian, Allahu Akbar",
                                        "resultSentence": "saudara-saudara kemenangan jatuh tangan allah yang percayalah saudara-saudara tuhan melindungi allahu akbar",
                                        "id": "meneruskansemangatjuangbungtomo46",
                                        "splitSentence": [
                                            "saudara-saudara",
                                            "kemenangan",
                                            "jatuh",
                                            "tangan",
                                            "allah",
                                            "yang",
                                            "percayalah",
                                            "saudara-saudara",
                                            "tuhan",
                                            "melindungi",
                                            "allahu",
                                            "akbar"
                                        ]
                                    },
                                    {
                                        "oldSentence": "",
                                        "resultSentence": "",
                                        "id": "meneruskansemangatjuangbungtomo47",
                                        "splitSentence": []
                                    },
                                    {
                                        "oldSentence": "! Allahu Akbar",
                                        "resultSentence": "allahu akbar",
                                        "id": "meneruskansemangatjuangbungtomo48",
                                        "splitSentence": [
                                            "allahu",
                                            "akbar"
                                        ]
                                    },
                                    {
                                        "oldSentence": "",
                                        "resultSentence": "",
                                        "id": "meneruskansemangatjuangbungtomo49",
                                        "splitSentence": []
                                    },
                                    {
                                        "oldSentence": "! Allahu Akbar",
                                        "resultSentence": "allahu akbar",
                                        "id": "meneruskansemangatjuangbungtomo50",
                                        "splitSentence": [
                                            "allahu",
                                            "akbar"
                                        ]
                                    },
                                    {
                                        "oldSentence": "",
                                        "resultSentence": "",
                                        "id": "meneruskansemangatjuangbungtomo51",
                                        "splitSentence": []
                                    },
                                    {
                                        "oldSentence": "!\\\\\\\"Ali MustofaGang Nusa Indah Cemani Grogol Sukoharjo Surakartaalie_jawi@yahoo",
                                        "resultSentence": "\\ \\ \\ ali mustofagang nusa indah cemani grogol sukoharjo surakartaalie_jawi @yahoo",
                                        "id": "meneruskansemangatjuangbungtomo52",
                                        "splitSentence": [
                                            "\\",
                                            "\\",
                                            "\\",
                                            "ali",
                                            "mustofagang",
                                            "nusa",
                                            "indah",
                                            "cemani",
                                            "grogol",
                                            "sukoharjo",
                                            "surakartaalie_jawi",
                                            "@yahoo"
                                        ]
                                    },
                                    {
                                        "oldSentence": "com02719272791<\\/strong><!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->(msh/msh)<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong",
                                        "resultSentence": "com 02719272791 <\\/strong> parallaxindetail msh msh polong",
                                        "id": "meneruskansemangatjuangbungtomo53",
                                        "splitSentence": [
                                            "com",
                                            "02719272791",
                                            "<\\/strong>",
                                            "parallaxindetail",
                                            "msh",
                                            "msh",
                                            "polong"
                                        ]
                                    },
                                    {
                                        "oldSentence": "create({ target: 'thepolong', id: 55 }) ",
                                        "resultSentence": "create target id 55",
                                        "id": "meneruskansemangatjuangbungtomo54",
                                        "splitSentence": [
                                            "create",
                                            "target",
                                            "id",
                                            "55"
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
        detik_result = []
        for x in tokenizer['feed']:
            title = x['title']
            result_row = []
            for y in x['result_content']:
                result_row.append(NaiveBayes(80, 20).sentiment_test_detik(query=y['resultSentence']))
            detik_result.append({
                'title' : title,
                'result' : result_row
            })

        return Response(detik_result)
    else:
        return Response(status.HTTP_204_NO_CONTENT)