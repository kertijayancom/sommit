from rest_framework.decorators import api_view
from rest_framework.response import Response
from api import tasks
from celery.result import AsyncResult
import datetime

@api_view(['GET'])
def get_hello(request):
    if request.method == 'GET':
        # for i in range(5):
        # tasks.tambah(10,10).apply_async(countdown=10)
        # r = tasks.create_result_news_detik.apply_async(countdown=10)
        time_a = datetime.datetime.now()
        tasks.scrap_detik_news.apply_async(countdown=10)
        time_b = datetime.datetime.now() - time_a
        print(str(time_b))
        # r1 = AsyncResult(id = r.id, app=tasks.create_result_news_detik)
        return Response("oke")


@api_view(['GET'])
def get_tambah(request):
    if request.method == 'GET':
        time_a = datetime.datetime.now()
        a = tasks.tambah.apply_async(countdown=4)
        res = AsyncResult(a.id)
        time_b = datetime.datetime.now() - time_a
        print(str(time_b))
        return Response(res.state)
    else:
        return Response("tidak oke")



