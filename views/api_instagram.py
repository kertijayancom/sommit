from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from api.models import SearchHistory, ResultDetik, ResultNewsDetik, ProjectSentiment, ResultTwitter, TopMentionTweet, ResultOtherNews, CSEGoogle, Datatrain, TopInfluencerTweet, TopContributorTweet, CobaResultInstagram, CobaResultTwitter, CobaResultFacebook
from django.db.models import Count
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from ..extras.tokenizer import tokenizer_news
from ..extras.algorithm import naivebayes

import datetime, requests, time
from datetime import timedelta
import numpy, nltk, tweepy
from nltk.corpus import stopwords
from nltk.tokenize import  TweetTokenizer
import geopy
from geopy.geocoders import Nominatim, GoogleV3
from api import tasks
from ..extras.experiment.instagram_cse import InstagramAnalysis


# get information of link instagram
@api_view(['GET'])
def detail_instagram_information_from_link(request):
    if request.method == 'GET':
        # ambil 10 instagram terakhir
        result = []

        last_x_data = CobaResultInstagram.objects.all()

        for x in last_x_data:
            #check validation
            link = x.url

            obj_ig = InstagramAnalysis(link_instagram=link)
            if(obj_ig.validation()):
                y = obj_ig.get_content()
                opinion = naivebayes.NaiveBayes(80, 20).sentiment_test_detik(query=y['content'])
                if(y is not None):
                    result.append(opinion['result'])

        res = {
            "positive" : result.count("positive"),
            "neutral": result.count("neutral"),
            "negative": result.count("negative"),
        }

        return Response(res, status=status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)
