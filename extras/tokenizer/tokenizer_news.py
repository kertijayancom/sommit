import json
import re, os
from nltk.tokenize import TweetTokenizer
# from nltk.corpus import stopwords
from bs4 import BeautifulSoup as soup
# from string import whitespace


class NewsTokenizer(object):

    stop_word_id = ["\n","thepolong","polong.create","newstag","oa_show","cdata","-->","\"",");","'","}","{","/","/n",")", "(", '', "rt", '“', ":", ",", "”", "-", ".", "!", "$", "%", "^", "&", "_", "=", "+", '[',
                              ']', '`', '~', '?', '>', '<', 'ada', 'adalah', 'adanya', 'adapun', 'agak', 'agaknya', 'agar',
                              'akan', 'akankah', 'akhir', 'akhiri', 'akhirnya', 'aku', 'akulah', 'amat', 'amatlah', 'anda',
                              'andalah', 'antar', 'antara', 'antaranya', 'apa', 'apaan', 'apabila', 'apakah', 'apalagi',
                              'apatah', 'artinya', 'asal', 'asalkan', 'atas', 'atau', 'ataukah', 'ataupun', 'awal',
                              'awalnya', 'bagai', 'bagaikan', 'bagaimana', 'bagaimanakah', 'bagaimanapun', 'bagi', 'bagian',
                              'bahkan', 'bahwa', 'bahwasanya', 'baik', 'bakal', 'bakalan', 'balik', 'banyak', 'bapak',
                              'baru', 'bawah', 'beberapa', 'begini', 'beginian', 'beginikah', 'beginilah', 'begitu',
                              'begitukah', 'begitulah', 'begitupun', 'bekerja', 'belakang', 'belakangan', 'belum',
                              'belumlah', 'benar', 'benarkah', 'benarlah', 'berada', 'berakhir', 'berakhirlah',
                              'berakhirnya', 'berapa', 'berapakah', 'berapalah', 'berapapun', 'berarti', 'berawal',
                              'berbagai', 'berdatangan', 'beri', 'berikan', 'berikut', 'berikutnya', 'berjumlah',
                              'berkali-kali', 'berkata', 'berkehendak', 'berkeinginan', 'berkenaan', 'berlainan', 'berlalu',
                              'berlangsung', 'berlebihan', 'bermacam', 'bermacam-macam', 'bermaksud', 'bermula', 'bersama',
                              'bersama-sama', 'bersiap', 'bersiap-siap', 'bertanya', 'bertanya-tanya', 'berturut',
                              'berturut-turut', 'bertutur', 'berujar', 'berupa', 'besar', 'betul', 'betulkah', 'biasa',
                              'biasanya', 'bila', 'bilakah', 'bisa', 'bisakah', 'boleh', 'bolehkah', 'bolehlah', 'buat',
                              'bukan', 'bukankah', 'bukanlah', 'bukannya', 'bulan', 'bung', 'cara', 'caranya', 'cukup',
                              'cukupkah', 'cukuplah', 'cuma', 'dahulu', 'dalam', 'dan', 'dapat', 'dari', 'daripada',
                              'datang', 'dekat', 'demi', 'demikian', 'demikianlah', 'dengan', 'depan', 'di', 'dia',
                              'diakhiri', 'diakhirinya', 'dialah', 'diantara', 'diantaranya', 'diberi', 'diberikan',
                              'diberikannya', 'dibuat', 'dibuatnya', 'didapat', 'didatangkan', 'digunakan', 'diibaratkan',
                              'diibaratkannya', 'diingat', 'diingatkan', 'diinginkan', 'dijawab', 'dijelaskan',
                              'dijelaskannya', 'dikarenakan', 'dikatakan', 'dikatakannya', 'dikerjakan', 'diketahui',
                              'diketahuinya', 'dikira', 'dilakukan', 'dilalui', 'dilihat', 'dimaksud', 'dimaksudkan',
                              'dimaksudkannya', 'dimaksudnya', 'diminta', 'dimintai', 'dimisalkan', 'dimulai', 'dimulailah',
                              'dimulainya', 'dimungkinkan', 'dini', 'dipastikan', 'diperbuat', 'diperbuatnya',
                              'dipergunakan', 'diperkirakan', 'diperlihatkan', 'diperlukan', 'diperlukannya',
                              'dipersoalkan',
                              'dipertanyakan', 'dipunyai', 'diri', 'dirinya', 'disampaikan', 'disebut', 'disebutkan',
                              'disebutkannya', 'disini', 'disinilah', 'ditambahkan', 'ditandaskan', 'ditanya', 'ditanyai',
                              'ditanyakan', 'ditegaskan', 'ditujukan', 'ditunjuk', 'ditunjuki', 'ditunjukkan',
                              'ditunjukkannya', 'ditunjuknya', 'dituturkan', 'dituturkannya', 'diucapkan', 'diucapkannya',
                              'diungkapkan', 'dong', 'dua', 'dulu', 'empat', 'enggak', 'enggaknya', 'entah', 'entahlah',
                              'guna', 'gunakan', 'hal', 'hampir', 'hanya', 'hanyalah', 'hari', 'harus', 'haruslah',
                              'harusnya', 'hendak', 'hendaklah', 'hendaknya', 'hingga', 'ia', 'ialah', 'ibarat',
                              'ibaratkan',
                              'ibaratnya', 'ibu', 'ikut', 'ingat', 'ingat-ingat', 'ingin', 'inginkah', 'inginkan', 'ini',
                              'inikah', 'inilah', 'itu', 'itukah', 'itulah', 'jadi', 'jadilah', 'jadinya', 'jangan',
                              'jangankan', 'janganlah', 'jauh', 'jawab', 'jawaban', 'jawabnya', 'jelas', 'jelaskan',
                              'jelaslah', 'jelasnya', 'jika', 'jikalau', 'juga', 'jumlah', 'jumlahnya', 'justru', 'kala',
                              'kalau', 'kalaulah', 'kalaupun', 'kalian', 'kami', 'kamilah', 'kamu', 'kamulah', 'kan',
                              'kapan', 'kapankah', 'kapanpun', 'karena', 'karenanya', 'kasus', 'kata', 'katakan',
                              'katakanlah', 'katanya', 'ke', 'keadaan', 'kebetulan', 'kecil', 'kedua', 'keduanya',
                              'keinginan', 'kelamaan', 'kelihatan', 'kelihatannya', 'kelima', 'keluar', 'kembali',
                              'kemudian', 'kemungkinan', 'kemungkinannya', 'kenapa', 'kepada', 'kepadanya', 'kesampaian',
                              'keseluruhan', 'keseluruhannya', 'keterlaluan', 'ketika', 'khususnya', 'kini', 'kinilah',
                              'kira', 'kira-kira', 'kiranya', 'kita', 'kitalah', 'kok', 'kurang', 'lagi', 'lagian', 'lah',
                              'lain', 'lainnya', 'lalu', 'lama', 'lamanya', 'lanjut', 'lanjutnya', 'lebih', 'lewat', 'lima',
                              'luar', 'macam', 'maka', 'makanya', 'makin', 'malah', 'malahan', 'mampu', 'mampukah', 'mana',
                              'manakala', 'manalagi', 'masa', 'masalah', 'masalahnya', 'masih', 'masihkah', 'masing',
                              'masing-masing', 'mau', 'maupun', 'melainkan', 'melakukan', 'melalui', 'melihat',
                              'melihatnya',
                              'memang', 'memastikan', 'memberi', 'memberikan', 'membuat', 'memerlukan', 'memihak',
                              'meminta',
                              'memintakan', 'memisalkan', 'memperbuat', 'mempergunakan', 'memperkirakan', 'memperlihatkan',
                              'mempersiapkan', 'mempersoalkan', 'mempertanyakan', 'mempunyai', 'memulai', 'memungkinkan',
                              'menaiki', 'menambahkan', 'menandaskan', 'menanti', 'menanti-nanti', 'menantikan', 'menanya',
                              'menanyai', 'menanyakan', 'mendapat', 'mendapatkan', 'mendatang', 'mendatangi',
                              'mendatangkan',
                              'menegaskan', 'mengakhiri', 'mengapa', 'mengatakan', 'mengatakannya', 'mengenai',
                              'mengerjakan', 'mengetahui', 'menggunakan', 'menghendaki', 'mengibaratkan',
                              'mengibaratkannya',
                              'mengingat', 'mengingatkan', 'menginginkan', 'mengira', 'mengucapkan', 'mengucapkannya',
                              'mengungkapkan', 'menjadi', 'menjawab', 'menjelaskan', 'menuju', 'menunjuk', 'menunjuki',
                              'menunjukkan', 'menunjuknya', 'menurut', 'menuturkan', 'menyampaikan', 'menyangkut',
                              'menyatakan', 'menyebutkan', 'menyeluruh', 'menyiapkan', 'merasa', 'mereka', 'merekalah',
                              'merupakan', 'meski', 'meskipun', 'meyakini', 'meyakinkan', 'minta', 'mirip', 'misal',
                              'misalkan', 'misalnya', 'mula', 'mulai', 'mulailah', 'mulanya', 'mungkin', 'mungkinkah',
                              'nah',
                              'naik', 'namun', 'nanti', 'nantinya', 'nyaris', 'nyatanya', 'oleh', 'olehnya', 'pada',
                              'padahal', 'padanya', 'pak', 'paling', 'panjang', 'pantas', 'para', 'pasti', 'pastilah',
                              'penting', 'pentingnya', 'per', 'percuma', 'perlu', 'perlukah', 'perlunya', 'pernah',
                              'persoalan', 'pertama', 'pertama-tama', 'pertanyaan', 'pertanyakan', 'pihak', 'pihaknya',
                              'pukul', 'pula', 'pun', 'punya', 'rasa', 'rasanya', 'rata', 'rupanya', 'saat', 'saatnya',
                              'saja', 'sajalah', 'saling', 'sama', 'sama-sama', 'sambil', 'sampai', 'sampai-sampai',
                              'sampaikan', 'sana', 'sangat', 'sangatlah', 'satu', 'saya', 'sayalah', 'se', 'sebab',
                              'sebabnya', 'sebagai', 'sebagaimana', 'sebagainya', 'sebagian', 'sebaik', 'sebaik-baiknya',
                              'sebaiknya', 'sebaliknya', 'sebanyak', 'sebegini', 'sebegitu', 'sebelum', 'sebelumnya',
                              'sebenarnya', 'seberapa', 'sebesar', 'sebetulnya', 'sebisanya', 'sebuah', 'sebut', 'sebutlah',
                              'sebutnya', 'secara', 'secukupnya', 'sedang', 'sedangkan', 'sedemikian', 'sedikit',
                              'sedikitnya', 'seenaknya', 'segala', 'segalanya', 'segera', 'seharusnya', 'sehingga',
                              'seingat', 'sejak', 'sejauh', 'sejenak', 'sejumlah', 'sekadar', 'sekadarnya', 'sekali',
                              'sekali-kali', 'sekalian', 'sekaligus', 'sekalipun', 'sekarang', 'sekarang', 'sekecil',
                              'seketika', 'sekiranya', 'sekitar', 'sekitarnya', 'sekurang-kurangnya', 'sekurangnya', 'sela',
                              'selain', 'selaku', 'selalu', 'selama', 'selama-lamanya', 'selamanya', 'selanjutnya',
                              'seluruh', 'seluruhnya', 'semacam', 'semakin', 'semampu', 'semampunya', 'semasa', 'semasih',
                              'semata', 'semata-mata', 'semaunya', 'sementara', 'semisal', 'semisalnya', 'sempat', 'semua',
                              'semuanya', 'semula', 'sendiri', 'sendirian', 'sendirinya', 'seolah', 'seolah-olah',
                              'seorang',
                              'sepanjang', 'sepantasnya', 'sepantasnyalah', 'seperlunya', 'seperti', 'sepertinya',
                              'sepihak',
                              'sering', 'seringnya', 'serta', 'serupa', 'sesaat', 'sesama', 'sesampai', 'sesegera',
                              'sesekali', 'seseorang', 'sesuatu', 'sesuatunya', 'sesudah', 'sesudahnya', 'setelah',
                              'setempat', 'setengah', 'seterusnya', 'setiap', 'setiba', 'setibanya', 'setidak-tidaknya',
                              'setidaknya', 'setinggi', 'seusai', 'sewaktu', 'siap', 'siapa', 'siapakah', 'siapapun',
                              'sini',
                              'sinilah', 'soal', 'soalnya', 'suatu', 'sudah', 'sudahkah', 'sudahlah', 'supaya', 'tadi',
                              'tadinya', 'tahu', 'tahun', 'tak', 'tambah', 'tambahnya', 'tampak', 'tampaknya', 'tandas',
                              'tandasnya', 'tanpa', 'tanya', 'tanyakan', 'tanyanya', 'tapi', 'tegas', 'tegasnya', 'telah',
                              'tempat', 'tengah', 'tentang', 'tentu', 'tentulah', 'tentunya', 'tepat', 'terakhir', 'terasa',
                              'terbanyak', 'terdahulu', 'terdapat', 'terdiri', 'terhadap', 'terhadapnya', 'teringat',
                              'teringat-ingat', 'terjadi', 'terjadilah', 'terjadinya', 'terkira', 'terlalu', 'terlebih',
                              'terlihat', 'termasuk', 'ternyata', 'tersampaikan', 'tersebut', 'tersebutlah', 'tertentu',
                              'tertuju', 'terus', 'terutama', 'tetap', 'tetapi', 'tiap', 'tiba', 'tiba-tiba', 'tidak',
                              'tidakkah', 'tidaklah', 'tiga', 'tinggi', 'toh', 'tunjuk', 'turut', 'tutur', 'tuturnya',
                              'ucap', 'ucapnya', 'ujar', 'ujarnya', 'umum', 'umumnya', 'ungkap', 'ungkapnya', 'untuk',
                              'usah', 'usai', 'waduh', 'wah', 'wahai', 'waktu', 'waktunya', 'walau', 'walaupun', 'wong',
                              'yaitu', 'yakin', 'yakni', 'ya']

    # belum tahu mau set apa dasarnya
    def __init__(self, file):
        self.file = file

    def news_tokenize_sentence(self, sentence):

        # stop_word = set(stopwords.words("english"))
        # stop_word.update(self.stop_word_id)
        stop_word = self.stop_word_id

        tweet_tokens = TweetTokenizer(strip_handles=True, reduce_len=True)
        word2 = tweet_tokens.tokenize(sentence)

        # create stopWord from template
        filtered_sentence = [w for w in word2 if not w in stop_word]
        filtered_sentence = []

        # filtered word and remove link + lowercase
        for w in word2:
            w = re.sub(r"http\S+", "", w)
            w = w.lower()
            if w.lower() not in stop_word:
                filtered_sentence.append(w.lower())

        return (filtered_sentence)


    def clean_detik(self, file):
        with open(file) as data_file:
            data = json.load(data_file)
            json_result = {}
            list_berita = []
            # stop_word = set(stopwords.words("english"))
            # stop_word.update(self.stop_word_id)
            stop_word = self.stop_word_id

            for berita in data['feed']:
                result_content = []
                title = berita['title']
                lokasi = soup(berita['location'],"html.parser").get_text()

                arrayofberita = re.split(r"\.\s*", berita['content'])
                title2 = re.sub(r'[^\x00-\x7f]',r'', title)
                regex_title = re.compile(r'[^\w]')
                title2 = regex_title.sub(' ', title2)

                count=0
                for kalimat in arrayofberita:

                    # tokenize word
                    tweet_tokens = TweetTokenizer(strip_handles=True, reduce_len=True)
                    word2 = tweet_tokens.tokenize(kalimat)

                    # create stopWord from template
                    filtered_sentence = [w for w in word2 if not w in stop_word]
                    filtered_sentence = []
                    for w in word2:
                        w = re.sub(r"http\S+", "", w)
                        w = w.lower()
                        if w.lower() not in stop_word:
                            filtered_sentence.append(w.lower())


                    result_content.append({
                        "id":str(title2).replace(" ","").lower()+str(count),
                        "splitSentence":filtered_sentence,
                        "resultSentence":" ".join(filtered_sentence),
                        "oldSentence":kalimat
                    })
                    count+=1
                berita = {
                    "title": title,
                    "location": lokasi,
                    "result_content": result_content
                }
                list_berita.append(berita)
            json_result["feed"] = list_berita
            json_result["source"] = "detik"
            json_result['_file'] = file
            return (json_result)


    def clean_detik_2(self, file):
        data = file
        json_result = {}
        list_berita = []

        # stop_word = set(stopwords.words("english"))
        # stop_word.update(self.stop_word_id)
        stop_word = self.stop_word_id

        for berita in data['feed']:
            result_content = []
            title = berita['title']
            url = berita['url']
            img = berita['img']
            lokasi = soup(berita['location'],"html.parser").get_text()

            arrayofberita = re.split(r"\.\s*", berita['content'])
            title2 = re.sub(r'[^\x00-\x7f]',r'', title)
            regex_title = re.compile(r'[^\w]')
            title2 = regex_title.sub(' ', title2)

            count=0
            for kalimat in arrayofberita:

                # tokenize word
                tweet_tokens = TweetTokenizer(strip_handles=True, reduce_len=True)
                word2 = tweet_tokens.tokenize(kalimat)

                # create stopWord from template
                filtered_sentence = [w for w in word2 if not w in stop_word]
                filtered_sentence = []
                for w in word2:
                    w = re.sub(r"http\S+", "", w)
                    w = w.lower()
                    if w.lower() not in stop_word:
                        filtered_sentence.append(w.lower())


                result_content.append({
                    "id":str(title2).replace(" ","").lower()+str(count),
                    "splitSentence":filtered_sentence,
                    "resultSentence":" ".join(filtered_sentence),
                    "oldSentence":kalimat
                })
                count+=1
            berita = {
                "title": title,
                "location": lokasi,
                "result_content": result_content,
                "url" : url,
                "img" : img
            }
            list_berita.append(berita)
        json_result["list_berita"] = list_berita
        json_result["source"] = "detik"
        # json_result['_file'] = file
        return (json_result)

    def clean_liputan6(self, file):
        with open(file) as data_file:
            data = json.load(data_file)
            json_result = {}
            list_berita = []

            # stop_word = set(stopwords.words("english"))
            # stop_word.update(self.stop_word_id)
            stop_word = self.stop_word_id

            for berita in data['feed']:
                result_content = []
                title = berita['title']
                lokasi = soup(berita['location'],"html.parser").get_text()

                arrayofberita = re.split(r"\.\s*", berita['content'])
                title2 = re.sub(r'[^\x00-\x7f]',r'', title)
                regex_title = re.compile(r'[^\w]')
                title2 = regex_title.sub(' ', title2)

                count=0
                for kalimat in arrayofberita:

                    # tokenize word
                    tweet_tokens = TweetTokenizer(strip_handles=True, reduce_len=True)
                    word2 = tweet_tokens.tokenize(kalimat)

                    # create stopWord from template
                    filtered_sentence = [w for w in word2 if not w in stop_word]
                    filtered_sentence = []
                    for w in word2:
                        w = re.sub(r"http\S+", "", w)
                        w = w.lower()
                        if w.lower() not in stop_word:
                            filtered_sentence.append(w.lower())


                    result_content.append({
                        "id":str(title2).replace(" ","").lower()+str(count),
                        "splitSentence":filtered_sentence,
                        "resultSentence":" ".join(filtered_sentence),
                        "oldSentence":kalimat
                    })
                    count+=1
                berita = {
                    "title": title,
                    "location": lokasi,
                    "result_content": result_content
                }
                list_berita.append(berita)
            json_result["feed"] = list_berita
            json_result["source"] = "liputan6"
            json_result['_file'] = file
            return (json_result)