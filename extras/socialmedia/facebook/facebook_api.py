import facebook
import json

class FacebookAPI(object):

    # access token
    access_token = "133252573984435|VesAEMW2yKNYjryNQg01CMLlBCA"
    # initialize graph API
    graph = facebook.GraphAPI(access_token=access_token, version=2.10)


    def __init__(self,idname):
        print("initialize")
        self.id_name = idname


    # get page feed for the first 25
    def get_page_feed(self,id_name):
        print("getpagefeed")
        page_feed = self.graph.get_connections(id=id_name,connection_name="feed")

        page_result = {}
        page_post = []
        count = 0
        for post in page_feed['data']:
            page_post.append({
                "post_id": post['id'],
                "message": post["message"],
                "created_time": post['created_time']
            })
            count += 1
        page_result['post'] = page_post
        page_result['after'] = page_feed['paging']['cursors']['after']
        page_result['before'] = page_feed['paging']['cursors']['before']
        page_result['count'] = count
        return(page_result)


    def get_page_feed_after(self,id_name,after):
        print("getpagefeedafter")
        page_feed = self.graph.get_connections(id=id_name,connection_name="feed",after=after)

        page_result = {}
        page_post = []
        count =0
        for post in page_feed['data']:
            page_post.append({
                "post_id" : post['id'],
                "message" : post['message'],
                "created_time": post['created_time']
            })
            count +=1
        page_result['post'] = page_post
        page_result['after'] = page_feed['paging']['cursors']['after']
        page_result['before'] = page_feed['paging']['cursors']['before']
        page_result['count'] = count
        return(page_result)

    def get_page_feed_before(self,id_name,before):
        print("getpagefeedbefore")
        page_feed = self.graph.get_connections(id=id_name,connection_name="feed",before=before)

        page_result = {}
        page_post = []
        count =0
        for post in page_feed['data']:
            page_post.append({
                "post_id" : post['id'],
                "message" : post['message'],
                "created_time": post['created_time']
            })
            count +=1
        page_result['post'] = page_post
        page_result['after'] = page_feed['paging']['cursors']['after']
        page_result['before'] = page_feed['paging']['cursors']['before']
        page_result['count'] = count
        return(page_result)


    def get_comment_post(self,post_id):
        print("getcommentpost")
        comment_post = self.graph.get_connections(id=post_id, connection_name="comments")

        comment_result = {}
        comment_message = []
        comment_count = 0
        for comment in comment_post['data']:
            # if get_comment_count_of_comment(comment['id']) > 0:
            #     result = get_comment_of_comment(comment['id'])
            #     for item in result['data']:
            #         comment_message.append(item['message'])
            #         comment_count += 1
            comment_message.append({
                "comment_id" : comment['id'],
                "message" : comment['message']
            })
            comment_count +=1
        comment_result['message'] = comment_message
        comment_result['count'] = comment_count
        return(comment_result)


    # def get_comment_count_of_comment(comment_id):
    #     comment_result = graph.get_connections(id=comment_id, connection_name="comments", fields="comment_count")
    #     return comment_result['comment_count']
    #
    # def get_comment_of_comment(comment_id):
    #     comment_result = graph.get_connections(id=comment_id, connection_name="comments")
    #     return(comment_result)

    def create_json(self):
        print("createjson")
        response_json = {}
        response_json["name"] = "result facebook API"

        feed = []
        var1 = self.get_page_feed(self.id_name)
        while(var1['before'] is not None):
            for i in var1['post']:
                var2 = self.get_comment_post(i['post_id'])
                feed.append({
                    "post_id" : str(self.id_name)+i['post_id'],
                    "post" : i['message'],
                    "comment" : var2['message']})
            try:
                var1 = self.get_page_feed_before(self.id_name, var1['before'])
            except:
                break
        response_json["feed"] = feed
        return(response_json)

    def create_json_after(self):
        print("createjson")
        response_json = {}
        response_json["name"] = "result facebook API"

        feed = []
        var1 = self.get_page_feed(self.id_name)
        while(var1['after'] is not None):
            for i in var1['post']:
                var2 = self.get_comment_post(i['post_id'])
                feed.append({
                    "post_id" : str(self.id_name)+i['post_id'],
                    "post" : i['message'],
                    "comment" : var2['message']})
            try:
                var1 = self.get_page_feed_after(self.id_name, var1['after'])
            except:
                break
        response_json["feed"] = feed
        return(response_json)

