from openpyxl import load_workbook
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from api.models import Datatrain, SearchHistory, ResultTwitter
from rest_framework.response import Response
from rest_framework import status
from ..extras.socialmedia.twitter import twitter_api
from ..extras.tokenizer.tokenizer import SosmedTokenizer
from ..extras.algorithm.naivebayes import NaiveBayes
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core import serializers
from api import tasks
from celery.result import AsyncResult
import datetime, time

@api_view(['POST'])
def get_tweets(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        search_history = SearchHistory.objects.filter(id = int(request.data['id_search_history'])).first()
        # compare user with search history
        print(user_token.id)
        print(Token.objects.filter(user=search_history.project.user).first().key)
        print(search_history.project.user.id)
        if(user_token.id == search_history.project.user.id):
            print('call api')
            tweets = twitter_api.TwitterApi(1).get_tweets(keywords=search_history.query,
                                                          num_of_post=int(request.data['num_of_post']),
                                                          num_of_city=int(request.data['num_of_city']))
            test = NaiveBayes(80,20)
            # print(tweets)
            print('calculate naive bayes')
            sentiment_result = []
            for tweet in tweets['tweets']:
                result_sentiment = test.sentiment_test_twitter(query=tweet['text'])
                print('run algorithm')
                sentiment_result.append({
                    'result' : result_sentiment,
                    'username' : tweet['name'],
                    'location' : tweet['points'],
                    'created_at': tweet['created_at']
                })
                print('cek date')
                sample_datetime = time.strptime(tweet['created_at'], '%a %b %d %H:%M:%S +0000 %Y')

                print('save result')
                ResultTwitter.objects.create(
                    opinion = result_sentiment['result'],
                    sentence = result_sentiment['query'],
                    history = search_history,
                    city = tweet['points'],
                    tweet_created=datetime.datetime(
                        year=sample_datetime.tm_year, month=sample_datetime.tm_mon, day=sample_datetime.tm_mday,
                        hour=sample_datetime.tm_hour, minute=sample_datetime.tm_min, second=sample_datetime.tm_sec
                    )
                ).save()
            return Response(sentiment_result)
        else:
            return Response(status.HTTP_204_NO_CONTENT)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def get_tweets_from_redis(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        search_history = SearchHistory.objects.filter(id = int(request.data['id_search_history'])).first()

        # preparation for status process
        information = {}
        information['UUID'] = None
        information['Status'] = "Gagal"
        # compare user with search history

        if(user_token.id == search_history.project.user.id):
            try:
                x = tasks.sentimenttweet.apply_async((10,search_history.id,request.data['num_of_post'],request.data['num_of_city']), countdown=10)
                information['UUID'] = x.id
                information['Status'] = "Started after 10 seconds"
            except:
                return Response(information)
            return Response(information)
        else:
            return Response(status.HTTP_204_NO_CONTENT)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def import_database(request):
    if request.method == 'GET':
        path_file = "datatrain_twitter.xlsx"
        obj_tokenizer = SosmedTokenizer("oke")

        file_excel = load_workbook(path_file)

        # sindonews
        tweets = file_excel['twitter']
        a = {}
        max_row = 107
        count = 0
        for position in range(4, max_row):
            split_message = {"split_message":  obj_tokenizer.twitter_tokenize_sentence(tweets['E'+str(position)].value)}
            Datatrain.objects.create(
                name="oki",
                message_id=tweets['C' + str(position)].value,
                result_message=tweets['D' + str(position)].value,
                old_message=tweets['E' + str(position)].value,
                split_message=split_message,
                media_type="twitter_train",
                opinion=tweets['G' + str(position)].value
            ).save()
            print(count)
            count += 1
        return Response(count)


@api_view(['GET'])
def set_new_model(request):
    if request.method == 'POST':
        query = request.data['query']
        # tweets = twitter_api.TwitterApi(1)
        # result = tweets.get_tweets(keywords=query, num_of_post=100, num_of_city=10)
        result = NaiveBayes(80, 20).sentiment_twitter_train()

        return Response(status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def test_static_file(request):
    if request.method == 'GET':
        url_link = staticfiles_storage.url('geocode_id.json')
        return Response(url_link)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def get_result_twitter(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user

        search_history = SearchHistory.objects.filter(id=int(request.data['id_search_history'])).first()
        # result_twitter = ResultTwitter.objects.filter(history = search_history).values_list('opinion')
        result_twitter = ResultTwitter.objects.filter(history=search_history).all()

        # prepare result
        result_data = {}
        result_sentiment = []
        for result in result_twitter:
            result_sentiment.append({
                'sentence' : result.sentence,
                'opinion' : result.opinion,
                'date_created' : result.date_created,
                'location' : result.city
            })
        result_data['query'] = search_history.query
        result_data['sentiment'] = result_sentiment

        total_sentiment_positive = len(result_twitter.filter(opinion='positive'))
        total_sentiment_neutral = len(result_twitter.filter(opinion='neutral'))
        total_sentiment_negative = len(result_twitter.filter(opinion='negative'))
        total_sentiment = total_sentiment_negative+total_sentiment_positive+total_sentiment_neutral

        result_data['count_sentiment'] = {
            'positive' : total_sentiment_positive,
            'negative' : total_sentiment_negative,
            'neutral' : total_sentiment_neutral
        }
        result_data['percentage_sentiment'] = {
            "positive": round(((total_sentiment_positive / total_sentiment) * 100), 2),
            "neutral": round(((total_sentiment_neutral / total_sentiment) * 100), 2),
            "negative": round(((total_sentiment_negative / total_sentiment) * 100), 2)
        }

        return Response(result_data)
    return Response(status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def get_result_twitter_by_city(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user

        # get id history
        search_history = SearchHistory.objects.filter(id=int(request.data['id_search_history'])).first()

        if (user_token.id == search_history.project.user.id):

            # get list of city
            res_twitter = ResultTwitter.objects.filter(history=search_history).all()
            cities = res_twitter.values("city").distinct()

            # set result
            result_data = {}
            result_sentiment = []

            for city in cities:
                res_city = res_twitter.filter(city=city['city'])

                total_sentiment_positive = len(res_city.filter(opinion='positive'))
                total_sentiment_neutral = len(res_city.filter(opinion='neutral'))
                total_sentiment_negative = len(res_city.filter(opinion='negative'))
                total_sentiment = total_sentiment_negative + total_sentiment_positive + total_sentiment_neutral


                result_sentiment.append({
                    'city' : city['city'],
                    'sentiment' : {
                        'positive' : total_sentiment_positive,
                        'neutral': total_sentiment_neutral,
                        'negative': total_sentiment_negative,
                        'total' : total_sentiment
                    },
                    'percentage' : {
                        'positive': round((total_sentiment_positive/total_sentiment)*100,2),
                        'neutral': round((total_sentiment_neutral/total_sentiment)*100,2),
                        'negative': round((total_sentiment_negative/total_sentiment)*100,2),
                    }
                })

            result_data['sentiment'] = result_sentiment
            result_data['query'] = search_history.query

            return Response(result_data)
        else:
            return Response(status.HTTP_204_NO_CONTENT)
    return Response(status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def get_result_twitter_by_last_week(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user

        # get id history
        search_history = SearchHistory.objects.filter(id=int(request.data['id_search_history'])).first()

        if (user_token.id == search_history.project.user.id):

            # get list of city
            res_twitter = ResultTwitter.objects.filter(history=search_history).all()
            # cities = res_twitter.values("city").distinct()
            date_search = {
                'year' : search_history.date_created.year,
                'month' : search_history.date_created.month,
                'day' : 29
            }

            result_sentiment = []
            for tweet in res_twitter:
                result_sentiment.append({
                    'year' : tweet.tweet_created.year,
                    'month': tweet.tweet_created.month,
                    'day': tweet.tweet_created.day,
                    'opinion': tweet.opinion,
                })

            result_sentiment_per_group = []
            # get distinct date
            date_now = search_history.date_created + datetime.timedelta(1)
            for _date in range(1,8):
                print(date_now)
                count_positive = 0
                count_neutral = 0
                count_negative = 0
                count_total = 0
                for item in result_sentiment:
                    print(item)
                    if((item['year'] == date_now.year) and (item['month'] == date_now.month) and (item['day'] == date_now.day)):
                        if(item['opinion'] == 'positive'):count_positive +=1
                        if (item['opinion'] == 'neutral'): count_neutral += 1
                        if (item['opinion'] == 'negative'): count_negative += 1
                        count_total +=1
                print(count_total)
                result_sentiment_per_group.append({
                    'date' : str(date_now.day)+"-"+str(date_now.month)+"-"+str(date_now.year),
                    'total' : count_total,
                    'sentiment' :{
                        'positive' : count_positive,
                        'neutral' : count_neutral,
                        'negative' : count_negative
                    }
                })
                date_now -= datetime.timedelta(1)

            # set result
            # result_data = {}
            # result_sentiment = []
            #
            # for city in cities:
            #     res_city = res_twitter.filter(city=city['city'])
            #
            #     total_sentiment_positive = len(res_city.filter(opinion='positive'))
            #     total_sentiment_neutral = len(res_city.filter(opinion='neutral'))
            #     total_sentiment_negative = len(res_city.filter(opinion='negative'))
            #     total_sentiment = total_sentiment_negative + total_sentiment_positive + total_sentiment_neutral
            #
            #
            #     result_sentiment.append({
            #         'city' : city['city'],
            #         'sentiment' : {
            #             'positive' : total_sentiment_positive,
            #             'neutral': total_sentiment_neutral,
            #             'negative': total_sentiment_negative,
            #             'total' : total_sentiment
            #         },
            #         'percentage' : {
            #             'positive': round((total_sentiment_positive/total_sentiment)*100,2),
            #             'neutral': round((total_sentiment_neutral/total_sentiment)*100,2),
            #             'negative': round((total_sentiment_negative/total_sentiment)*100,2),
            #         }
            #     })
            #
            # result_data['sentiment'] = result_sentiment
            # result_data['query'] = search_history.query

            return Response(result_sentiment_per_group, status.HTTP_200_OK)
        else:
            return Response(status.HTTP_204_NO_CONTENT)
    return Response(status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def get_result_sentence_twitter(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user

        search_history = SearchHistory.objects.filter(id=int(request.data['id_search_history'])).first()
        result_twitter = ResultTwitter.objects.filter(history = search_history).values_list('sentence','opinion')

        sentences = []
        for sentence in result_twitter:
            sentences.append(sentence)

        # prepare result
        result_data = {}
        result_data['query'] = search_history.query
        result_data['id_history'] = search_history.id
        result_data['sentences'] = sentences

        return Response(result_data)
    return Response(status.HTTP_204_NO_CONTENT)