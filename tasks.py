from __future__ import absolute_import, unicode_literals
from api.models import SearchHistory, ResultNewsDetik, ResultDetik, ResultTwitter, CSEGoogle, ProjectSentiment, ResultOtherNews, Datatrain, CountSentimentNews, TopMentionTweet, TopInfluencerTweet, TopContributorTweet
from celery import task
from celery.result import AsyncResult
from .extras.news.detik import detik_api
from .extras.tokenizer.tokenizer_news import NewsTokenizer
from .extras.algorithm import naivebayes
from .extras.socialmedia.twitter import twitter_api
from datetime import timedelta
from time import mktime
import datetime, time, numpy
from .extras.tokenizer import tokenizer_news
from .extras.algorithm import naivebayes
import numpy, tweepy
from django.db.models import Count
from geopy.geocoders import Nominatim, GoogleV3

max_day = 8

@task(name="scrapdetik")
def scrapdetik(a,b):
    # get SearchHistory info
    history= SearchHistory.objects.filter(id=b).first()
    user_project = ProjectSentiment.objects.filter(id=int(history.project.id)).first()

    datenow = datetime.datetime.now()
    # scrap detik news using API

    print("Starting collect data from API on : " + str(datetime.datetime.now()))
    for i in range(1, max_day):

        isoke = True
        page = 1
        obj1 = detik_api.DetikApi(1)
        while(isoke):
            user_project.status_project = "sedang scrap dari detik"
            user_project.save()
            scrap_detik = obj1.get_news(query=history.query,day=datenow.day,
                                        month=datenow.month,year=datenow.year,page=page)
            print("scrap finished : " + str(datetime.datetime.now()))
            if (scrap_detik['feed'] == []):
                isoke == False
                break
            else:
                # clean, split result, and calculate sentiment
                result = NewsTokenizer("1").clean_detik_2(file=scrap_detik)


                print("sentiment calculated : " + str(datetime.datetime.now()))

                # call naive bayes module
                run_algorithm = naivebayes.NaiveBayes(80, 20)

                # save inform into db
                for sentences in result['list_berita']:
                    RND = ResultNewsDetik.objects.create(
                        history=history,
                        link=sentences['title'],
                        date_news_created = datetime.datetime(
                            year=datenow.year, month=datenow.month, day=datenow.day,
                            hour=1, minute=0, second=0,microsecond=0
                        ),
                        urls = sentences['url'],
                        img = sentences['img']
                    )
                    RND.save()
                    print("save Title News ["+str(RND.id)+"]"+sentences['title']+"on : " + str(datetime.datetime.now()))

                    # siapkan kontainer untuk sentiment
                    count_sentiment_positive = 0
                    count_sentiment_neutral = 0
                    count_sentiment_negative = 0

                    for sentence in sentences['result_content']:

                        # illegal_sentence
                        except_sentence = "create target id"

                        # check if illegal_sentence is not in sentence
                        if(except_sentence not in sentence['resultSentence']):
                            print("calculating sentiment analysis for " + sentence['resultSentence'] + "on : " + str(datetime.datetime.now()))
                            sentiment_detik = run_algorithm.sentiment_test_detik(query=sentence['resultSentence'])

                            ResultDetik.objects.create(
                                opinion=sentiment_detik['result'],
                                sentence=sentiment_detik['query'],
                                result_news_detik=RND
                            ).save()
                            if (sentiment_detik['result'] == 'positive'): count_sentiment_positive+=1
                            if (sentiment_detik['result'] == 'neutral'): count_sentiment_neutral += 1
                            if (sentiment_detik['result'] == 'negative'): count_sentiment_negative += 1

                            print("save Sentence [" + str(RND.id) + "]" + sentence['resultSentence'] + "on : " + str(datetime.datetime.now()))

                    # tentukan sebuah berita itu positive, negative, atau tidak
                    RND.total_sentiment_positive = count_sentiment_positive
                    RND.total_sentiment_neutral = count_sentiment_neutral
                    RND.total_sentiment_negative = count_sentiment_negative

                    current_opinion = "neutral"
                    if((count_sentiment_positive > count_sentiment_negative) and (count_sentiment_positive > count_sentiment_neutral)):
                        RND.opinion = "positive"
                        current_opinion = "positive"
                    if ((count_sentiment_negative > count_sentiment_positive) and (
                        count_sentiment_negative > count_sentiment_neutral)):
                        RND.opinion = "negative"
                        current_opinion = "negative"
                    RND.save()

                    # masukan kedalam CountSentimentNews
                    # jika id_history sudah ada, maka diupdate
                    count_db = len(CountSentimentNews.objects.filter(history=history,situs_utama="Detik"))

                    if (count_db == 0):
                        c_pos = 0
                        c_neg = 0
                        c_neu = 0
                        if (current_opinion == 'positive'): c_pos += 1
                        if (current_opinion == 'neutral'): c_neu += 1
                        if (current_opinion == 'negative'): c_neg += 1

                        CountSentimentNews.objects.create(
                            situs_utama="Detik",
                            total_post=1,
                            total_sentiment_positive=c_pos,
                            total_sentiment_neutral=c_neu,
                            total_sentiment_negative=c_neg,
                            history=history
                        )
                    else:
                        get_last = CountSentimentNews.objects.filter(history=history,situs_utama="Detik").first()
                        get_last.total_post += 1

                        # check opinion
                        if (current_opinion == 'positive'): get_last.total_sentiment_positive += 1
                        if (current_opinion == 'neutral'): get_last.total_sentiment_neutral += 1
                        if (current_opinion == 'negative'): get_last.total_sentiment_negative += 1
                        get_last.save()

                page+=1


        print("data saved! : " + str(datetime.datetime.now()))
        datenow += timedelta(days=-1)

    print("task done")

    scrapdetik.update_state(state='SUCCESS')

    user_project.status_project = "selesai scrap dari detik"
    user_project.save()
    # print(str(AsyncResult(scrap_detik.request.id).state))

    # tambah ambil data dari situs yang lain
    # kemudian hitung sentimennya

    import pprint, numpy, time, requests
    print(a)

    id_project = history.project.id
    user_project = ProjectSentiment.objects.filter(id=int(id_project)).first()
    search_history = SearchHistory.objects.filter(project=user_project).first()

    my_api_key = "AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o"
    # my_cse_id = "004152435857447790448:0zvezzh8-cg"

    cse_news = []
    listcse = CSEGoogle.objects.filter(total_post__gt=0).all()
    for listcse_item in listcse:
        cse_news.append({
            "news": str(listcse_item.situs_utama),
            "situs": str(listcse_item.situs),
            "id": str(listcse_item.cse_id)
        })

    set_date = search_history.date_created

    tanggal = []
    # create array of date in x days
    for i in range(1, max_day):
        day_now = set_date.day
        month_now = set_date.month
        year_now = set_date.year
        date_now = ""

        if (day_now > 9):
            if (month_now > 9):
                date_now = str(year_now) + str(month_now) + str(day_now)
            else:
                date_now = str(year_now) + "0" + str(month_now) + str(day_now)
        else:
            if (month_now > 9):
                date_now = str(year_now) + str(month_now) + "0" + str(day_now)
            else:
                date_now = str(year_now) + "0" + str(month_now) + "0" + str(day_now)
        tanggal.append(date_now)
        set_date = set_date - timedelta(days=1)

    query = search_history.query
    total_semua_situs = 0
    detail_setiap_situs = []

    print("query " + str(query))
    status = "sedang memulai pengambilan situs"
    user_project.status_project = status
    user_project.save()
    for in_cse in cse_news:

        current_cse = in_cse['id']
        print("total untuk situs " + str(in_cse['news']))
        user_project.status_project = "sedang mengambil berita di " + str(in_cse['news'])
        user_project.save()

        total_seluruhnya = 0
        detail_setiap_tanggal = []

        for x in tanggal:
            # tanggal = "20180101"
            date = "sort=date:r:" + x + ":" + x
            start = 1
            total = 0
            for i in range(1, 10):
                time.sleep(3)
                urls = "https://www.googleapis.com/customsearch/v1?q=" + str(
                    query) + "+&key=AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o&cx=" + current_cse + "&start=" + str(
                    start) + "&" + date
                r = requests.get(url=urls)
                data = r.json()
                start += 10
                try:
                    total += len(data['items'])
                    # fetch data setiap title

                    for item in data['items']:
                        img = ""
                        try:
                            img = item['pagemap']['cse_image'][0]['src']
                        except:
                            print("tak ada img")

                        opinion = "neutral"
                        sentence = str(item['title']) + " " + str(item['snippet'])
                        try:
                            result = naivebayes.NaiveBayes(80, 20).sentiment_test_detik(query=sentence)
                            opinion = result['result']
                        except:
                            print("tak bisa dihitung sentiment, set ke neutral")

                        ResultOtherNews.objects.create(
                            history=search_history,
                            title=item['title'],
                            sentence=item['snippet'],
                            urls=item['link'],
                            date_news_created=datetime.datetime(day=int(x[6:8]), month=int(x[4:6]), year=int(x[0:4])),
                            img=img,
                            situs=in_cse['news'],
                            subsitus=in_cse['situs'],
                            opinion=opinion
                        )

                        # masukan kedalam CountSentimentNews
                        # jika id_history sudah ada, maka diupdate
                        count_db = len(CountSentimentNews.objects.filter(history = search_history,
                                                                         situs_utama=in_cse['news']))

                        if(count_db == 0):
                            c_pos = 0
                            c_neg = 0
                            c_neu = 0
                            if (opinion =='positive'): c_pos +=1
                            if (opinion == 'neutral'): c_neu += 1
                            if (opinion == 'negative'): c_neg += 1

                            CountSentimentNews.objects.create(
                                situs_utama=in_cse['news'],
                                total_post =1,
                                total_sentiment_positive = c_pos,
                                total_sentiment_neutral = c_neu,
                                total_sentiment_negative = c_neg,
                                history = search_history
                            )
                        else:
                            get_last = CountSentimentNews.objects.filter(history = search_history,situs_utama=in_cse['news']).first()
                            get_last.total_post+=1

                            # check opinion
                            if (opinion =='positive'): get_last.total_sentiment_positive +=1
                            if (opinion == 'neutral'): get_last.total_sentiment_neutral += 1
                            if (opinion == 'negative'): get_last.total_sentiment_negative += 1
                            get_last.save()

                    detail_setiap_tanggal.append({
                        "tanggal": x,
                        "jumlah": total
                    })
                except:
                    break
            print("-- tanggal " + x + " ada berita sebanyak " + str(total))
            total_seluruhnya += total

        detail_setiap_situs.append({
            "situs": str(in_cse['news']),
            "total": total_seluruhnya,
            "detail": detail_setiap_tanggal
        })
        print("dan totalnya adalah " + str(total_seluruhnya))
        total_semua_situs += total_seluruhnya

    print(total_semua_situs)
    user_project.status_project = "selesai mengambil berita dengan total" + str(total_semua_situs)
    user_project.save()


@task(name="tambah")
def tambah():
    tambah.update_state(state='SUCCESS')
    print(str(AsyncResult(tambah.request.id).state))
    print(10+10)



@task(name="sentiment_tweet_bytweety")
def sentimenttweet_bytweety(a,b,num_of_post,num_of_city):
    # get SearchHistory info
    history= SearchHistory.objects.filter(id=b).first()

    # get current date
    date_now = datetime.datetime.now()
    endUntil = str(date_now.year) + '-' + str(date_now.month) + '-' + str(date_now.day - 1)
    # using tweepy
    # list_query
    list_query = ['-filter:retweets filter:verified -filter:links',  # original post without link from verified account
                  '-filter:retweets filter:verified',  # original post from verified account
                  '-filter:retweets -filter:verified -filter:links',
                  # original post without link from unverified account
                  '-filter:retweets -filter:verified',  # original post from unverified account

                  'filter:retweets -filter:verified url:detik.com',
                  # retweet post from non verified account for detik.com link
                  'filter:retweets -filter:verified -filter:links'
                  # retweet post from non verified account without links
                  ]
    query = "joko widodo "+list_query[0]
    new_tweepy = twitter_api.TwitterApi(1)
    new_tweepy.get_tweets_tweepy(keywords=query,num_of_post=10,num_of_city=3,initial_date=endUntil)

    # hitung top contributor
    # hitung lokasi


@task(name="sentiment_tweet")
def sentimenttweet(a,b,num_of_post,num_of_city):
    # get SearchHistory info
    history= SearchHistory.objects.filter(id=b).first()
    user_project = ProjectSentiment.objects.filter(id=int(history.project.id)).first()

    # get current date
    date_now = datetime.datetime.now()

    print("Starting collect data from API on : " + str(datetime.datetime.now()))

    #
    user_project.status_project = "sedang mengambil tweet"
    user_project.save()
    for i in range(1, max_day):
        # get date from date_now
        day = date_now.day
        month = date_now.month
        year = date_now.year

        # current_date
        current_date = str(year) + "-" + str(month) + "-" + str(day + 1)

        tweets = twitter_api.TwitterApi(1).get_tweets(keywords=history.query,
                                                      num_of_post=num_of_post,
                                                      num_of_city=num_of_city,
                                                      initial_date=current_date)
        print("data collected from API on : " + str(datetime.datetime.now()))


        test = naivebayes.NaiveBayes(80, 20)
        # sentiment_result = []
        for tweet in tweets['tweets']:

            print("calculate sentiment for  : " + str(tweet['text']))
            result_sentiment = test.sentiment_test_twitter(query=tweet['text'])

            print("convert datetime for current tweet")
            sample_datetime = time.strptime(tweet['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
            sample_datetime1 = datetime.datetime.fromtimestamp(mktime(sample_datetime)) + timedelta(hours=7)

            print("saving sentiment result on : " + str(datetime.datetime.now()))
            ResultTwitter.objects.create(
                opinion=result_sentiment['result'],
                sentence=result_sentiment['query'],
                history=history,
                city= tweet['points'],
                tweet_created=datetime.datetime(
                    year=sample_datetime1.year, month=sample_datetime1.month, day=sample_datetime1.day,
                    hour=sample_datetime1.hour, minute=sample_datetime1.minute, second=sample_datetime1.second,
                    microsecond=sample_datetime1.microsecond
                ),
                retweet_count=tweet['retweet_count'],
                favourite_count=tweet['favorite_count'],
                user_name=tweet['user_name'],
                user_screen_name=tweet['user_screen_name'],
                user_id=tweet['user_id' ],
                profile_image_url=tweet['profile_image_url'],
                id_str=tweet['id_str']
            ).save()
            print("sentiment result saved on : " + str(datetime.datetime.now()))

        # get prev date
        date_now += timedelta(days=-1)

    # tambah hitung top mention
    # pindahkan kode mention analysis kesini ya

    # start hitung top mention
    user_project.status_project = "sedang menghitung top mention"
    user_project.save()

    tweets = ResultTwitter.objects.filter(history=history).all()
    ###calculate mention overview section
    ## calculate total mention
    total_mention = 0
    # list of user
    array_of_user = []
    # calculate total like_retweet
    total_like_retweet = 0
    # calculate top mention
    top_mention_tweets = []

    for tweet in tweets:
        if ((tweet.retweet_count > 0) or (tweet.favourite_count > 0)):
            total_mention += 1
            total_like_retweet = total_like_retweet + tweet.retweet_count + tweet.favourite_count
            if (str(tweet.user_screen_name) not in array_of_user):
                array_of_user.append(tweet.user_screen_name)

            # masukan tweet ke mentiontoptweet
            if (len(TopMentionTweet.objects.filter(resulttweet=tweet).all()) is 0):
                TopMentionTweet.objects.create(
                    resulttweet=tweet,
                    retweet_count=tweet.retweet_count,
                    favourite_count=tweet.favourite_count,
                    user_screen_name=tweet.user_screen_name,
                    history=tweet.history
                ).save()

    # end hitung top mention

    # start kalkulasi influencer
    user_project.status_project = "sedang menghitung influencer"
    user_project.save()


    # hitung influencer yang paling sering ngetweet based on TopMentionTweeet
    res1 = TopMentionTweet.objects.filter(history=history).values('user_screen_name').annotate(
        freq_count=Count('user_screen_name')).order_by("-freq_count")
    # new_res1 = sorted(res1, key=lambda k: k['freq_count'])

    # set tweepy untuk influencer
    # set key for twitter
    # consumer_key = "UsJuIf3Xkz7w6MIUnQpnArLW9"
    # consumer_secret = "XXqoia5GVOqQnuEaKzopdhj39b87o2mrficvAH20bqd8CixI4K"
    # access_key = "938132848057266176-5dSTpaGTtwq2d8tp8mOlCUD6nVDHNDE"
    # access_secret = "UIPvTiD86GZgneRlMHOia1nGYvYkjHreByO8nGzyzUlKW"

    consumer_secret = "sGTZKmt8XbXpkxlL42fj9xaWX7ysZOZL7TBn1JpCc7PIzOnU67"
    consumer_key = "4TINjDD3recUl8U86RM1zhtdS"
    access_key = "912143740289957888-SdvPjPPD2Cics6y5hF1tSzMaLxCuWkU"
    access_secret = "zFvHNFqiOdHcWEVTCb0rgYQsG6ZI7NwjPqvQk4reVe44a"

    # create tweepy handler
    auth = tweepy.OAuthHandler(consumer_key=consumer_key, consumer_secret=consumer_secret)
    auth.set_access_token(access_key, access_secret)
    twitter = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    top_influencer = []
    for x in res1:
        y = twitter.get_user(screen_name=str(x['user_screen_name']))
        is_verified = "false"
        try:
            is_verified = str(y.verified)
        except:
            print("username undetected")

        TopInfluencerTweet.objects.create(
            history=history,
            user_screen_name=str(x['user_screen_name']),
            user_twitter_link="https://twitter.com/" + str(x['user_screen_name']),
            user_photo_link=str(y.profile_image_url),
            total_post=int(x['freq_count']),
            is_verified=str(is_verified)
        )
    # end kalkulasi influencer

    # tambah hitung top contributor
    user_project.status_project = "sedang menghitung daftar top contributor"
    user_project.save()
    # start hitung top contributor
    res = ResultTwitter.objects.filter(history=history).values('user_id').annotate(
        freq_count=Count('user_id')).order_by("-freq_count")

    # container for contributor
    result_contributor = []

    # set key for twitter
    consumer_key = "MInEi9Ag1G5in5uC91STZK8Hm"
    consumer_secret = "TUoi95s7tym5uOe7dVN8r6Hutdz1GCZ9ithycE2VXrF3NPL87g"
    access_key = "912140736702722048-G0eNkxzDU1EhXi0KAE8RH7EwYRkZHkZ"
    access_secret = "O3hatQEdrOka0UOaWSIhRJ4iKDUwaQKFe04asSLMpM4yR"

    # create tweepy handler
    auth = tweepy.OAuthHandler(consumer_key=consumer_key, consumer_secret=consumer_secret)
    auth.set_access_token(access_key, access_secret)
    twitter = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    for x in res:
        try:
            y = twitter.get_user(id=str(x['user_id']))
            TopContributorTweet.objects.create(
                history=history,
                user_screen_name=str(y.screen_name),
                user_twitter_link="https://twitter.com/" + str(y.screen_name),
                user_photo_link=str(y.profile_image_url),
                total_post=str(x['freq_count']),
                is_verified=str(y.verified),
                num_of_posts=str(y.statuses_count),
                num_of_friend=str(y.friends_count),
                num_of_favorites=str(y.favourites_count),
                num_of_follower=str(y.followers_count),
            )
        except:
            print("user tidak ada. lanjut ke berikutnya")

    # end hitung top contributor

    # start kalkulasi location analysis
    user_project.status_project = "sedang menghitung location analysis"
    user_project.save()

    # set tweepy config
    # if(user_project):
    consumer_key = "b9eimyr8G7Ebq98yCaasuKd8z"
    consumer_secret = "ihGIovjoeh7yX6YB6eWFVHUgWfwYEzmFSdzETisurIPMFWkorz"
    access_key = "912974958002708481-DC6U3kMFaHgWYsOK8OMwZXxYOc4VuIX"
    access_secret = "jRy97Zf2c1tFOw7iZH8frLT35TngwKyJfJdP30XnUIJTf"

    auth = tweepy.OAuthHandler(consumer_key=consumer_key, consumer_secret=consumer_secret)
    auth.set_access_token(access_key, access_secret)
    twitter = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    # hitung influencer yang paling sering ngetweet based on TopMentionTweeet
    res = ResultTwitter.objects.filter(history=history).all()
    res1 = res.values_list("id_str").distinct()
    geolocator = Nominatim()
    geolocator_google = GoogleV3()

    for x in res1:
        selected_id_str = x[0]
        get_tweet = ResultTwitter.objects.filter(id_str=selected_id_str).first()

        # id_status = str(x[0])
        # print(id_status)
        try:
            # get location
            detail_status = twitter.get_status(selected_id_str)
            lokasi = ""
            longitude = ""
            latitude = ""
            if (detail_status.user.location is not None):
                lokasi = detail_status.user.location
            elif (detail_status.extended_entities["media"][0]["additional_media_info"]["source_user"][
                      "location"] is not None):
                lokasi = detail_status.extended_entities["media"][0]["additional_media_info"]["source_user"][
                    "location"]
            else:
                lokasi = ""

            # get coordinate
            location = ""
            try:
                location = geolocator_google.geocode(lokasi)
                longitude = str(location.longitude)
                latitude = str(location.latitude)
            except:
                try:
                    location = geolocator.geocode(lokasi)
                    longitude = str(location.longitude)
                    latitude = str(location.latitude)
                except:
                    print("lokasi tidak ditemukan")
            get_tweet.location = lokasi
            get_tweet.marker_text = str(ResultTwitter.objects.filter(id_str=selected_id_str).first().sentence)
            get_tweet.lng = longitude
            get_tweet.lat=latitude
            get_tweet.profile_image_url = str(ResultTwitter.objects.filter(id_str=selected_id_str).first().profile_image_url)
            get_tweet.save()
        except:
            print("status of " + selected_id_str + " isnt valid / deleted")

    # end kalkulasi location analysis
    user_project.status_project = "selesai menghitung location analysis"
    user_project.save()


@task(name="get_news_from_other_detik")
def get_news_from_other_detik(a,b):
    import pprint, numpy, time, requests
    print(a)
    # print(b)
    # list_id_project = ProjectSentiment.objects.all()

    # for item_project in list_id_project:
    #     id_project = item_project.id
    id_project = b
    user_project = ProjectSentiment.objects.filter(id=int(id_project)).first()
    search_history = SearchHistory.objects.filter(project=user_project).first()

    my_api_key = "AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o"
    # my_cse_id = "004152435857447790448:0zvezzh8-cg"

    cse_news = []
    listcse = CSEGoogle.objects.filter(total_post__gt=0).all()[1:5]
    for listcse_item in listcse:
        cse_news.append({
            "news": str(listcse_item.situs_utama),
            "situs" : str(listcse_item.situs),
            "id": str(listcse_item.cse_id)
        })

    set_date = search_history.date_created

    tanggal = []
    # create array of date in x days
    for i in range(1, 2):
        day_now = set_date.day
        month_now = set_date.month
        year_now = set_date.year
        date_now = ""

        if (day_now > 9):
            if (month_now > 9):
                date_now = str(year_now) + str(month_now) + str(day_now)
            else:
                date_now = str(year_now) + "0" + str(month_now) + str(day_now)
        else:
            if (month_now > 9):
                date_now = str(year_now) + str(month_now) + "0" + str(day_now)
            else:
                date_now = str(year_now) + "0" + str(month_now) + "0" + str(day_now)
        tanggal.append(date_now)
        set_date = set_date - timedelta(days=1)

    query = search_history.query
    total_semua_situs = 0
    detail_setiap_situs = []

    print("query " + str(query))
    status = "sedang memulai pengambilan situs"
    user_project.status_project = status
    user_project.save()
    for in_cse in cse_news:

        current_cse = in_cse['id']
        print("total untuk situs " + str(in_cse['news']))
        user_project.status_project = "sedang mengambil berita di "+str(in_cse['news'])
        user_project.save()

        total_seluruhnya = 0
        detail_setiap_tanggal = []

        for x in tanggal:
            # tanggal = "20180101"
            date = "sort=date:r:" + x + ":" + x
            start = 1
            total = 0
            for i in range(1, 10):
                time.sleep(3)
                urls = "https://www.googleapis.com/customsearch/v1?q="+str(query)+"+&key=AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o&cx=" + current_cse + "&start=" + str(
                    start) + "&" + date
                r = requests.get(url=urls)
                data = r.json()
                start += 10
                try:
                    total += len(data['items'])
                    # fetch data setiap title

                    for item in data['items']:
                        img = ""
                        try:
                            img = item['pagemap']['cse_image'][0]['src']
                        except:
                            print("tak ada img")

                        opinion = "neutral"
                        sentence = str(item['title'])+" "+str(item['snippet'])
                        try:
                            result = naivebayes.NaiveBayes(80, 20).sentiment_test_detik(query=sentence)
                            opinion = result['result']
                        except:
                            print("tak bisa dihitung sentiment, set ke neutral")

                        ResultOtherNews.objects.create(
                            history = search_history,
                            title=item['title'],
                            sentence=item['snippet'],
                            urls=item['link'],
                            date_news_created=datetime.datetime(day=int(x[6:8]),month=int(x[4:6]),year=int(x[0:4])),
                            img=img,
                            situs=in_cse['news'],
                            subsitus=in_cse['situs'],
                            opinion=opinion
                        )

                    detail_setiap_tanggal.append({
                        "tanggal": x,
                        "jumlah": total
                    })
                except:
                    break
            print("-- tanggal " + x + " ada berita sebanyak " + str(total))
            total_seluruhnya += total

        detail_setiap_situs.append({
            "situs": str(in_cse['news']),
            "total": total_seluruhnya,
            "detail": detail_setiap_tanggal
        })
        print("dan totalnya adalah " + str(total_seluruhnya))
        total_semua_situs += total_seluruhnya

    print(total_semua_situs)
    user_project.status_project = "selesai mengambil berita"
    user_project.save()



@task(name="migrasidataset")
def migrasidataset(a,b):

    print(a)
    print(b)
    # detik
    data1 = ResultDetik.objects.filter(is_edited = True).all()
    obj_tokenizer = tokenizer_news.NewsTokenizer("oke")

    for item in data1:
        opinion = 0
        if(item.opinion == 'positive'): opinion = 1
        if (item.opinion == 'negative'): opinion = -1

        split_message = obj_tokenizer.news_tokenize_sentence(str(item.sentence))

        x = Datatrain.objects.create(
            name="admin",
            message_id = str(item.id),
            result_message = item.sentence,
            old_message = item.sentence,
            split_message = split_message,
            media_type = "detik_train_2018",
            opinion = opinion
        )

        print("sedang memasukkan detik dg id "+str(item.id))

    # twitter
    data2 = ResultTwitter.objects.filter(is_edited=True).all()
    obj_tokenizer = tokenizer_news.NewsTokenizer("oke")

    for item in data2:
        opinion = 0
        if (item.opinion == 'positive'): opinion = 1
        if (item.opinion == 'negative'): opinion = -1

        split_message = obj_tokenizer.news_tokenize_sentence(str(item.sentence))

        x = Datatrain.objects.create(
            name="admin",
            message_id=str(item.id),
            result_message=item.sentence,
            old_message=item.sentence,
            split_message=split_message,
            media_type="twitter_train_2018",
            opinion=opinion
        )
        print("sedang memasukkan twitter dg id " + str(item.id))


    print("selesai migrasi")


@task(name="create_model")
def create_model(a,b):
    print(a)
    print(b)
    run_algorithm = naivebayes.NaiveBayes(100, 50)
    print(run_algorithm.sentiment_detik_train())
    print(run_algorithm.sentiment_twitter_train(100,50))