from rest_framework import serializers
from django.contrib.auth.models import User,Group
from  .._models.news import SearchHistory

class SearchSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""
    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = SearchHistory
        fields = ('id','query','user','category','project','date_created')
        read_only_fields = ('date_created','user')