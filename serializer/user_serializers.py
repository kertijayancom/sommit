from rest_framework import serializers
from django.contrib.auth.models import User,Group


class UserSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""
    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = User
        fields = ('id','username','password', 'first_name', 'last_name', 'email','is_staff','last_login','date_joined')
        read_only_fields = ('last_login','date_joined')