from bs4 import BeautifulSoup as soup
import re,json,datetime,requests
from datetime import timedelta
import os, time
from lxml import html
import datetime

#-----------------------------------------------------------------------
# create detik API object
#-----------------------------------------------------------------------
class DetikApi(object):
    def __init__(self,id):
        self.id = id

    def get_news(self,query,day,month,year,page):
        global location
        query = query.replace(' ', '+')
        # datenow = datetime.datetime.now()
        datenow = str(day)+"/"+str(month)+"/"+str(year)
        url = "https://www.detik.com/search/searchall?query="+query+"&siteid=3&sortby=time&sorttime=1&fromdatex="+datenow+"&todatex="+datenow+"&page="+str(page)
        req = requests.get(url)
        page = soup(req.text, "html.parser")
        link_url = []
        img_url = []
        raw_content = []
        result_content = []
        json_result = {}

        # 1. Mendapatkan url setiap berita
        for links in page.select('div.list-berita'):
            for link in links('a'):
                if 'href' in link.attrs:
                    x = link.attrs['href']
                    if((x.find('detikflash') == -1) and (x.find('e-flash') == -1) and (x.find('detiktv') == -1)):
                        link_url.append(x)

        for links in page.select('article'):
            for y in links('img'):
                if 'src' in y.attrs:
                    img = y.attrs['src']
                    if(img.find('videoservice') == -1):
                        img_url.append(img)

        # print("banyak data : "+str(len(link_url)))
        # print(link_url)

        # 2. Mendapatkan content setiap url
        for y in link_url:
            req2 = requests.get(y)
            data = req2.text
            page = soup(data, "html.parser")
            if (str(soup(str(page.select('div.jdl h1')), "html.parser").get_text()).strip('[]')) is not '':
                raw_content.append(page)
        # print("banyak kontent : "+str(len(raw_content)))

        # 3. Mendapatkan element di setiap content dan menghasilkan output json
        print("\n")
        position = 0
        for z in raw_content:
            page = soup(str(z), "html.parser")
            if (str(soup(str(page.select('div.jdl h1')), "html.parser").get_text()).strip('[]')) is not None:
                title = str(soup(str(page.select('div.jdl h1')), "html.parser").get_text()).strip('[]')

                raw_content = str(soup(str(page.select('div.detail_text')), "html.parser").get_text()).strip('[]')
                regex = re.compile(r'[\n\r\t]')
                content = regex.sub('', raw_content)

                all_text = page.select('div.detail_text')

                select_location = soup(str(all_text), "html.parser")

                loc = soup(str(select_location.find('b')), "html.parser").get_text()
                get_loc = loc.split()
                location = get_loc[0]

                # urls = "detik.com"
                # search_title = "-".join(title.split(" "))
                # for x in link_url:
                #     if(search_title in x):
                #         urls = x

                result_content.append(
                    {
                        "title": title,
                        "location": location,
                        "content": content,
                        "url" : link_url[position],
                        "img" : img_url[position]
                    }
                )
                # print(title)
                # print(link_url[position])
                # print(img_url[position])
            position+=1
        # print("banyak kalimat = "+str(len(result_content)))
        source = "detik"
        a_time = str(datetime.datetime.now())
        a_time = a_time.replace(" ", "__")
        a_time = a_time.replace(":", "_")
        a_time = a_time.replace("-", "_")

        file_name = '_' + a_time + '_' + source + ".json"

        json_result["feed"] = result_content
        json_result["sumber"] = source
        json_result["file_name"] = file_name
        json_result["date_created"] = a_time

        return(json_result)



import numpy
# import time
#
# datenow = datetime.datetime.now() - datetime.timedelta(days=1)
# query = "anies"
# print("Starting collect data from API on : " + str(datetime.datetime.now()))
#
# # for i in numpy.arange(1,30):
# isoke = True
# page = 1
# obj1 = DetikApi(1)
# total = 0
#
# while(isoke):
#     # print("=== page ke :"+str(page))
#     a = obj1.get_news(query=query, day=datenow.day, month=datenow.month, year=datenow.year, page=page)
#     if(a['feed'] == []):
#         isoke = False
#     else:
#         for x in a['feed']:
#             print(str(x['title'])+" "+str(x['url']))
#         page +=1

# print(a)
#         print("disini baru nge run")
#         if(a['feed'] == []):
#             print("break")
#             isoke = False
#             break
#         else:
#             print("lanjut cetak")
#             print("for now "+str(len(a['feed'])))
#             for x in a['feed']:
#                 print(x['title'])
#                 total+=1
#             page+=1
#             time.sleep(5)
#
#         print("scrap finished : " + str(datetime.datetime.now()))
#     print("tanggal "+str(datenow)+" sebanyak : "+str(total))
#     datenow = datenow - datetime.timedelta(days=1)
