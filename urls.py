from .views import _main, news, socialmedia
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import api_user, api_token, api_search, api_twitter, api_validation, api_detik, api_redis, api_project, latihan, api_instagram
# from .views import latihan
# from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    # url(r'^$', _main.index, name='index'),
    # url(r'^list_user', _main.list_user),

    # #------------------------------------ News ---------------------------------------------
    # url(r'^get_detik/(?P<q>[a-zA-Z0-9_ ]+)', news.get_detik),
    # url(r'^get_liputan6/(?P<q>[a-zA-Z0-9_ ]+)', news.get_liputan6),
    # url(r'^tokenizer_detik/(?P<path_id>\d+)/(?P<user_id>\d+)/', news.tokenizer_detik),
    # url(r'^tokenizer_liputan6/(?P<path_id>\d+)/(?P<user_id>\d+)/', news.tokenizer_liputan6),
    # url(r'^detikjson_to_db/(?P<path_id>[0-9]+)/(?P<user_id>[0-9]+)', news.detikjson_to_db),
    # url(r'^readcsv', news.readcsv, name='newcsv'),
    url(r'^algorithm', news.algorithm, name='algorithm'),
    url(r'^cek_data', api_project.cek_data, name='cek_data'),

    # # detik test using pickle
    # url(r'^test_detik_query/(?P<query>[a-zA-Z0-9_ ]+)', news.test_detik_query),
    # url(r'^result_detik', news.test_detik),


    # OKE
    # ----------------------------------- Auth Login Session API---------------------------------
    url(r'^get_auth_token', obtain_auth_token, name='get_auth_token'),
    url(r'^token/$', api_token.get_user_token, name="token"),
    url(r'^token/info$', api_token.get_user_information, name="token_info"),


    # OKE
    #------------------------------------ User API----------------------------------------------
    # users/   [GET]
    # users/   [POST] username password
    url(r'^users/$', api_user.CreateView.as_view(), name="create_user"),
    # users/<pk>    [GET]
    # users/<pk>    [PUT] username password
    # users/<pk>    [DELETE]
    url(r'^users/(?P<pk>[0-9]+)/$', api_user.DetailView.as_view(), name="detail_user"),
    url(r'^users/validation/$', api_validation.user_validation, name="user_validation"),
    # url(r'^user_validation/$', api_user.SearchUserView, name="search_user"),

    # OKE

    # ------------------------------------ Project API---------------------------------------------

    # create project by user
    # POST
    url(r'^project/$', api_project.create_project, name="create_project"),

    # list project filter by user
    # GET
    url(r'^project/list/$', api_project.list_project_by_user, name="list_project"),

    # list all search by specific project
    # GET
    url(r'^project/list/(?P<id_project>[0-9]+)$', api_project.list_search_by_project, name="list_search_by_project"),
    url(r'^project/sentiment/(?P<id_project>[0-9]+)$', api_project.result_sentiment_by_project, name="result_sentiment_by_project"),
    # url(r'^project/sentiment/graph(?P<id_project>[0-9]+)$', api_project.result_sentiment_graph_by_project, name="result_sentiment_graph_by_project"),
    # url(r'^project/sentiment/data/(?P<id_project>[0-9]+)$
    # url(r'^project/sentiment/analytics/(?P<id_project>[0-9]+)$

    # list project/sentiment/graph
    url(r'^project/sentiment/graph/(?P<id_project>[0-9]+)$', api_project.result_sentiment_graph_by_project, name="result_sentiment_graph_by_project"),

    # list project/sentiment/data
    url(r'^project/sentiment/data/sosmed/twitter/(?P<id_project>[0-9]+)/(?P<page>[0-9]+)/(?P<per_page>[0-9]+)/$', api_project.result_sentiment_data_sosmed_twitter_by_project, name="result_sentiment_data_sosmed_twitter_by_project"),
    url(r'^project/sentiment/data/news/detik/(?P<id_project>[0-9]+)/(?P<page>[0-9]+)/(?P<per_page>[0-9]+)/$', api_project.result_sentiment_data_news_detik_by_project, name="result_sentiment_data_news_detik_by_project"),
    url(r'^project/sentiment/data/news/other/(?P<id_project>[0-9]+)/(?P<page>[0-9]+)/(?P<per_page>[0-9]+)/$', api_project.result_sentiment_data_other_news_by_project, name="result_sentiment_data_other_news_by_project"),

    # list project status by specific project
    # GET
    url(r'^project/status/(?P<id_project>[0-9]+)/$', api_project.status_project_by_id, name="status_project"),
    url(r'^project/current_status/(?P<id_project>[0-9]+)$', api_project.current_status_project_by_id, name="current_status_project"),

    # compare project
    # GET
    # url(r'^project/comparation/(?P<id_project_one>[0-9]+)/(?P<id_project_two>[0-9]+)/$', api_project.comparation_result_by_two_project, name="compare_two_project"),
    # url(r'^project/comparation/(?P<id_project_one>[0-9]+)/(?P<id_project_two>[0-9]+)/(?P<id_project_three>[0-9]+)/$', api_project.comparation_result_by_three_project, name="compare_three_project"),

    # mention analysis
    url(r'^project/mention/(?P<id_project>[0-9]+)/$', api_project.mention_analysis_by_project, name="mention_analysis_project"),
    # trends analysis
    url(r'^project/trend/(?P<id_project>[0-9]+)/$', api_project.trend_analysis_by_project, name="trend_analysis_project"),
    # hastag analysis
    url(r'^project/hastag/(?P<id_project>[0-9]+)/$', api_project.hastag_analysis_by_project, name="hastag_analysis_project"),
    # top_influencer analysis
    url(r'^project/influencer/(?P<id_project>[0-9]+)/$', api_project.influencer_analysis_by_project, name="influencer_analysis_project"),
    # location analysis
    url(r'^project/location/(?P<id_project>[0-9]+)/$', api_project.location_analysis_by_project,
        name="location_analysis_project"),
    # top contributor analysis
    url(r'^project/contributor/(?P<id_project>[0-9]+)/$', api_project.top_contributor_analysis_by_project,
        name="top_contributor_analysis_project"),

    url(r'^project/othernews/(?P<id_project>[0-9]+)/$', api_project.coba_get_news_all_news,
        name="other_data_news_by_project"),
    # url(r'^project/update_location/(?P<id_project>[0-9]+)/$', api_project.local_set_location, name="set_location_by_local"),

    # url(r'^project/create_model/$', api_project.create_new_model_detik, name="create_model"),
    # url(r'^google/search/$', api_project.get_data_sosmed_from_cse, name="get_data_from_cse"),
    url(r'^google/search/review$', api_project.review_data_cse_sosmed, name="review_data_cse_sosmed"),
    # url(r'^google/result/instagram$', api_project.get_detail_post_instagram, name="view_detail_instagram"),
    # url(r'^google/result/facebook$', api_project.get_detail_post_facebook, name="view_detail_facebook"),

    #------------------------------------ Search API---------------------------------------------
    # Search/   [GET]
    # Search/   [POST] query
    url(r'^searches/$', api_search.CreateView.as_view(), name="search"),
    # Search/      [PUT] pk
    # Search/     [DELETE} pk
    url(r'^searches/(?P<pk>[0-9]+)/$', api_search.DetailView.as_view(), name="detail_search"),
    # url(r'^searches/status/(?P<pk>[0-9]+)/$', api_search.DetailView.as_view(), name="status_search"),

    #------------------------------------ Detik API---------------------------------------------
    # Detik/   [GET]
    # Detik/   [POST] query
    # url(r'^detik/$', api_detik.CreateResultDetikView, name="create_detik"),
    # url(r'^detik/(?P<pk>[0-9]+)/$', api_detik.DetailResultDetikView, name="detail_result"),
    url(r'^search_news_detik/$', api_detik.get_news_detik_redis, name="news_detik"),

    # get result detik per sentences
    url(r'^search_news_detik/result/$', api_detik.get_result_sentence_detik, name="news_detik_result"),

    # get status detik by redis
    url(r'^status_news_detik/(?P<task_id>.*)/$', api_detik.get_status_detik_redis, name="status_news_detik"),
    # url(r'^search_news_detik/result_sentence/$', api_detik.get_news_detik, name="news_detik_sentence"),

    #------------------------------------ Twitter API---------------------------------------------
    # create sentiment twitter
    url(r'^search_sosmed_twitter/$', api_twitter.get_tweets_from_redis, name="sosmed_twitter"),

    # result twitter
    url(r'^search_sosmed_twitter/result/$', api_twitter.get_result_twitter, name="sosmed_twitter_result"),
    url(r'^search_sosmed_twitter/result/date/$', api_twitter.get_result_twitter_by_last_week, name="sosmed_twitter_result_by_last_week"),

    # result twitter by city
    url(r'^search_sosmed_twitter/result/city/$', api_twitter.get_result_twitter_by_city, name="sosmed_twitter_result_by_city"),

    # result twitter per sentence
    url(r'^search_sosmed_twitter/result_sentence/$', api_twitter.get_result_sentence_twitter, name="sosmed_twitter_result_sentence"),

    # import database from file
    # url(r'^import_sosmed_twitter/$', api_twitter.import_database, name="import_sosmed_twitter"),
    # url(r'^test_static/$', api_twitter.test_static_file, name="test_static_file"),

    # coba redis
    # url(r'^cobaredis/$', api_redis.get_tambah, name="cobaredis"),
    # url(r'^latihan/(?P<lemparan>.*)/$', latihan.sample_lemparan, name="latihan"),

    #------------------------------------ Instagram API---------------------------------------------
    url(r'^test_ig/$', api_instagram.detail_instagram_information_from_link, name="test_instagram"),
]