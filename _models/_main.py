# Create your models here.
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User


class User(models.Model):
    username = models.CharField(max_length=10)
    password = models.CharField(max_length=10)

    def __str__(self):
        return "%s %s" % (self.username, self.password)


class Rawpathjson(models.Model):
    path_name = models.CharField(max_length=250)
    user_id = models.ForeignKey(User)
    media_type = models.CharField(max_length=100, null=True)

    def __str__(self):
        return "%s %s" % (self.path_name, self.user_id.username)


class Splitpathjson(models.Model):
    path_name = models.CharField(max_length=250)
    user_id = models.ForeignKey(User)
    media_type = models.CharField(max_length=100, null=True)

    def __str__(self):
        return "%s %s" % (self.path_name, self.user_id.username)


class ContohSplitjson(models.Model):
    result_message = models.CharField(max_length=300)
    old_message = models.CharField(max_length=300)
    split_message = JSONField()

class Datatrain(models.Model):
    name = models.CharField(max_length=300)
    message_id = models.CharField(max_length=300)
    result_message = models.TextField()
    old_message = models.TextField()
    split_message = JSONField()
    media_type = models.CharField(max_length=100)
    opinion = models.IntegerField()


# class ProjectSentiment(models.Model):
