# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-02 17:43
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0041_cobaresultinstagram'),
    ]

    operations = [
        migrations.CreateModel(
            name='CobaResultFacebook',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True)),
                ('title', models.CharField(blank=True, max_length=500, null=True)),
                ('snippet', models.CharField(blank=True, max_length=500, null=True)),
                ('url', models.CharField(blank=True, max_length=500, null=True)),
                ('img', models.CharField(blank=True, max_length=500, null=True)),
                ('sesi', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='CobaResultTwitter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True)),
                ('title', models.CharField(blank=True, max_length=500, null=True)),
                ('snippet', models.CharField(blank=True, max_length=500, null=True)),
                ('url', models.CharField(blank=True, max_length=500, null=True)),
                ('img', models.CharField(blank=True, max_length=500, null=True)),
                ('sesi', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True)),
            ],
        ),
    ]
