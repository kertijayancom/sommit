from ..extras.tokenizer.tokenizer_news import NewsTokenizer
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from api.models import SearchHistory, ResultDetik, ResultNewsDetik
from api import tasks
from celery.result import AsyncResult


# # ============================== DETIK SEARCH API ==============================
# class CreateResultDetikView(generics.ListCreateAPIView):
#     queryset = ResultDetik.objects.all()
#     serializer_class = DetikSerializer
#
#     def perform_create(self, serializer):
#         serializer.save()
#
#
# # ============================== DETIK SEARCH API ==============================
# class DetailResultDetikView(generics.RetrieveUpdateDestroyAPIView):
#     queryset = ResultDetik.objects.all()
#     serializer_class = DetikSerializer



@api_view(['POST'])
def get_news_detik(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        search_history = SearchHistory.objects.filter(id = int(request.data['id_search_history'])).first()
        # compare user with search history
        if(user_token.id == search_history.user.id):
            # get data news
            # new_detik = detik_api.DetikApi(1).get_news(query=search_history.query)
            new_detik = {
                            "feed": [
                                {
                                    "location": "Jakarta",
                                    "title": "Prabowo Turun Gunung, Efeknya Sedahsyat Apa?",
                                    "content": "Jakarta - Ketua Umum Gerindra Prabowo Subianto tampak sudah 'turun gunung'. Hal tersebut ditandai dengan aktifnya kembali Prabowo di sosial media hingga berkeliling Yogyakarta untuk berziarah. Setelah memposting ucapan Idul Fitri pada Juni 2017 lalu, akun Twitter Prabowo tampak tak ada aktivitas. Baru Senin (12/11) pagi kemarin @prabowo kembali mencuitkan sesuatu lagi disertai sebuah foto.\"Alhamdulillah memulai hari dengan subuh berjamaah di masjid Kristal Khadijah. Selamat beraktivitas. Bangun bangsamu dengan karyamu. #Yogya,\" tulis pria 66 tahun itu.<!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->Alhamdulillah memulai hari dengan subuh berjamaah di masjid Kristal Khadijah. Selamat beraktivitas. Bangun bangsamu dengan karyamu. #Yogya pic.twitter.com/IIsQKOV8XXâ\u0080\u0094 Prabowo Subianto (@prabowo) November 12, 2017Beberapa jam berikutnya setelah tweet pertama, Prabowo kembali mengupload foto bersama Amien Rais beserta anak-anak kecil yang berpakaian adat Bali. Prabowo memang bertemu dengan Ketua Dewan Kehormatan PAN Amien Rais di Yogyakarta\"Menggunakan Twitter lagi? Ya saya kira hanya masalah waktu saja. Beliau bisa jadi sangat amat sibuk,\" ujar Ketua DPP Gerindra Sodik Mudjahid saat dimintai tanggapan, Senin (13/11).Baca juga: Ziarah di Kotagede, Prabowo Berdoa di Makam Panembahan SenopatiMenurut Sodik, selama ini Prabowo tidak aktif di sosmed karena begitu sibuk dalam menghadapi Pilkada 2018. \"Seperti kemarin, kan ada konferensi nasional menghadapi pilgub-pilkada, ya kan. Kemudian beberapa pembinaan internal, bertemu dengan DPD-DPD, DPC-DPC,\" jelasnya. Selain bertemu Amien Rais dan sejumlah tokoh, Prabowo juga berziarah ke makam Raja Mataram dan Taman Makam Pahlawan saat berada di Yogyakarta. Prabowo menjelaskan ziarahnya ke Makam Raja-raja Mataram di Kotagede dan Imogiri karena dia masih memiliki trah keluarga Mataram Islam. Namun Prabowo tidak menjelaskan secara rinci soal hal itu. \"Agenda pribadi, agenda keluarga,\" kata Prabowo. Prabowo mengatakan ini sebagai tradisi menghormati leluhur dan memperingati sejarah. \"Saya kira itu saja, masalah pribadi,\" tuturnya.Baca juga: Prabowo di Yogyakarta: Lebih Baik Hancur Daripada Dijajah KembaliHal sama dikatakan Prabowo kepada wartawan saat berada di Taman Makam Pahlawan Kusumanegara Yogyakarta. \"Berdoa menjelang Pilpres, Pak?\" tanya wartawan. \"Siapa yang nanya, kamu? Kok nanya ke situ sih,\" tutur Prabowo. \"Kita kan orang Indonesia, apalagi orang Jawa. Nyekar itu kan budaya kita,\" lanjutnya. Wasekjen Gerindra Andre Rosiade menyebut memang sudah saatnya Prabowo 'turun gunung'. Kunjungan Prabowo ke Yogyakarta, kata Andre, bisa disebut sebagai awal dari segalanya. \"Ya, intinya Pak Prabowo akan silaturahmi ke berbagai tokoh dan ke berbagai daerah. Sudah saatnyalah. Kalau saya sebagai kader menganggap sudah saatnya Pak Prabowo untuk turun gunung, gitulah,\" ujar Andre saat dihubungi, Senin (13/11).Baca juga: Prabowo 'Turun Gunung', Gerindra Yakin Kini Bisa Kalahkan JokowiSebagai pucuk pimpinan partai, Prabowo disebut Andre sudah seharusnya melakukan konsolidasi politik. Dia membandingkan gerak politik Prabowo dengan Agus Harimurti Yudhoyono (AHY), sang kontestan dalam Pilgub DKI 2017. \"AHY aja, AHY aja hampir setiap minggu ke daerah kan. Iya, itu wajarlah. Setiap politisi, ya beliau kan Ketum Partai Gerindra, tentu harus mulai rajin ke daerah untuk melakukan konsolidasi,\" jelas Andre. Andre menyebut Prabowo sedang mengembalikan ingatan para pemilihnya dengan terjun ke daerah-daerah. Menurut Andre, Prabowo merupakan sosok terkuat selain Jokowi untuk menjadi RI-1.(rna/fai)gerindraprabowo subianto<!--// <![CDATA[    OA_show('newstag');     // ]]> -->polong.create({target: 'thepolong',id: 55})"
                                },
                                {
                                    "location": "Yogyakarta",
                                    "title": "Prabowo di Yogyakarta: Lebih Baik Hancur Daripada Dijajah Kembali",
                                    "content": "Yogyakarta - Prabowo Subianto berziarah di makam Jenderal Soedirman di Yogyakarta. Setelahnya, Prabowo bicara soal meneladani pahlawan dan kewajiban menanam benih cinta tanah air. \"Menghormati nilai-nilai yang mereka (para pahlawan) junjung tinggi, yaitu nilai cinta tanah air, nilai rame ing gawe sepi ing pamrih,\" kata Prabowo kepada wartawan di Taman Makam Pahlawan (TMP) Kusumanegara Kota Yogya, Senin (13/11/2017).\"Nilai cinta rakyat, nilai kejujuran, nilai menegakkan kebenaran, keadilan,\" imbuhnya.<!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->Prabowo melanjutkan, sudah semestinya negara Indonesia berdaulat berdasar nilai-nilai perjuangan para pahlawan. Dia berpesan agar bangsa ini ke depannya lebih percaya dengan kekuatan sendiri. \"Pertahanankan setiap jengkal tanah, pekarangan kita sendiri. Karena sekali merdeka tetap merdeka. Lebih baik hancur daripada dijajah kembali. Itu nilai yang harus kita ingat di saat-saat sekarang,\" pungkasnya.(sip/mbr)yogyakartaprabowoprabowo subianto<!--// <![CDATA[    OA_show('newstag');     // ]]> -->polong.create({target: 'thepolong',id: 55})"
                                },
                                {
                                    "location": "Yogyakarta",
                                    "title": "Ziarah di Yogya untuk Persiapan Pilpres 2019? Begini Jawaban Prabowo",
                                    "content": "Yogyakarta - Ketua Umum Partai Gerindra Prabowo Subianto berziarah ke makam Raja Mataram dan Taman Makam Pahlawan di Yogyakarta. Berdoa menjelang Pilpres, Pak?\"Kamu siapa sih, pribadi saja, heh,\" ujar Prabowo usai berziarah di makam Raja Mataram di Imogiri Bantul, Senin (13/11/2017). Dia mengatakan bahwa kegiatannya ini merupakan agenda pribadinya. <!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->\"Agenda pribadi, agenda keluarga,\" kata Prabowo. Prabowo mengatakan ini sebagai tradisi menghormati leluhur dan memperingati sejarah. \"Saya kira itu saja, masalah pribadi,\" tuturnya.Hal sama dikatakan Prabowo kepada wartawan saat berada di Taman Makam Pahlawan Kusumanegara Yogyakarta.\"Siapa yang nanya, kamu? Kok nanya kesitu sih,\" kata Prabowo. \"Kita kan orang Indonesia, apalagi orang Jawa. Nyekar itu kan budaya kita,\" lanjutnya. Prabowo menjelaskan ziarahnya ke Makam Raja-raja Mataram di Kotagede dan Imogiri karena dia masih memiliki trah keluarga Mataram Islam. Namun Prabowo tidak menjelaskan secara rinci soal hal itu. \"Ya memang (terkait keturunan),\" pungkas dia.(sip/bgs)yogyakartaprabowoprabowo subianto<!--// <![CDATA[    OA_show('newstag');     // ]]> -->polong.create({target: 'thepolong',id: 55})"
                                },
                                {
                                    "location": "Yogyakarta",
                                    "title": "Usai ke Makam Raja, Prabowo Ziarahi Makam Jenderal Soedirman di Yogya",
                                    "content": "Yogyakarta - Ketua Umum Partai Gerindra, Prabowo Subianto menyempatkan diri berziarah ke Makam Jenderal Soedirman di Taman Makam Pahlawan (TMP) Kusumanegara, Kota Yogyakarta. Kunjungan ini dilangsungkan seusai Prabowo berziarah ke Makam Raja-raja Mataram di Kotagede dan Imogiri.Prabowo sampai ke TMP Kusumanegara sekitar pukul 16.48 WIB, Senin (13/11/2017). Setelah turun dari mobil, dia langsung menuju pintu masuk TMP Kusumanegara. Sebelum melangkahkan kaki ke pusara Jenderal Soedirman, Prabowo terlebih dahulu melakukan salam hormat ala militer.Setelahnya Prabowo menuju makam Jenderal Soedirman. Sama seperti sesampainya di pintu masuk TMP Kusumanegara, persis di depan pusara Jenderal Soedirman dia melakukan salam hormat ala militer. Kemudian dia menaburkan bunga lalu duduk bersila di samping pusara.<!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->Prabowo di TMP Kusumanegara, Yogyakarta. Foto: Usman HadiSelesai berdoa di Makam Jenderal Soedirman, berturut-turut Prabowo menuju Makam Djenderal Raden Derip Soemohardjo dan ke Makam Menteri Soepeno. Sama saat berada di Makan Jenderal Soedirman, di kedua makam ini Prabowo juga melakukan tabur bunga dan berdoa.Penampilan Prabowo saat berziarah ke TMP Kusumanegara berbeda dengan pakaian yang dikenakan saat berziarah ke Makam Raja-raja Mataram. Bila di Makam Raja-raja Prabowo mengenakan busana peranakan Jawa, di TMP Prabowo memakai baju putih bercelana hitam. \"Ya habis itu (acara di SD Budi Mulia Dua), saya sempatkan juga untuk nyekar di (Makam Raja-raja Mataram) Imogiri, ke (Makam Raja-raja Mataram) Kotagede, dan hari ini mampir ke (makan) Pak Dirman dan juga Menteri Soepeno,\" kata Prabowo sesuai berziarah ke TMP Kusumanegara.(sip/sip)yogyakartaprabowo subiantoprabowo<!--// <![CDATA[    OA_show('newstag');     // ]]> -->polong.create({target: 'thepolong',id: 55})"
                                },
                                {
                                    "location": "Jakarta",
                                    "title": "Prabowo 'Turun Gunung', Gerindra Yakin Kini Bisa Kalahkan Jokowi",
                                    "content": "Jakarta - Ketum Partai Gerindra Prabowo Subianto disebut mulai 'turun gunung' menjelang Pilpres 2019, diawali kunjungan ke Yogyakarta. Wasekjen Gerindra Andre Rosiade menyebut Prabowo memang kandidat kuat untuk memenangkan Pilpres 2019.Andre punya alasan di balik ucapannya tersebut. Dia mengingatkan soal beberapa lembaga survei yang menempatkan Prabowo sebagai penantang terkuat petahana saat ini, yaitu Joko Widodo. \"Karena faktanya, apapun lembaga surveinya, Pak Prabowo adalah penantang terkuat Pak Jokowi. Bedanya nggak jauh. Kalau dulu kan 2014 Pak Jokowi kan waktu mencalonkan 60-70 elektabilitas, Pak Prabowo 5-10 persen,\" kata Andre kepada wartawan, Senin (13/11/2017). <!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->  Baca juga: Saatnya Prabowo 'Turun Gunung'Baca juga: Ngetweet Lagi, Prabowo Bicara soal Infrastruktur dan Pendidikan\"Alhamdulillah, faktanya ya beda tipis di Pilpres. Sekarang (elektabilitas) Pak Jokowi 38-40 (persen), Pak Prabowo dibilang 20-25 (persen). Ya bedanya 10-15 persen lah,\" lanjut dia. Menurut Andre, elektabilitas Prabowo saat ini dapat menjadi modal awal mengalahkan Jokowi. Dia yakin sang ketum dapat berjaya di Pilpres 2019.\"Jadi, tidak terlalu jauh perbedaan di berbagai lembaga survei yang ada. Ini modal awal Pak Prabowo untuk mengalahkan Pak Jokowi di 2019. Dan kami, Partai Gerindra, meyakini itu,\" pungkas Andre. (gbr/elz)prabowo subiantopilpres 2019gerindra<!--// <![CDATA[ OA_show('newstag');  // ]]> --> polong.create({ target: 'thepolong', id: 55 }) "
                                },
                                {
                                    "location": "Yogyakarta",
                                    "title": "Ziarah di Kotagede, Prabowo Berdoa di Makam Panembahan Senopati",
                                    "content": "Yogyakarta - Ketua Umum Partai Gerindra Prabowo Subianto hari ini mengunjungi makam Raja Mataram di Kotagede, Yogyakarta. Di sana, Prabowo berdoa di makam Panembahan Senopati. \"Jadi Pak Prabowo pada hari ini, tadi berziarah ke makam Kanjeng Panembahan Senopati. Juga ke keluarga Panembahan Senopati, juga istrinya dan kerabatnya yang berada di kompleks makam Raja-raja Mataram di Kotagede,\" ujar Ketua Abdi Dalem di Makam Raja-raja Mataram di Kotagede dari Kasunanan Surakarta, Raden Temenggung Pujodipuro (54) kepada wartawan, Senin (13/11/2017).Pujodipuro mengatakan bahwa Prabowo mengirim doa untuk leluhur Mataram. <!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->Prabowo saat keluar dari kompleks Makam Raja Mataram di Kotagede. Foto: Usman Hadi\"Dengan tujuan supaya Allah SWT memberikan syafaatnya kepada bangsa kita, negeri kita. Supaya diberikan ketentraman,\" imbuhnya. Lantunan doa diwakili oleh abdi dalem yang menemani Prabowo. Baca juga: Usai ke Kotagede, Prabowo Ziarah Ke Makam Sultan Agung di Imogiri\"Jadi Pak Prabowo hanya diam saja, tadi (sebelumnya) beliau sudah berpesan, intinya kirim doa kepada (Raja-raja Mataram),\" ungkap Pujodipuro.Pujodipuro menceritakan saat berziarah ke Makam Raja-raja Mataram di Kotagede, Prabowo memakai busana pranakan Jawa. Pakaian yang dipakai Prabowo, kata Pujodipuro, sesuai dengan aturan yang ditetapkan Keraton. Baca juga: Prabowo akan Ziarah ke Makam Raja Mataram dan TMP Kusumanegara Yogya\"Ada pengageng (yang mendampingi Prabowo), tetapi saya kurang jelas (siapa orangnya). Saya sendiri dari Keraton Surakarta, tadi (abdi dalem) yang mengikuti yakni pengageng dari Keraton Ngayogyakarta,\" tuturnya.Tak sempat mengobrol langsung, Pujodipuro mengatakan Prabowo sempat meninggalkan pesan untuk para abdi dalem di sana. \"Intinya beliau (Prabowo) meminta supaya nguri-nguri (melestarikan) kebudayaan, dijaga, dirawat, dilestarikan. Supaya tetap langgeng peninggalan sejarah yang ada di Mataram,\" ungkapnya. Prabowo menyapa warga Kotagede, Yogyakarta. Foto: Usman HadiDi Kompleks Masjid Gede Mataram di Kotagede ini menjadi komplek pemakaman Raja-raja Mataram generasi awal. Seperti makam Panembahan Senopati dan makam Ki Ageng Pamenahan.Baca juga: Ketika Prabowo Pakai Pranakan Mataram dan Salami Warga Kotagede Yogya\"Intinya itu merupakan makam keluarga Raja-raja Mataram yang sepuh, Mataram Islam yang pertama,\" paparnya.\"Jadi begini, di sini ada beberapa raja. Itu ada putra dari Panembahan Senopati, kemudian (makam raja) yang paling muda itu ada HB II kemudian Pakualam I sampai Pakualam IV juga ada di sini,\" jelas Pujodipuro.(sip/sip)yogyakartaprabowoprabowo subianto<!--// <![CDATA[    OA_show('newstag');     // ]]> -->polong.create({target: 'thepolong',id: 55})"
                                },
                                {
                                    "location": "Yogyakarta",
                                    "title": "Foto-foto Upacara Hari Pahlawan Prabowo-Amien Rais di Yogyakarta",
                                    "content": "Yogyakarta detikNews - Prabowo Subianto mengikuti upacara di Budi Mulia Dua, Sleman, Yogyakarta, sekolah milik Amien Rais. Para siswa itu mengikuti upacara tersebut."
                                },
                                {
                                    "location": "Jakarta",
                                    "title": "Deretan Tokoh yang Ditemui AHY: Jokowi hingga Prabowo",
                                    "content": "Jakarta detikNews - Agus Harimurti Yudhoyono (AHY) bergerilya menemui sejumlah tokoh. Sebut saja Presiden Jokowi hingga Ketum Gerindra Prabowo Subianto."
                                },
                                {
                                    "location": "Jakarta",
                                    "title": "Potret Gerakan Revolusi Putih yang Digagas Prabowo",
                                    "content": "Jakarta detikNews - Prabowo Subianto mengusulkan gerakan Revolusi Putih ke Gubernur DKI Anies Baswedan. Gerakan serupa sudah dijalankan Gerindra di sejumlah daerah. "
                                },
                                {
                                    "location": "Jakarta",
                                    "title": "Jokowi Hingga Prabowo Beri Selamat ke Anies-Sandi",
                                    "content": "Jakarta detikNews - Anies-Sandi resmi menjadi gubernur dan wakil gubernur DKI Jakarta. Presiden Jokowi hingga Prabowo Subianto memberikan selamat kepada gubernur baru DKI itu."
                                },
                                {
                                    "location": "Jakarta",
                                    "title": "Momen Prabowo Hormat ke Jokowi Usai Pelantikan Anies-Sandi",
                                    "content": "Jakarta detikNews - Presiden Jokowi dan Prabowo Subianto melakukan pertemuan usai pelantikan Anies-Sandi. Dalam pertemuan itu Prabowo sempat memberi hormat kepada Jokowi."
                                },
                                {
                                    "location": "Jakarta",
                                    "title": "Saatnya Prabowo 'Turun Gunung'",
                                    "content": "Jakarta - Ketua Umum Gerindra Prabowo Subianto mengunjungi Yogyakarta dan bertemu beberapa tokoh hari ini, salah satunya Ketua Dewan Kehormatan PAN Amien Rais. Kunjungan Prabowo itu disebut sebagai awal dari pergerakan politik menuju Pilpres 2019. Benarkah demikian? Wasekjen Gerindra Andre Rosiade menyebut memang sudah saatnya Prabowo 'turun gunung'. Kunjungan Prabowo ke Yogyakarta, kata Andre, bisa disebut sebagai awal dari segalanya. \"Ya, intinya Pak Prabowo akan silaturahmi ke berbagai tokoh dan ke berbagai daerah. Sudah saatnyalah. Kalau saya sebagai kader menganggap sudah saatnya Pak Prabowo untuk turun gunung, gitulah,\" ujar Andre saat dihubungi, Senin (13/11/2017).<!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->Cek Video 20Detik: Prabowo Subianto Turun Gunung[Gambas:Video 20detik]Baca juga: Foto-foto Upacara Hari Pahlawan Prabowo-Amien Rais di YogyakartaAndre menyebut Prabowo, sebagai pucuk pimpinan partai, sudah seharusnya melakukan konsolidasi politik. Dia membandingkan gerak politik Prabowo dengan Agus Harimurti Yudhoyono, sang kontestan dalam Pilgub DKI 2017. \"AHY aja, AHY aja hampir setiap minggu ke daerah kan. Iya, itu wajarlah. Setiap politisi, ya beliau kan Ketum Partai Gerindra, tentu harus mulai rajin ke daerah untuk melakukan konsolidasi,\" jelas Andre. Baca juga: Ketika Prabowo Pakai Pranakan Mataram dan Salami Warga Kotagede YogyaAndre menyebut Prabowo sedang mengembalikan ingatan para pemilihnya dengan terjun ke daerah-daerah. Menurut Andre, Prabowo merupakan sosok terkuat selain Jokowi untuk menjadi RI-1.\"Lalu yang kedua, mengembalikan memori pemilih beliau. Kan Pak Prabowo ini kan hanya kalah tipis ya dengan Pak Jokowi. Apalagi beliau sekarang kan harus mulai menyerap nih aspirasi rakyat,\" ungkap Andre. Baca juga: Prabowo Ngetweet Lagi, Ini Kata GerindraMenurut Andre, negara ini sedang dilanda isu ekonomi yang melemah. Prabowo, kata Andre, dapat memanfaatkan momen ini. \"Kenapa sih janji-janji kampanye Pak Jokowi tidak berhasil. Itu kan Pak Prabowo harus turun ke lapangan menyerap aspirasi masyarakat dan silaturahmi ke berbagai tokoh,\" sebut Andre. \"Saya rasa, sebagai kader Gerindra, saya sangat men-support ya, agar Pak Prabowo turun ke lapangan kembali, turun gunung, untuk mengingatkan pemilihnya dan juga bersilaturahmi dengan berbagai tokoh dan elemen masyarakat,\" tutur dia.Tak hanya bergerak di Yogyakarta, Prabowo juga kembali aktif di Twitter. Lebih dari 5 bulan tak berkicau, hari ini Prabowo kembali ngetweet, diawali dengan salat subuh berjemaah yang dilakukannya di Masjid Kristal Khadija, Yogyakarta tadi. (gbr/tor)prabowo subianto<!--// <![CDATA[    OA_show('newstag');     // ]]> -->polong.create({target: 'thepolong',id: 55})"
                                },
                                {
                                    "location": "Bantul",
                                    "title": "Usai ke Kotagede, Prabowo Ziarah Ke Makam Sultan Agung di Imogiri",
                                    "content": "Bantul - Prabowo Subianto berziarah ke makam Raja-Raja Mataram di Imogiri Bantul. Prabowo dan rombongan mengenakan pakaian pranakan Mataram sesuai aturan untuk masuk ke makam Raja-Raja Mataram.Prabowo tiba sekitar pukul 14.28 WIB, Senin (13/11/2017). Prabowo dan rombongan menyalami abdi dalem dan lainnya kemudian langsung menuju ke makam Sultan Agung Hanyokrokusumo. Prabowo hanya tersenyum tak berkata sedikitpun. Salah seorang dari rombongan berkata pada wartawan untuk tidak mewawancarai Prabowo. \"Nanti (wawancaranya). Setelah ini ya,\" kata pria tersebut. <!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->Baca juga: Ziarah di Kotagede, Prabowo Berdoa di Makam Panembahan SenopatiUntuk bisa ke makam Raja memang diwajibkan memakai baju adat.Prabowo menyalami abdi dalem di kompleks makam Raja Mataram di Imogiri, Bantul. Foto: Edzan RaharjoPrabowo masuk ke komplek makam dari pintu timur arah Mangunan, Bantul. Dia bersama rombongan berjalan kaki dari area parkir mobil menuju pintu gerbang makam sejauh sekitar 150 meter. Sekretaris DPD Partai Gerindra DIY, Darma Setyawan mengatakan ziarah ini merupakan kegiatan memperingati hari pahlawan. Ziarah ke makam Raja sebelumnya sudah pernah dilakukan. Apalagi Prabowo juga keturunan trah Sultan HB II.Baca juga: Prabowo Ziarah ke Imogiri, Gerindra: Beliau Trah Sultan HB IISaat ditanya wartawan apakah agenda hari ini berkaitan dengan persiapan Pilpres 2019, Dharma menjawab diplomatis. \"Untuk 2019 itu masih 2 tahun lagi. Ziarah dulu, mudah-mudahan diberikan kebaikan,\" kata Darma Setiawan yang juga anggota DPRD DIY ini.(sip/sip)bantulprabowoprabowo subianto<!--// <![CDATA[    OA_show('newstag');     // ]]> -->polong.create({target: 'thepolong',id: 55})"
                                },
                                {
                                    "location": "Jakarta",
                                    "title": "Soal Peluang Duet dengan Taufik Kurniawan, Ini Kata Ferry",
                                    "content": "Jakarta - Pilgub Jawa Tengah 2018 menjadi salah satu hal yang dibahas dalam pertemuan tertutup antara Ketum Gerindra Prabowo Subianto dan Ketua Majelis Kehormatan PAN Amien Rais di Yogyakarta. Muncul potensi duet Waketum Gerindra Ferry Juliantono dengan Waketum PAN Taufik Kurniawan.Di sela pertemuan Prabowo dengan Amien, Ferry sempat berbicara empat mata dengan Taufik soal Pilgub Jateng. Namun Prabowo dan Amien pun sempat membahas Pilgub Jateng, yang akan digelar pada 2018.\"Pak Amien ngomong ke Pak Prabowo bahwa beliau mendukung (saya), ada pembicaraan soal itu,\" ujar Ferry dalam perbincangan dengan detikcom, Senin (13/11/2017).<!--// <![CDATA[OA_show('parallaxindetail'); // ]]> -->Baca juga: Di Sela Pertemuan Prabowo-Amien, Taufik-Ferry Bicara soal JatengFerry memang masih menantikan tiket dari Partai Gerindra untuk maju dalam Pilgub Jateng. Gerindra belum memutuskan apakah akan mengusung Ferry atau Sudirman Said.Menurut Ferry, Prabowo menyambut positif dukungan Amien untuk dirinya. Prabowo meminta Gerindra dan PAN bisa bekerja sama untuk bisa menang di Jawa Tengah.\"Pak Amien menyampaikan mendukung dan Pak Prabowo jawab kita harus menang di Jawa Tengah. Insyaallah beliau berdua mendukung,\" jelas Ferry.Baca juga: Foto-foto Upacara Hari Pahlawan Prabowo-Amien Rais di Yogyakarta\"Beliau berdua banyak memberikan nasihat, intinya beliau mengatakan kita harus menang karena Jateng penting. Jadi harus siap,\" imbuhnya.Meski begitu, Ferry tidak membocorkan isi pembicaraan empat matanya dengan Taufik. Dia juga belum mau menjawab lebih lanjut mengenai peluang duet dengan Taufik di Pilgub Jateng.\"Kalau soal pasangan, saya serahkan ke partai,\" ujar Ferry saat ditanya soal kans berpasangan dengan Waketum PAN Taufik Kurniawan dalam Pilgub Jateng 2018.Ferry Juliantono dan Amien Rais. (Dok. Istimewa).Sebelumnya diberitakan, Prabowo dan Amien Rais menggelar pertemuan tertutup dengan sejumlah ulama seusai peringatan Hari Pahlawan di Yogyakarta. Dalam pertemuan itu, dibahas soal pilkada hingga pilpres.Taufik Kurniawan, yang ikut dalam pertemuan itu, mengakui dirinya berbicara empat mata dengan Ferry. Keduanya membahas Pilgub Jateng.\"Artinya, secara umum, kita kan sama-sama Waketum, Mas Ferry dan saya orang Jawa, bicara heart to heart untuk kebesaran Jawa Tengah. Akan bagaimana nanti, tunggu tanggal mainnya,\" sebut Taufik, Senin (13/11).(elz/tor)pilkada serentak 2018jawa tengahferry juliantonotaufik kurniawangerindrapan<!--// <![CDATA[    OA_show('newstag');     // ]]> -->polong.create({target: 'thepolong',id: 55})"
                                }
                            ],
                            "sumber": "detik",
                            "file_name": "_2017_11_14__19_04_47.545045_detik.json",
                            "date_created": "2017_11_14__19_04_47.545045"
                        }
            # call tokenizer
            detik_tokenizer = NewsTokenizer("1")
            # sentiment_engine = NaiveBayes(80,20).sentiment_test_detik()
            # split news into sentences
            result = detik_tokenizer.clean_detik_2(file=new_detik)

            # result_SA = []
            for sentences in result['list_berita']:
                # sen = {}
                # berita = []
                RND = ResultNewsDetik.objects.create(
                    history = search_history,
                    link = sentences['title'],

                ).save()
                for sentence in sentences['result_content']:
                    # berita.append({
                    #     'sentence' : sentence['splitSentence'],
                    #     'opinion' : "opinion"
                    # })
                    # sentencedetik = \
                    ResultDetik.objects.create(
                        opinion= "opinion",
                        sentence = sentence['resultSentence'],
                        result_news_detik = RND
                    ).save()
                    # RelationNewsDetik.objects.create(
                    #     news_detik= RND,
                    #     sentence_detik = sentencedetik
                    # ).save()
                    # split berita into sentence
                # sen['berita'] = berita
                # sen['title'] = sentences['title']

                # result_SA.append(sen)
            # sentiment analysis sentences
            # save into db
            # return response
            return Response(status.HTTP_200_OK)
        else:
            return Response(status.HTTP_204_NO_CONTENT)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def get_result_sentence_detik(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        search_history = SearchHistory.objects.filter(id = int(request.data['id_search_history'])).first()

        # compare user with search history
        if(user_token.id == search_history.project.user.id):
            result_search = {}
            result_rnd = []
            total_sentiment_positive = 0
            total_sentiment_negative = 0
            total_sentiment_neutral = 0

            # ambil dari resultnewsdetik yang ada search history nya
            result_raw_rnd = ResultNewsDetik.objects.filter(history=search_history).all()

            for raw_rnd in result_raw_rnd:
                result_detik = []
                result_sentiment_positive = 0
                result_sentiment_neutral = 0
                result_sentiment_negative = 0

                # ambil dari resultnews yang ada resultnewsdetik.id nya
                result_raw_detik = ResultDetik.objects.filter(result_news_detik=raw_rnd.id).all()

                for raw_detik in result_raw_detik:
                    result_detik.append(
                        {
                        "opinion" : raw_detik.opinion,
                        "sentence" : raw_detik.sentence
                        }
                    )
                    # count sentiment per sentence
                    if (raw_detik.opinion == "positive"):
                        result_sentiment_positive+=1
                        print(result_sentiment_positive)
                    if (raw_detik.opinion == "neutral"):
                        result_sentiment_neutral+= 1
                    if (raw_detik.opinion == "negative"):
                        result_sentiment_negative += 1

                result_rnd.append(
                    {
                        "news_title": raw_rnd.link,
                        "content" : result_detik,
                        "sentiment" : {
                            "positive" : result_sentiment_positive,
                            "neutral": result_sentiment_neutral,
                            "negative": result_sentiment_negative
                        }
                    }
                )

                # count sentiment per news
                total_sentiment_positive += result_sentiment_positive
                total_sentiment_neutral += result_sentiment_neutral
                total_sentiment_negative += result_sentiment_negative

            total_sentiment = total_sentiment_negative + total_sentiment_positive + total_sentiment_neutral

            result_sentiment = {
                "positive" : total_sentiment_positive,
                "neutral" : total_sentiment_neutral,
                "negative" : total_sentiment_negative,
                "total" : total_sentiment
            }

            result_percentage_sentiment = {
                "positive": round(((total_sentiment_positive/total_sentiment)*100),2),
                "neutral": round(((total_sentiment_neutral/total_sentiment)*100),2),
                "negative": round(((total_sentiment_negative/total_sentiment)*100),2)
            }

            result_search["detik"] = result_rnd
            result_search["sentiment"] = result_sentiment
            result_search["percentage"] = result_percentage_sentiment


            return Response(result_search)
        else:
            return Response(status.HTTP_204_NO_CONTENT)
    else:
        return Response(status.HTTP_204_NO_CONTENT)



@api_view(['POST'])
def get_news_detik_redis(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        search_history = SearchHistory.objects.filter(id = int(request.data['id_search_history'])).first()

        information = {}
        information['UUID'] = None
        information['Status'] = "Gagal"
        # compare user with search history
        if(user_token.id == search_history.project.user.id):
            # get data news
            try:
                x = tasks.scrapdetik.apply_async((10,search_history.id), countdown=20)
                information['UUID'] = x.id
                information['Status'] = "Started after 20 seconds"
            except:
                return Response(information)
            return Response(information)
        else:
            return Response(status.HTTP_204_NO_CONTENT)
    else:
        return Response(status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def get_status_detik_redis(request, task_id):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        information = {}

        res = AsyncResult(task_id)
        res.state
        information['UUID'] = res.id
        information['Status'] = res.state
        return Response(information)
    else:
        return Response(status.HTTP_204_NO_CONTENT)