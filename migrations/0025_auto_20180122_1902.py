# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-01-22 19:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0024_resultothernews'),
    ]

    operations = [
        migrations.AddField(
            model_name='resulttwitter',
            name='lat',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='resulttwitter',
            name='lng',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='resulttwitter',
            name='location',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
        migrations.AddField(
            model_name='resulttwitter',
            name='marker_text',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
