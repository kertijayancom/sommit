import json
import urllib.request
import requests

class InstagramAPI(object):
    username = "sundanukuring"
    pwd      = "ciamis01"
    media_id = "1477006830906870775_19343908"
    client_id = "84af0b052c0549de91cc52aa610894f1"
    client_secret = " 7835e48cc13c404b9119594c69338b57"
    redirect_id = "https://ig84af0b052c0549de91cc52aa610894f1"

    def __init__(self,username):
        self.username = username


    #get all post
    def get_post(self):

        url = "https://www.instagram.com/"+self.username+"?__a=1"
        response_json = {}
        response_json["name"] = "instagram api post and comment"
        list_post = []
        response = urllib.request.urlopen(url)
        with response as link:
            data = json.loads(link.read().decode())
            count = 0
            username = data['user']['username']
            for post in data['user']['media']['nodes']:
                print("get post")
                # retrieve all possible post link
                count+=1
                list_post.append({
                    "caption" : post['caption'],
                    "comment" : self.get_comment_of_post(post['code']),
                    "post_id" : username+post['code']
                })
        response_json["post"] = list_post
        return(response_json)


    #get all comment
    def get_comment_of_post(self, code_post):
        url = "https://www.instagram.com/p/"+code_post+"/?__a=1"
        response = urllib.request.urlopen(url)
        list_comment = []
        with response as link:
            data = json.loads(link.read().decode())
            total = data['graphql']['shortcode_media']['edge_media_to_comment']['count']
            for item in data['graphql']['shortcode_media']['edge_media_to_comment']['edges']:
                print("get comment")
                list_comment.append({
                    "message" : item['node']['text'],
                    "message_id" : item['node']['id']
                })
        return(list_comment)

