from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
from .news import SearchHistory
import datetime

class Splitfacebookpost(models.Model):
    name = models.CharField(max_length=300)
    file = models.CharField(max_length=300)
    message_id = models.CharField(max_length=300)
    result_message = models.TextField()
    old_message = models.TextField()
    split_message = JSONField()


class Splitfacebookreply(models.Model):
    name = models.CharField(max_length=300)
    file = models.CharField(max_length=300)
    reply_id = models.CharField(max_length=300)
    result_message = models.TextField()
    old_message = models.TextField()
    split_message = JSONField()


class Splittwitterpost(models.Model):
    name = models.CharField(max_length=300)
    file = models.CharField(max_length=300)
    tweet_id = models.CharField(max_length=300)
    result_message = models.TextField()
    old_message = models.TextField()
    split_message = JSONField()


class Splitinstagrampost(models.Model):
    name = models.CharField(max_length=300)
    file = models.CharField(max_length=300)
    message_id = models.CharField(max_length=300)
    result_message = models.TextField()
    old_message = models.TextField()
    split_message = JSONField()


class Sentimentmodel(models.Model):
    media_type = models.CharField(max_length=100)
    num_of_data = models.IntegerField()
    path_model = models.CharField(max_length=300)

class ResultTwitter(models.Model):
    date_created = models.DateTimeField(
        default= datetime.datetime.now,
        null=True,
        blank=True
    )
    opinion = models.CharField(max_length=50)
    sentence = models.TextField()
    history = models.ForeignKey(
        SearchHistory,
        null=True,
        blank=True
    )
    city = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    tweet_created = models.DateTimeField(
        default= datetime.datetime.now,
        null=True,
        blank=True
    )
    retweet_count = models.IntegerField(
        default=0,
        null=True,
        blank=True
    )
    favourite_count = models.IntegerField(
        default=0,
        null=True,
        blank=True
    )
    user_name = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    user_screen_name = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    user_id = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    profile_image_url = models.CharField(
        max_length=300,
        null=True,
        blank=True
    )
    id_str = models.CharField(
        max_length=300,
        null=True,
        blank=True
    )
    is_edited = models.BooleanField(
        default=False,
        blank=True
    )
    location = models.CharField(
        max_length=300,
        null=True,
        blank=True
    )
    marker_text = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    lng = models.FloatField(
        null=True,
        blank=True
    )
    lat = models.FloatField(
        null=True,
        blank=True
    )


class TopMentionTweet(models.Model):
    date_created = models.DateTimeField(
        default= datetime.datetime.now,
        null=True,
        blank=True
    )
    resulttweet = models.ForeignKey(
        ResultTwitter,
        null=True,
        blank=True
    )
    retweet_count = models.IntegerField(
        default=0,
        null=True,
        blank=True
    )
    favourite_count = models.IntegerField(
        default=0,
        null=True,
        blank=True
    )
    user_screen_name = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    history = models.ForeignKey(
        SearchHistory,
        null=True,
        blank=True
    )


class TopInfluencerTweet(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    history = models.ForeignKey(
        SearchHistory,
        null=True,
        blank=True
    )
    user_screen_name = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    user_twitter_link = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    user_photo_link = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    total_post = models.IntegerField(
        default=0,
        null=True
    )
    is_verified = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )

class TopContributorTweet(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    history = models.ForeignKey(
        SearchHistory,
        null=True,
        blank=True
    )
    user_screen_name = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    user_twitter_link = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    user_photo_link = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    total_post = models.IntegerField(
        default=0,
        null=True
    )
    is_verified = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    num_of_follower = models.IntegerField(
        default=0,
        null=True
    )
    num_of_posts = models.IntegerField(
        default=0,
        null=True
    )
    num_of_friend = models.IntegerField(
        default=0,
        null=True
    )
    num_of_favorites = models.IntegerField(
        default=0,
        null=True
    )

class CobaResultInstagram(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    title= models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    snippet = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    url = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    img = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    sesi = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )

class CobaResultFacebook(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    title= models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    snippet = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    url = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    img = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    sesi = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    tipe = models.CharField(
        max_length=500,
        default="post",
        null=True,
        blank=True
    )


class CobaResultTwitter(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    title= models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    snippet = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    url = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    img = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    sesi = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )

class ResultInstagramPost(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    content = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    valid = models.BooleanField(
        default=False,
        blank=True
    )
    link = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    opinion = models.CharField(
        default="neutral",
        max_length=50
    )

    #GraphImage, GraphVideo, GraphSidecar,
    type = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    like_count = models.IntegerField(
        default=0,
        blank=True
    )
    comment_count = models.IntegerField(
        default=0,
        blank=True
    )
    post_created_at = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    location_id = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    location_name = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    location_slug = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    location_has_public_page = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    display_url = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )

    user_text_name = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    username_account = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    profile_pict_account = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    id_account = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    is_verified_account = models.BooleanField(
        default=False,
        blank=True
    )
