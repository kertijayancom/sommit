import requests
import datetime

# buat kelas untuk menangani link dari instagram CSE kemudian hasilnya masuk ke DB
# result socialmedia instagram

class InstagramAnalysis(object):



    def __init__(self,link_instagram):
        x = link_instagram.split("/")
        new_link = "/".join(x[0:5])

        self.link_instagram = new_link+"?__a=1"

        # cek validasi link
        # pertama pastikan menghasilkan 200 respond ketika diambil dengan request
        req = requests.get(url=self.link_instagram)

        if(req.status_code is 200):
            self.is_valid = True
        else:
            self.is_valid = False

    def validation(self):
        return(self.is_valid)

    # harusnya deliver content berdasarkan jenis dari tipe post nya, karena ada yang memiliki yang berbeda

    def get_content(self):
        result = {
            'content' : None,
            'valid' : False,

            'nama' : None,
            'username_account' : None,
            'profile_pict_account' : None,
            'id_account' : None,
            'is_verifiec_account' : None,
            'user_follower_count' : None,
            'user_following_count': None,
            'user_post_count': None,

            'link' :None,
            'type' : None, #GraphImage, GraphVideo, GraphSidecar,
            'like_count' : None,
            'comment_count' : None,
            'post_created_at': None,
            'location_id' : None,
            'location_name': None,
            'location_slug': None,
            'location_has_public_page' : None,
            'display_url' : None,

        }

        if(self.validation()):
            req = requests.get(url=self.link_instagram)
            res = req.json()

            # get content
            try:
                result['content'] = res['graphql']['shortcode_media']['edge_media_to_caption']['edges'][0]['node']['text']
            except:
                result['content'] = None

            # get like count
            try:
                result['like_count'] = res['graphql']['shortcode_media']['edge_media_preview_like']['count']
            except:
                result['like_count'] = None

            # get comment count
            try:
                result['comment_count'] = res['graphql']['shortcode_media']['edge_media_to_comment']['count']
            except:
                result['comment_count'] = None

            # get nama author
            try:
                result['nama'] = res['graphql']['shortcode_media']['owner']['full_name']
            except:
                result['nama'] = None

            # get username author
            try:
                result['username_account'] = res['graphql']['shortcode_media']['owner']['username']
            except:
                result['username_account'] = None

            # get profile_pict author
            try:
                result['profile_pict_account'] = res['graphql']['shortcode_media']['owner']['profile_pic_url']
            except:
                result['profile_pict_account'] = None

            # get id author
            try:
                result['id_account'] = res['graphql']['shortcode_media']['owner']['id']
            except:
                result['id_account'] = None

            # get link post
                result['link'] = self.link_instagram

            # get type post
            try:
                result['type'] = res['graphql']['shortcode_media']['__typename']
            except:
                result['type'] = None

            # get post_created_at
            try:
                x = res['graphql']['shortcode_media']['taken_at_timestamp']
                result['post_created_at'] = datetime.datetime.fromtimestamp(int(x)).strftime('%Y-%m-%d %H:%M:%S')
            except:
                result['post_created_at'] = None

            # get location information
            try:
                result['location_name'] = res['graphql']['shortcode_media']['location']['name']
            except:
                result['location_name'] = None

            try:
                result['location_id'] = res['graphql']['shortcode_media']['location']['id']
            except:
                result['location_id'] = None

            try:
                result['location_slug'] = res['graphql']['shortcode_media']['location']['slug']
            except:
                result['location_slug'] = None

            try:
                result['location_has_public_page'] = res['graphql']['shortcode_media']['location']['has_public_page']
            except:
                result['location_has_public_page'] = None

            # get hastag (if possible)
            try:
                result['display_url'] = res['graphql']['shortcode_media']['display_url']
            except:
                result['display_url'] = None

            # check verified account
            try:
                req1 = requests.get(url="https://www.instagram.com/"+result['username_account']+"/?__a=1")
                res1 = req1.json()
                if(req1.status_code is 200):
                    # set verified account
                    result['is_verifiec_account'] = res1['user']['is_verified']

                    # get user follower count
                    result['user_follower_count'] = res1['user']['followed_by']['count']

                    # get user following count
                    result['user_following_count'] = res1['user']['follows']['count']

                    # get user post count
                    result['user_post_count'] = res1['user']['media']['count']
            except:
                result['is_verifiec_account'] = None
            result['valid'] = True

        return result




# # jenis graphimage
# x1= "https://www.instagram.com/p/BezQULLDDrH/?__a=1"
#
# # jenis graphsidecar
# x2 = "https://www.instagram.com/p/BfTiBCID-uQ/?taken-by=aniesbaswedan&__a=1"
#
# # jenis graphvideo
# x3 = "https://www.instagram.com/p/Be0Gwwdlpoc/"
#
# x = InstagramAnalysis(link_instagram=x1)
# print(x.validation())
# print(x.get_content())
#
# x = InstagramAnalysis(link_instagram=x2)
# print(x.validation())
# print(x.get_content())
#
# x = InstagramAnalysis(link_instagram=x3)
# print(x.validation())
# print(x.get_content())