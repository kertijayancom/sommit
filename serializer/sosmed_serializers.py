from rest_framework import serializers
from  .._models.socialmedia import ResultTwitter

class TwiterSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""
    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = ResultTwitter
        fields = ('id','opinion','sentence','date_created','history')
        read_only_fields = ('id','date_created')