import json
import re
import nltk
import urllib.request
from nltk.tokenize import TweetTokenizer
# from nltk.corpus import stopwords

class SosmedTokenizer(object):

    # stopword dari talla 2003 : https://github.com/masdevid/ID-Stopwords
    stop_word_id = [")", "(", '', "rt", '“', ":", ",", "”", "-", ".", "!", "$", "%", "^", "&", "_", "=", "+", '[',
                              ']', '`', '~', '?', '>', '<', 'ada', 'adalah', 'adanya', 'adapun', 'agak', 'agaknya', 'agar',
                              'akan', 'akankah', 'akhir', 'akhiri', 'akhirnya', 'aku', 'akulah', 'amat', 'amatlah', 'anda',
                              'andalah', 'antar', 'antara', 'antaranya', 'apa', 'apaan', 'apabila', 'apakah', 'apalagi',
                              'apatah', 'artinya', 'asal', 'asalkan', 'atas', 'atau', 'ataukah', 'ataupun', 'awal',
                              'awalnya', 'bagai', 'bagaikan', 'bagaimana', 'bagaimanakah', 'bagaimanapun', 'bagi', 'bagian',
                              'bahkan', 'bahwa', 'bahwasanya', 'baik', 'bakal', 'bakalan', 'balik', 'banyak', 'bapak',
                              'baru', 'bawah', 'beberapa', 'begini', 'beginian', 'beginikah', 'beginilah', 'begitu',
                              'begitukah', 'begitulah', 'begitupun', 'bekerja', 'belakang', 'belakangan', 'belum',
                              'belumlah', 'benar', 'benarkah', 'benarlah', 'berada', 'berakhir', 'berakhirlah',
                              'berakhirnya', 'berapa', 'berapakah', 'berapalah', 'berapapun', 'berarti', 'berawal',
                              'berbagai', 'berdatangan', 'beri', 'berikan', 'berikut', 'berikutnya', 'berjumlah',
                              'berkali-kali', 'berkata', 'berkehendak', 'berkeinginan', 'berkenaan', 'berlainan', 'berlalu',
                              'berlangsung', 'berlebihan', 'bermacam', 'bermacam-macam', 'bermaksud', 'bermula', 'bersama',
                              'bersama-sama', 'bersiap', 'bersiap-siap', 'bertanya', 'bertanya-tanya', 'berturut',
                              'berturut-turut', 'bertutur', 'berujar', 'berupa', 'besar', 'betul', 'betulkah', 'biasa',
                              'biasanya', 'bila', 'bilakah', 'bisa', 'bisakah', 'boleh', 'bolehkah', 'bolehlah', 'buat',
                              'bukan', 'bukankah', 'bukanlah', 'bukannya', 'bulan', 'bung', 'cara', 'caranya', 'cukup',
                              'cukupkah', 'cukuplah', 'cuma', 'dahulu', 'dalam', 'dan', 'dapat', 'dari', 'daripada',
                              'datang', 'dekat', 'demi', 'demikian', 'demikianlah', 'dengan', 'depan', 'di', 'dia',
                              'diakhiri', 'diakhirinya', 'dialah', 'diantara', 'diantaranya', 'diberi', 'diberikan',
                              'diberikannya', 'dibuat', 'dibuatnya', 'didapat', 'didatangkan', 'digunakan', 'diibaratkan',
                              'diibaratkannya', 'diingat', 'diingatkan', 'diinginkan', 'dijawab', 'dijelaskan',
                              'dijelaskannya', 'dikarenakan', 'dikatakan', 'dikatakannya', 'dikerjakan', 'diketahui',
                              'diketahuinya', 'dikira', 'dilakukan', 'dilalui', 'dilihat', 'dimaksud', 'dimaksudkan',
                              'dimaksudkannya', 'dimaksudnya', 'diminta', 'dimintai', 'dimisalkan', 'dimulai', 'dimulailah',
                              'dimulainya', 'dimungkinkan', 'dini', 'dipastikan', 'diperbuat', 'diperbuatnya',
                              'dipergunakan', 'diperkirakan', 'diperlihatkan', 'diperlukan', 'diperlukannya',
                              'dipersoalkan',
                              'dipertanyakan', 'dipunyai', 'diri', 'dirinya', 'disampaikan', 'disebut', 'disebutkan',
                              'disebutkannya', 'disini', 'disinilah', 'ditambahkan', 'ditandaskan', 'ditanya', 'ditanyai',
                              'ditanyakan', 'ditegaskan', 'ditujukan', 'ditunjuk', 'ditunjuki', 'ditunjukkan',
                              'ditunjukkannya', 'ditunjuknya', 'dituturkan', 'dituturkannya', 'diucapkan', 'diucapkannya',
                              'diungkapkan', 'dong', 'dua', 'dulu', 'empat', 'enggak', 'enggaknya', 'entah', 'entahlah',
                              'guna', 'gunakan', 'hal', 'hampir', 'hanya', 'hanyalah', 'hari', 'harus', 'haruslah',
                              'harusnya', 'hendak', 'hendaklah', 'hendaknya', 'hingga', 'ia', 'ialah', 'ibarat',
                              'ibaratkan',
                              'ibaratnya', 'ibu', 'ikut', 'ingat', 'ingat-ingat', 'ingin', 'inginkah', 'inginkan', 'ini',
                              'inikah', 'inilah', 'itu', 'itukah', 'itulah', 'jadi', 'jadilah', 'jadinya', 'jangan',
                              'jangankan', 'janganlah', 'jauh', 'jawab', 'jawaban', 'jawabnya', 'jelas', 'jelaskan',
                              'jelaslah', 'jelasnya', 'jika', 'jikalau', 'juga', 'jumlah', 'jumlahnya', 'justru', 'kala',
                              'kalau', 'kalaulah', 'kalaupun', 'kalian', 'kami', 'kamilah', 'kamu', 'kamulah', 'kan',
                              'kapan', 'kapankah', 'kapanpun', 'karena', 'karenanya', 'kasus', 'kata', 'katakan',
                              'katakanlah', 'katanya', 'ke', 'keadaan', 'kebetulan', 'kecil', 'kedua', 'keduanya',
                              'keinginan', 'kelamaan', 'kelihatan', 'kelihatannya', 'kelima', 'keluar', 'kembali',
                              'kemudian', 'kemungkinan', 'kemungkinannya', 'kenapa', 'kepada', 'kepadanya', 'kesampaian',
                              'keseluruhan', 'keseluruhannya', 'keterlaluan', 'ketika', 'khususnya', 'kini', 'kinilah',
                              'kira', 'kira-kira', 'kiranya', 'kita', 'kitalah', 'kok', 'kurang', 'lagi', 'lagian', 'lah',
                              'lain', 'lainnya', 'lalu', 'lama', 'lamanya', 'lanjut', 'lanjutnya', 'lebih', 'lewat', 'lima',
                              'luar', 'macam', 'maka', 'makanya', 'makin', 'malah', 'malahan', 'mampu', 'mampukah', 'mana',
                              'manakala', 'manalagi', 'masa', 'masalah', 'masalahnya', 'masih', 'masihkah', 'masing',
                              'masing-masing', 'mau', 'maupun', 'melainkan', 'melakukan', 'melalui', 'melihat',
                              'melihatnya',
                              'memang', 'memastikan', 'memberi', 'memberikan', 'membuat', 'memerlukan', 'memihak',
                              'meminta',
                              'memintakan', 'memisalkan', 'memperbuat', 'mempergunakan', 'memperkirakan', 'memperlihatkan',
                              'mempersiapkan', 'mempersoalkan', 'mempertanyakan', 'mempunyai', 'memulai', 'memungkinkan',
                              'menaiki', 'menambahkan', 'menandaskan', 'menanti', 'menanti-nanti', 'menantikan', 'menanya',
                              'menanyai', 'menanyakan', 'mendapat', 'mendapatkan', 'mendatang', 'mendatangi',
                              'mendatangkan',
                              'menegaskan', 'mengakhiri', 'mengapa', 'mengatakan', 'mengatakannya', 'mengenai',
                              'mengerjakan', 'mengetahui', 'menggunakan', 'menghendaki', 'mengibaratkan',
                              'mengibaratkannya',
                              'mengingat', 'mengingatkan', 'menginginkan', 'mengira', 'mengucapkan', 'mengucapkannya',
                              'mengungkapkan', 'menjadi', 'menjawab', 'menjelaskan', 'menuju', 'menunjuk', 'menunjuki',
                              'menunjukkan', 'menunjuknya', 'menurut', 'menuturkan', 'menyampaikan', 'menyangkut',
                              'menyatakan', 'menyebutkan', 'menyeluruh', 'menyiapkan', 'merasa', 'mereka', 'merekalah',
                              'merupakan', 'meski', 'meskipun', 'meyakini', 'meyakinkan', 'minta', 'mirip', 'misal',
                              'misalkan', 'misalnya', 'mula', 'mulai', 'mulailah', 'mulanya', 'mungkin', 'mungkinkah',
                              'nah',
                              'naik', 'namun', 'nanti', 'nantinya', 'nyaris', 'nyatanya', 'oleh', 'olehnya', 'pada',
                              'padahal', 'padanya', 'pak', 'paling', 'panjang', 'pantas', 'para', 'pasti', 'pastilah',
                              'penting', 'pentingnya', 'per', 'percuma', 'perlu', 'perlukah', 'perlunya', 'pernah',
                              'persoalan', 'pertama', 'pertama-tama', 'pertanyaan', 'pertanyakan', 'pihak', 'pihaknya',
                              'pukul', 'pula', 'pun', 'punya', 'rasa', 'rasanya', 'rata', 'rupanya', 'saat', 'saatnya',
                              'saja', 'sajalah', 'saling', 'sama', 'sama-sama', 'sambil', 'sampai', 'sampai-sampai',
                              'sampaikan', 'sana', 'sangat', 'sangatlah', 'satu', 'saya', 'sayalah', 'se', 'sebab',
                              'sebabnya', 'sebagai', 'sebagaimana', 'sebagainya', 'sebagian', 'sebaik', 'sebaik-baiknya',
                              'sebaiknya', 'sebaliknya', 'sebanyak', 'sebegini', 'sebegitu', 'sebelum', 'sebelumnya',
                              'sebenarnya', 'seberapa', 'sebesar', 'sebetulnya', 'sebisanya', 'sebuah', 'sebut', 'sebutlah',
                              'sebutnya', 'secara', 'secukupnya', 'sedang', 'sedangkan', 'sedemikian', 'sedikit',
                              'sedikitnya', 'seenaknya', 'segala', 'segalanya', 'segera', 'seharusnya', 'sehingga',
                              'seingat', 'sejak', 'sejauh', 'sejenak', 'sejumlah', 'sekadar', 'sekadarnya', 'sekali',
                              'sekali-kali', 'sekalian', 'sekaligus', 'sekalipun', 'sekarang', 'sekarang', 'sekecil',
                              'seketika', 'sekiranya', 'sekitar', 'sekitarnya', 'sekurang-kurangnya', 'sekurangnya', 'sela',
                              'selain', 'selaku', 'selalu', 'selama', 'selama-lamanya', 'selamanya', 'selanjutnya',
                              'seluruh', 'seluruhnya', 'semacam', 'semakin', 'semampu', 'semampunya', 'semasa', 'semasih',
                              'semata', 'semata-mata', 'semaunya', 'sementara', 'semisal', 'semisalnya', 'sempat', 'semua',
                              'semuanya', 'semula', 'sendiri', 'sendirian', 'sendirinya', 'seolah', 'seolah-olah',
                              'seorang',
                              'sepanjang', 'sepantasnya', 'sepantasnyalah', 'seperlunya', 'seperti', 'sepertinya',
                              'sepihak',
                              'sering', 'seringnya', 'serta', 'serupa', 'sesaat', 'sesama', 'sesampai', 'sesegera',
                              'sesekali', 'seseorang', 'sesuatu', 'sesuatunya', 'sesudah', 'sesudahnya', 'setelah',
                              'setempat', 'setengah', 'seterusnya', 'setiap', 'setiba', 'setibanya', 'setidak-tidaknya',
                              'setidaknya', 'setinggi', 'seusai', 'sewaktu', 'siap', 'siapa', 'siapakah', 'siapapun',
                              'sini',
                              'sinilah', 'soal', 'soalnya', 'suatu', 'sudah', 'sudahkah', 'sudahlah', 'supaya', 'tadi',
                              'tadinya', 'tahu', 'tahun', 'tak', 'tambah', 'tambahnya', 'tampak', 'tampaknya', 'tandas',
                              'tandasnya', 'tanpa', 'tanya', 'tanyakan', 'tanyanya', 'tapi', 'tegas', 'tegasnya', 'telah',
                              'tempat', 'tengah', 'tentang', 'tentu', 'tentulah', 'tentunya', 'tepat', 'terakhir', 'terasa',
                              'terbanyak', 'terdahulu', 'terdapat', 'terdiri', 'terhadap', 'terhadapnya', 'teringat',
                              'teringat-ingat', 'terjadi', 'terjadilah', 'terjadinya', 'terkira', 'terlalu', 'terlebih',
                              'terlihat', 'termasuk', 'ternyata', 'tersampaikan', 'tersebut', 'tersebutlah', 'tertentu',
                              'tertuju', 'terus', 'terutama', 'tetap', 'tetapi', 'tiap', 'tiba', 'tiba-tiba', 'tidak',
                              'tidakkah', 'tidaklah', 'tiga', 'tinggi', 'toh', 'tunjuk', 'turut', 'tutur', 'tuturnya',
                              'ucap', 'ucapnya', 'ujar', 'ujarnya', 'umum', 'umumnya', 'ungkap', 'ungkapnya', 'untuk',
                              'usah', 'usai', 'waduh', 'wah', 'wahai', 'waktu', 'waktunya', 'walau', 'walaupun', 'wong',
                              'yaitu', 'yakin', 'yakni', 'ya']

    # belum tahu mau set apa dasarnya
    def __init__(self,file):
        self.file = file


    #tokenize twitter data using TweetTokenizer NLTK
    def twitter_tokenize(self,file):
        with open(file) as data_file:
            data = json.load(data_file)
            response_json = {}
            tweets = []
            # stop_word = set(stopwords.words("english"))
            # stop_word.update(self.stop_word_id)
            stop_word = self.stop_word_id

            for tweet in data['tweets']:
                # set stopword

                # tokenize word
                tweet_tokens = TweetTokenizer(strip_handles=True, reduce_len=True)
                word2 = tweet_tokens.tokenize(tweet['text'])

                # create stopWord from template
                filtered_sentence = [w for w in word2 if not w in stop_word]
                filtered_sentence = []

                #filtered word and remove link + lowercase
                for w in word2:
                    w = re.sub(r"http\S+", "", w)
                    w = w.lower()
                    if w.lower() not in stop_word:
                        filtered_sentence.append(w.lower())

                # append every tweet into json
                tweets.append({
                    "splitMessage" : filtered_sentence,
                    "resultMessage" : " ".join(filtered_sentence),
                    "oldMessage" : tweet['text'],
                    "tweet_id" : tweet['tweet_id']
                })
            response_json["feed"] = tweets
            response_json["name"] = "twitter tokenize result"
            response_json['_file'] = file
            return(response_json)


    # example for tokenize facebook data using Native Tokenizer NLTK
    def facebook_tokenize(self, file):
        with open(file) as data_file:
            data = json.load(data_file)
            response_json = {}
            facebook_data = []

            # stop_word = set(stopwords.words("english"))
            # stop_word.update(self.stop_word_id)
            stop_word = self.stop_word_id

            # tokenize post
            for comment in data['feed']:
                # tokenizer nltk
                word2 = nltk.word_tokenize(comment['post'])

                # create stopWord from template
                filtered_sentence = [w for w in word2 if not w in stop_word]
                filtered_sentence = []

                # filtered word and remove link + lowercase
                for w in word2:
                    w = re.sub(r"http\S+", "", w)
                    w = w.lower()
                    if w.lower() not in stop_word:
                        filtered_sentence.append(w.lower())

                # create tokenize for comment of post facebook data
                replies = []

                for reply in comment['comment']:
                    word3 = nltk.word_tokenize(reply['message'])

                    # create stopWord from template
                    reply_filtered_sentence = [w for w in word3 if not w in stop_word]
                    reply_filtered_sentence = []

                    # filtered word and remove link + lowercase
                    for w in word3:
                        w = re.sub(r"http\S+", "", w)
                        w = w.lower()
                        if w.lower() not in stop_word:
                            reply_filtered_sentence.append(w.lower())

                    replies.append({
                        "splitMessage" : reply_filtered_sentence,
                        "resultMessage" : " ".join(reply_filtered_sentence),
                        "reply_id" : reply['comment_id'],
                        "oldMessage": reply['message']
                    })

                # append all data into one element of json
                facebook_data.append({
                    "splitMessage" : filtered_sentence,
                    "resultMessage" : " ".join(filtered_sentence),
                    "oldMessage" : comment['post'],
                    "message_id" : comment['post_id'],
                    "reply" : replies
                })
            response_json["feed"] = facebook_data
            response_json["name"] = "facebook tokenize result"
            response_json["_file"] = file
            return(response_json)

    def instagram_tokenize(self, file):
        with open(file) as data_file:
            data = json.load(data_file)
            response_json = {}
            instagram_data = []

            # stop_word = set(stopwords.words("english"))
            # stop_word.update(self.stop_word_id)
            stop_word = self.stop_word_id

            # tokenize post
            for posting in data['post']:
                # tokenizer nltk
                # tokenize word
                tweet_tokens = TweetTokenizer(strip_handles=True, reduce_len=True)
                word4 = tweet_tokens.tokenize(posting['caption'])
                # word4 = nltk.word_tokenize(posting['caption'])

                # create stopWord from template
                filtered_sentence = [w for w in word4 if not w in stop_word]
                filtered_sentence = []

                # filtered word and remove link + lowercase
                for w in word4:
                    w = re.sub(r"http\S+", "", w)
                    w = w.lower()
                    if w.lower() not in stop_word:
                        filtered_sentence.append(w.lower())

                # create tokenize for comment of post facebook data
                replies = []

                for reply in posting['comment']:
                    word5 = tweet_tokens.tokenize(reply['message'])
                    # word5 = nltk.word_tokenize(reply['message'])

                    # create stopWord from template
                    reply_filtered_sentence = [w for w in word5 if not w in stop_word]
                    reply_filtered_sentence = []

                    # filtered word and remove link + lowercase
                    for w in word5:
                        w = re.sub(r"http\S+", "", w)
                        w = w.lower()
                        if w.lower() not in stop_word:
                            reply_filtered_sentence.append(w.lower())

                    replies.append({
                        "splitMessage": reply_filtered_sentence,
                        "resultMessage": " ".join(reply_filtered_sentence),
                        "oldMessage": reply['message'],
                        "reply_id" : reply['message_id']
                    })

                # append all data into one element of json
                instagram_data.append({
                    "splitMessage" : filtered_sentence,
                    "resultMessage" : " ".join(filtered_sentence),
                    "oldMessage" : posting['caption'],
                    "message_id" : posting['post_id'],
                    "reply ": replies
                })
            response_json["feed"] = instagram_data
            response_json["name"] = "instagram tokenize result"
            response_json["_file"] = file
            return(response_json)

    def twitter_tokenize_sentence(self,sentence):

        # stop_word = set(stopwords.words("english"))
        # stop_word.update(self.stop_word_id)
        stop_word = self.stop_word_id

        tweet_tokens = TweetTokenizer(strip_handles=True, reduce_len=True)
        word2 = tweet_tokens.tokenize(sentence)

        # create stopWord from template
        filtered_sentence = [w for w in word2 if not w in stop_word]
        filtered_sentence = []

        #filtered word and remove link + lowercase
        for w in word2:
            w = re.sub(r"http\S+", "", w)
            w = w.lower()
            if w.lower() not in stop_word:
                filtered_sentence.append(w.lower())

        return(filtered_sentence)

    def twitter_tokenize_sentence_to_split(self,sentence):

        # stop_word = set(stopwords.words("english"))
        # stop_word.update(self.stop_word_id)
        stop_word = self.stop_word_id

        tweet_tokens = TweetTokenizer(strip_handles=True, reduce_len=True)
        word2 = tweet_tokens.tokenize(sentence)

        # create stopWord from template
        filtered_sentence = [w for w in word2 if not w in stop_word]
        filtered_sentence = []

        #filtered word and remove link + lowercase
        for w in word2:
            w = re.sub(r"http\S+", "", w)
            w = w.lower()
            if w.lower() not in stop_word:
                filtered_sentence.append(w.lower())

        return(filtered_sentence)

    def facebook_tokenize_sentence(self, sentence):

        # stop_word = set(stopwords.words("english"))
        # stop_word.update(self.stop_word_id)
        stop_word = self.stop_word_id

        word2 = nltk.word_tokenize(sentence)

        # create stopWord from template
        filtered_sentence = [w for w in word2 if not w in stop_word]
        filtered_sentence = []

        # filtered word and remove link + lowercase
        for w in word2:
            w = re.sub(r"http\S+", "", w)
            w = w.lower()
            if w.lower() not in stop_word:
                filtered_sentence.append(w.lower())

        return (filtered_sentence)

    def instagram_tokenize_sentence(self, sentence):

        # stop_word = set(stopwords.words("english"))
        # stop_word.update(self.stop_word_id)
        stop_word = self.stop_word_id

        tweet_tokens = TweetTokenizer(strip_handles=True, reduce_len=True)
        word2 = tweet_tokens.tokenize(sentence)

        # create stopWord from template
        filtered_sentence = [w for w in word2 if not w in stop_word]
        filtered_sentence = []

        # filtered word and remove link + lowercase
        for w in word2:
            w = re.sub(r"http\S+", "", w)
            w = w.lower()
            if w.lower() not in stop_word:
                filtered_sentence.append(w.lower())

        return (filtered_sentence)