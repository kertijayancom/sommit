from django.db import models
from ._models._main import *
from ._models.news import *
from ._models.socialmedia import *

class Cobamodel(models.Model):
    media_type = models.CharField(max_length=100)
    num_of_data = models.IntegerField()
    path_model = models.CharField(max_length=200)