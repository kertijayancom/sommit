from bs4 import BeautifulSoup as soup
import re,json,datetime,requests
from datetime import timedelta
import os, time
from lxml import html

def sindonews(query):
    global location
    query = query.replace(' ','+')
    url = "https://search.sindonews.com/search?type=artikel&q="+query
    req = requests.get(url)
    page = soup(req.text,"html.parser")
    link_url = []
    raw_content = []
    result_content = []
    json_result = {}

    #1. Mendapatkan url setiap berita
    for links in page.select('div.news-title'):
        for link in links('a'):
            if 'href' in link.attrs:
                x = link.attrs['href']
                link_url.append(x)
    print(len(link_url))
    #2. Mendapatkan content setiap url
    for y in link_url:
        req2 = requests.get(y)
        data = req2.text
        page = soup(data, "html.parser")
        if (str(soup(str(page.select('div.article h1')), "html.parser").get_text()).strip('[]')) is not '':
            raw_content.append(page)
    print(len(raw_content))

    # 3. Mendapatkan element di setiap content dan menghasilkan output json
    for z in raw_content:
        page = soup(str(z), "html.parser")
        if (str(soup(str(page.select('div.article h1')), "html.parser").get_text()).strip('[]')) is not None:
            title = str(soup(str(page.select('div.article h1')), "html.parser").get_text()).strip('[]')

            raw_content = str(soup(str(page.select('#content')), "html.parser").get_text()).strip('[]')
            regex = re.compile(r'[\n\r\t]')
            content = regex.sub('', raw_content)

            all_text = page.select('#content')

            select_location = soup(str(all_text), "html.parser")

            location = soup(str(select_location.find('strong')), "html.parser").get_text()

            result_content.append(
                {
                    "title": title,
                    "location": location,
                    "content": content,
                }
            )

    source = "sindonews"
    a_time = str(datetime.datetime.now())
    a_time = a_time.replace(" ", "__")
    a_time = a_time.replace(":", "_")
    a_time = a_time.replace("-", "_")

    file_name = '_' + a_time + '_' + source + ".json"
    file_name1 = 'result_cache\\' + file_name
    path_file = os.path.join(os.path.dirname(__file__), file_name1)

    json_result["feed"] = result_content
    json_result["sumber"] = source
    json_result["file_name"] = file_name

    # with open(path_file, "w") as outfile:
    #     json.dump(json_result, outfile)

    # json_parsing = tokenizer.NewsTokenizer(json_result)
    # json_parsing.clean_sindonews()