import requests

class FacebookUser(object):

    access_token = "access_token=133252573984435%7CVesAEMW2yKNYjryNQg01CMLlBCA"
    def __init__(self,id_user):
        self.id_user = str(id_user)

    # get picture
    def get_picture(self):
        urls = "https://graph.facebook.com/v2.12/"+self.id_user+"/picture?"+self.access_token
        req = requests.get(url=urls)
        return (req)

    def get_basic_info(self):
        urls = "https://graph.facebook.com/v2.12/"+self.id_user+"?self.access_token"
        req = requests.get(url=urls)
        return (req)

    # get books collection
    def get_books(self):
        urls = "https://graph.facebook.com/v2.12/"+self.id_user+"/books?"+self.access_token
        req = requests.get(url=urls)
        return (req)

    # get achievement : sepertinya prestasi, maksudnya prestasi dalam bermain facebook apps
    def get_achievement(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "/achievements?" + self.access_token
        req = requests.get(url=urls)
        return (req)

    # get friends : untuk melihat total teman
    def get_friends(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "/friends?" + self.access_token
        req = requests.get(url=urls)
        return (req)

    def get_about(self):

        return("ok")

class FacebookGroup(object):
    access_token = "access_token=133252573984435%7CVesAEMW2yKNYjryNQg01CMLlBCA"

    def __init__(self, link_post):
        self.username = link_post.split("/")[3]
        self.id_post = link_post.split("/")[6]

        # check len of link
        len_link = len(link_post.split("/"))

        # if(len_link is 6):
        #     self.username = link_post.split("/")[3]
        #     self.id_post = link_post.split("/")[6]
        #
        # elif(len_link is ):
        #     self.username = link_post.split("/")[3]
        #     self.id_post = link_post.split("/")[6]
        #
        # else:
        #     self.username = None
        #     self.id_post = None

    # def isvalid(self):


    def get_id_user(self):
        return(self.id_user)

    def get_id_group(self):
        return("ok")

    def get_username(self):
        return(self.username)

    def get_id_post(self):
        return(self.id_post)

    def get_info_group(self):
        urls = "https://graph.facebook.com/" + self.get_id_group() + "?fields=purpose%2Cname%2Cdescription%2Cicon&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        return (res)

class FacebookPhotos(object):
    access_token = "access_token=133252573984435%7CVesAEMW2yKNYjryNQg01CMLlBCA"

    def __init__(self, link_post):
        self.username = link_post.split("/")[3]
        self.id_post = (link_post.split("/")[5]).split(".")[1]
        self.id_post_gambar = link_post.split("/")[6]


        # get id_user

        # split datadiri yang benar
        url_datadiri = "https://graph.facebook.com/" + self.username + "?" + self.access_token
        req = requests.get(url=url_datadiri)
        result = req.json()
        self.id_user = str(result['id'])


    def get_id_user(self):
        return(self.id_user)

    def get_username(self):
        return(self.username)

    def get_id_post(self):
        return(self.id_post)

    def get_content(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        return (res)

    def get_count_comment(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=comments.summary(true)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "comment_count": res['comments']['summary']['total_count'],
        }
        return (result)

    def get_count_likes(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=likes.limit(1).summary(true)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "likes_count": res['likes']['summary']['total_count'],
        }
        return (result)

    def get_count_reactions(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),reactions.type(NONE).limit(0).summary(total_count).as(reactions_all),reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        return(res)

    def get_count_shares(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=shares.limit(1).summary(true)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "shares_count": res['shares']['count'],
        }
        return (result)

    def get_attachment(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=attachments&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        return (res)

class FacebookVideos(object):
    access_token = "access_token=133252573984435%7CVesAEMW2yKNYjryNQg01CMLlBCA"

    def __init__(self, link_post):
        self.username = link_post.split("/")[3]
        self.id_post = link_post.split("/")[5]

        # get id_user

        # split datadiri yang benar
        url_datadiri = "https://graph.facebook.com/" + self.username + "?" + self.access_token
        req = requests.get(url=url_datadiri)
        result = req.json()
        self.id_user = str(result['id'])

    def get_id_user(self):
        return(self.id_user)

    def get_username(self):
        return(self.username)

    def get_id_post(self):
        return(self.id_post)

    def get_content(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        return (res)

    def get_videos(self):
        return("ok")

    def get_count_comment(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=comments.summary(true)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "comment_count": res['comments']['summary']['total_count'],
        }
        return (result)

    def get_count_likes(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=likes.limit(1).summary(true)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "likes_count": res['likes']['summary']['total_count'],
        }
        return (result)

    def get_count_reactions(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),reactions.type(NONE).limit(0).summary(total_count).as(reactions_all),reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        return(res)

    def get_count_shares(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=shares.limit(1).summary(true)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "shares_count": res['shares']['count'],
        }
        return (result)

    def get_attachment(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=attachments&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        return (res)

class FacebookPost(object):
    access_token = "access_token=133252573984435%7CVesAEMW2yKNYjryNQg01CMLlBCA"

    def __init__(self,link_post):
        self.username = link_post.split("/")[3]
        self.id_post = link_post.split("/")[5]

        # get id_user
        url_datadiri = "https://graph.facebook.com/" + self.username + "?" + self.access_token
        req = requests.get(url=url_datadiri)
        result = req.json()
        self.id_user = str(result['id'])

    def get_id_post(self):
        return(self.id_post)

    def get_username_author(self):
        return(self.username)

    def get_id_username_author(self):
        return(self.id_user)


    # mengambil informasi dasar dari post
    def get_info_post(self):
        urls = "https://graph.facebook.com/v2.12/"+self.id_user+"_"+self.id_post+"?fields=created_time%2Cmessage%2Cfrom%2Cobject_id%2Cplace%2Cproperties%2Csource%2Cstatus_type&"+self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "id_post" : self.id_post,
            "created_time" : res['created_time'],
            "message" : res['message'],
            "from" : res['from'],
            "status_type" : res['status_type'],
            "id" : res['id']
        }
        return(result)

    def get_count_comment(self):
        urls = "https://graph.facebook.com/v2.12/"+self.id_user+"_"+self.id_post+"?fields=comments.summary(true)&"+self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "comment_count" : res['comments']['summary']['total_count'],
        }
        return(result)
    def get_first_comment(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=comments.summary(true)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "comment_data": res['comments']['data']
        }
        return (result)

    def get_count_likes(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=likes.limit(1).summary(true)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "likes_count": res['likes']['summary']['total_count'],
        }
        return (result)

    def get_count_shares(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=shares.limit(1).summary(true)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        result = {
            "shares_count": res['shares']['count'],
        }
        return (result)

    def get_count_reactions(self):
        urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love),reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha),reactions.type(NONE).limit(0).summary(total_count).as(reactions_all),reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry),reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful)&" + self.access_token
        req = requests.get(url=urls)
        res = req.json()
        return(res)


    def get_attachment_of_post(self):
        try:
            urls = "https://graph.facebook.com/v2.12/" + self.id_user + "_" + self.id_post + "?fields=attachments&" + self.access_token
            req = requests.get(url=urls)
            res = req.json()
            result = {
                "attachments": res['attachments']
            }
        except:
            result = {
                "attachments": None
            }
        return (result)

    def get_user_made_post(self):

        if(AuthorValidation(id_author=self.id_user) is True):

            try:
                cek_user = FacebookUser(id_user=self.id_user)
                friends = cek_user.get_friends().json()
            except:
                friends = []

            result = {
                "username" : self.username,
                "id" : self.id_user,
                "friends" : friends
            }
            return(result)
        else:
            result = {
                "username" : self.username,
                "id" : self.id_user
            }
            return(result)


    def get_page_made_post(self):
        try:
            cek_user = FacebookUser(id_user=self.id_user)
            friends = cek_user.get_friends().json()
        except:
            friends = []

        result = {
            "username" : self.username,
            "id" : self.id_user,
            "friends" : friends
        }

    # mengambil informasi comment dari post

class AuthorValidation(object):

    access_token = "access_token=133252573984435%7CVesAEMW2yKNYjryNQg01CMLlBCA"

    def __init__(self,id_author):
        self.id_author = str(id_author)


    # mengambil informasi dasar dari post
    def is_page(self):
        result = False
        url_fans = "https://graph.facebook.com/v2.12/"+self.id_author+"?fields=fan_count&"+self.access_token
        # url_fans = "https://graph.facebook.com/v2.12/602859519886846?fields=fan_count&access_token=133252573984435%7CVesAEMW2yKNYjryNQg01CMLlBCA"
        req = requests.get(url=url_fans)
        res = req.json()
        try:
            if(res['fan_count'] > 0):
                result = True
        except:
            result = False
        return(result)


# # test user
# id facebook.com/ziatech
# id_user = "1500009420083906"
# new_user = FacebookUser(id_user=id_user)
# # print(.get_picture().json())
# print(new_user.get_books().json())
# print(new_user.get_achievement().json())
# print(new_user.get_friends().json())


# test post
# link_post = "https://www.facebook.com/Maklambeturah/posts/948686755304119"
# new_post = FacebookPost(link_post=link_post)
# print(new_post.get_info_post())
# print(new_post.get_user_made_post())

# # id_author = "948686755304119"
# id_author = "602859519886846"
# new_author = AuthorValidation(id_author=id_author)
# print(new_author.is_page())
# # print(new_author.is_user())




# skenario mendapatkan post dari facebook
# pertama untuk post jenis biasa
# [ok] pakai CSE dari google untuk facebook post biasa
# [ok] kemudian simpan ke db
# [ok] ambil satu persatu
# lakukan pengambilan detail dengan Facebook API

contoh_facebook_post = "https://www.facebook.com/Maklambeturah/posts/946395382199923"
contoh_facebook_video_post = "https://www.facebook.com/aniesbaswedan/videos/1302483013122015/"
contoh_facebook_photo_post = "https://www.facebook.com/aniesbaswedan/photos/pcb.1504968159540165/1504967846206863/?type=3"
contoh_facebook_group_post = "https://www.facebook.com/groups/1058578774159261/"
contoh_facebook_group_post1 = "https://www.facebook.com/groups/fafhh/permalink/573640402968499/"
contoh_facebook_group_post2 = "https://www.facebook.com/groups/indonesiantionghoa/"

#
# ambil data dasar dari facebook_post
print("tipe post")
# fb_post = FacebookPost(link_post=contoh_facebook_post)
fb_post = FacebookPost(link_post="https://www.facebook.com/maz.lookman/posts/1649438171802892")
# dapatkan id user
print(contoh_facebook_post)
print("id user = "+ fb_post.get_id_username_author())
# dapatkan username dari user author
print("username user = "+fb_post.get_username_author())
# dapatkan id post
print("id post = "+fb_post.get_id_post())
print(fb_post.get_count_comment())
print(fb_post.get_count_likes())
print(fb_post.get_count_shares())
#
# print("+====================================+")
# # ambil data dasar dari facebook_video
# print("tipe facebook video")
# fb_video = FacebookVideos(link_post=contoh_facebook_video_post)
# print(fb_video.get_id_post())
# print(fb_video.get_username())
# print(fb_video.get_id_user())
# print(fb_video.get_count_comment())
# print(fb_video.get_count_likes())
# print(fb_video.get_count_shares())
# # print(fb_video.get_content())
# # print(fb_video.get_attachment())
# print(fb_video.get_count_reactions())
#

# ambil data dasar dari facebook_photo

# print("tipe facebook photo")
# fb_photo = FacebookPhotos(link_post=contoh_facebook_photo_post)
# print(fb_photo.get_id_user())
# print(fb_photo.get_username())
# print(fb_photo.get_id_post())
# print(fb_photo.get_content())
# print(fb_photo.get_count_comment())
# print(fb_photo.get_count_likes())
# print(fb_photo.get_count_reactions())
# print(fb_photo.get_count_shares())

#
# # ambil data dasar dari facebook group
# print("tipe facebook group")
# fb_group_post = contoh_facebook_group_post