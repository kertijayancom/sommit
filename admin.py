from django.contrib import admin
from django.forms import ModelForm, ChoiceField, Form
from ._models import news, socialmedia, _main

# setting header for django admin
admin.site.site_header = 'Sentiment Analysis - Administrator'
admin.site.site_title = 'Sommit Administrator'

class OtherNews(admin.ModelAdmin):
    list_display = ['id','title','situs','subsitus','date_news_created']
    search_fields = ['title','situs']

admin.site.register(news.ResultOtherNews,OtherNews)

class LevelCSETipeform(ModelForm):
    MY_CHOICES = [('news','news'),('sosialmedia','sosialmedia')]
    def __init__(self, *args, **kwargs):
       super(LevelCSETipeform, self).__init__(*args, **kwargs)
       if self.instance.id:
           CHOICES_INCLUDING_DB_VALUE = [(self.instance.tipe,)*2] + self.MY_CHOICES
           self.fields['tipe'] = ChoiceField(
                choices=CHOICES_INCLUDING_DB_VALUE)


class LevelCSEAdmin(admin.ModelAdmin):
    list_display = ['id','nama','tipe','cakupan']
    search_fields = ['nama']
    form = LevelCSETipeform

admin.site.register(news.LevelCSE, LevelCSEAdmin)


class LevelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LevelForm, self).__init__(*args, **kwargs)
        self.fields['level'].queryset = news.LevelCSE.objects.all()
        self.fields['level'].label_from_instance = lambda obj: "%s-%s" % (obj.tipe, obj.nama)

class CSEGoogleAdmin(admin.ModelAdmin):
    list_display = ['id','situs_utama','situs', 'link','level_','link_test','is_valid','total_post']
    search_fields = ['situs']
    form = LevelForm

    def link_test(self, obj):
        link_source = ""
        link = "https://www.googleapis.com/customsearch/v1?q=jokowi+&key=AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o&cx="
        link1 = "&start=1&sort=date:r:20180128:20180128"
        try:
            link_source = "<a href ='"+link + str(obj.cse_id) + link1+"'> link</a>"
        except:
            link_source = ""
        return link_source

    def level_(self, obj):
        return(obj.level.tipe+" - "+obj.level.nama)

    link_test.allow_tags = True

admin.site.register(news.CSEGoogle,CSEGoogleAdmin)

# register model into django admin site
class ProjectSentimentAdmin(admin.ModelAdmin):
    list_display = ('id','name', 'date_created', 'user')
    search_fields = ['name']
    date_hierarchy = 'date_created'
#
# class PAdmin(admin.TabularInline):
#     model = news.ProjectSentiment
#
# admin.site.register(news.ProjectSentiment, PAdmin)
admin.site.register(news.ProjectSentiment,ProjectSentimentAdmin)
# admin.register(news.SearchHistory)(admin.ModelAdmin)

class ResultNewsDetikAdmin(admin.ModelAdmin):
    list_display = ('id','link','project_name','is_edited')
    search_fields =  ['link']
    date_hierarchy = 'date_created'

    def project_name(self, obj):
        p_name = ""
        try:
            p_name = obj.history.project.name
        except:
            p_name=""
        return p_name
    project_name.short_description = 'project'

    # def sudah_diedit(self, obj):
    #     return "belum"
    # sudah_diedit.short_description = 'diedit'

admin.site.register(news.ResultNewsDetik,ResultNewsDetikAdmin)

class DetikOpinionform(ModelForm):
    MY_CHOICES = [('positive','positive'),('neutral','neutral'),('negative','negative')]
    def __init__(self, *args, **kwargs):
       super(DetikOpinionform, self).__init__(*args, **kwargs)
       if self.instance.id:
           CHOICES_INCLUDING_DB_VALUE = [(self.instance.opinion,)*2] + self.MY_CHOICES
           self.fields['opinion'] = ChoiceField(
                choices=CHOICES_INCLUDING_DB_VALUE)

class ResultDetikAdmin(admin.ModelAdmin):
    list_display = ('id','sentence','opinion','project_name','link_situs','is_edited')
    search_fields =  ['sentence','opinion']
    date_hierarchy = 'date_created'
    ordering = ['id']
    form = DetikOpinionform

    def project_name(self, obj):
        p_name = ""
        try:
            p_name = obj.result_news_detik.history.project.name
        except:
            p_name = ""
        return p_name
    project_name.short_description = 'project'

    def link_situs(self, obj):
        linknya = "kosong"
        try:
            linknya = "<a href='"+str(obj.result_news_detik.urls)+"'>link<a>"
        except:
            linknya = "kosong"
        return linknya

    link_situs.allow_tags = True

    # def sudah_diedit(self, obj):
    #     return "belum"
    # sudah_diedit.short_description = 'diedit'

admin.site.register(news.ResultDetik, ResultDetikAdmin)

admin.register(news.RelationNewsDetik)(admin.ModelAdmin)

class SentimentModelAdmin(admin.ModelAdmin):
    list_display = ('media_type','num_of_data','path_model')
    search_fields = ['media_type']

admin.site.register(socialmedia.Sentimentmodel, SentimentModelAdmin)

class Myform(ModelForm):
    MY_CHOICES = [('positive','positive'),('neutral','neutral'),('negative','negative')]
    def __init__(self, *args, **kwargs):
       super(Myform, self).__init__(*args, **kwargs)
       if self.instance.id:
           CHOICES_INCLUDING_DB_VALUE = [(self.instance.opinion,)*2] + self.MY_CHOICES
           self.fields['opinion'] = ChoiceField(
                choices=CHOICES_INCLUDING_DB_VALUE)

class ResultTwitterAdmin(admin.ModelAdmin):
    list_display = ('id','sentence', 'opinion','tweet_created','link_url_tweets','get_query','is_edited')
    search_fields = ['sentence']
    date_hierarchy = 'tweet_created'
    ordering = ['id']
    form = Myform

    def get_query(self, obj):
        p_name = ""
        try:
            p_name = obj.history.project.name
        except:
            p_name = ""
        return p_name
    get_query.short_description = 'project'

    def link_url_tweets(self, obj):
        link_source = "kosong"
        try:
            link_source = "<a href = 'https://twitter.com/" + str(obj.user_screen_name) + "/status/" + str(obj.id_str)+"'>link</a>"
        except:
            link_source = "kosong"
        return link_source

    link_url_tweets.short_description = 'url'
    link_url_tweets.allow_tags = True



admin.site.register(socialmedia.ResultTwitter, ResultTwitterAdmin)

class DataTrainAdmin(admin.ModelAdmin):
    list_display = ('old_message','media_type','opinion')
    search_fields = ['old_message','media_type']

admin.site.register(_main.Datatrain, DataTrainAdmin)

class SearchHistoryAdmin(admin.ModelAdmin):
    list_display = ('id','date_created', 'query','category')
    search_fields = ['query']

admin.site.register(news.SearchHistory, SearchHistoryAdmin)

class BondowosoNewsAdmin(admin.ModelAdmin):
    list_display = ('id','nama_situs', 'link_url','nama_wartawan', 'is_scrape_able')
    search_fields = ['nama_situs']

    def link_url(self, obj):
        link_source = "kosong"
        try:
            link_source = "<a href = '"+ str(obj.url) +"'>"+str(obj.url)+"</a>"
        except:
            link_source = "kosong"
        return link_source

    link_url.short_description = 'link_url'
    link_url.allow_tags = True

admin.site.register(news.BondowosoNews, BondowosoNewsAdmin)

admin.register(news.CountSentimentNews)(admin.ModelAdmin)
admin.register(socialmedia.TopInfluencerTweet)(admin.ModelAdmin)
admin.register(socialmedia.TopContributorTweet)(admin.ModelAdmin)
admin.register(socialmedia.TopMentionTweet)(admin.ModelAdmin)



class CobaResultFacebookAdmin(admin.ModelAdmin):
    list_display = ('date_created', 'title', 'url', 'sesi')
    search_fields = ['title']

admin.site.register(socialmedia.CobaResultFacebook, CobaResultFacebookAdmin)


class CobaResultTwitterAdmin(admin.ModelAdmin):
    list_display = ('date_created', 'title', 'url', 'sesi')
    search_fields = ['title']

admin.site.register(socialmedia.CobaResultTwitter, CobaResultTwitterAdmin)


class CobaResultInstagramAdmin(admin.ModelAdmin):
    list_display = ('date_created', 'title', 'url', 'sesi')
    search_fields = ['title']

admin.site.register(socialmedia.CobaResultInstagram, CobaResultInstagramAdmin)
