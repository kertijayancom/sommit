from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from api.models import SearchHistory, ResultDetik, ResultNewsDetik, ProjectSentiment, ResultTwitter, TopMentionTweet, ResultOtherNews, CSEGoogle, Datatrain, TopInfluencerTweet, TopContributorTweet, CobaResultInstagram, CobaResultTwitter, CobaResultFacebook
from django.db.models import Count
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from ..extras.tokenizer import tokenizer_news
from ..extras.algorithm import naivebayes

import datetime, requests, time
from datetime import timedelta
import numpy, nltk, tweepy
from nltk.corpus import stopwords
from nltk.tokenize import  TweetTokenizer
import geopy
from geopy.geocoders import Nominatim, GoogleV3
from api import tasks


@api_view(['POST'])
def create_project(request):
    if request.method == 'POST':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        query = request.data['query']
        category = request.data['category']
        name = request.data['name']
        user_token = Token.objects.filter(key=info_token).first().user

        cek_project = len(ProjectSentiment.objects.filter(name=name, user=user_token))

        if(cek_project == 0):

            # create project
            new_project = ProjectSentiment.objects.create(
                user=user_token,
                name=name
            )
            new_project.save()

            # create first search
            new_search = SearchHistory.objects.create(
                query=query,
                category=category,
                project=new_project
            )
            new_search.save()

            result = {}
            result['id_project'] = new_project.id
            result['id_search'] = new_search.id

            return Response(result, status=status.HTTP_200_OK)
        else:
            result = {
                'error' : 'project already exists'
            }
            return Response(result,status.HTTP_204_NO_CONTENT)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def list_project_by_user(request):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user

        # skeleton result
        result_projects_user = {}
        result_projects = []

        # get user
        user_projects = ProjectSentiment.objects.filter(user=user_token).all()

        for project in user_projects:
            result_searches = []
            search_projects = SearchHistory.objects.filter(project = project)
            for item in search_projects:
                # status twitter
                res_twitter = len(ResultTwitter.objects.filter(history=item).all())
                res_twitter_status = "pending"
                if (res_twitter > 0): res_twitter_status = "success"

                # status detik
                # check for detik result
                res_detik = len(ResultNewsDetik.objects.filter(history=item).all())
                res_detik_status = "pending"
                if (res_detik > 0):
                    res_detik_status = "success"

                result_searches.append(
                    {
                        "search_id" : item.id,
                        "date_created": {
                            "date" : str(item.date_created.day)+"-"+str(item.date_created.month)+"-"+str(item.date_created.year),
                            "time" : str(item.date_created.hour)+":"+str(item.date_created.minute)+":"+str(item.date_created.second)
                        },
                        "query" : item.query,
                        "status" : {
                            "twitter" : res_twitter_status,
                            "detik" : res_detik_status
                        }

                    }
                )
            result_projects.append(
                {
                    "project_id": project.id,
                    "name" : project.name,
                    "date_created": {
                        "date" : str(project.date_created.day)+"-"+str(project.date_created.month)+"-"+str(project.date_created.year),
                        "time" : str(project.date_created.hour)+":"+str(project.date_created.minute)+":"+str(project.date_created.second)
                    },
                    "user": project.user.username,
                    "search" : result_searches
                }
            )
        result_projects_user["projects"] = result_projects
        # print(result_projects_user)
        # page_result = {}
        # page_result['oke'] = "oke"
        return Response(result_projects_user)
    else:
        return Response(status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def current_status_project_by_id(request, id_project):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(id=id_project).first()

        result = {
            "status": False,
            "keterangan": user_project.status_project
        }
        if("selesai mengambil berita" in user_project.status_project):
            result = {
                "status": True,
                "keterangan": user_project.status_project
            }

        return Response(result, status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def status_project_by_id(request, id_project):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(id=id_project).first()

        result_status = {}
        result_status['project'] = user_project.name
        result_status['date_created'] = user_project.date_created
        result_status['status'] = []

        if (user_token.id == user_project.user.id):
            #ambil semua search dari project
            user_searches = SearchHistory.objects.filter(project = user_project).all()
            result_project = []
            for user_search in user_searches:

                # check for twitter result
                res_twitter = len(ResultTwitter.objects.filter(history=user_search).all())
                res_twitter_status = "pending"
                if(res_twitter > 10): res_twitter_status = "success"


                # check for detik result
                res_detik = len(ResultNewsDetik.objects.filter(history=user_search).all())
                res_detik_status = "pending"
                if(res_detik > 10): res_detik_status = "success"

                result_project.append({
                    # search id
                    "search": {
                        "id" : user_search.id,
                        "query" : user_search.query,
                        # social media results
                        "social_media": {
                            # twitter results
                            "twitter": {
                                "count": res_twitter,
                                "status": res_twitter_status
                            },
                        },
                        # news results
                        "news": {
                            # detik results
                            "detik": {
                                "count": res_detik,
                                "status": res_detik_status
                            }
                        }
                    }
                })
            result_status['status'] = result_project
            return Response(result_status)
        else:
            return Response(status.HTTP_204_NO_CONTENT)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def list_search_by_project(request, id_project):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user

        # skeleton result
        result_projects_user = {}

        # get user
        user_project = ProjectSentiment.objects.filter(user=user_token, id = int(id_project)).first()

        result_searches = []
        search_projects = SearchHistory.objects.filter(project = user_project)
        for item in search_projects:

            # check for twitter result
            twitter_res = len(ResultTwitter.objects.filter(history=item).all())
            twitter_res_status = "pending"
            twitter_latest_data = []
            twitter_sentiment_positive = 0
            twitter_sentiment_neutral = 0
            twitter_sentiment_negative = 0

            twitter_result_sentiment_per_group = []
            detik_result_sentiment_per_group = []

            if (twitter_res > 0):
                twitter_res_status = "success"
                # latest data from tweet
                t_count = 0
                tweets_data = ResultTwitter.objects.filter(history=item).all().order_by('-date_created')
                for tweet in tweets_data:

                    # count sentiment
                    if(tweet.opinion == "positive"): twitter_sentiment_positive += 1
                    if (tweet.opinion == "neutral"): twitter_sentiment_neutral += 1
                    if (tweet.opinion == "negative"): twitter_sentiment_negative += 1


                    if(t_count < 5):
                        try:
                            twitter_latest_data.append({
                                "number" : t_count+1,
                                "text" : tweet.sentence,
                                "sentiment" : tweet.opinion,
                                "city" : tweet.city,
                                "created" : {
                                    "date": str(tweet.date_created.day) + "-" + str(tweet.date_created.month) + "-" + str(
                                        tweet.date_created.year),
                                    "time": str(tweet.date_created.hour) + ":" + str(tweet.date_created.minute) + ":" + str(
                                        tweet.date_created.second)
                                },
                                "retweet_count" : tweet.retweet_count,
                                "like_count" : tweet.favourite_count,
                                "comment_count" : 0,
                                "source" : "twitter",
                                "profile_image_url" : tweet.profile_image_url,
                                "user_screen_name" : tweet.user_screen_name,
                                "link_source" : "https://twitter.com/"+tweet.user_screen_name+"/status/"+tweet.id_str
                            })
                        except:
                            print("belum memenuhi standar")
                        t_count+=1

                # process for create result graph
                tweets_data_all = ResultTwitter.objects.filter(history=item).all().order_by('-date_created')
                result_sentiment = []
                for tweet in tweets_data_all:
                    result_sentiment.append({
                        'year': tweet.tweet_created.year,
                        'month': tweet.tweet_created.month,
                        'day': tweet.tweet_created.day,
                        'opinion': tweet.opinion,
                    })

                date_now = item.date_created
                for _date in range(1, 8):
                    count_positive = 0
                    count_neutral = 0
                    count_negative = 0
                    count_total = 0
                    for items in result_sentiment:
                        if ((items['year'] == date_now.year) and (items['month'] == date_now.month) and (
                            items['day'] == date_now.day)):
                            if (items['opinion'] == 'positive'): count_positive += 1
                            if (items['opinion'] == 'neutral'): count_neutral += 1
                            if (items['opinion'] == 'negative'): count_negative += 1
                            count_total += 1
                    twitter_result_sentiment_per_group.append({
                        'date': str(date_now.day) + "-" + str(date_now.month) + "-" + str(date_now.year),
                        'total': count_total,
                        'sentiment': {
                            'positive': count_positive,
                            'neutral': count_neutral,
                            'negative': count_negative
                        }
                    })
                    date_now -= datetime.timedelta(1)




            # check for detik result
            detik_res = len(ResultNewsDetik.objects.filter(history=item).all())
            detik_res_sentence = 0
            detik_res_status = "pending"
            detik_latest_data = []

            detik_sentiment_positive = 0
            detik_sentiment_neutral = 0
            detik_sentiment_negative = 0

            if (detik_res > 0):
                detik_res_status = "success"
                # latest data from detik
                detik_data = ResultNewsDetik.objects.filter(history=item).all().order_by('-date_created')

                detik_count = 0

                for detik in detik_data:

                    # count sentiment
                    detik_count_positive = len(ResultDetik.objects.filter(opinion="positive",result_news_detik=detik).all())
                    detik_count_neutral = len(ResultDetik.objects.filter(opinion="neutral",result_news_detik=detik).all())
                    detik_count_negative = len(ResultDetik.objects.filter(opinion="negative",result_news_detik=detik).all())

                    detik_res_sentence = detik_res_sentence+detik_count_positive+detik_count_neutral+detik_count_negative

                    detik_opinion_result = "neutral"
                    if((detik_count_positive > detik_count_neutral) and (detik_count_positive > detik_count_negative)):
                        detik_opinion_result = "positive"
                        detik_sentiment_positive += 1
                    elif ((detik_count_negative > detik_count_positive) and (detik_count_negative > detik_count_neutral)):
                        detik_opinion_result = "negative"
                        detik_sentiment_negative += 1
                    else:
                        detik_sentiment_neutral += 1

                    if(detik_count < 5):

                        # get content_data
                        content_detik = ResultDetik.objects.filter(result_news_detik=detik).all().first()
                        # content_detik = content_detik.replace(item.query, "<strong>"+item.query+"</strong>")

                        detik_latest_data.append({
                            "number": detik_count + 1,
                            "title": detik.link,
                            "content": content_detik.sentence,
                            "sentiment": {
                                "positive" :detik_count_positive,
                                "neutral" :detik_count_neutral,
                                "negative " : detik_count_negative,
                                "result" : detik_opinion_result
                            },
                            "created": {
                                "date": str(detik.date_news_created.day) + "-" + str(detik.date_news_created.month) + "-" + str(
                                    detik.date_news_created.year),
                                "time": str(detik.date_news_created.hour) + ":" + str(detik.date_news_created.minute) + ":" + str(
                                    detik.date_news_created.second)
                            },
                            "source": "detik",
                            "img" : str(detik.img),
                            "url" : str(detik.urls)
                        })
                        detik_count+=1

            # tambahkan ke detik_latest_data
            if(len(detik_latest_data) == 0):

                latest_other = ResultOtherNews.objects.filter(history=item).all().order_by('-date_created')[1:6]
                detik_count = 0
                for news_other in latest_other:
                    detik_latest_data.append({
                        "number": detik_count + 1,
                        "title": news_other.title,
                        "content": news_other.sentence,
                        "sentiment": {
                            "positive": 0,
                            "neutral": 0,
                            "negative ": 0,
                            "result": news_other.opinion
                        },
                        "created": {
                            "date": str(news_other.date_news_created.day) + "-" + str(news_other.date_news_created.month) + "-" + str(
                                news_other.date_news_created.year),
                            "time": str(news_other.date_news_created.hour) + ":" + str(news_other.date_news_created.minute) + ":" + str(
                                news_other.date_news_created.second)
                        },
                        "source": str(news_other.situs),
                        "img": str(news_other.img),
                        "url": str(news_other.urls)
                    })
                    detik_count+=1


            # process for create result graph
            detik_data_all = ResultNewsDetik.objects.filter(history=item).all().order_by('-date_news_created')
            result_sentiment = []
            for detik_data in detik_data_all:
                # detik_sentence = ResultDetik.objects.filter(result_news_detik = detik_data).all()

                default_sentiment = "neutral"
                count_positive = len(ResultDetik.objects.filter(result_news_detik = detik_data,opinion="positive").all())
                count_neutral = len(ResultDetik.objects.filter(result_news_detik = detik_data,opinion="neutral").all())
                count_negative = len(ResultDetik.objects.filter(result_news_detik = detik_data,opinion="negative").all())

                if((count_positive > count_neutral) and (count_positive > count_negative)):
                    default_sentiment = "positive"
                if((count_negative > count_positive) and (count_negative > count_neutral)):
                    default_sentiment = "negative"

                result_sentiment.append({
                    'year': detik_data.date_news_created.year,
                    'month': detik_data.date_news_created.month,
                    'day': detik_data.date_news_created.day,
                    'id' : detik_data.id,
                    'opinion': default_sentiment
                })

            # ambil data dari resultothernews kemudian masukan ke result_sentiment
            resultothernews_all = ResultOtherNews.objects.filter(history=item).all().order_by('-date_news_created')
            for datares in resultothernews_all:
                result_sentiment.append({
                    'year': datares.date_news_created.year,
                    'month': datares.date_news_created.month,
                    'day': datares.date_news_created.day,
                    'id': datares.id,
                    'opinion': datares.opinion
                })

            date_now = item.date_created
            for _date in range(1, 8):
                count_news_positive = 0
                count_news_neutral = 0
                count_news_negative = 0
                count_total = 0
                for items in result_sentiment:
                    if ((items['year'] == date_now.year) and (items['month'] == date_now.month) and (
                                items['day'] == date_now.day)):
                        if (items['opinion'] == 'positive'): count_news_positive += 1
                        if (items['opinion'] == 'neutral'): count_news_neutral += 1
                        if (items['opinion'] == 'negative'): count_news_negative += 1
                        count_total += 1
                detik_result_sentiment_per_group.append({
                    'date': str(date_now.day) + "-" + str(date_now.month) + "-" + str(date_now.year),
                    'total': count_total,
                    'sentiment': {
                        'positive': count_news_positive,
                        'neutral': count_news_neutral,
                        'negative': count_news_negative
                    }
                })
                date_now -= datetime.timedelta(1)

            # summary

            # count & precentage content
            total_count = twitter_res+detik_res
            total_detik_percentage = 0
            total_twitter_percentage =  0

            if (total_count > 0):
                total_detik_percentage = round((detik_res / total_count) * 100, 2)
                total_twitter_percentage =round((twitter_res / total_count) * 100, 2)

            other_news_positive = len(ResultOtherNews.objects.filter(history = item, opinion = "positive"))
            other_news_neutral = len(ResultOtherNews.objects.filter(history=item, opinion="neutral"))
            other_news_negative = len(ResultOtherNews.objects.filter(history=item, opinion="negative"))

            # count sentiment
            total_sentiment_positive = twitter_sentiment_positive + detik_sentiment_positive + other_news_positive
            total_sentiment_neutral = twitter_sentiment_neutral + detik_sentiment_neutral + other_news_neutral
            total_sentiment_negative = twitter_sentiment_negative + detik_sentiment_negative +other_news_negative
            total_sentiment = total_sentiment_positive+total_sentiment_neutral+total_sentiment_negative

            # persentage sentiment
            percentage_sentiment_positive = 0
            percentage_sentiment_neutral = 0
            percentage_sentiment_negative = 0

            if(total_sentiment > 0):
                percentage_sentiment_positive = round( (total_sentiment_positive/total_sentiment)*100,2)
                percentage_sentiment_neutral = round((total_sentiment_neutral / total_sentiment)*100, 2)
                percentage_sentiment_negative = round((total_sentiment_negative / total_sentiment)*100, 2)

            # ambil othernews
            # ambil distinctnya dulu
            list_situs = ResultOtherNews.objects.filter(history = item).values('situs').annotate(freq_count=Count('situs')).order_by("-freq_count")
            gudang = []

            total_semua = detik_res+twitter_res
            for s1 in list_situs:
                if(s1['situs'] is not "Detik"):
                    total_semua+=s1['freq_count']

            total_detik_percentage = 0
            if (detik_res > 0): total_detik_percentage = round((detik_res / total_semua) * 100, 2)
            total_twitter_percentage = 0
            if (twitter_res > 0): total_twitter_percentage = round((twitter_res / total_semua) * 100, 2)

            gudang.append({
                "no": 1,
                "percentage": total_twitter_percentage,
                "total": twitter_res,
                "link": "Twitter"
            })

            gudang.append({
                "no" : 2,
                "percentage": total_detik_percentage,
                "total": detik_res,
                "link": "Detik"
            })


            no = 3
            for situs1 in list_situs:
                if (s1['situs'] != "Detik"):
                    nama_situs = situs1['situs']
                    total_post = situs1['freq_count']
                    presentase = 0
                    if(total_post > 0):
                        presentase = round((total_post / total_semua) * 100, 2)
                    gudang.append(
                        {
                            "no": no,
                            "percentage" : presentase,
                            "total": total_post,
                            "link": nama_situs
                        }
                    )
                    no+=1


            result_searches.append(
                {
                    "search_id" : item.id,
                    "date_created" : item.date_created,
                    "query" : item.query,
                    "latest" : {
                        "social_media": {
                            # twitter results
                            "twitter": {
                                "count": twitter_res,
                                "status": twitter_res_status,
                                "latest_data": twitter_latest_data
                            },
                        },
                        "news": {
                            # detik results
                            "detik": {
                                "count": detik_res,
                                "status": detik_res_status,
                                "latest_data": detik_latest_data
                            }
                        },
                    },
                    "source" : gudang,
                    "summary" :{
                        "reach" :{
                            "detik": {
                                "news": detik_res,
                                "sentence": detik_res_sentence
                            },
                            "twitter": twitter_res,
                            "total": total_count,

                        },
                        "sentiment" :{
                            "count" : {
                                "positive": total_sentiment_positive,
                                "neutral": total_sentiment_neutral,
                                "negative": total_sentiment_negative,
                                "total" : total_sentiment
                            },
                            "precentage" : {
                                "positive": percentage_sentiment_positive,
                                "neutral": percentage_sentiment_neutral,
                                "negative": percentage_sentiment_negative
                            },
                        }
                    },
                    "result_stat": {
                        "twitter": twitter_result_sentiment_per_group,
                        "detik": detik_result_sentiment_per_group
                    }
                }
            )
        result_projects_user['detail']= {
            "project_id": user_project.id,
            "name" : user_project.name,
            "date_created": user_project.date_created,
        }
        result_projects_user["searches"] = result_searches
        return Response(result_projects_user)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def result_sentiment_by_project(request, id_project):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()

        # set a result_sentiment_projects
        result_projects = []

        search_projects = SearchHistory.objects.filter(project = user_project)

        for item in search_projects:

            twitter_count_sentiment_positive = 0
            twitter_count_sentiment_neutral = 0
            twitter_count_sentiment_negative = 0

            twitter_retweet_count_sentiment_positive = 0
            twitter_retweet_count_sentiment_neutral = 0
            twitter_retweet_count_sentiment_negative = 0

            twitter_favorite_count_sentiment_positive = 0
            twitter_favorite_count_sentiment_neutral = 0
            twitter_favorite_count_sentiment_negative = 0

            twitter_result_sentiment_positive = []
            twitter_result_sentiment_neutral = []
            twitter_result_sentiment_negative = []

            # preparation for graph result twitter
            twitter_graph_result_sentiment = []


            # get all data from twitter db
            tweets_data = ResultTwitter.objects.filter(history=item).all().order_by('-date_created')
            for tweet in tweets_data:

                twitter_graph_result_sentiment.append({
                    'year': tweet.tweet_created.year,
                    'month': tweet.tweet_created.month,
                    'day': tweet.tweet_created.day,
                    'opinion': tweet.opinion,
                    "like": tweet.favourite_count,
                    "retweet": tweet.retweet_count,
                    "comment": 0,
                })

                # count sentiment
                if(tweet.opinion == "positive"):
                    twitter_count_sentiment_positive += 1
                    twitter_retweet_count_sentiment_positive += tweet.retweet_count
                    twitter_favorite_count_sentiment_positive += tweet.favourite_count
                    twitter_result_sentiment_positive.append({
                        "text" : tweet.sentence,
                        "sentiment" : tweet.opinion,
                        "city" : tweet.city,
                        "created" : {
                            "date": str(tweet.date_created.day) + "-" + str(tweet.date_created.month) + "-" + str(
                                tweet.date_created.year),
                            "time": str(tweet.date_created.hour) + ":" + str(tweet.date_created.minute) + ":" + str(
                                tweet.date_created.second)
                        },
                        "like" : tweet.favourite_count,
                        "retweet" : tweet.retweet_count,
                        "comment" : 0,
                        "profile_image_url" : tweet.profile_image_url
                    })

                if (tweet.opinion == "neutral"):
                    twitter_count_sentiment_neutral += 1
                    twitter_retweet_count_sentiment_neutral += tweet.retweet_count
                    twitter_favorite_count_sentiment_neutral += tweet.favourite_count
                    twitter_result_sentiment_neutral.append({
                        "text": tweet.sentence,
                        "sentiment": tweet.opinion,
                        "city": tweet.city,
                        "created": {
                            "date": str(tweet.date_created.day) + "-" + str(tweet.date_created.month) + "-" + str(
                                tweet.date_created.year),
                            "time": str(tweet.date_created.hour) + ":" + str(tweet.date_created.minute) + ":" + str(
                                tweet.date_created.second)
                        },
                        "like" : tweet.favourite_count,
                        "retweet" : tweet.retweet_count,
                        "comment": 0,
                        "profile_image_url": tweet.profile_image_url
                    })

                if (tweet.opinion == "negative"):
                    twitter_count_sentiment_negative += 1
                    twitter_retweet_count_sentiment_negative += tweet.retweet_count
                    twitter_favorite_count_sentiment_negative += tweet.favourite_count
                    twitter_result_sentiment_negative.append({
                        "text": tweet.sentence,
                        "sentiment": tweet.opinion,
                        "city": tweet.city,
                        "created": {
                            "date": str(tweet.date_created.day) + "-" + str(tweet.date_created.month) + "-" + str(
                                tweet.date_created.year),
                            "time": str(tweet.date_created.hour) + ":" + str(tweet.date_created.minute) + ":" + str(
                                tweet.date_created.second)
                        },
                        "like" : tweet.favourite_count,
                        "retweet" : tweet.retweet_count,
                        "comment": 0,
                        "profile_image_url": tweet.profile_image_url
                    })

            # calculate percentage for twitter count
            twitter_count_sentiment_total = twitter_count_sentiment_positive+twitter_count_sentiment_neutral+twitter_count_sentiment_negative
            twitter_percentage_sentiment_positive = 0
            twitter_percentage_sentiment_neutral = 0
            twitter_percentage_sentiment_negative = 0
            if(twitter_count_sentiment_total > 0):
                twitter_percentage_sentiment_positive = round((twitter_count_sentiment_positive / twitter_count_sentiment_total) * 100, 2)
                twitter_percentage_sentiment_neutral = round((twitter_count_sentiment_neutral / twitter_count_sentiment_total) * 100, 2)
                twitter_percentage_sentiment_negative = round((twitter_count_sentiment_negative / twitter_count_sentiment_total) * 100, 2)


            # calculate percentage for twitter retweet_count
            twitter_retweet_count_sentiment_total = twitter_retweet_count_sentiment_positive + twitter_retweet_count_sentiment_neutral + twitter_retweet_count_sentiment_negative
            twitter_retweet_percentage_sentiment_positive = 0
            twitter_retweet_percentage_sentiment_neutral = 0
            twitter_retweet_percentage_sentiment_negative = 0
            if (twitter_retweet_count_sentiment_total > 0):
                twitter_retweet_percentage_sentiment_positive = round(
                    (twitter_retweet_count_sentiment_positive / twitter_retweet_count_sentiment_total) * 100, 2)
                twitter_retweet_percentage_sentiment_neutral = round(
                    (twitter_retweet_count_sentiment_neutral / twitter_retweet_count_sentiment_total) * 100, 2)
                twitter_retweet_percentage_sentiment_negative = round(
                    (twitter_retweet_count_sentiment_negative / twitter_retweet_count_sentiment_total) * 100, 2)


            # calculate percentage for twitter favorite_count
            twitter_favorite_count_sentiment_total = twitter_favorite_count_sentiment_positive + twitter_favorite_count_sentiment_neutral + twitter_favorite_count_sentiment_negative
            twitter_favorite_percentage_sentiment_positive = 0
            twitter_favorite_percentage_sentiment_neutral = 0
            twitter_favorite_percentage_sentiment_negative = 0
            if (twitter_favorite_count_sentiment_total > 0):
                twitter_favorite_percentage_sentiment_positive = round(
                    (twitter_favorite_count_sentiment_positive / twitter_favorite_count_sentiment_total) * 100, 2)
                twitter_favorite_percentage_sentiment_neutral = round(
                    (twitter_favorite_count_sentiment_neutral / twitter_favorite_count_sentiment_total) * 100, 2)
                twitter_favorite_percentage_sentiment_negative = round(
                    (twitter_favorite_count_sentiment_negative / twitter_favorite_count_sentiment_total) * 100, 2)


            twitter_result_sentiment = {
                "data" : {
                    "positive" : twitter_result_sentiment_positive,
                    "neutral" : twitter_result_sentiment_neutral,
                    "negative" : twitter_result_sentiment_negative
                },
                "sentiment" : {
                    "percentage": {
                        "positive": twitter_percentage_sentiment_positive,
                        "neutral": twitter_percentage_sentiment_neutral,
                        "negative": twitter_percentage_sentiment_negative,
                    },
                    "count": {
                        "positive": twitter_count_sentiment_positive,
                        "neutral": twitter_count_sentiment_neutral,
                        "negative": twitter_count_sentiment_negative,
                        "total" : twitter_count_sentiment_total
                    },
                },
                "retweet" :{
                    "count": {
                        "positive": twitter_retweet_count_sentiment_positive,
                        "neutral": twitter_retweet_count_sentiment_neutral,
                        "negative": twitter_retweet_count_sentiment_negative
                    },
                    "percentage": {
                        "positive": twitter_retweet_percentage_sentiment_positive,
                        "neutral": twitter_retweet_percentage_sentiment_neutral,
                        "negative": twitter_retweet_percentage_sentiment_negative
                    }
                },
                "favorite" :{
                    "count": {
                        "positive": twitter_favorite_count_sentiment_positive,
                        "neutral": twitter_favorite_count_sentiment_neutral,
                        "negative": twitter_favorite_count_sentiment_negative
                    },
                    "percentage": {
                        "positive": twitter_favorite_percentage_sentiment_positive,
                        "neutral": twitter_favorite_percentage_sentiment_neutral,
                        "negative": twitter_favorite_percentage_sentiment_negative
                    }
                },
                "total": {
                    "favorite": twitter_favorite_count_sentiment_total,
                    "retweet": twitter_retweet_count_sentiment_total,
                    "tweet": twitter_count_sentiment_total
                }
            }

            # and then calculate for detik
            # for all news
            detik_total_sentiment_positive = 0
            detik_total_sentiment_neutral = 0
            detik_total_sentiment_negative = 0

            # for all sentence in news
            detik_count_sentiment_positive = 0
            detik_count_sentiment_neutral = 0
            detik_count_sentiment_negative = 0

            detik_result_sentiment_positive = []
            detik_result_sentiment_neutral = []
            detik_result_sentiment_negative = []

            # get all data from twitter db
            detik_data = ResultNewsDetik.objects.filter(history=item).all().order_by('-date_created')
            for detik_news in detik_data:

                detik_news_count_sentiment_positive = 0
                detik_news_count_sentiment_neutral = 0
                detik_news_count_sentiment_negative = 0

                detik_news_result_sentiment_positive = []
                detik_news_result_sentiment_neutral = []
                detik_news_result_sentiment_negative = []

                detik_news_original_title = detik_news.link
                detik_news_original_content = []

                detik_sentences = ResultDetik.objects.filter(result_news_detik = detik_news).order_by('date_created')
                for detik_sentence in detik_sentences:

                    detik_news_original_content.append(detik_sentence.sentence)

                    # count sentiment
                    if (detik_sentence.opinion == "positive"):
                        detik_news_count_sentiment_positive += 1
                        detik_news_result_sentiment_positive.append({
                            "text": detik_sentence.sentence,
                            "sentiment": detik_sentence.opinion,
                            "created": {
                                "date": str(detik_sentence.date_created.day) + "-" + str(detik_sentence.date_created.month) + "-" + str(detik_sentence.date_created.year),
                                "time": str(detik_sentence.date_created.hour) + ":" + str(detik_sentence.date_created.minute) + ":" + str(detik_sentence.date_created.second)
                            }
                        })

                    if (detik_sentence.opinion == "neutral"):
                        detik_news_count_sentiment_neutral += 1
                        detik_news_result_sentiment_neutral.append({
                            "text": detik_sentence.sentence,
                            "sentiment": detik_sentence.opinion,
                            "created": {
                                "date": str(detik_sentence.date_created.day) + "-" + str(detik_sentence.date_created.month) + "-" + str(detik_sentence.date_created.year),
                                "time": str(detik_sentence.date_created.hour) + ":" + str(detik_sentence.date_created.minute) + ":" + str(detik_sentence.date_created.second)
                            }
                        })

                    if (detik_sentence.opinion == "negative"):
                        detik_news_count_sentiment_negative += 1
                        detik_news_result_sentiment_negative.append({
                            "text": detik_sentence.sentence,
                            "sentiment": detik_sentence.opinion,
                            "created": {
                                "date": str(detik_sentence.date_created.day) + "-" + str(detik_sentence.date_created.month) + "-" + str(detik_sentence.date_created.year),
                                "time": str(detik_sentence.date_created.hour) + ":" + str(detik_sentence.date_created.minute) + ":" + str(detik_sentence.date_created.second)
                            }
                        })

                detik_count_sentiment_positive += detik_news_count_sentiment_positive
                detik_count_sentiment_neutral += detik_news_count_sentiment_neutral
                detik_count_sentiment_negative += detik_news_count_sentiment_negative
                detik_news_total_sentiment = detik_news_count_sentiment_positive+detik_news_count_sentiment_neutral+detik_news_count_sentiment_negative

                detik_news_percentage_positive = 0
                detik_news_percentage_neutral = 0
                detik_news_percentage_negative = 0

                if(detik_news_total_sentiment > 0):
                    detik_news_percentage_positive = round((detik_news_count_sentiment_positive / detik_news_total_sentiment) * 100, 2)
                    detik_news_percentage_neutral = round((detik_news_count_sentiment_neutral / detik_news_total_sentiment) * 100, 2)
                    detik_news_percentage_negative = round((detik_news_count_sentiment_negative / detik_news_total_sentiment) * 100, 2)


                # comparation for sentiment detik
                if((detik_news_count_sentiment_positive > detik_news_count_sentiment_neutral) and (detik_news_count_sentiment_positive > detik_news_count_sentiment_negative)):
                    detik_total_sentiment_positive +=1
                    detik_result_sentiment_positive.append({
                        "title" : detik_news_original_title,
                        "content" : detik_news_original_content,
                        "content_sentiment" : {
                            "positive" : detik_news_result_sentiment_positive,
                            "neutral": detik_news_result_sentiment_neutral,
                            "negative": detik_news_result_sentiment_negative,
                        },
                        "total_sentences" : detik_news_total_sentiment,
                        "sentiment" : {
                            "count" : {
                                "positive" : detik_news_count_sentiment_positive,
                                "neutral" : detik_news_count_sentiment_neutral,
                                "negative" : detik_news_count_sentiment_negative,
                            },
                            "percentage" : {
                                "positive" : detik_news_percentage_positive,
                                "neutral": detik_news_percentage_neutral,
                                "negative": detik_news_percentage_negative,
                            }
                        }
                    })

                elif((detik_news_count_sentiment_neutral > detik_news_count_sentiment_positive) and (detik_news_count_sentiment_neutral > detik_news_count_sentiment_negative)):
                    detik_total_sentiment_neutral +=1
                    detik_result_sentiment_neutral.append({
                        "title" : detik_news_original_title,
                        "content" : detik_news_original_content,
                        "content_sentiment" : {
                            "positive" : detik_news_result_sentiment_positive,
                            "neutral": detik_news_result_sentiment_neutral,
                            "negative": detik_news_result_sentiment_negative,
                        },
                        "total_sentences" : detik_news_total_sentiment,
                        "sentiment" : {
                            "count" : {
                                "positive" : detik_news_count_sentiment_positive,
                                "neutral" : detik_news_count_sentiment_neutral,
                                "negative" : detik_news_count_sentiment_negative,
                            },
                            "percentage" : {
                                "positive" : detik_news_percentage_positive,
                                "neutral": detik_news_percentage_neutral,
                                "negative": detik_news_percentage_negative,
                            }
                        }
                    })

                elif((detik_news_count_sentiment_negative > detik_news_count_sentiment_positive) and (detik_news_count_sentiment_negative > detik_news_count_sentiment_neutral)):
                    detik_total_sentiment_negative += 1
                    detik_result_sentiment_negative.append({
                        "title" : detik_news_original_title,
                        "content" : detik_news_original_content,
                        "content_sentiment" : {
                            "positive" : detik_news_result_sentiment_positive,
                            "neutral": detik_news_result_sentiment_neutral,
                            "negative": detik_news_result_sentiment_negative,
                        },
                        "total_sentences" : detik_news_total_sentiment,
                        "sentiment" : {
                            "count" : {
                                "positive" : detik_news_count_sentiment_positive,
                                "neutral" : detik_news_count_sentiment_neutral,
                                "negative" : detik_news_count_sentiment_negative,
                            },
                            "percentage" : {
                                "positive" : detik_news_percentage_positive,
                                "neutral": detik_news_percentage_neutral,
                                "negative": detik_news_percentage_negative,
                            }
                        }
                    })
                else:
                    detik_total_sentiment_neutral += 1
                    detik_result_sentiment_neutral.append({
                        "title" : detik_news_original_title,
                        "content" : detik_news_original_content,
                        "content_sentiment" : {
                            "positive" : detik_news_result_sentiment_positive,
                            "neutral": detik_news_result_sentiment_neutral,
                            "negative": detik_news_result_sentiment_negative,
                        },
                        "total_sentences" : detik_news_total_sentiment,
                        "sentiment" : {
                            "count" : {
                                "positive" : detik_news_count_sentiment_positive,
                                "neutral" : detik_news_count_sentiment_neutral,
                                "negative" : detik_news_count_sentiment_negative,
                            },
                            "percentage" : {
                                "positive" : detik_news_percentage_positive,
                                "neutral": detik_news_percentage_neutral,
                                "negative": detik_news_percentage_negative,
                            }
                        }
                    })

            detik_count_sentiment_total = detik_count_sentiment_positive+detik_count_sentiment_neutral+detik_count_sentiment_negative

            # calculate percentage sentiment per sentence
            detik_percentage_sentiment_positive = 0
            detik_percentage_sentiment_neutral = 0
            detik_percentage_sentiment_negative = 0

            if(detik_count_sentiment_total > 0):
                detik_percentage_sentiment_positive= round((detik_count_sentiment_positive / detik_count_sentiment_total) * 100, 2)
                detik_percentage_sentiment_neutral = round((detik_count_sentiment_neutral / detik_count_sentiment_total) * 100, 2)
                detik_percentage_sentiment_negative = round((detik_count_sentiment_negative / detik_count_sentiment_total) * 100, 2)

            # calculate percentage sentiment per news
            detik_count_news_sentiment_total = detik_total_sentiment_positive+detik_total_sentiment_neutral+detik_total_sentiment_negative
            detik_percentage_news_sentiment_positive = 0
            detik_percentage_news_sentiment_neutral = 0
            detik_percentage_news_sentiment_negative = 0

            if(detik_count_news_sentiment_total > 0):
                detik_percentage_news_sentiment_positive = round((detik_total_sentiment_positive / detik_count_news_sentiment_total) * 100, 2),
                detik_percentage_news_sentiment_positive =  sum(detik_percentage_news_sentiment_positive)
                detik_percentage_news_sentiment_neutral = round((detik_total_sentiment_neutral/ detik_count_news_sentiment_total) * 100, 2),
                detik_percentage_news_sentiment_neutral = sum(detik_percentage_news_sentiment_neutral)
                detik_percentage_news_sentiment_negative = round((detik_total_sentiment_negative / detik_count_news_sentiment_total) * 100, 2),
                detik_percentage_news_sentiment_negative = sum(detik_percentage_news_sentiment_negative)


            detik_result_sentiment = {
                "data": {
                    "positive": detik_result_sentiment_positive,
                    "neutral": detik_result_sentiment_neutral,
                    "negative": detik_result_sentiment_negative,
                },
                "percentage_by_sentence": {
                    "positive": detik_percentage_sentiment_positive,
                    "neutral": detik_percentage_sentiment_neutral,
                    "negative": detik_percentage_sentiment_negative
                },
                "count_by_sentence": {
                    "positive": detik_count_sentiment_positive,
                    "neutral": detik_count_sentiment_neutral,
                    "negative": detik_count_sentiment_negative,
                    "total" : detik_count_sentiment_total
                },
                "percentage_by_news": {
                    "positive": detik_percentage_news_sentiment_positive,
                    "neutral": detik_percentage_news_sentiment_neutral,
                    "negative": detik_percentage_news_sentiment_negative
                },
                "count_by_news": {
                    "positive": detik_total_sentiment_positive,
                    "neutral": detik_total_sentiment_neutral,
                    "negative": detik_total_sentiment_negative,
                    "total" : detik_count_news_sentiment_total
                }
            }


            # calculate for sentiment twitter + detik

            # count sentiment
            all_count_sentiment_positive = detik_total_sentiment_positive+twitter_count_sentiment_positive,
            all_count_sentiment_neutral = detik_total_sentiment_neutral+twitter_count_sentiment_neutral,
            all_count_sentiment_negative = detik_total_sentiment_negative+twitter_count_sentiment_negative,

            all_count_sentiment_positive = sum(all_count_sentiment_positive)
            all_count_sentiment_neutral = sum(all_count_sentiment_neutral)
            all_count_sentiment_negative = sum(all_count_sentiment_negative)

            all_count_sentiment_total = all_count_sentiment_positive+all_count_sentiment_neutral+all_count_sentiment_negative
            all_count_sentiment_total = all_count_sentiment_total
            # percentage sentiment
            all_percentage_sentiment_positive = 0
            all_percentage_sentiment_neutral = 0
            all_percentage_sentiment_negative = 0

            if(all_count_sentiment_total > 0):
                all_percentage_sentiment_positive = round((all_count_sentiment_positive/all_count_sentiment_total)*100,2)
                all_percentage_sentiment_neutral = round((all_count_sentiment_neutral/all_count_sentiment_total)*100,2)
                all_percentage_sentiment_negative = round((all_count_sentiment_negative/all_count_sentiment_total)*100,2)

            all_result_sentiment = {
                "percentage": {
                    "positive": all_percentage_sentiment_positive,
                    "neutral": all_percentage_sentiment_neutral,
                    "negative": all_percentage_sentiment_negative
                },
                "count": {
                    "positive": all_count_sentiment_positive,
                    "neutral": all_count_sentiment_neutral,
                    "negative": all_count_sentiment_negative,
                    "total": all_count_sentiment_total
                }
            }

            result_sentiment = {
                "twitter": twitter_result_sentiment,
                "detik" : detik_result_sentiment,
                "all_result" : all_result_sentiment
            }


            # calculate graph
            twitter_graph_result_sentiment_group_by_day = []

            # get date initial from date of project created
            date_now = item.date_created

            for _date in range(1, 8):

                # set initial count
                count_positive = 0
                count_neutral = 0
                count_negative = 0
                count_total = 0

                # set initial reach
                reach_positive = 0
                reach_neutral = 0
                reach_negative = 0
                reach_total = 0


                for items in twitter_graph_result_sentiment:
                    if ((items['year'] == date_now.year) and (items['month'] == date_now.month) and (
                                items['day'] == date_now.day)):
                        if (items['opinion'] == 'positive'):
                            count_positive += 1
                            reach_positive = reach_positive + items['like'] + items['retweet'] + items['comment']
                        if (items['opinion'] == 'neutral'):
                            count_neutral += 1
                            reach_neutral = reach_neutral + items['like'] + items['retweet'] + items['comment']
                        if (items['opinion'] == 'negative'):
                            count_negative += 1
                            reach_negative = reach_negative + items['like'] + items['retweet'] + items['comment']
                        count_total += 1
                        reach_total = reach_total + items['like'] + items['retweet'] + items['comment']

                    #  save into graph current day
                    twitter_graph_result_sentiment_group_by_day.append({
                        'date': str(date_now.day) + "-" + str(date_now.month) + "-" + str(date_now.year),
                        'sentiment': {
                            'positive': count_positive,
                            'neutral': count_neutral,
                            'negative': count_negative,
                            'total': count_total,
                        },
                        'reach': {
                            'positive': count_positive,
                            'neutral': count_neutral,
                            'negative': count_negative,
                            'total': count_total,
                        }
                    })
                    # decrease datenow into prev-day
                    date_now = date_now - datetime.timedelta(1)


            result ={
                "project_id":user_project.id,
                "name": user_project.name,
                "date_created": user_project.date_created,
                "title": "summary for sentiment result"
            }
            result["sentiment"] = result_sentiment
            result["graph"] = {
                "twitter": twitter_graph_result_sentiment_group_by_day
            }

            result_projects.append(result)

        return Response(result_projects, status=status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def result_sentiment_graph_by_project(request, id_project):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()

        # set a result_sentiment_projects
        result_graph = {}

        search_projects = SearchHistory.objects.filter(project = user_project)

        for item in search_projects:

            # calculate for detik

            # for all sentence in news
            detik_count_sentiment_positive = 0
            detik_count_sentiment_neutral = 0
            detik_count_sentiment_negative = 0

            # var for store graph structure
            detik_graph_result_sentiment = []

            # get all data from twitter db
            detik_data = ResultNewsDetik.objects.filter(history=item).all().order_by('-date_created')
            for detik_news in detik_data:

                detik_graph_result_sentiment_temp ={
                    'year': detik_news.date_news_created.year,
                    'month': detik_news.date_news_created.month,
                    'day': detik_news.date_news_created.day,
                    'opinion': "",
                }

                detik_news_count_sentiment_positive = 0
                detik_news_count_sentiment_neutral = 0
                detik_news_count_sentiment_negative = 0

                detik_sentences = ResultDetik.objects.filter(result_news_detik = detik_news).order_by('date_created')
                for detik_sentence in detik_sentences:

                    # count sentiment
                    if (detik_sentence.opinion == "positive"):
                        detik_news_count_sentiment_positive += 1

                    if (detik_sentence.opinion == "neutral"):
                        detik_news_count_sentiment_neutral += 1

                    if (detik_sentence.opinion == "negative"):
                        detik_news_count_sentiment_negative += 1

                detik_count_sentiment_positive += detik_news_count_sentiment_positive
                detik_count_sentiment_neutral += detik_news_count_sentiment_neutral
                detik_count_sentiment_negative += detik_news_count_sentiment_negative

                # comparation for sentiment detik
                if((detik_news_count_sentiment_positive > detik_news_count_sentiment_neutral) and (detik_news_count_sentiment_positive > detik_news_count_sentiment_negative)):
                    detik_graph_result_sentiment_temp['opinion'] = "positive"
                elif((detik_news_count_sentiment_neutral > detik_news_count_sentiment_positive) and (detik_news_count_sentiment_neutral > detik_news_count_sentiment_negative)):
                    detik_graph_result_sentiment_temp['opinion'] = "neutral"
                elif((detik_news_count_sentiment_negative > detik_news_count_sentiment_positive) and (detik_news_count_sentiment_negative > detik_news_count_sentiment_neutral)):
                    detik_graph_result_sentiment_temp['opinion'] = "negative"
                else:
                    detik_graph_result_sentiment_temp['opinion'] = "neutral"

                detik_graph_result_sentiment.append(detik_graph_result_sentiment_temp)

            # calculate graph
            detik_graph_result_sentiment_group_by_day = []

            # get date initial from date of project created
            date_now = item.date_created

            for _date in range(1, 8):

                # set initial count
                count_positive = 0
                count_neutral = 0
                count_negative = 0
                count_total = 0

                for items in detik_graph_result_sentiment:
                    if ((items['year'] == date_now.year) and (items['month'] == date_now.month) and (items['day'] == date_now.day)):
                        if (items['opinion'] == 'positive'):
                            count_positive += 1
                        if (items['opinion'] == 'neutral'):
                            count_neutral += 1
                        if (items['opinion'] == 'negative'):
                            count_negative += 1
                        count_total += 1

                #  save into graph current day
                detik_graph_result_sentiment_group_by_day.append({
                    'date': str(date_now.day) + "-" + str(date_now.month) + "-" + str(date_now.year),
                    'sentiment': {
                        'positive': count_positive,
                        'neutral': count_neutral,
                        'negative': count_negative,
                        'total': count_total,
                    }
                })
                # decrease datenow into prev-day
                date_now = date_now - datetime.timedelta(1)


            # preparation for graph result twitter
            twitter_graph_result_sentiment = []

            # get all data from twitter db
            tweets_data = ResultTwitter.objects.filter(history=item).all().order_by('-date_created')
            for tweet in tweets_data:
                twitter_graph_result_sentiment.append({
                    'year': tweet.tweet_created.year,
                    'month': tweet.tweet_created.month,
                    'day': tweet.tweet_created.day,
                    'opinion': tweet.opinion,
                    "like": tweet.favourite_count,
                    "retweet": tweet.retweet_count,
                    "comment": 0,
                })

            # calculate graph
            twitter_graph_result_sentiment_group_by_day = []

            # get date initial from date of project created
            date_now = item.date_created

            for _date in range(1, 8):

                # set initial count
                count_positive = 0
                count_neutral = 0
                count_negative = 0
                count_total = 0

                # set initial reach
                engagements_positive = 0
                engagements_neutral = 0
                engagements_negative = 0
                engagements_total = 0

                for items in twitter_graph_result_sentiment:
                    if ((items['year'] == date_now.year) and (items['month'] == date_now.month) and (
                        items['day'] == date_now.day)):
                        if (items['opinion'] == 'positive'):
                            count_positive += 1
                            engagements_positive = engagements_positive + items['like'] + items['retweet'] + items[
                                'comment']
                        if (items['opinion'] == 'neutral'):
                            count_neutral += 1
                            engagements_neutral = engagements_neutral + items['like'] + items['retweet'] + items[
                                'comment']
                        if (items['opinion'] == 'negative'):
                            count_negative += 1
                            engagements_negative = engagements_negative + items['like'] + items['retweet'] + items[
                                'comment']
                        count_total += 1
                        engagements_total = engagements_total + items['like'] + items['retweet'] + items['comment']

                        # save into graph current day
                twitter_graph_result_sentiment_group_by_day.append({
                    'date': str(date_now.day) + "-" + str(date_now.month) + "-" + str(date_now.year),
                    'sentiment': {
                        'positive': count_positive,
                        'neutral': count_neutral,
                        'negative': count_negative,
                        'total': count_total,
                    },
                    'engagements': {
                        'positive': engagements_positive,
                        'neutral': engagements_neutral,
                        'negative': engagements_negative,
                        'total': engagements_total,
                    }
                })
                # decrease datenow into prev-day
                date_now = date_now - datetime.timedelta(1)

            result_projects = {}
            result_projects['project'] = {
                "id": user_project.id,
                "name": user_project.name,
                "date_created": user_project.date_created,
                "title": "sentiment for sosmed twitter"
            }
            result_projects['graph'] = {
                "detik" : detik_graph_result_sentiment_group_by_day,
                "twitter" : twitter_graph_result_sentiment_group_by_day
            }


        return Response(result_projects, status=status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def result_sentiment_data_other_news_by_project(request, id_project, page, per_page):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()

        # set a result_sentiment_projects
        result_projects = {}
        search_projects = SearchHistory.objects.filter(project=user_project)
        for item in search_projects:
            # calculate for detik
            # for all news
            # set
            pos_page = int(per_page)

            total_data = len(ResultOtherNews.objects.filter(history=item).all().order_by('-date_created'))
            if(total_data > 9):
                # get total page
                total_page = 10
                total_data_per_page = total_data / total_page

                if ((int(page) <= total_page) and (int(page) >= 1)):

                    # var for store graph structure
                    result_other_news = []

                    # get all data from twitter db

                    othernews_data = ResultOtherNews.objects.filter(history=item).all().order_by('-date_created')[
                                 (int(page) - 1):(int(page) - 1 + total_data_per_page)]

                    no = (total_data_per_page * (int(page) - 1)) + 1
                    for n_data in othernews_data:
                        result_other_news.append({
                            'no': str(int(no)),
                            'title': n_data.title,
                            'sentiment': n_data.opinion,
                            "created": {
                                "date": str(n_data.date_news_created.day) + "-" + str(
                                    n_data.date_news_created.month) + "-" + str(
                                    n_data.date_news_created.year),
                                "time": str(n_data.date_news_created.hour) + ":" + str(
                                    n_data.date_news_created.minute) + ":" + str(
                                    n_data.date_news_created.second)
                            },
                            'url': n_data.urls,
                            'img': n_data.img,
                            'five_sentence': n_data.sentence
                        })
                        no+=1


                    result_projects["data"] = result_other_news
                    result_projects["page"] = {
                        "current": page,
                        "total": total_page,
                        "index": numpy.arange(1, total_page + 1),
                        "total_data":total_data,
                        "total_data_per_page":total_data_per_page
                    }
            else:
                result_other_news = []
                othernews_data = ResultOtherNews.objects.filter(history=item).all().order_by('-date_created')
                no = 1
                for n_data in othernews_data:
                    result_other_news.append({
                        'no': str(int(no)),
                        'title': n_data.title,
                        'sentiment': n_data.opinion,
                        "created": {
                            "date": str(n_data.date_news_created.day) + "-" + str(
                                n_data.date_news_created.month) + "-" + str(
                                n_data.date_news_created.year),
                            "time": str(n_data.date_news_created.hour) + ":" + str(
                                n_data.date_news_created.minute) + ":" + str(
                                n_data.date_news_created.second)
                        },
                        'url': n_data.urls,
                        'img': n_data.img,
                        'five_sentence': n_data.sentence
                    })
                    no += 1

                result_projects["data"] = result_other_news
                result_projects["page"] = {
                    "current": page,
                    "total": 1,
                    "index": [1],
                    "total_data": total_data,
                }
        return Response(result_projects, status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)
@api_view(['GET'])
def result_sentiment_data_news_detik_by_project(request, id_project, page, per_page):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()

        # set a result_sentiment_projects
        result_projects = {}

        search_projects = SearchHistory.objects.filter(project = user_project)

        for item in search_projects:

            # calculate for detik
            # for all news
            # set
            pos_page = int(per_page)

            total_data = len(ResultNewsDetik.objects.filter(history=item).all().order_by('-date_created'))
            if(total_data > 9):
                # get total page
                total_page = 10
                total_data_per_page = total_data/total_page


                if ((int(page) <= total_page) and (int(page) >= 1)):

                    # var for store graph structure
                    result_detik_news = []

                    # get all data from twitter db
                    detik_data = ResultNewsDetik.objects.filter(history=item).all().order_by('-date_created')[(int(page)-1):(int(page)-1+total_data_per_page)]
                    no=1
                    no = (total_data_per_page * (int(page) - 1)) + 1
                    for detik_news in detik_data:

                        detik_result_news = {
                            'no' : str(int(no)),
                            'title': detik_news.link,
                            'sentiment': "neutral",
                            "created": {
                                "date": str(detik_news.date_news_created.day) + "-" + str(detik_news.date_news_created.month) + "-" + str(
                                    detik_news.date_news_created.year),
                                "time": str(detik_news.date_news_created.hour) + ":" + str(detik_news.date_news_created.minute) + ":" + str(
                                    detik_news.date_news_created.second)
                            },
                            'percentage' : {
                                'positive' : 0,
                                'neutral' : 0,
                                'negative' : 0
                            },
                            'count_sentence' : 0,
                            'url' : detik_news.urls,
                            'img' : detik_news.img
                        }

                        detik_news_count_sentiment_positive = 0
                        detik_news_count_sentiment_neutral = 0
                        detik_news_count_sentiment_negative = 0

                        detik_news_original_content = []
                        detik_news_content = ""
                        count_sentence = 0
                        max_sentence = 2

                        detik_sentences = ResultDetik.objects.filter(result_news_detik = detik_news).order_by('date_created')

                        for detik_sentence in detik_sentences:

                            # count sentiment
                            if (detik_sentence.opinion == 'positive'):
                                detik_news_count_sentiment_positive +=1
                            if (detik_sentence.opinion == 'neutral'):
                                detik_news_count_sentiment_neutral += 1
                            if (detik_sentence.opinion == 'negative'):
                                detik_news_count_sentiment_negative += 1

                            detik_news_original_content.append({
                                "text": detik_sentence.sentence,
                                "sentiment": detik_sentence.opinion,
                                "created": {
                                    "date": str(detik_sentence.date_created.day) + "-" + str(detik_sentence.date_created.month) + "-" + str(detik_sentence.date_created.year),
                                    "time": str(detik_sentence.date_created.hour) + ":" + str(detik_sentence.date_created.minute) + ":" + str(detik_sentence.date_created.second)
                                }
                            })

                            # added sentence
                            if(max_sentence > 0):
                                detik_news_content = detik_news_content+detik_sentence.sentence+". "
                                max_sentence-=1
                            count_sentence+=1

                        if( (detik_news_count_sentiment_positive > detik_news_count_sentiment_neutral) and
                                (detik_news_count_sentiment_positive > detik_news_count_sentiment_negative)):
                            detik_result_news['sentiment'] = 'positive'

                        if ((detik_news_count_sentiment_negative > detik_news_count_sentiment_neutral) and
                                (detik_news_count_sentiment_negative > detik_news_count_sentiment_positive)):
                            detik_result_news['sentiment'] = 'negative'

                        detik_result_news['count_sentence'] = count_sentence
                        detik_result_news['percentage']['positive'] = detik_news_count_sentiment_positive
                        detik_result_news['percentage']['neutral'] = detik_news_count_sentiment_neutral
                        detik_result_news['percentage']['negative'] = detik_news_count_sentiment_negative
                        detik_result_news['five_sentence'] = detik_news_content
                        detik_result_news['all_sentence'] = detik_news_original_content

                        result_detik_news.append(detik_result_news)
                        no+=1


                    # result ={
                    #     "project_id":user_project.id,
                    #     "name": user_project.name,
                    #     "date_created": user_project.date_created,
                    #     "title": "sentiment for news detik"
                    # }


                    result_projects["data"] = result_detik_news
                    result_projects["page"] = {
                        "current": page,
                        "total": total_page,
                        "index": numpy.arange(1, total_page + 1)
                    }
                else:
                    result_projects = {}
            else:

                result_detik_news = []

                # get all data from twitter db
                detik_data = ResultNewsDetik.objects.filter(history=item).all().order_by('-date_created')
                no = 1
                for detik_news in detik_data:

                    detik_result_news = {
                        'no': str(int(no)),
                        'title': detik_news.link,
                        'sentiment': "neutral",
                        "created": {
                            "date": str(detik_news.date_news_created.day) + "-" + str(
                                detik_news.date_news_created.month) + "-" + str(
                                detik_news.date_news_created.year),
                            "time": str(detik_news.date_news_created.hour) + ":" + str(
                                detik_news.date_news_created.minute) + ":" + str(
                                detik_news.date_news_created.second)
                        },
                        'percentage': {
                            'positive': 0,
                            'neutral': 0,
                            'negative': 0
                        },
                        'count_sentence': 0,
                        'url': detik_news.urls,
                        'img': detik_news.img
                    }

                    detik_news_count_sentiment_positive = 0
                    detik_news_count_sentiment_neutral = 0
                    detik_news_count_sentiment_negative = 0

                    detik_news_original_content = []
                    detik_news_content = ""
                    count_sentence = 0
                    max_sentence = 2

                    detik_sentences = ResultDetik.objects.filter(result_news_detik=detik_news).order_by(
                        'date_created')

                    for detik_sentence in detik_sentences:

                        # count sentiment
                        if (detik_sentence.opinion == 'positive'):
                            detik_news_count_sentiment_positive += 1
                        if (detik_sentence.opinion == 'neutral'):
                            detik_news_count_sentiment_neutral += 1
                        if (detik_sentence.opinion == 'negative'):
                            detik_news_count_sentiment_negative += 1

                        detik_news_original_content.append({
                            "text": detik_sentence.sentence,
                            "sentiment": detik_sentence.opinion,
                            "created": {
                                "date": str(detik_sentence.date_created.day) + "-" + str(
                                    detik_sentence.date_created.month) + "-" + str(
                                    detik_sentence.date_created.year),
                                "time": str(detik_sentence.date_created.hour) + ":" + str(
                                    detik_sentence.date_created.minute) + ":" + str(
                                    detik_sentence.date_created.second)
                            }
                        })

                        # added sentence
                        if (max_sentence > 0):
                            detik_news_content = detik_news_content + detik_sentence.sentence + ". "
                            max_sentence -= 1
                        count_sentence += 1

                    if ((detik_news_count_sentiment_positive > detik_news_count_sentiment_neutral) and
                            (detik_news_count_sentiment_positive > detik_news_count_sentiment_negative)):
                        detik_result_news['sentiment'] = 'positive'

                    if ((detik_news_count_sentiment_negative > detik_news_count_sentiment_neutral) and
                            (detik_news_count_sentiment_negative > detik_news_count_sentiment_positive)):
                        detik_result_news['sentiment'] = 'negative'

                    detik_result_news['count_sentence'] = count_sentence
                    detik_result_news['percentage']['positive'] = detik_news_count_sentiment_positive
                    detik_result_news['percentage']['neutral'] = detik_news_count_sentiment_neutral
                    detik_result_news['percentage']['negative'] = detik_news_count_sentiment_negative
                    detik_result_news['five_sentence'] = detik_news_content
                    detik_result_news['all_sentence'] = detik_news_original_content

                    result_detik_news.append(detik_result_news)
                    no += 1

                result_projects["data"] = result_detik_news
                result_projects["page"] = {
                    "current": page,
                    "total": 1,
                    "index": [1]
                }


        return Response(result_projects, status=status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)



@api_view(['GET'])
def result_sentiment_graph_sosmed_twitter_by_project(request, id_project):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()

        # set a result_sentiment_projects
        result_projects = {}

        search_projects = SearchHistory.objects.filter(project = user_project)

        for item in search_projects:

            # preparation for graph result twitter
            twitter_graph_result_sentiment = []


            # get all data from twitter db
            tweets_data = ResultTwitter.objects.filter(history=item).all().order_by('-date_created')
            for tweet in tweets_data:

                twitter_graph_result_sentiment.append({
                    'year': tweet.tweet_created.year,
                    'month': tweet.tweet_created.month,
                    'day': tweet.tweet_created.day,
                    'opinion': tweet.opinion,
                    "like": tweet.favourite_count,
                    "retweet": tweet.retweet_count,
                    "comment": 0,
                })

            # calculate graph
            twitter_graph_result_sentiment_group_by_day = []

            # get date initial from date of project created
            date_now = item.date_created

            for _date in range(1, 8):

                # set initial count
                count_positive = 0
                count_neutral = 0
                count_negative = 0
                count_total = 0

                # set initial reach
                engagements_positive = 0
                engagements_neutral = 0
                engagements_negative = 0
                engagements_total = 0

                for items in twitter_graph_result_sentiment:
                    if ((items['year'] == date_now.year) and (items['month'] == date_now.month) and (items['day'] == date_now.day)):
                        if (items['opinion'] == 'positive'):
                            count_positive += 1
                            engagements_positive = engagements_positive + items['like'] + items['retweet'] + items['comment']
                        if (items['opinion'] == 'neutral'):
                            count_neutral += 1
                            engagements_neutral = engagements_neutral + items['like'] + items['retweet'] + items['comment']
                        if (items['opinion'] == 'negative'):
                            count_negative += 1
                            engagements_negative = engagements_negative + items['like'] + items['retweet'] + items['comment']
                        count_total += 1
                        engagements_total = engagements_total + items['like'] + items['retweet'] + items['comment']

                    # save into graph current day
                twitter_graph_result_sentiment_group_by_day.append({
                    'date': str(date_now.day) + "-" + str(date_now.month) + "-" + str(date_now.year),
                    'sentiment': {
                        'positive': count_positive,
                        'neutral': count_neutral,
                        'negative': count_negative,
                        'total': count_total,
                    },
                    'engagements': {
                        'positive': engagements_positive,
                        'neutral': engagements_neutral,
                        'negative': engagements_negative,
                        'total': engagements_total,
                    }
                })
                # decrease datenow into prev-day
                date_now = date_now - datetime.timedelta(1)

            result_projects['project'] = {
                "id": user_project.id,
                "name": user_project.name,
                "date_created": user_project.date_created,
                "title": "sentiment for sosmed twitter"
            }
            result_projects["graph"] = twitter_graph_result_sentiment_group_by_day


        return Response(result_projects, status=status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def result_sentiment_data_sosmed_twitter_by_project(request,id_project, page, per_page):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()

        # set a result_sentiment_projects
        result_projects = {}

        search_projects = SearchHistory.objects.filter(project = user_project)

        for item in search_projects:

            twitter_result_sentiment = []

            # get all data from twitter db
            total_data = len(ResultTwitter.objects.filter(history=item).all().order_by('-date_created'))

            # set
            pos_page = int(per_page)
            total_page = 10
            total_data_per_page = total_data/total_page

            # get total page
            # total_page = (total_data//pos_page)+(total_data%pos_page)

            if((int(page) <= total_page) and (int(page) >= 1)):
                tweets_data = ResultTwitter.objects.filter(history=item).all().order_by('-retweet_count')[(int(page)-1):(int(page)-1+total_data_per_page)]
                no = (total_data_per_page*(int(page)-1)) +1
                # no = 1
                for tweet in tweets_data:
                    # print(no1)
                    twitter_result_sentiment.append({
                        "no": str(int(no)),
                        "source": "twitter",
                        "user_screen_name": tweet.user_screen_name,
                        "text": tweet.sentence,
                        "sentiment": tweet.opinion,
                        "city": tweet.city,
                        "created": {
                            "date": str(tweet.date_created.day) + "-" + str(tweet.date_created.month) + "-" + str(
                                tweet.date_created.year),
                            "time": str(tweet.date_created.hour) + ":" + str(tweet.date_created.minute) + ":" + str(
                                tweet.date_created.second)
                        },
                        "like": tweet.favourite_count,
                        "retweet": tweet.retweet_count,
                        "comment": 0,
                        "profile_image_url": tweet.profile_image_url,
                        "link_source": "https://twitter.com/" + tweet.user_screen_name + "/status/" + tweet.id_str
                    })
                    no+=1

                result_projects['project'] = {
                    "project_id": user_project.id,
                    "name": user_project.name,
                    "date_created": user_project.date_created,
                    "title": "sentiment for sosmed twitter"
                }
                result_projects["data"] = twitter_result_sentiment
                result_projects["page"] ={
                    "current" : page,
                    "total" : total_page,
                    "index": numpy.arange(1,total_page+1)
                }

            else:
                result_projects = {}

        return Response(result_projects, status=status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def comparation_result_by_two_project(request,id_project_one,id_project_two):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        new_array_project = [id_project_one,id_project_two]
        # user_project = ProjectSentiment.objects.filter(user=user_token, id=int(new_array_project)).first()
        print(new_array_project)

        return Response("two", status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def comparation_result_by_three_project(request,id_project_one,id_project_two,id_project_three):
    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        new_array_project = [id_project_one,id_project_two,id_project_three]
        # user_project = ProjectSentiment.objects.filter(user=user_token, id=int(new_array_project)).first()
        print(new_array_project)

        # calculate for first project

        # calculate for second project


        return Response("three", status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)



# mention analysis page
@api_view(['GET'])
def mention_analysis_by_project(request,id_project):
    if request.method == 'GET':

        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()
        search_history = SearchHistory.objects.filter(project=user_project).first()

        # get tweet
        tweets = ResultTwitter.objects.filter(history=search_history).all()

        ###calculate mention overview section
        ## calculate total mention
        total_mention = 0
        # list of user
        array_of_user = []
        # calculate total like_retweet
        total_like_retweet = 0
        # calculate top mention
        top_mention_tweets = []

        for tweet in tweets:
            if((tweet.retweet_count > 0) or (tweet.favourite_count > 0)):
                total_mention +=1
                total_like_retweet = total_like_retweet + tweet.retweet_count + tweet.favourite_count
                if (str(tweet.user_screen_name) not in array_of_user):
                    array_of_user.append(tweet.user_screen_name)
                #
                # # masukan tweet ke mentiontoptweet
                # if(len(TopMentionTweet.objects.filter(resulttweet = tweet).all()) is 0):
                #     TopMentionTweet.objects.create(
                #         resulttweet = tweet,
                #         retweet_count = tweet.retweet_count,
                #         favourite_count = tweet.favourite_count,
                #         user_screen_name = tweet.user_screen_name,
                #         history = tweet.history
                #     ).save()

        # ambil seluruh Topmention yang paling besar
        res_topmention = TopMentionTweet.objects.extra(
            select={'fieldsum': 'retweet_count + favourite_count'},
            order_by=('-fieldsum',)
        ).filter(history=search_history)

        filter_top_mention = []
        pos = 5
        for x in res_topmention:
            if(str(x.resulttweet.sentence) not in filter_top_mention):
                filter_top_mention.append(str(x.resulttweet.sentence))
                if(pos > 0):
                    top_mention_tweets.append({
                        "text": str(x.resulttweet.sentence),
                        "retweet_count": str(x.retweet_count),
                        "favorite_count": str(x.favourite_count),
                        "link_source": "https://twitter.com/" + str(x.resulttweet.user_screen_name) + "/status/" + str(x.resulttweet.id_str)
                    })
                    pos-=1


        #hitung influencer yang paling sering ngetweet based on TopMentionTweeet
        # res1 = TopMentionTweet.objects.filter(history = search_history).values('user_screen_name').annotate(freq_count=Count('user_screen_name')).order_by("-freq_count")
        # # new_res1 = sorted(res1, key=lambda k: k['freq_count'])

        top_influencer = []
        res1 = TopInfluencerTweet.objects.filter(history = search_history).all().order_by('-total_post')[0:5]

        for x in res1:
            top_influencer.append(
                {
                    "user_screen_name": x.user_screen_name,
                    "user_twitter_link": x.user_twitter_link,
                    "user_photo_link": x.user_photo_link,
                    "total_post": x.total_post,
                    "is_verified" : x.is_verified
                }
            )

        result_structure = {
            "mention_overview" : {
                "total_mention" : str(total_mention),
                "total_user" : str(len(array_of_user)),
                "total_like_retweet" : str(total_like_retweet)
            },
            "top_mention_tweets" :top_mention_tweets,
            "top_influencer" :top_influencer
        }
        # Ada 5 yang dihasilkan:
        # = mention overview:\
        #     Total mention: jumlah seluruh tweet yang ada mentionnya(favorite / retweet)
        #     Total user: jumlah user yg tweet yg ada mentionnya(favorite / retweet)
        #     Like / retweet: total tweet yg ada mentionnya
        # = top mention tweets: ambil n tweet yg paling besar mention nya(base favorite / retweet)
        # = top 5 influencer users: ambil n user yng paling sering mention

        return Response(result_structure, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)



#trends
@api_view(['GET'])
def trend_analysis_by_project(request,id_project):
    if request.method == 'GET':

        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()

        # if(user_project):
        consumer_key = "UsJuIf3Xkz7w6MIUnQpnArLW9"
        consumer_secret = "XXqoia5GVOqQnuEaKzopdhj39b87o2mrficvAH20bqd8CixI4K"
        access_key = "938132848057266176-5dSTpaGTtwq2d8tp8mOlCUD6nVDHNDE"
        access_secret = "UIPvTiD86GZgneRlMHOia1nGYvYkjHreByO8nGzyzUlKW"

        auth = tweepy.OAuthHandler(consumer_key=consumer_key,
                                   consumer_secret=consumer_secret)
        auth.set_access_token(access_key, access_secret)

        twitter = tweepy.API(auth_handler=auth,
                             wait_on_rate_limit=True,
                             wait_on_rate_limit_notify=True)

        # konsepnya cari seluruh kata yang ada didalam data.hitung frekuensinya.kemudian hasil kata
        # setiap dari list frekuensi dicari nilai trend tweetnya dengan tweepy

        # ============================================================
        # get list trends in indonesia
        id_indonesia = 23424846
        result = twitter.trends_place(id = id_indonesia)

        # struktur result[0]
        # ["locations"]
            # ["locations"]["woeid"]
            # ["locations"]["name"]
        # ["as_of"]
        # ["trends"]
            # ["trends"]["promoted_content"]
            # ["trends"]["url"]
            # ["trends"]["tweet_volume"]
            # ["trends"]["name"]
            # ["trends"]["query"]

        trends_list = []
        for x in result[0]["trends"]:
            if(x["tweet_volume"] is not None):
                trends_list.append(
                    {
                        "name" : x["name"],
                        "volume" : x["tweet_volume"]
                    }
                )

        # ============================================================
        # create freq table of tweet
        # preparation data
        list_kalimat = ""

        tweets = ResultTwitter.objects.filter(is_edited = True)[1:10]
        for tweet in tweets:
            list_kalimat=list_kalimat+" "+ tweet.sentence

        # clean the data from freq table using stopwords
        stop_word_id = [")", "yang", "(", '', "rt", '“', ":", ";", "ah", ",", "‘", "’","”", "-", ".", "!", "$", "%", "^", "&", "_", "=", "+", '[', ']', '`', '~', '?', '>', '<', 'ada', 'adalah', 'adanya', 'adapun', 'agak', 'agaknya', 'agar', 'akan', 'akankah', 'akhir', 'akhiri', 'akhirnya', 'aku', 'akulah', 'amat', 'amatlah', 'anda', 'andalah', 'antar', 'antara', 'antaranya', 'apa', 'apaan', 'apabila', 'apakah', 'apalagi', 'apatah', 'artinya', 'asal', 'asalkan', 'atas', 'atau', 'ataukah', 'ataupun', 'awal', 'awalnya', 'bagai', 'bagaikan', 'bagaimana', 'bagaimanakah', 'bagaimanapun', 'bagi', 'bagian', 'bahkan', 'bahwa', 'bahwasanya', 'baik', 'bakal', 'bakalan', 'balik', 'banyak', 'bapak', 'baru', 'bawah', 'beberapa', 'begini', 'beginian', 'beginikah', 'beginilah', 'begitu', 'begitukah', 'begitulah', 'begitupun', 'bekerja', 'belakang', 'belakangan', 'belum', 'belumlah', 'benar', 'benarkah', 'benarlah', 'berada', 'berakhir', 'berakhirlah', 'berakhirnya', 'berapa', 'berapakah', 'berapalah', 'berapapun', 'berarti', 'berawal', 'berbagai', 'berdatangan', 'beri', 'berikan', 'berikut', 'berikutnya', 'berjumlah', 'berkali-kali', 'berkata', 'berkehendak', 'berkeinginan', 'berkenaan', 'berlainan', 'berlalu', 'berlangsung', 'berlebihan', 'bermacam', 'bermacam-macam', 'bermaksud', 'bermula', 'bersama', 'bersama-sama', 'bersiap', 'bersiap-siap', 'bertanya', 'bertanya-tanya', 'berturut', 'berturut-turut', 'bertutur', 'berujar', 'berupa', 'besar', 'betul', 'betulkah', 'biasa', 'biasanya', 'bila', 'bilakah', 'bisa', 'bisakah', 'boleh', 'bolehkah', 'bolehlah', 'buat', 'bukan', 'bukankah', 'bukanlah', 'bukannya', 'bulan', 'bung', 'cara', 'caranya', 'cukup', 'cukupkah', 'cukuplah', 'cuma', 'dahulu', 'dalam', 'dan', 'dapat', 'dari', 'daripada', 'datang', 'dekat', 'demi', 'demikian', 'demikianlah', 'dengan', 'depan', 'di', 'dia', 'diakhiri', 'diakhirinya', 'dialah', 'diantara', 'diantaranya', 'diberi', 'diberikan', 'diberikannya', 'dibuat', 'dibuatnya', 'didapat', 'didatangkan', 'digunakan', 'diibaratkan', 'diibaratkannya', 'diingat', 'diingatkan', 'diinginkan', 'dijawab', 'dijelaskan', 'dijelaskannya', 'dikarenakan', 'dikatakan', 'dikatakannya', 'dikerjakan', 'diketahui', 'diketahuinya', 'dikira', 'dilakukan', 'dilalui', 'dilihat', 'dimaksud', 'dimaksudkan', 'dimaksudkannya', 'dimaksudnya', 'diminta', 'dimintai', 'dimisalkan', 'dimulai', 'dimulailah', 'dimulainya', 'dimungkinkan', 'dini', 'dipastikan', 'diperbuat', 'diperbuatnya', 'dipergunakan', 'diperkirakan', 'diperlihatkan', 'diperlukan', 'diperlukannya', 'dipersoalkan', 'dipertanyakan', 'dipunyai', 'diri', 'dirinya', 'disampaikan', 'disebut', 'disebutkan', 'disebutkannya', 'disini', 'disinilah', 'ditambahkan', 'ditandaskan', 'ditanya', 'ditanyai', 'ditanyakan', 'ditegaskan', 'ditujukan', 'ditunjuk', 'ditunjuki', 'ditunjukkan', 'ditunjukkannya', 'ditunjuknya', 'dituturkan', 'dituturkannya', 'diucapkan', 'diucapkannya', 'diungkapkan', 'dong', 'dua', 'dulu', 'empat', 'enggak', 'enggaknya', 'entah', 'entahlah', 'guna', 'gunakan', 'hal', 'hampir', 'hanya', 'hanyalah', 'hari', 'harus', 'haruslah', 'harusnya', 'hendak', 'hendaklah', 'hendaknya', 'hingga', 'ia', 'ialah', 'ibarat', 'ibaratkan', 'ibaratnya', 'ibu', 'ikut', 'ingat', 'ingat-ingat', 'ingin', 'inginkah', 'inginkan', 'ini', 'inikah', 'inilah', 'itu', 'itukah', 'itulah', 'jadi', 'jadilah', 'jadinya', 'jangan', 'jangankan', 'janganlah', 'jauh', 'jawab', 'jawaban', 'jawabnya', 'jelas', 'jelaskan', 'jelaslah', 'jelasnya', 'jika', 'jikalau', 'juga', 'jumlah', 'jumlahnya', 'justru', 'kala', 'kalau', 'kalaulah', 'kalaupun', 'kalian', 'kami', 'kamilah', 'kamu', 'kamulah', 'kan', 'kapan', 'kapankah', 'kapanpun', 'karena', 'karenanya', 'kasus', 'kata', 'katakan', 'katakanlah', 'katanya', 'ke', 'keadaan', 'kebetulan', 'kecil', 'kedua', 'keduanya', 'keinginan', 'kelamaan', 'kelihatan', 'kelihatannya', 'kelima', 'keluar', 'kembali', 'kemudian', 'kemungkinan', 'kemungkinannya', 'kenapa', 'kepada', 'kepadanya', 'kesampaian', 'keseluruhan', 'keseluruhannya', 'keterlaluan', 'ketika', 'khususnya', 'kini', 'kinilah', 'kira', 'kira-kira', 'kiranya', 'kita', 'kitalah', 'kok', 'kurang', 'lagi', 'lagian', 'lah', 'lain', 'lainnya', 'lalu', 'lama', 'lamanya', 'lanjut', 'lanjutnya', 'lebih', 'lewat', 'lima','luar', 'macam', 'maka', 'makanya', 'makin', 'malah', 'malahan', 'mampu', 'mampukah', 'mana', 'manakala', 'manalagi', 'masa', 'masalah', 'masalahnya', 'masih', 'masihkah', 'masing', 'masing-masing', 'mau', 'maupun', 'melainkan', 'melakukan', 'melalui', 'melihat', 'melihatnya', 'memang', 'memastikan', 'memberi', 'memberikan', 'membuat', 'memerlukan', 'memihak', 'meminta', 'memintakan', 'memisalkan', 'memperbuat', 'mempergunakan', 'memperkirakan', 'memperlihatkan', 'mempersiapkan', 'mempersoalkan', 'mempertanyakan', 'mempunyai', 'memulai', 'memungkinkan', 'menaiki', 'menambahkan', 'menandaskan', 'menanti', 'menanti-nanti', 'menantikan', 'menanya', 'menanyai', 'menanyakan', 'mendapat', 'mendapatkan', 'mendatang', 'mendatangi', 'mendatangkan', 'menegaskan', 'mengakhiri', 'mengapa', 'mengatakan', 'mengatakannya', 'mengenai', 'mengerjakan', 'mengetahui', 'menggunakan', 'menghendaki', 'mengibaratkan', 'mengibaratkannya', 'mengingat', 'mengingatkan', 'menginginkan', 'mengira', 'mengucapkan', 'mengucapkannya', 'mengungkapkan', 'menjadi', 'menjawab', 'menjelaskan', 'menuju', 'menunjuk', 'menunjuki', 'menunjukkan', 'menunjuknya', 'menurut', 'menuturkan', 'menyampaikan', 'menyangkut', 'menyatakan', 'menyebutkan', 'menyeluruh', 'menyiapkan', 'merasa', 'mereka', 'merekalah', 'merupakan', 'meski', 'meskipun', 'meyakini', 'meyakinkan', 'minta', 'mirip', 'misal', 'misalkan', 'misalnya', 'mula', 'mulai', 'mulailah', 'mulanya', 'mungkin', 'mungkinkah', 'nah', 'naik', 'namun', 'nanti', 'nantinya', 'nyaris', 'nyatanya', 'oleh', 'olehnya', 'pada', 'padahal', 'padanya', 'pak', 'paling', 'panjang', 'pantas', 'para', 'pasti', 'pastilah', 'penting', 'pentingnya', 'per', 'percuma', 'perlu', 'perlukah', 'perlunya', 'pernah', 'persoalan', 'pertama', 'pertama-tama', 'pertanyaan', 'pertanyakan', 'pihak', 'pihaknya', 'pukul', 'pula', 'pun', 'punya', 'rasa', 'rasanya', 'rata', 'rupanya', 'saat', 'saatnya', 'saja', 'sajalah', 'saling', 'sama', 'sama-sama', 'sambil', 'sampai', 'sampai-sampai', 'sampaikan', 'sana', 'sangat', 'sangatlah', 'satu', 'saya', 'sayalah', 'se', 'sebab', 'sebabnya', 'sebagai', 'sebagaimana', 'sebagainya', 'sebagian', 'sebaik', 'sebaik-baiknya', 'sebaiknya', 'sebaliknya', 'sebanyak', 'sebegini', 'sebegitu', 'sebelum', 'sebelumnya', 'sebenarnya', 'seberapa', 'sebesar', 'sebetulnya', 'sebisanya', 'sebuah', 'sebut', 'sebutlah', 'sebutnya', 'secara', 'secukupnya', 'sedang', 'sedangkan', 'sedemikian', 'sedikit', 'sedikitnya', 'seenaknya', 'segala', 'segalanya', 'segera', 'seharusnya', 'sehingga', 'seingat', 'sejak', 'sejauh', 'sejenak', 'sejumlah', 'sekadar', 'sekadarnya', 'sekali', 'sekali-kali', 'sekalian', 'sekaligus', 'sekalipun', 'sekarang', 'sekarang', 'sekecil', 'seketika', 'sekiranya', 'sekitar', 'sekitarnya', 'sekurang-kurangnya', 'sekurangnya', 'sela', 'selain', 'selaku', 'selalu', 'selama', 'selama-lamanya', 'selamanya', 'selanjutnya', 'seluruh', 'seluruhnya', 'semacam', 'semakin', 'semampu', 'semampunya', 'semasa', 'semasih', 'semata', 'semata-mata', 'semaunya', 'sementara', 'semisal', 'semisalnya', 'sempat', 'semua', 'semuanya', 'semula', 'sendiri', 'sendirian', 'sendirinya', 'seolah', 'seolah-olah', 'seorang', 'sepanjang', 'sepantasnya', 'sepantasnyalah', 'seperlunya', 'seperti', 'sepertinya', 'sepihak', 'sering', 'seringnya', 'serta', 'serupa', 'sesaat', 'sesama', 'sesampai', 'sesegera', 'sesekali', 'seseorang', 'sesuatu', 'sesuatunya', 'sesudah', 'sesudahnya', 'setelah', 'setempat', 'setengah', 'seterusnya', 'setiap', 'setiba', 'setibanya', 'setidak-tidaknya', 'setidaknya', 'setinggi', 'seusai', 'sewaktu', 'siap', 'siapa', 'siapakah', 'siapapun', 'sini', 'sinilah', 'soal', 'soalnya', 'suatu', 'sudah', 'sudahkah', 'sudahlah', 'supaya', 'tadi', 'tadinya', 'tahu', 'tahun', 'tak', 'tambah', 'tambahnya', 'tampak', 'tampaknya', 'tandas', 'tandasnya', 'tanpa', 'tanya', 'tanyakan', 'tanyanya', 'tapi', 'tegas', 'tegasnya', 'telah', 'tempat', 'tengah', 'tentang', 'tentu', 'tentulah', 'tentunya', 'tepat', 'terakhir', 'terasa', 'terbanyak', 'terdahulu', 'terdapat', 'terdiri', 'terhadap', 'terhadapnya', 'teringat', 'teringat-ingat', 'terjadi', 'terjadilah', 'terjadinya', 'terkira', 'terlalu', 'terlebih', 'terlihat', 'termasuk', 'ternyata', 'tersampaikan', 'tersebut', 'tersebutlah', 'tertentu', 'tertuju', 'terus', 'terutama', 'tetap', 'tetapi', 'tiap', 'tiba', 'tiba-tiba', 'tidak', 'tidakkah', 'tidaklah', 'tiga', 'tinggi', 'toh', 'tunjuk', 'turut', 'tutur', 'tuturnya', 'ucap', 'ucapnya', 'ujar', 'ujarnya', 'umum', 'umumnya', 'ungkap', 'ungkapnya', 'untuk', 'usah', 'usai', 'waduh', 'wah', 'wahai', 'waktu', 'waktunya', 'walau', 'walaupun', 'wong', 'yaitu', 'yakin', 'yakni', 'ya', 'RT',"…"]
        word2 = TweetTokenizer(strip_handles=True, reduce_len=True).tokenize(list_kalimat.lower())
        filtered_sentence = [w for w in word2 if not w in stop_word_id]

        # split_kalimat = list_kalimat.split(" ")
        wordlist = nltk.probability.FreqDist(filtered_sentence)
        ten_wordlist = wordlist.most_common(n=10)

        # ============================================================
        # check data in freq table
        for kata in ten_wordlist:
            for trend in trends_list:
                if(trend["name"].find(kata[0]) is not -1):
                    print("ada "+str(kata[0])+" di trending : "+ trend["name"])



        return Response(trends_list,status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)


#hastag analysis
@api_view(['GET'])
def hastag_analysis_by_project(request,id_project):
    if request.method == 'GET':

        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()
        search_history = SearchHistory.objects.filter(project=user_project).first()
        # print(search_history.query)
        tweets = ResultTwitter.objects.filter(history=search_history).all()

        # ============================================================
        # create freq table of tweet
        # preparation data
        list_kalimat = ""

        # tweets = ResultTwitter.objects.filter(is_edited = True)[1:10]
        for tweet in tweets:
            list_kalimat=list_kalimat+" "+ tweet.sentence

        # clean the data from freq table using stopwords
        stop_word_id = ["...", "yg","1","2","3","4","5","6","7","8","9","10",")", "\"", "yang", "(",  "",'', "rt", '“', ":", ";", "ah", ",", "‘", "’","”", "-", ".", "!", "$", "%", "^", "&", "_", "=", "+", '[', ']', '`', '~', '?', '>', '<', 'ada', 'adalah', 'adanya', 'adapun', 'agak', 'agaknya', 'agar', 'akan', 'akankah', 'akhir', 'akhiri', 'akhirnya', 'aku', 'akulah', 'amat', 'amatlah', 'anda', 'andalah', 'antar', 'antara', 'antaranya', 'apa', 'apaan', 'apabila', 'apakah', 'apalagi', 'apatah', 'artinya', 'asal', 'asalkan', 'atas', 'atau', 'ataukah', 'ataupun', 'awal', 'awalnya', 'bagai', 'bagaikan', 'bagaimana', 'bagaimanakah', 'bagaimanapun', 'bagi', 'bagian', 'bahkan', 'bahwa', 'bahwasanya', 'baik', 'bakal', 'bakalan', 'balik', 'banyak', 'bapak', 'baru', 'bawah', 'beberapa', 'begini', 'beginian', 'beginikah', 'beginilah', 'begitu', 'begitukah', 'begitulah', 'begitupun', 'bekerja', 'belakang', 'belakangan', 'belum', 'belumlah', 'benar', 'benarkah', 'benarlah', 'berada', 'berakhir', 'berakhirlah', 'berakhirnya', 'berapa', 'berapakah', 'berapalah', 'berapapun', 'berarti', 'berawal', 'berbagai', 'berdatangan', 'beri', 'berikan', 'berikut', 'berikutnya', 'berjumlah', 'berkali-kali', 'berkata', 'berkehendak', 'berkeinginan', 'berkenaan', 'berlainan', 'berlalu', 'berlangsung', 'berlebihan', 'bermacam', 'bermacam-macam', 'bermaksud', 'bermula', 'bersama', 'bersama-sama', 'bersiap', 'bersiap-siap', 'bertanya', 'bertanya-tanya', 'berturut', 'berturut-turut', 'bertutur', 'berujar', 'berupa', 'besar', 'betul', 'betulkah', 'biasa', 'biasanya', 'bila', 'bilakah', 'bisa', 'bisakah', 'boleh', 'bolehkah', 'bolehlah', 'buat', 'bukan', 'bukankah', 'bukanlah', 'bukannya', 'bulan', 'bung', 'cara', 'caranya', 'cukup', 'cukupkah', 'cukuplah', 'cuma', 'dahulu', 'dalam', 'dan', 'dapat', 'dari', 'daripada', 'datang', 'dekat', 'demi', 'demikian', 'demikianlah', 'dengan', 'depan', 'di', 'dia', 'diakhiri', 'diakhirinya', 'dialah', 'diantara', 'diantaranya', 'diberi', 'diberikan', 'diberikannya', 'dibuat', 'dibuatnya', 'didapat', 'didatangkan', 'digunakan', 'diibaratkan', 'diibaratkannya', 'diingat', 'diingatkan', 'diinginkan', 'dijawab', 'dijelaskan', 'dijelaskannya', 'dikarenakan', 'dikatakan', 'dikatakannya', 'dikerjakan', 'diketahui', 'diketahuinya', 'dikira', 'dilakukan', 'dilalui', 'dilihat', 'dimaksud', 'dimaksudkan', 'dimaksudkannya', 'dimaksudnya', 'diminta', 'dimintai', 'dimisalkan', 'dimulai', 'dimulailah', 'dimulainya', 'dimungkinkan', 'dini', 'dipastikan', 'diperbuat', 'diperbuatnya', 'dipergunakan', 'diperkirakan', 'diperlihatkan', 'diperlukan', 'diperlukannya', 'dipersoalkan', 'dipertanyakan', 'dipunyai', 'diri', 'dirinya', 'disampaikan', 'disebut', 'disebutkan', 'disebutkannya', 'disini', 'disinilah', 'ditambahkan', 'ditandaskan', 'ditanya', 'ditanyai', 'ditanyakan', 'ditegaskan', 'ditujukan', 'ditunjuk', 'ditunjuki', 'ditunjukkan', 'ditunjukkannya', 'ditunjuknya', 'dituturkan', 'dituturkannya', 'diucapkan', 'diucapkannya', 'diungkapkan', 'dong', 'dua', 'dulu', 'empat', 'enggak', 'enggaknya', 'entah', 'entahlah', 'guna', 'gunakan', 'hal', 'hampir', 'hanya', 'hanyalah', 'hari', 'harus', 'haruslah', 'harusnya', 'hendak', 'hendaklah', 'hendaknya', 'hingga', 'ia', 'ialah', 'ibarat', 'ibaratkan', 'ibaratnya', 'ibu', 'ikut', 'ingat', 'ingat-ingat', 'ingin', 'inginkah', 'inginkan', 'ini', 'inikah', 'inilah', 'itu', 'itukah', 'itulah', 'jadi', 'jadilah', 'jadinya', 'jangan', 'jangankan', 'janganlah', 'jauh', 'jawab', 'jawaban', 'jawabnya', 'jelas', 'jelaskan', 'jelaslah', 'jelasnya', 'jika', 'jikalau', 'juga', 'jumlah', 'jumlahnya', 'justru', 'kala', 'kalau', 'kalaulah', 'kalaupun', 'kalian', 'kami', 'kamilah', 'kamu', 'kamulah', 'kan', 'kapan', 'kapankah', 'kapanpun', 'karena', 'karenanya', 'kasus', 'kata', 'katakan', 'katakanlah', 'katanya', 'ke', 'keadaan', 'kebetulan', 'kecil', 'kedua', 'keduanya', 'keinginan', 'kelamaan', 'kelihatan', 'kelihatannya', 'kelima', 'keluar', 'kembali', 'kemudian', 'kemungkinan', 'kemungkinannya', 'kenapa', 'kepada', 'kepadanya', 'kesampaian', 'keseluruhan', 'keseluruhannya', 'keterlaluan', 'ketika', 'khususnya', 'kini', 'kinilah', 'kira', 'kira-kira', 'kiranya', 'kita', 'kitalah', 'kok', 'kurang', 'lagi', 'lagian', 'lah', 'lain', 'lainnya', 'lalu', 'lama', 'lamanya', 'lanjut', 'lanjutnya', 'lebih', 'lewat', 'lima','luar', 'macam', 'maka', 'makanya', 'makin', 'malah', 'malahan', 'mampu', 'mampukah', 'mana', 'manakala', 'manalagi', 'masa', 'masalah', 'masalahnya', 'masih', 'masihkah', 'masing', 'masing-masing', 'mau', 'maupun', 'melainkan', 'melakukan', 'melalui', 'melihat', 'melihatnya', 'memang', 'memastikan', 'memberi', 'memberikan', 'membuat', 'memerlukan', 'memihak', 'meminta', 'memintakan', 'memisalkan', 'memperbuat', 'mempergunakan', 'memperkirakan', 'memperlihatkan', 'mempersiapkan', 'mempersoalkan', 'mempertanyakan', 'mempunyai', 'memulai', 'memungkinkan', 'menaiki', 'menambahkan', 'menandaskan', 'menanti', 'menanti-nanti', 'menantikan', 'menanya', 'menanyai', 'menanyakan', 'mendapat', 'mendapatkan', 'mendatang', 'mendatangi', 'mendatangkan', 'menegaskan', 'mengakhiri', 'mengapa', 'mengatakan', 'mengatakannya', 'mengenai', 'mengerjakan', 'mengetahui', 'menggunakan', 'menghendaki', 'mengibaratkan', 'mengibaratkannya', 'mengingat', 'mengingatkan', 'menginginkan', 'mengira', 'mengucapkan', 'mengucapkannya', 'mengungkapkan', 'menjadi', 'menjawab', 'menjelaskan', 'menuju', 'menunjuk', 'menunjuki', 'menunjukkan', 'menunjuknya', 'menurut', 'menuturkan', 'menyampaikan', 'menyangkut', 'menyatakan', 'menyebutkan', 'menyeluruh', 'menyiapkan', 'merasa', 'mereka', 'merekalah', 'merupakan', 'meski', 'meskipun', 'meyakini', 'meyakinkan', 'minta', 'mirip', 'misal', 'misalkan', 'misalnya', 'mula', 'mulai', 'mulailah', 'mulanya', 'mungkin', 'mungkinkah', 'nah', 'naik', 'namun', 'nanti', 'nantinya', 'nyaris', 'nyatanya', 'oleh', 'olehnya', 'pada', 'padahal', 'padanya', 'pak', 'paling', 'panjang', 'pantas', 'para', 'pasti', 'pastilah', 'penting', 'pentingnya', 'per', 'percuma', 'perlu', 'perlukah', 'perlunya', 'pernah', 'persoalan', 'pertama', 'pertama-tama', 'pertanyaan', 'pertanyakan', 'pihak', 'pihaknya', 'pukul', 'pula', 'pun', 'punya', 'rasa', 'rasanya', 'rata', 'rupanya', 'saat', 'saatnya', 'saja', 'sajalah', 'saling', 'sama', 'sama-sama', 'sambil', 'sampai', 'sampai-sampai', 'sampaikan', 'sana', 'sangat', 'sangatlah', 'satu', 'saya', 'sayalah', 'se', 'sebab', 'sebabnya', 'sebagai', 'sebagaimana', 'sebagainya', 'sebagian', 'sebaik', 'sebaik-baiknya', 'sebaiknya', 'sebaliknya', 'sebanyak', 'sebegini', 'sebegitu', 'sebelum', 'sebelumnya', 'sebenarnya', 'seberapa', 'sebesar', 'sebetulnya', 'sebisanya', 'sebuah', 'sebut', 'sebutlah', 'sebutnya', 'secara', 'secukupnya', 'sedang', 'sedangkan', 'sedemikian', 'sedikit', 'sedikitnya', 'seenaknya', 'segala', 'segalanya', 'segera', 'seharusnya', 'sehingga', 'seingat', 'sejak', 'sejauh', 'sejenak', 'sejumlah', 'sekadar', 'sekadarnya', 'sekali', 'sekali-kali', 'sekalian', 'sekaligus', 'sekalipun', 'sekarang', 'sekarang', 'sekecil', 'seketika', 'sekiranya', 'sekitar', 'sekitarnya', 'sekurang-kurangnya', 'sekurangnya', 'sela', 'selain', 'selaku', 'selalu', 'selama', 'selama-lamanya', 'selamanya', 'selanjutnya', 'seluruh', 'seluruhnya', 'semacam', 'semakin', 'semampu', 'semampunya', 'semasa', 'semasih', 'semata', 'semata-mata', 'semaunya', 'sementara', 'semisal', 'semisalnya', 'sempat', 'semua', 'semuanya', 'semula', 'sendiri', 'sendirian', 'sendirinya', 'seolah', 'seolah-olah', 'seorang', 'sepanjang', 'sepantasnya', 'sepantasnyalah', 'seperlunya', 'seperti', 'sepertinya', 'sepihak', 'sering', 'seringnya', 'serta', 'serupa', 'sesaat', 'sesama', 'sesampai', 'sesegera', 'sesekali', 'seseorang', 'sesuatu', 'sesuatunya', 'sesudah', 'sesudahnya', 'setelah', 'setempat', 'setengah', 'seterusnya', 'setiap', 'setiba', 'setibanya', 'setidak-tidaknya', 'setidaknya', 'setinggi', 'seusai', 'sewaktu', 'siap', 'siapa', 'siapakah', 'siapapun', 'sini', 'sinilah', 'soal', 'soalnya', 'suatu', 'sudah', 'sudahkah', 'sudahlah', 'supaya', 'tadi', 'tadinya', 'tahu', 'tahun', 'tak', 'tambah', 'tambahnya', 'tampak', 'tampaknya', 'tandas', 'tandasnya', 'tanpa', 'tanya', 'tanyakan', 'tanyanya', 'tapi', 'tegas', 'tegasnya', 'telah', 'tempat', 'tengah', 'tentang', 'tentu', 'tentulah', 'tentunya', 'tepat', 'terakhir', 'terasa', 'terbanyak', 'terdahulu', 'terdapat', 'terdiri', 'terhadap', 'terhadapnya', 'teringat', 'teringat-ingat', 'terjadi', 'terjadilah', 'terjadinya', 'terkira', 'terlalu', 'terlebih', 'terlihat', 'termasuk', 'ternyata', 'tersampaikan', 'tersebut', 'tersebutlah', 'tertentu', 'tertuju', 'terus', 'terutama', 'tetap', 'tetapi', 'tiap', 'tiba', 'tiba-tiba', 'tidak', 'tidakkah', 'tidaklah', 'tiga', 'tinggi', 'toh', 'tunjuk', 'turut', 'tutur', 'tuturnya', 'ucap', 'ucapnya', 'ujar', 'ujarnya', 'umum', 'umumnya', 'ungkap', 'ungkapnya', 'untuk', 'usah', 'usai', 'waduh', 'wah', 'wahai', 'waktu', 'waktunya', 'walau', 'walaupun', 'wong', 'yaitu', 'yakin', 'yakni', 'ya', 'RT',"…"]
        word2 = TweetTokenizer(strip_handles=True, reduce_len=True).tokenize(list_kalimat.lower())
        filtered_sentence = [w for w in word2 if not w in stop_word_id]

        # split_kalimat = list_kalimat.split(" ")
        wordlist = nltk.probability.FreqDist(filtered_sentence)
        twenty_wordlist = wordlist.most_common(n=50)
        trend_list = []
        max_freq = twenty_wordlist[0][1]
        max_font = 10

        for x in twenty_wordlist:

            if(len(str(x[0])) > 2 and ((str(x[0])).find("http") == -1 )):
                font_size = (max_font*x[1])/max_freq
                trend_list.append(
                    {
                        "text": str(x[0]),
                        "weight" : round(font_size)+1
                    }
                )

        return Response(trend_list,status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)


#influencer analysis
@api_view(['GET'])
def influencer_analysis_by_project(request,id_project):

    if request.method == 'GET':

        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()
        search_history = SearchHistory.objects.filter(project=user_project).first()

        # set tweepy config
        # if(user_project):
        consumer_key = "UsJuIf3Xkz7w6MIUnQpnArLW9"
        consumer_secret = "XXqoia5GVOqQnuEaKzopdhj39b87o2mrficvAH20bqd8CixI4K"
        access_key = "938132848057266176-5dSTpaGTtwq2d8tp8mOlCUD6nVDHNDE"
        access_secret = "UIPvTiD86GZgneRlMHOia1nGYvYkjHreByO8nGzyzUlKW"

        auth = tweepy.OAuthHandler(consumer_key=consumer_key, consumer_secret=consumer_secret)
        auth.set_access_token(access_key, access_secret)
        twitter = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)


        # hitung influencer yang paling sering ngetweet based on TopMentionTweeet
        res1 = TopMentionTweet.objects.filter(history=search_history).values('user_screen_name').annotate(
            freq_count=Count('user_screen_name')).order_by("-freq_count")
        top_influencer = res1
        # for x in res1:
        #     try:
        #         y = twitter.get_user(screen_name=str(x['user_screen_name']))
        #         top_influencer.append(
        #             {
        #                 "user_screen_name": str(x['user_screen_name']),
        #                 "user_twitter_link": "https://twitter.com/" + str(x['user_screen_name']),
        #                 "user_photo_link": str(y.profile_image_url),
        #                 "total_post": str(x['freq_count']),
        #                 "user_name" : str(y.name),
        #                 "num_of_follower" : str(y.followers_count),
        #                 "num_of_posts" : str(y.statuses_count),
        #                 "num_of_friend" : str(y.friends_count),
        #                 "num_of_favorites" : str(y.favourites_count),
        #                 "is_verified" : str(y.verified)
        #             },
        #         )
        #     except:
        #         print(str(x['user_screen_name']))

        result_structure = top_influencer

        return Response(result_structure, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)


#influencer analysis
@api_view(['GET'])
def location_analysis_by_project(request,id_project):

    if request.method == 'GET':

        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()
        search_history = SearchHistory.objects.filter(project=user_project).first()

        # # set tweepy config
        # # if(user_project):
        # consumer_key = "UsJuIf3Xkz7w6MIUnQpnArLW9"
        # consumer_secret = "XXqoia5GVOqQnuEaKzopdhj39b87o2mrficvAH20bqd8CixI4K"
        # access_key = "938132848057266176-5dSTpaGTtwq2d8tp8mOlCUD6nVDHNDE"
        # access_secret = "UIPvTiD86GZgneRlMHOia1nGYvYkjHreByO8nGzyzUlKW"
        #
        # auth = tweepy.OAuthHandler(consumer_key=consumer_key, consumer_secret=consumer_secret)
        # auth.set_access_token(access_key, access_secret)
        # twitter = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

        # hitung influencer yang paling sering ngetweet based on TopMentionTweeet
        res = ResultTwitter.objects.filter(history = search_history).all()
        res1 = res.values_list("id_str").distinct()
        all_location = []
        for x in res1:
            selected_id_str = x[0]
            get_tweet = ResultTwitter.objects.filter(id_str=selected_id_str).first()
            if(get_tweet.location is not None):
                all_location.append(
                    {
                        "marker_text": str(get_tweet.sentence),
                        "img_url": str(get_tweet.profile_image_url),
                        "location": get_tweet.location,
                        "lng": get_tweet.lng,
                        "lat": get_tweet.lat,
                        "link_source": "https://twitter.com/" + get_tweet.user_screen_name + "/status/" + selected_id_str,
                        "draggable": "false"
                    }
                )

        # hitung frekuensinya
        res2 = res.values('location').annotate(freq_count=Count('location')).order_by("-freq_count")

        # geolocator = Nominatim()
        # geolocator_google = GoogleV3()
        # max_status = 20
        #
        # for x in res1:
        #     if(max_status > 0):
        #         max_status-=1
        #         id_status = str(x[0])
        #         print(id_status)
        #         try:
        #             # get location
        #             detail_status = twitter.get_status(id_status)
        #             lokasi = ""
        #             longitude = ""
        #             latitude = ""
        #             if(detail_status.user.location is not None):
        #                 lokasi = detail_status.user.location
        #             elif(detail_status.extended_entities["media"][0]["additional_media_info"]["source_user"]["location"] is not None):
        #                 lokasi = detail_status.extended_entities["media"][0]["additional_media_info"]["source_user"]["location"]
        #             else:
        #                 lokasi = ""
        #
        #             # get coordinate
        #             location = ""
        #             try:
        #                 location = geolocator_google.geocode(lokasi)
        #                 longitude = str(location.longitude)
        #                 latitude = str(location.latitude)
        #             except:
        #                 try:
        #                     location = geolocator.geocode(lokasi)
        #                     longitude = str(location.longitude)
        #                     latitude = str(location.latitude)
        #                 except:
        #                     print("lokasi tidak ditemukan")
        #
        #             all_location.append({
        #                 "marker_text" : str(ResultTwitter.objects.filter(id_str = id_status).first().sentence),
        #                 "img_url": str(ResultTwitter.objects.filter(id_str = id_status).first().profile_image_url),
        #                 "location" : lokasi,
        #                 "lng" : longitude,
        #                 "lat" : latitude,
        #                 "draggable": "false"
        #             })
        #         except:
        #             print("status of "+id_status+" isnt valid / deleted")

        result_structure = {
            "lokasi" : all_location,
            "freq" : res2[0:5]
        }

        # result_structure = [
        #     {
        #         "lat": -6.17511,
        #         "lng": 106.8650395,
        #         # "label": "Airlangga Perintahkan Kader Golkar Dukung Jokowi di Pilpres 2019 https://t.co/xxXkcu6csk https://t.co/y1MXI7EOQG",
        #         "img_url" : 'http://pbs.twimg.com/profile_images/939314627489406977/KXOMtk-B_normal.jpg',
        #         "location" : "Jakarta Indonesia",
        #         "marker_text": "Airlangga Perintahkan Kader Golkar Dukung Jokowi di Pilpres 2019 https://t.co/xxXkcu6csk https://t.co/y1MXI7EOQG",
        #         "draggable" : "false"
        #     },
        #     {
        #         "lat": -6.17511,
        #         "lng": 106.8650395,
        #         # "text": "@jokowi kirim pasukan pak. Gempur israel",
        #         "img_url": 'http://pbs.twimg.com/profile_images/939314627489406977/KXOMtk-B_normal.jpg',
        #         "location": "jakarta",
        #         "marker_text": "@jokowi kirim pasukan pak. Gempur israel",
        #         "draggable": "false"
        #     },
        #     {
        #         "lat": -6.1346522,
        #         "lng": 106.812262,
        #         # "text": "RT @wahhabicc_jabar: Wahai Bani Toa Kopar-kapir\r\nWahai Bani Toa PKI-PKI\r\nWahai Bani Toa Osang Asing\r\nWahai Bani Toa Penyinyir Sejati!\r\n\r\nLihat!!…",
        #         "img_url": 'http://pbs.twimg.com/profile_images/939314627489406977/KXOMtk-B_normal.jpg',
        #         "location" : "Kota tua jakarta barat",
        #         "marker_text": "RT @wahhabicc_jabar: Wahai Bani Toa Kopar-kapir\r\nWahai Bani Toa PKI-PKI\r\nWahai Bani Toa Osang Asing\r\nWahai Bani Toa Penyinyir Sejati!\r\n\r\nLihat!!…",
        #         "draggable": "false"
        #     },
        #     {
        #         "lat": -6.2614927,
        #         "lng": 106.8105998,
        #         # "text": "RT @wahhabicc_jabar: Wahai Bani Toa Kopar-kapir\r\nWahai Bani Toa PKI-PKI\r\nWahai Bani Toa Osang Asing\r\nWahai Bani Toa Penyinyir Sejati!\r\n\r\nLihat!!…",
        #         "img_url": 'http://pbs.twimg.com/profile_images/939314627489406977/KXOMtk-B_normal.jpg',
        #         "location" : "Jakarta Selatan, DKI Jakarta",
        #         "marker_text": "RT @wahhabicc_jabar: Wahai Bani Toa Kopar-kapir\r\nWahai Bani Toa PKI-PKI\r\nWahai Bani Toa Osang Asing\r\nWahai Bani Toa Penyinyir Sejati!\r\n\r\nLihat!!…" ,
        #         "draggable": "false"
        #     },
        #     {
        #         "lat": 48.2081743,
        #         "lng": 16.3738189,
        #         # "text": "RT @Metro_TV: Sikap Jokowi Terhadap Palestina Diapresiasi 57 Negara OKI https://t.co/Ui03eQ8XWU https://t.co/xixYAvhm5L",
        #         "img_url": 'http://pbs.twimg.com/profile_images/939314627489406977/KXOMtk-B_normal.jpg',
        #         "location": "Vienna, Austria",
        #         "marker_text": "RT @Metro_TV: Sikap Jokowi Terhadap Palestina Diapresiasi 57 Negara OKI https://t.co/Ui03eQ8XWU https://t.co/xixYAvhm5L",
        #         "draggable": "false"
        #     },
        #     {
        #         "lat": -7.0051453,
        #         "lng": 110.4381254,
        #         # "text": "@jokowi@\" Bagaimana  Desa quuu Bisa Maju Dan Berkembang / Bila Jalan Infrakstruktur Seperti Ini / Pada Hal Perekono… https://t.co/OKw9ESiUuS",
        #         "img_url": 'http://pbs.twimg.com/profile_images/939314627489406977/KXOMtk-B_normal.jpg',
        #         "location": "Semarang, Jawa Tengah",
        #         "marker_text": "@jokowi@\" Bagaimana  Desa quuu Bisa Maju Dan Berkembang / Bila Jalan Infrakstruktur Seperti Ini / Pada Hal Perekono… https://t.co/OKw9ESiUuS",
        #         "draggable": "false"
        #     },
        #     {
        #         "lat": -0.789275,
        #         "lng": 113.921327,
        #         # "text": "RT @BadjaNuswantara: Demo Palestina serukan khilafah? Mereka tidak peduli kemanusiaan. Yg dipikirkan hanya kepentingan grombolannya. Eksplo…",
        #         "img_url": 'http://pbs.twimg.com/profile_images/939314627489406977/KXOMtk-B_normal.jpg',
        #         "location": "Indonesia",
        #         "marker_text": "RT @BadjaNuswantara: Demo Palestina serukan khilafah? Mereka tidak peduli kemanusiaan. Yg dipikirkan hanya kepentingan grombolannya. Eksplo…",
        #         "draggable": "false"
        #     },
        #     {
        #         "lat": -6.9344694,
        #         "lng": 107.6049539,
        #         # "text": "@ebithEBITH Yg diresmikan itu kebanyakan kerjaannya Pak @SBYudhoyono , memang Pak @jokowi spesialisasi peresmian untuk sekarang,",
        #         "img_url": 'http://pbs.twimg.com/profile_images/939314627489406977/KXOMtk-B_normal.jpg',
        #         "location": "Bandung, Indonesia",
        #         "marker_text": "@ebithEBITH Yg diresmikan itu kebanyakan kerjaannya Pak @SBYudhoyono , memang Pak @jokowi spesialisasi peresmian untuk sekarang,",
        #         "draggable": "false"
        #     },
        #     {
        #         "lat": 19.1399952,
        #         "lng": -72.3570972,
        #         # "text": "RT @MurtadhaOne: Mantab!!!\r\n\r\nJokowi:\r\n“Dalam setiap helaan napas diplomasi Indonesia, disitu terdapat keberpihakan terhadap Palestina",
        #         "img_url": 'http://pbs.twimg.com/profile_images/939314627489406977/KXOMtk-B_normal.jpg',
        #         "location": "Di Hati Mu",
        #         "marker_text": "RT @MurtadhaOne: Mantab!!!\r\n\r\nJokowi:\r\n“Dalam setiap helaan napas diplomasi Indonesia, disitu terdapat keberpihakan terhadap Palestina",
        #         "draggable": "false"
        #     }
        # ]

        return Response(result_structure, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)


#top contributor analysis
@api_view(['GET'])
def top_contributor_analysis_by_project(request,id_project):

    if request.method == 'GET':

        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()
        search_history = SearchHistory.objects.filter(project=user_project).first()


        # ambil semua tweet
        res = ResultTwitter.objects.filter(history=search_history).values('user_id').annotate(
            freq_count=Count('user_id')).order_by("-freq_count")

        # container for contributor
        result_contributor = []

        # set key for twitter
        consumer_key = "4TINjDD3recUl8U86RM1zhtdS"
        consumer_secret = "sGTZKmt8XbXpkxlL42fj9xaWX7ysZOZL7TBn1JpCc7PIzOnU67"
        access_key = "912143740289957888-SdvPjPPD2Cics6y5hF1tSzMaLxCuWkU"
        access_secret = "zFvHNFqiOdHcWEVTCb0rgYQsG6ZI7NwjPqvQk4reVe44a"

        # create tweepy handler
        auth = tweepy.OAuthHandler(consumer_key=consumer_key, consumer_secret=consumer_secret)
        auth.set_access_token(access_key, access_secret)
        twitter = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

        max_contributor = 5

        for x in res:
            try:
                y = twitter.get_user(id=str(x['user_id']))
                if(max_contributor > 0 ):
                    result_contributor.append(
                        {
                            "user_screen_name": str(y.screen_name),
                            "user_twitter_link": "https://twitter.com/" + str(y.screen_name),
                            "user_photo_link": str(y.profile_image_url),
                            "total_post": str(x['freq_count']),
                            "user_name" : str(y.name),
                            "num_of_follower" : str(y.followers_count),
                            "num_of_posts" : str(y.statuses_count),
                            "num_of_friend" : str(y.friends_count),
                            "num_of_favorites" : str(y.favourites_count),
                            "is_verified" : str(y.verified)
                        }
                    )
                    max_contributor -=1
            except:
                print("user tidak ada")

        result_structure = result_contributor

        return Response(result_structure, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)



#top contributor analysis
@api_view(['GET'])
def local_set_location(request,id_project):

    if request.method == 'GET':
        get_all_id = ProjectSentiment.objects.all()
        for get_id in get_all_id:

            id_project= get_id.id

            user_project = ProjectSentiment.objects.filter(id=int(id_project)).first()
            search_history = SearchHistory.objects.filter(project=user_project).first()

            # consumer_key = "UsJuIf3Xkz7w6MIUnQpnArLW9"
            # consumer_secret = "XXqoia5GVOqQnuEaKzopdhj39b87o2mrficvAH20bqd8CixI4K"
            # access_secret = "UIPvTiD86GZgneRlMHOia1nGYvYkjHreByO8nGzyzUlKW"
            # access_key = "938132848057266176-5dSTpaGTtwq2d8tp8mOlCUD6nVDHNDE"
            consumer_key = "9JfwFDoYwm8IN9LkUBAUY2c8C"
            consumer_secret = "PHA7DBXTSSHWgnc6c0b6E9JBxuanvI2kdKGsdOhzHSNwoU5Yeb"
            access_key = "923109140427259905-MSf0edpLDW3x1dmqeLlZiE6pZNXDo2i"
            access_secret = "HxtD0hzHDGxyPEmcbG6dRnf7U4pNMOf9hsEUTV7ucr9n2"


            auth = tweepy.OAuthHandler(consumer_key=consumer_key, consumer_secret=consumer_secret)
            auth.set_access_token(access_key, access_secret)
            twitter = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

            res = ResultTwitter.objects.filter(history = search_history).all()
            geolocator = Nominatim()
            geolocator_google = GoogleV3()
            print("for project "+str(ProjectSentiment.id))

            for x in res:
                try:
                    # get location
                    detail_status = twitter.get_status(x.id_str)
                    lokasi = ""
                    longitude = ""
                    latitude = ""

                    if (detail_status.user.location is not None):
                        lokasi = detail_status.user.location
                    elif (detail_status.extended_entities["media"][0]["additional_media_info"]["source_user"][
                              "location"] is not None):
                        lokasi = detail_status.extended_entities["media"][0]["additional_media_info"]["source_user"][
                            "location"]
                    else:
                        lokasi = ""

                    # get coordinate
                    location = ""
                    try:
                        location = geolocator_google.geocode(lokasi)
                        longitude = str(location.longitude)
                        latitude = str(location.latitude)
                    except:
                        try:
                            location = geolocator.geocode(lokasi)
                            longitude = str(location.longitude)
                            latitude = str(location.latitude)
                        except:
                            print("lokasi tidak ditemukan")
                    x.marker_text = x.sentence
                    x.img_url = x.profile_image_url
                    x.location = lokasi
                    x.lng = longitude
                    x.lat = latitude
                    x.save()
                    print("berhasil merubah " + str(x.id_str) + "!")
                except:
                    print("status of " + str(x.id_str) + " isnt valid / deleted")

        return Response("oke", status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)


#top contributor analysis
@api_view(['GET'])
def get_other_data_news(request,id_project):
    import pprint, numpy, time, requests

    if request.method == 'GET':
        info_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
        user_token = Token.objects.filter(key=info_token).first().user
        user_project = ProjectSentiment.objects.filter(user=user_token, id=int(id_project)).first()
        search_history = SearchHistory.objects.filter(project=user_project).first()

        my_api_key = "AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o"
        # my_cse_id = "004152435857447790448:0zvezzh8-cg"

        cse_news = []
        listcse = CSEGoogle.objects.all()
        for listcse_item in listcse:
            cse_news.append({
                "news": str(listcse_item.situs),
                "id": str(listcse_item.cse_id)
            })

        import numpy

        set_date = search_history.date_created

        tanggal = []
        # create array of date in 30 days
        for i in range(1,30):
            day_now = set_date.day
            month_now = set_date.month
            year_now = set_date.year
            date_now = ""

            if(day_now > 9):
                if(month_now > 9):
                    date_now = str(year_now)+str(month_now)+str(day_now)
                else:
                    date_now = str(year_now) + "0" +str(month_now) + str(day_now)
            else:
                if (month_now > 9):
                    date_now = str(year_now) + str(month_now) + "0" + str(day_now)
                else:
                    date_now = str(year_now) + "0" + str(month_now) + "0" + str(day_now)
            tanggal.append(date_now)
            set_date = set_date - timedelta(days=1)


        query = search_history.query
        total_semua_situs = 0
        detail_setiap_situs = []

        print("query " + str(query))

        for in_cse in cse_news:

            current_cse = in_cse['id']
            print("total untuk situs " + str(in_cse['news']))
            total_seluruhnya = 0
            detail_setiap_tanggal = []

            for x in tanggal:
                # tanggal = "20180101"
                date = "sort=date:r:" + x + ":" + x
                start = 1
                total = 0
                for i in range(1, 10):
                    urls = "https://www.googleapis.com/customsearch/v1?q=jokowi+&key=AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o&cx=" + current_cse + "&start=" + str(
                        start) + "&" + date
                    r = requests.get(url=urls)
                    data = r.json()
                    start += 10
                    try:
                        total += len(data['items'])
                        detail_setiap_tanggal.append({
                            "tanggal" : x,
                            "jumlah" : total
                        })
                    except:
                        break
                print("-- tanggal " + x + " ada berita sebanyak " + str(total))
                total_seluruhnya += total

            detail_setiap_situs.append({
                "situs" : str(in_cse['news']),
                "total" : total_seluruhnya,
                "detail" : detail_setiap_tanggal
            })
            print("dan totalnya adalah " + str(total_seluruhnya))
            total_semua_situs += total_seluruhnya

        print(total_semua_situs)

        detail_seluruhnya = {
            "query" : query,
            "total" : total_semua_situs,
            "detail" : detail_setiap_situs
        }

        return Response(detail_seluruhnya, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)


#top contributor analysis
@api_view(['GET'])
def coba_get_news_all_news(request,id_project):
    if(id_project):
        x = tasks.get_news_from_other_detik.apply_async((10, id_project), countdown=20)
        return Response(status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


#top contributor analysis
@api_view(['GET'])
def create_new_model_detik(request):
    if request.method == 'GET':
        # # x = tasks.migrasidataset.apply_async((10, 10), countdown=20)
        # x = tasks.create_model.apply_async((10, 10), countdown=20)
        # # # ambil beberapa twitter detik
        # # data = ResultDetik.objects.filter(is_edited = True)
        # # # length1 = len(data)
        # #
        # # # ambil 5 data terakhir
        # # # num = 100
        # #
        # # obj_tokenizer = tokenizer_news.NewsTokenizer("oke")
        # #
        # # for i in data:
        # # # for i in data[(length1-num):length1]:
        # #     try:
        # #         # print(i.sentence)
        # #         split_message = obj_tokenizer.news_tokenize_sentence(str(i.sentence))
        # #         # set opinion
        # #         opinion = 0
        # #         if(i.opinion is "positive"): opinion = 1
        # #         if (i.opinion is "negative"): opinion = -1
        # #
        # #         new_datatrain = Datatrain.objects.create(
        # #             name = "admin",
        # #             message_id = "detik_admin",
        # #             result_message = str(i.sentence),
        # #             old_message = str(i.sentence),
        # #             split_message = split_message,
        # #             media_type = "detik_train_2018",
        # #             opinion = opinion
        # #         )
        # #         new_datatrain.save()
        # #     except:
        # #         print("")

        run_algorithm = naivebayes.NaiveBayes(100, 50)
        x = run_algorithm.sentiment_detik_train()
        x1 = run_algorithm.sentiment_twitter_train()


        return Response(status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)

#top contributor analysis
@api_view(['GET'])
def cek_data(request):
    if request.method == 'GET':
        x = ResultOtherNews.objects.all()
        no = 0
        for i in x:
            print("data ke"+str(no))
            no+=1
            try:
                sentence = str(i.title)+" "+str(i.sentence)
                result = naivebayes.NaiveBayes(80, 20).sentiment_test_detik(query=sentence)
                i.opinion = result['result']
                i.save()
            except:
                i.opinion = "neutral"
                i.save()
                print("ada error")
        # # get from resultdetik
        # data1 = ResultDetik.objects.filter(is_edited = True)
        # rev_data1 = {
        #     "positive": len(data1.filter(opinion = "positive")),
        #     "neutral": len(data1.filter(opinion="neutral")),
        #     "negative": len(data1.filter(opinion="negative"))
        # }
        #
        # # get from resulttwiter
        # data2 = ResultTwitter.objects.filter(is_edited = True)
        # rev_data2 = {
        #     "positive": len(data2.filter(opinion = "positive")),
        #     "neutral": len(data2.filter(opinion="neutral")),
        #     "negative": len(data2.filter(opinion="negative"))
        # }
        #
        # # get data in datatrain detik
        # datatrain1 = Datatrain.objects.filter(media_type = "detik_train_2018")
        # rev_datatrain1 = {
        #     "positive": len(datatrain1.filter(opinion = 1)),
        #     "neutral": len(datatrain1.filter(opinion = 0)),
        #     "negative": len(datatrain1.filter(opinion =-1))
        # }
        # # get data in datatrain detik
        # datatrain2 = Datatrain.objects.filter(media_type="twitter_train_2018")
        # rev_datatrain2 = {
        #     "positive": len(datatrain2.filter(opinion=1)),
        #     "neutral": len(datatrain2.filter(opinion=0)),
        #     "negative": len(datatrain2.filter(opinion=-1))
        # }
        #
        # result = {
        #     "expected twitter" : data2,
        #     "expected detik": data1,
        #     "actual twitter": datatrain2,
        #     "actual detik": datatrain1,
        # }

        return Response("ok", status=status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)

#top contributor analysis
@api_view(['GET'])
def get_data_sosmed_from_cse(request):

    CSETwitter = CSEGoogle.objects.filter(id=296).all().first()
    CSEInstagram = CSEGoogle.objects.filter(id=295).all().first()
    CSEFacebook = CSEGoogle.objects.filter(id=294).all().first()
    result = {}
    if request.method == 'GET':
        query = ["anies", "anies baswedan", "gubernur dki", "anies sandi", "pak gubernur", "pak anies"]
        # untuk facebook
        max_hour = 16
        for i in range(1,max_hour):
            current_time = datetime.datetime.now()
            start = 1
            max_page = 11
            print("ambil facebook")
            # untuk facebook
            for x in query:
                print("query = " + str(x))
                for y in range(1,max_page):
                    try:
                        print("jam ke " + str(current_time))

                        urls = "https://www.googleapis.com/customsearch/v1?q=" + str(x) + "+&key=AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o&cx=" + CSEFacebook.cse_id + "&start=" + str(start) + "&date:r:20180201:20180202"
                        r = requests.get(url=urls)
                        data = r.json()

                        print(len(data['items']))
                        for item in data['items']:
                            cekFB = CobaResultFacebook.objects.filter(url=item['link']).all()

                            if(len(cekFB) == 0):
                                img = ""
                                try:
                                    img = item['pagemap']['cse_image'][0]['src']
                                except:
                                    print("tak ada img")

                                CobaResultFacebook.objects.create(
                                    title=item['title'],
                                    snippet=item['snippet'],
                                    url=item['link'],
                                    img=img,
                                    sesi=current_time
                                )
                                print("save fb")
                        start +=10
                    except:
                        print("habis")
                        break

            start = 1
            print("ambil instagram")
            # untuk instagram
            for x in query:
                print("query = " + str(x))
                for y in range(1, max_page):
                    try:
                        print("jam ke " + str(current_time))
                        urls = "https://www.googleapis.com/customsearch/v1?q=" + str(
                            x) + "+&key=AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o&cx=" + CSEInstagram.cse_id + "&start=" + str(
                            start) + "&date:r:20180201:20180202"
                        r = requests.get(url=urls)
                        data = r.json()

                        print(len(data['items']))
                        for item in data['items']:
                            cekIG = CobaResultInstagram.objects.filter(url=item['link']).all()

                            if (len(cekIG) == 0):
                                img = ""
                                try:
                                    img = item['pagemap']['cse_image'][0]['src']
                                except:
                                    print("tak ada img")

                                CobaResultInstagram.objects.create(
                                    title=item['title'],
                                    snippet=item['snippet'],
                                    url=item['link'],
                                    img=img,
                                    sesi=current_time
                                )
                                print("save ig")
                        start +=10
                    except:
                        print("habis")
                        break

            start = 1
            print("ambil twitter")
            # untuk twitter
            for x in query:
                print("query = " + str(x))
                for y in range(1, max_page):
                    try:
                        print("jam ke " + str(current_time))
                        urls = "https://www.googleapis.com/customsearch/v1?q=" + str(
                            x) + "+&key=AIzaSyC8KX5yTZ9QMfXzGo0ThtNRdpaHnFX5P1o&cx=" + CSETwitter.cse_id + "&start=" + str(
                            start) + "&date:r:20180201:20180202"
                        r = requests.get(url=urls)
                        data = r.json()

                        print(len(data['items']))
                        for item in data['items']:
                            cekTW = CobaResultInstagram.objects.filter(url=item['link']).all()

                            if (len(cekTW) == 0):
                                img = ""
                                try:
                                    img = item['pagemap']['cse_image'][0]['src']
                                except:
                                    print("tak ada img")

                                CobaResultTwitter.objects.create(
                                    title=item['title'],
                                    snippet=item['snippet'],
                                    url=item['link'],
                                    img=img,
                                    sesi=current_time
                                )
                                print("save tw")
                        start +=10
                    except:
                        print("habis")
                        break

            time.sleep(900)


        return Response(query, status=status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


#top contributor analysis
@api_view(['GET'])
def review_data_cse_sosmed(request):

    CSETwitter = CobaResultTwitter.objects.all()
    CSEInstagram = CobaResultInstagram.objects.all()
    CSEFacebook = CobaResultFacebook.objects.all()
    # result = {}

    # ambil uniknya aja dari tiap cse
    res1 = CSETwitter.values('sesi').annotate(freq_count=Count('sesi')).order_by("-sesi")
    print("twitter")
    for x in res1:
        print(str(x['sesi']) +" : "+str(x['freq_count']))

    res2 = CSEInstagram.values('sesi').annotate(freq_count=Count('sesi')).order_by("-sesi")
    print("instagram")
    for x in res2:
        print(str(x['sesi']) + " : " + str(x['freq_count']))

    res3 = CSEFacebook.values('sesi').annotate(freq_count=Count('sesi')).order_by("-sesi")
    print("facebook")
    for x in res3:
        print(str(x['sesi']) + " : " + str(x['freq_count']))


    result = {
        "twitter" : res1,
        "facebook" : res3,
        "instagram" : res2
    }

    return Response(result, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_detail_post_facebook(request):
    # import facebook sdk
    access_token = "access_token=133252573984435|VesAEMW2yKNYjryNQg01CMLlBCA"
    token_app = "133252573984435|VesAEMW2yKNYjryNQg01CMLlBCA"
    x = CobaResultFacebook.objects.all()[0:5]
    res = []
    for i in x:
        try:
            username = i.url.split("/")[3]
            id_post = i.url.split("/")[5]

            # get id of user
            url_datadiri = "https://graph.facebook.com/"+username+"?"+access_token
            req = requests.get(url=url_datadiri)
            result = req.json()

            # get detail post
            time_post = ""
            full_id_post = ""

            url_post = "https://graph.facebook.com/" + str(result['id']) + "_" + str(id_post) + "?" + access_token
            req_post = requests.get(url=url_post)
            result1 = req_post.json()

            try:
                time_post = result1['created_time']
            except:
                time_post = ""

            try:
                full_id_post = result1['id']
            except:
                full_id_post = ""

            # get list comment
            list_comment = {}
            total_comment = 0
            data_comment = []
            cursor_next_comment = ""
            url_comment = url_post+"&fields=comments"
            req_comment = requests.get(url=url_comment)
            result3 = req_comment.json()

            try:
                data_comment = result3['comments']['data']
                total_comment = len(result3['comments']['data'])
                cursor_next_comment = result3['comments']['paging']['next']
                for data_c in data_comment:
                    print(data_c['message'])
            except:
                total_comment = 0

            list_comment = {
                "total" : total_comment,
                "data" : data_comment,
                "next" : cursor_next_comment
            }

            # get sentiment for comment

            # output structure
            res.append({
                "data_diri" : {
                    "username": username,
                    "name": result['name'],
                    "user_id": result['id'],

                },
                "post" : {
                    "full_url": i.url,
                    "post_id": id_post,
                    "full_post_id": full_id_post,
                    "time" : time_post
                },
                "comment" : list_comment
            })
        except:
            print("ada error")

    return Response(res, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_all_possible_facebook_data(request):
    type = ['user', 'page', 'event','group','place','placetopic']

    return Response(type, status=status.HTTP_200_OK)

@api_view(['GET'])
def get_detail_post_instagram(request):
    # get detail instagram
    x = CobaResultInstagram.objects.last()
    urls = x.url+"?__a=1"
    req = requests.get(url=urls)
    result = req.json()

    return Response(result, status=status.HTTP_200_OK)