from .._models.socialmedia import ResultTwitter
from rest_framework.decorators import api_view
from twitter import *
from rest_framework.response import Response
from rest_framework import status
from datetime import timedelta
from time import mktime
import time, datetime, numpy
from ..extras.algorithm import naivebayes


@api_view(['GET'])
def sample_detik(request):
    if request.method == 'GET':

        # get twitter
        # buat query untuk berita
        # hanya ambil yang bukan retweet
        keywords = "zulkifli hasan from:liputan6dotcom"
        num_of_post = 100
        date_now = datetime.datetime.now()

        # get date from date_now
        day = date_now.day
        month = date_now.month
        year = date_now.year

        tanggal = str(year)+"-"+str(month)+"-"+str(day)

        new_twitter = Twitter(auth=OAuth("220014659-4zpJkYJu1dlQTnHjRznBerJpS96oopjcVkrZlief",
                                         "OFXOSMRJxMs8oEiKvZGMsuwTDsr2oTAyiBJjjWg4YEj2N",
                                         "06GAwlT1hqND4OcS0dOFQz510",
                                         "yqr8uLrDMgiFlJ1xSDbBu4nBoxApyFjvXMFJSH90urXjqQ36Hc"))

        query = new_twitter.search.tweets(q=keywords,count=num_of_post,lang="id",
                                              result_type="recent")

        return Response(query, status=status.HTTP_200_OK)
    else:
        return Response(status.HTTP_204_NO_CONTENT)


        # keywords = "joko widodo"
        # num_of_post = 5
        # yes = {}
        # # ambil untuk satu hari selama satu minggu, satu hari max 100
        # date_now = datetime.datetime.now()
        # total_all = 0
        # for i in numpy.arange(1, 2):
        #
        #     # get date from date_now
        #     day = date_now.day
        #     month = date_now.month
        #     year = date_now.year
        #
        #     tanggal = str(year)+"-"+str(month)+"-"+str(day+1)
        #     print(tanggal)
        #     query = new_twitter.search.tweets(q=keywords,
        #                                       count=num_of_post,
        #                                       lang="id",
        #                                       result_type="recent",
        #                                       until=tanggal,
        #                                       include_entities="true")
        #     print('data')
        #     total_data = 0
        #     test = naivebayes.NaiveBayes(80, 20)
        #     for item in query["statuses"]:
        #         sample_datetime = time.strptime(item["created_at"], '%a %b %d %H:%M:%S +0000 %Y')
        #         sample_datetime1 = datetime.datetime.fromtimestamp(mktime(sample_datetime))+timedelta(hours=7)
        #
        #         # print("calculate sentiment for  : " + str(item['text']))
        #
        #         result_sentiment = test.sentiment_test_twitter(query=item['text'])
        #         print(" ")
        #         print(str(sample_datetime1))
        #         print('sentiment : '+ result_sentiment['result'])
        #         print('retweet_count : ' + str(item['retweet_count']))
        #         print('favourite_count : ' + str(item['favorite_count']))
        #         print('user name : ' + str(item['user']['name']))
        #         print('user screen name : ' + str(item['user']['screen_name']))
        #         print('user id : ' + str(item['user']['id']))
        #         print('profile_image_url'+ str(item['user']['profile_image_url']))
        #
        #
        #         # print("saving sentiment result on : " + str(datetime.datetime.now()))
        #         # ResultTwitter.objects.create(
        #         #     opinion=result_sentiment['result'],
        #         #     sentence=result_sentiment['query'],
        #         #     history=history,
        #         #     city=item['points'],
        #         #     tweet_created=datetime.datetime(
        #         #         year=sample_datetime1.year, month=sample_datetime1.month, day=sample_datetime1.day,
        #         #         hour=sample_datetime1.hour, minute=sample_datetime1.minute, second=sample_datetime1.second
        #         #     )
        #         # ).save()
        #         # print("sentiment result saved on : " + str(datetime.datetime.now()))
        #
        #         # print(str(sample_datetime1))
        #         #
        #         # if ((int(sample_datetime1.year) == year) and (int(sample_datetime1.month) == month) and (int(sample_datetime1.day) == day)):
        #         #     total_data += 1
        #     print("tanggal" + tanggal + " : " + str(len(query['statuses'])))
        #     # print("tanggal" + tanggal + " : " + str(total_data))
        #     date_now += timedelta(days=-1)
        #     time.sleep(5)
        #     total_all+=total_data




@api_view(['GET'])
def sample_lemparan(request,lemparan):
    if request.method == 'GET':
        print(lemparan[0])
        return Response(status=status.HTTP_200_OK)