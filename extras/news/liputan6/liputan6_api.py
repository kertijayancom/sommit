from bs4 import BeautifulSoup as soup
import re,json,datetime,requests
from datetime import timedelta
import os, time
from lxml import html

class Liputan6Api(object):
    def __init__(self,id):
        self.id = id

    def get_news(self,query):
        query_date_now = time.strftime("%Y%m%d")
        N = 1
        date_N_days_ago = datetime.datetime.now() - timedelta(days=N)
        day_ago = str(date_N_days_ago).split(' ')[0]
        query_date_ago = day_ago.replace('-','')
        print(query_date_ago+":"+query_date_now)

        url = "https://www.googleapis.com/customsearch/v1?q="+query+"&cx=004152435857447790448%3Afah92kjzau8&key=AIzaSyBVVNlspPVHdPlrA0V7NwuRWk1mt8v2rME&sort=date:r:"+query_date_now
        url2 = "https://www.googleapis.com/customsearch/v1?q=" + query + "&cx=004152435857447790448%3Afah92kjzau8&key=AIzaSyBVVNlspPVHdPlrA0V7NwuRWk1mt8v2rME&sort=date:r:" + query_date_ago
        raw_content = []
        result_content = []
        json_result = {}

        #1. Mendapatkan URL setiap berita
        # sending get request and saving the response as response object
        r = requests.get(url=url)
        # extracting data in json format
        data = r.json()
        # extracting title,url
        res_url = []
        count_res = data['queries']['request']
        x_count = []
        for xx in count_res:
            x_count.append(xx['count'])
        print(x_count)

        if x_count[0] < 10:
            for item in data['items']:
                item_url = item['link']
                res_url.append(item_url)
            #mendapatkan data di url2
            r2 = requests.get(url=url2)
            data2 = r2.json()
            for item2 in data2['items']:
                item_url2 = item2['link']
                res_url.append(item_url2)
        else:
            for item in data['items']:
                item_url = item['link']
                res_url.append(item_url)
        # printing the output
        print(len(res_url))

        # 2. Mendapatkan content setiap url
        for y in res_url:
            req2 = requests.get(y)
            data = req2.text
            page = soup(data, "html.parser")
            if (str(soup(str(page.select('header > h1.read-page--header--title')), "html.parser").get_text()).strip('[]')) is not '':
                raw_content.append(page)
        print(len(raw_content))

        # 3. Mendapatkan element di setiap content dan menghasilkan output json
        for z in raw_content[0:10]:
            page = soup(str(z), "html.parser")
            if (str(soup(str(page.select('header > h1.read-page--header--title')), "html.parser").get_text()).strip('[]')) is not None:
                title = str(soup(str(page.select('header > h1.read-page--header--title')), "html.parser").get_text()).strip('[]')
                raw_content = str(soup(str(page.select('div.article-content-body__item-content')), "html.parser").get_text()).strip('[]')
                regex = re.compile(r'[\n\r\t]')
                content = regex.sub('', raw_content)

                all_text = page.select('div.article-content-body__item-content')

                select_location = soup(str(all_text), "html.parser")

                loc = soup(str(select_location.find('b')), "html.parser").get_text()
                get_loc = loc.split(',')
                location = get_loc[1]
                regex_loc = re.compile(r'[^\w]')
                location = regex_loc.sub('', location)

                result_content.append(
                    {
                        "title": title,
                        "location": location,
                        "content": content,
                    }
                )

        source = "liputan6"
        a_time = str(datetime.datetime.now())
        a_time = a_time.replace(" ", "__")
        a_time = a_time.replace(":", "_")
        a_time = a_time.replace("-", "_")

        file_name = '_' + a_time + '_' + source + ".json"

        json_result["feed"] = result_content
        json_result["sumber"] = source
        json_result["file_name"] = file_name

        return (json_result)