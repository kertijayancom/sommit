import nltk, csv, re, random
from ...models import Datatrain, Cobamodel, Sentimentmodel
import pickle
import os, datetime
from django.conf import settings

class NaiveBayes(object):


    def __init__(self,ratio_train, ratio_test):
        self.ratio_test = ratio_test
        self.ratio_train = ratio_train


    def get_words_in_tweets(self, tweets):
        # function for get word in one tweet
        all_words = []
        for (words, sentiment) in tweets:
            all_words.extend(words)
        return all_words


    def get_word_features(self, wordlist):
        # function for get feature of list word using nltk library, to calcualte frequency
        wordlist = nltk.FreqDist(wordlist)
        word_features = wordlist.keys()
        return word_features


    def extract_features(self, document):
        # function for extract every word in document (a feature extractor)
        document_words = set(document)
        features = {}
        # word_features = self.get_word_features(self.get_words_in_tweets(tweets))
        for word in document:
            features['contains(%s)' % word] = (word in document_words)
        return features

    def refine_split_message(self, split_message):
        # all_data = Datatrain.objects.filter(media_type="twitter", opinion="1")
        result_split_message = []
        new_split_message = []
        for i in split_message:
            try:
                print(i)
                new_split_message.append(i)
            except:
                print("tidak boleh masuk")


        for data2 in new_split_message:
            if(data2 is not None):
                # try:
                data2 = self.refine_word(data2)
                if(len(data2) > 0):
                    result_split_message.append(data2)

        return(result_split_message)

    def refine_word(self, data):
        data = ''.join([i for i in data if i.isalpha()])
        return(data)

    def sentiment_twitter_train(self):

        tweets = []
        temp_acc = 0

        # persiapkan data
        media_type = "twitter_train_2018"
        data1 = Datatrain.objects.filter(media_type=media_type, opinion="1")
        data2 = Datatrain.objects.filter(media_type=media_type, opinion="0")
        data3 = Datatrain.objects.filter(media_type=media_type, opinion="-1")

        for a in data1: tweets.append((self.refine_split_message(a.split_message), "positive"))
        for a in data2: tweets.append((self.refine_split_message(a.split_message), "neutral"))
        for a in data3: tweets.append((self.refine_split_message(a.split_message), "negative"))

        length_tweets = len(tweets)

        training_data = tweets[:int((length_tweets * self.ratio_train / 100))]
        testing_data = tweets[int((length_tweets * self.ratio_test / 100)) + 1:]

        word_features = self.get_word_features(self.get_words_in_tweets(tweets))
        training_set = nltk.classify.apply_features(self.extract_features, tweets)
        classifier = nltk.NaiveBayesClassifier.train(labeled_featuresets=training_set)

        # save model to _file
        a_time = str(datetime.datetime.now())
        a_time = a_time.replace(" ", "__")
        a_time = a_time.replace(":", "_")
        a_time = a_time.replace("-", "_")
        file_path_name = '//model//_twitter_' + a_time + '.pickle'
        file_name = settings.STATIC_ROOT+ file_path_name
        print(file_path_name)

        f = open(file_name, 'wb')
        pickle.dump(classifier, f)
        f.close()

        Sentimentmodel.objects.create(
            media_type=media_type,
            num_of_data=length_tweets,
            path_model=file_path_name
        ).save()
        # pos, net, neg
        matrix_result = [[0 for x in range(3)] for y in range(3)]

        for query, opinion in testing_data:
            result = classifier.classify(self.extract_features(query))

            # +1 for accuracy
            if (opinion == result): temp_acc += 1

            # for positive
            if ((opinion is "positive") and (result is "positive")): matrix_result[0][0] += 1
            if ((opinion is "neutral") and (result is "positive")): matrix_result[0][1] += 1
            if ((opinion is "negative") and (result is "positive")): matrix_result[0][2] += 1
            if ((opinion is "positive") and (result is "neutral")): matrix_result[1][0] += 1
            if ((opinion is "neutral") and (result is "neutral")): matrix_result[1][1] += 1
            if ((opinion is "negative") and (result is "neutral")): matrix_result[1][2] += 1
            if ((opinion is "positive") and (result is "negative")): matrix_result[2][0] += 1
            if ((opinion is "neutral") and (result is "negative")): matrix_result[2][1] += 1
            if ((opinion is "negative") and (result is "negative")): matrix_result[2][2] += 1

        result = {}
        try:
            result['accuracy'] = temp_acc / length_tweets
        except:
            result['accuracy'] = 0


        result['data'] = {
            'positive': len(data1),
            'neutral': len(data2),
            'negative': len(data3),
            'total': length_tweets
        }
        result['training_set'] = training_set
        result['matrix'] = matrix_result

        return (result)


    def sentiment_facebook_comment_train(self):

        comments = []
        temp_acc = 0

        # persiapkan data
        media_type = "facebookcomment"
        data1 = Datatrain.objects.filter(media_type=media_type, opinion="1")
        data2 = Datatrain.objects.filter(media_type=media_type, opinion="0")
        data3 = Datatrain.objects.filter(media_type=media_type, opinion="-1")


        for a in data1: comments.append((self.refine_split_message(a.split_message['split_message']),"positive"))
        for a in data2: comments.append((self.refine_split_message(a.split_message['split_message']), "neutral"))
        for a in data3: comments.append((self.refine_split_message(a.split_message['split_message']), "negative"))

        length_comments = len(comments)

        training_data = comments[:int((length_comments * self.ratio_train / 100))]
        testing_data = comments[int((length_comments * self.ratio_test / 100)) + 1:]

        length_comments = len(comments)
        word_features = self.get_word_features(self.get_words_in_tweets(comments))
        training_set = nltk.classify.apply_features(self.extract_features, training_data)
        classifier = nltk.NaiveBayesClassifier.train(labeled_featuresets=training_set)

        # pos, net, neg
        matrix_result = [[0 for x in range(3)] for y in range(3)]


        for query, opinion in testing_data:
            result = classifier.classify(self.extract_features(query))

            # +1 for accuracy
            if (opinion == result): temp_acc+=1

            # for positive
            if ((opinion is "positive") and (result is "positive")):matrix_result[0][0] +=1
            if ((opinion is "neutral") and (result is "positive")): matrix_result[0][1] += 1
            if ((opinion is "negative") and (result is "positive")): matrix_result[0][2] += 1
            if ((opinion is "positive") and (result is "neutral")): matrix_result[1][0] += 1
            if ((opinion is "neutral") and (result is "neutral")): matrix_result[1][1] += 1
            if ((opinion is "negative") and (result is "neutral")): matrix_result[1][2] += 1
            if ((opinion is "positive") and (result is "negative")): matrix_result[2][0] += 1
            if ((opinion is "neutral") and (result is "negative")): matrix_result[2][1] += 1
            if ((opinion is "negative") and (result is "negative")): matrix_result[2][2] += 1

        result = {}
        try:
            result['accuracy'] = temp_acc / length_comments
        except:
            result['accuracy'] = 0

        result['matrix'] = matrix_result
        result['data'] = {
            'positive': len(data1),
            'neutral': len(data2),
            'negative': len(data3),
            'total': length_comments
        }
        result['training_set'] = training_set

        return (result)


    def sentiment_facebook_post_train(self):

        posts = []
        temp_acc = 0

        # persiapkan data
        media_type = "facebookpost"
        data1 = Datatrain.objects.filter(media_type=media_type, opinion="1")
        data2 = Datatrain.objects.filter(media_type=media_type, opinion="0")
        data3 = Datatrain.objects.filter(media_type=media_type, opinion="-1")

        for a in data1: posts.append((self.refine_split_message(a.split_message['split_message']), "positive"))
        for a in data2: posts.append((self.refine_split_message(a.split_message['split_message']), "neutral"))
        for a in data3: posts.append((self.refine_split_message(a.split_message['split_message']), "negative"))

        length_posts = len(posts)

        training_data = posts[:int((length_posts * self.ratio_train / 100))]
        testing_data = posts[int((length_posts * self.ratio_test / 100)) + 1:]

        word_features = self.get_word_features(self.get_words_in_tweets(posts))
        training_set = nltk.classify.apply_features(self.extract_features, training_data)
        classifier = nltk.NaiveBayesClassifier.train(labeled_featuresets=training_set)

        # pos, net, neg
        matrix_result = [[0 for x in range(3)] for y in range(3)]

        for query, opinion in testing_data:
            result = classifier.classify(self.extract_features(query))

            # +1 for accuracy
            if (opinion == result): temp_acc += 1

            # insert into matrix
            if ((opinion is "positive") and (result is "positive")): matrix_result[0][0] += 1
            if ((opinion is "neutral") and (result is "positive")): matrix_result[0][1] += 1
            if ((opinion is "negative") and (result is "positive")): matrix_result[0][2] += 1
            if ((opinion is "positive") and (result is "neutral")): matrix_result[1][0] += 1
            if ((opinion is "neutral") and (result is "neutral")): matrix_result[1][1] += 1
            if ((opinion is "negative") and (result is "neutral")): matrix_result[1][2] += 1
            if ((opinion is "positive") and (result is "negative")): matrix_result[2][0] += 1
            if ((opinion is "neutral") and (result is "negative")): matrix_result[2][1] += 1
            if ((opinion is "negative") and (result is "negative")): matrix_result[2][2] += 1


        result = {}
        try:
            result['accuracy'] =  temp_acc / length_posts
        except:
            result['accuracy'] = 0

        result['data'] = {
            'positive': len(data1),
            'neutral': len(data2),
            'negative': len(data3),
            'total': length_posts
        }

        result['training_set'] = training_set
        result['matrix'] = matrix_result

        return (result)


    def sentiment_instagram_post_train(self):

        posts = []
        temp_acc = 0

        # persiapkan data
        media_type = "instagrampost"
        data1 = Datatrain.objects.filter(media_type=media_type, opinion="1")
        data2 = Datatrain.objects.filter(media_type=media_type, opinion="0")
        data3 = Datatrain.objects.filter(media_type=media_type, opinion="-1")

        for a in data1: posts.append((self.refine_split_message(a.split_message['split_message']), "positive"))
        for a in data2: posts.append((self.refine_split_message(a.split_message['split_message']), "neutral"))
        for a in data3: posts.append((self.refine_split_message(a.split_message['split_message']), "negative"))

        length_posts = len(posts)

        training_data = posts[:int((length_posts * self.ratio_train / 100))]
        testing_data = posts[int((length_posts * self.ratio_test / 100)) + 1:]

        word_features = self.get_word_features(self.get_words_in_tweets(posts))
        training_set = nltk.classify.apply_features(self.extract_features, training_data)
        classifier = nltk.NaiveBayesClassifier.train(labeled_featuresets=training_set)

        # pos, net, neg
        matrix_result = [[0 for x in range(3)] for y in range(3)]

        for query, opinion in testing_data:
            result = classifier.classify(self.extract_features(query))

            # +1 for accuracy
            if (opinion == result): temp_acc += 1

            # for positive
            if ((opinion is "positive") and (result is "positive")): matrix_result[0][0] += 1
            if ((opinion is "neutral") and (result is "positive")): matrix_result[0][1] += 1
            if ((opinion is "negative") and (result is "positive")): matrix_result[0][2] += 1
            if ((opinion is "positive") and (result is "neutral")): matrix_result[1][0] += 1
            if ((opinion is "neutral") and (result is "neutral")): matrix_result[1][1] += 1
            if ((opinion is "negative") and (result is "neutral")): matrix_result[1][2] += 1
            if ((opinion is "positive") and (result is "negative")): matrix_result[2][0] += 1
            if ((opinion is "neutral") and (result is "negative")): matrix_result[2][1] += 1
            if ((opinion is "negative") and (result is "negative")): matrix_result[2][2] += 1

        result = {}
        try:
            result['accuracy'] = temp_acc / length_posts
        except:
            result['accuracy'] = 0

        result['matrix'] = matrix_result
        result['data'] = {
            'positive': len(data1),
            'neutral': len(data2),
            'negative': len(data3),
            'total': length_posts
        }
        result['training_set'] = training_set

        return (result)


    def sentiment_instagram_comment_train(self):

        comments = []
        temp_acc = 0

        # persiapkan data
        media_type = "instagramcomment"
        data1 = Datatrain.objects.filter(media_type=media_type, opinion="1")
        data2 = Datatrain.objects.filter(media_type=media_type, opinion="0")
        data3 = Datatrain.objects.filter(media_type=media_type, opinion="-1")

        for a in data1: comments.append((self.refine_split_message(a.split_message['split_message']), "positive"))
        for a in data2: comments.append((self.refine_split_message(a.split_message['split_message']), "neutral"))
        for a in data3: comments.append((self.refine_split_message(a.split_message['split_message']), "negative"))

        length_comments = len(comments)

        training_data = comments[:int((length_comments * self.ratio_train / 100))]
        testing_data = comments[int((length_comments * self.ratio_test / 100)) + 1:]

        word_features = self.get_word_features(self.get_words_in_tweets(comments))
        training_set = nltk.classify.apply_features(self.extract_features, training_data)
        classifier = nltk.NaiveBayesClassifier.train(labeled_featuresets=training_set)

        # pos, net, neg
        matrix_result = [[0 for x in range(3)] for y in range(3)]

        for query, opinion in testing_data:
            result = classifier.classify(self.extract_features(query))

            # +1 for accuracy
            if (opinion == result): temp_acc += 1

            # for positive
            if ((opinion is "positive") and (result is "positive")): matrix_result[0][0] += 1
            if ((opinion is "neutral") and (result is "positive")): matrix_result[0][1] += 1
            if ((opinion is "negative") and (result is "positive")): matrix_result[0][2] += 1
            if ((opinion is "positive") and (result is "neutral")): matrix_result[1][0] += 1
            if ((opinion is "neutral") and (result is "neutral")): matrix_result[1][1] += 1
            if ((opinion is "negative") and (result is "neutral")): matrix_result[1][2] += 1
            if ((opinion is "positive") and (result is "negative")): matrix_result[2][0] += 1
            if ((opinion is "neutral") and (result is "negative")): matrix_result[2][1] += 1
            if ((opinion is "negative") and (result is "negative")): matrix_result[2][2] += 1

        result = {}
        try:
            result['accuracy'] = temp_acc / length_comments
        except:
            result['accuracy'] = 0

        result['matrix'] = matrix_result
        result['data'] = {
            'positive': len(data1),
            'neutral': len(data2),
            'negative': len(data3),
            'total': length_comments
        }
        result['training_set'] = training_set

        return (result)


    def sentiment_test_twitter(self, query):

        result = "oke"
        page_result = {}

        query_refined = self.refine_split_message(query.split())
        twitter_model = Sentimentmodel.objects.filter(media_type="twitter_train_2018").last()
        # path_file = os.path.dirname(__file__) + twitter_model.path_model  + a_time + '.pickle'
        path_file = settings.STATIC_ROOT+'model/'+twitter_model.path_model
        f = open(path_file, 'rb')
        classifier = pickle.load(f)
        try:
            result = classifier.classify(self.extract_features(query_refined))

            page_result["query"] = query
            page_result['query_refined'] = query_refined
            page_result['result'] = result
            return (page_result)
        except:
            return (page_result)

    def sentiment_test_detik(self, query):
        # get model from db
        result = "oke"
        print(result)
        page_result = {}

        query_refined = self.refine_split_message(query.split())
        # for develop
        # detik_model = Sentimentmodel.objects.filter(id=5).first()
        # for production
        # twitter_model = Sentimentmodel.objects.filter(media_type="twitter_train").last()
        # # path_file = os.path.dirname(__file__) + twitter_model.path_model  + a_time + '.pickle'
        # path_file = settings.STATIC_ROOT+'model/'+twitter_model.path_model
        # f = open(path_file, 'rb')
        detik_model = Sentimentmodel.objects.filter(media_type="detik_train_2018").last()
        path_file = settings.STATIC_ROOT+'model/'+detik_model.path_model
        f = open(path_file, 'rb')
        classifier = pickle.load(f)
        try:
            result = classifier.classify(self.extract_features(query_refined))

            page_result["query"] = query
            page_result['query_refined'] = query_refined
            page_result['result'] = result
            return (page_result)
        except:
            return (page_result)

    # def save_model(self, media_type):
    #
    #     if(media_type == "twitter"):
    #         result = self.sentiment_twitter_train()
    #         print(result.data)
    #         Sentimentmodel.objects.create(
    #             media_type= "twitter",
    #             num_of_data = result.data.total,
    #             num_of_positive = result.data.positive,
    #             num_of_neutral = result.data.neutral,
    #             num_of_negative = result.data.negative,
    #             model = result.training_set
    #         ).save()

    def sentiment_detik_train(self):

        sentence = []
        temp_acc = 0

        # persiapkan data
        media_type = "detik_train_2018"
        data1 = Datatrain.objects.filter(media_type=media_type, opinion="1")
        data2 = Datatrain.objects.filter(media_type=media_type, opinion="0")
        data3 = Datatrain.objects.filter(media_type=media_type, opinion="-1")

        # for a in data1: sentence.append((self.refine_split_message(a.split_message['split_message']), "positive"))
        # for a in data2: sentence.append((self.refine_split_message(a.split_message['split_message']), "neutral"))
        # for a in data3: sentence.append((self.refine_split_message(a.split_message['split_message']), "negative"))

        for a in data1: sentence.append((self.refine_split_message(a.split_message), "positive"))
        for a in data2: sentence.append((self.refine_split_message(a.split_message), "neutral"))
        for a in data3: sentence.append((self.refine_split_message(a.split_message), "negative"))

        length_sentence = len(sentence)

        training_data = sentence[:int((length_sentence * self.ratio_train / 100))]
        testing_data = sentence[int((length_sentence * self.ratio_test / 100)) + 1:]

        word_features = self.get_word_features(self.get_words_in_tweets(sentence))
        training_set = nltk.classify.apply_features(self.extract_features, training_data)
        classifier = nltk.NaiveBayesClassifier.train(labeled_featuresets=training_set)

        # save model to file in pickle
        a_time = str(datetime.datetime.now())
        a_time = a_time.replace(" ", "__")
        a_time = a_time.replace(":", "_")
        a_time = a_time.replace("-", "_")
        file_path_name = '//model//_detik_' + a_time + '.pickle'
        file_name = settings.STATIC_ROOT + file_path_name
        print(file_path_name)

        f = open(file_name, 'wb')
        pickle.dump(classifier, f)
        f.close()

        Sentimentmodel.objects.create(
            media_type = media_type,
            num_of_data = len(training_data),
            path_model = file_path_name
        ).save()

        #
        # Cobamodel.objects.create(
        #     media_type = "twitter_model",
        #     num_of_data = length_tweets,
        #     path_model = file_name
        # ).save()

        # pos, net, neg
        matrix_result = [[0 for x in range(3)] for y in range(3)]

        for query, opinion in testing_data:
            result = classifier.classify(self.extract_features(query))

            # +1 for accuracy
            if (opinion == result): temp_acc += 1

            # for positive
            if ((opinion is "positive") and (result is "positive")): matrix_result[0][0] += 1
            if ((opinion is "neutral") and (result is "positive")): matrix_result[0][1] += 1
            if ((opinion is "negative") and (result is "positive")): matrix_result[0][2] += 1
            if ((opinion is "positive") and (result is "neutral")): matrix_result[1][0] += 1
            if ((opinion is "neutral") and (result is "neutral")): matrix_result[1][1] += 1
            if ((opinion is "negative") and (result is "neutral")): matrix_result[1][2] += 1
            if ((opinion is "positive") and (result is "negative")): matrix_result[2][0] += 1
            if ((opinion is "neutral") and (result is "negative")): matrix_result[2][1] += 1
            if ((opinion is "negative") and (result is "negative")): matrix_result[2][2] += 1

        result = {}
        try:
            result['accuracy'] = temp_acc / len(testing_data)
        except:
            result['accuracy'] = 0


        result['data'] = {
            'positive': len(data1),
            'neutral': len(data2),
            'negative': len(data3),
            'total': length_sentence
        }
        result['training_set'] = training_set
        result['matrix'] = matrix_result

        return (file_name)

    def sentiment_liputan6_train(self):

        sentence = []
        temp_acc = 0

        # persiapkan data
        media_type = "liputan6"
        data1 = Datatrain.objects.filter(media_type=media_type, opinion="1")
        data2 = Datatrain.objects.filter(media_type=media_type, opinion="0")
        data3 = Datatrain.objects.filter(media_type=media_type, opinion="-1")

        for a in data1: sentence.append((self.refine_split_message(a.split_message['split_message']), "positive"))
        for a in data2: sentence.append((self.refine_split_message(a.split_message['split_message']), "neutral"))
        for a in data3: sentence.append((self.refine_split_message(a.split_message['split_message']), "negative"))

        length_sentence = len(sentence)

        training_data = sentence[:int((length_sentence * self.ratio_train / 100))]
        testing_data = sentence[int((length_sentence * self.ratio_test / 100)) + 1:]

        word_features = self.get_word_features(self.get_words_in_tweets(sentence))
        training_set = nltk.classify.apply_features(self.extract_features, training_data)
        classifier = nltk.NaiveBayesClassifier.train(labeled_featuresets=training_set)

        # pos, net, neg
        matrix_result = [[0 for x in range(3)] for y in range(3)]

        for query, opinion in testing_data:
            result = classifier.classify(self.extract_features(query))

            # +1 for accuracy
            if (opinion == result): temp_acc += 1

            # for positive
            if ((opinion is "positive") and (result is "positive")): matrix_result[0][0] += 1
            if ((opinion is "neutral") and (result is "positive")): matrix_result[0][1] += 1
            if ((opinion is "negative") and (result is "positive")): matrix_result[0][2] += 1
            if ((opinion is "positive") and (result is "neutral")): matrix_result[1][0] += 1
            if ((opinion is "neutral") and (result is "neutral")): matrix_result[1][1] += 1
            if ((opinion is "negative") and (result is "neutral")): matrix_result[1][2] += 1
            if ((opinion is "positive") and (result is "negative")): matrix_result[2][0] += 1
            if ((opinion is "neutral") and (result is "negative")): matrix_result[2][1] += 1
            if ((opinion is "negative") and (result is "negative")): matrix_result[2][2] += 1

        result = {}
        try:
            result['accuracy'] = temp_acc / length_sentence
        except:
            result['accuracy'] = 0


        result['data'] = {
            'positive': len(data1),
            'neutral': len(data2),
            'negative': len(data3),
            'total': length_sentence
        }
        result['training_set'] = training_set
        result['matrix'] = matrix_result

        # save model to _file
        a_time = str(datetime.datetime.now())
        a_time = a_time.replace(" ", "__")
        a_time = a_time.replace(":", "_")
        a_time = a_time.replace("-", "_")
        file_name = os.path.dirname(__file__)+'\_model\_liputan6_'+a_time+'.pickle'

        f = open(file_name, 'wb')
        pickle.dump(classifier, f)
        f.close()

        Cobamodel.objects.create(
            media_type = "liputan6",
            num_of_data = length_sentence,
            path_model = file_name
        ).save()
        return (result)

    def sentiment_sindonews_train(self):

        sentence = []
        temp_acc = 0

        # persiapkan data
        media_type = "sindonews"
        data1 = Datatrain.objects.filter(media_type=media_type, opinion="1")
        data2 = Datatrain.objects.filter(media_type=media_type, opinion="0")
        data3 = Datatrain.objects.filter(media_type=media_type, opinion="-1")

        for a in data1: sentence.append((self.refine_split_message(a.split_message['split_message']), "positive"))
        for a in data2: sentence.append((self.refine_split_message(a.split_message['split_message']), "neutral"))
        for a in data3: sentence.append((self.refine_split_message(a.split_message['split_message']), "negative"))

        length_sentence = len(sentence)

        training_data = sentence[:int((length_sentence * self.ratio_train / 100))]
        testing_data = sentence[int((length_sentence * self.ratio_test / 100)) + 1:]

        word_features = self.get_word_features(self.get_words_in_tweets(sentence))
        training_set = nltk.classify.apply_features(self.extract_features, training_data)
        classifier = nltk.NaiveBayesClassifier.train(labeled_featuresets=training_set)

        # pos, net, neg
        matrix_result = [[0 for x in range(3)] for y in range(3)]

        for query, opinion in testing_data:
            result = classifier.classify(self.extract_features(query))

            # +1 for accuracy
            if (opinion == result): temp_acc += 1

            # for positive
            if ((opinion is "positive") and (result is "positive")): matrix_result[0][0] += 1
            if ((opinion is "neutral") and (result is "positive")): matrix_result[0][1] += 1
            if ((opinion is "negative") and (result is "positive")): matrix_result[0][2] += 1
            if ((opinion is "positive") and (result is "neutral")): matrix_result[1][0] += 1
            if ((opinion is "neutral") and (result is "neutral")): matrix_result[1][1] += 1
            if ((opinion is "negative") and (result is "neutral")): matrix_result[1][2] += 1
            if ((opinion is "positive") and (result is "negative")): matrix_result[2][0] += 1
            if ((opinion is "neutral") and (result is "negative")): matrix_result[2][1] += 1
            if ((opinion is "negative") and (result is "negative")): matrix_result[2][2] += 1

        result = {}
        try:
            result['accuracy'] = temp_acc / length_sentence
        except:
            result['accuracy'] = 0


        result['data'] = {
            'positive': len(data1),
            'neutral': len(data2),
            'negative': len(data3),
            'total': length_sentence
        }
        result['training_set'] = training_set
        result['matrix'] = matrix_result

        # save model to _file
        a_time = str(datetime.datetime.now())
        a_time = a_time.replace(" ", "__")
        a_time = a_time.replace(":", "_")
        a_time = a_time.replace("-", "_")
        file_name = os.path.dirname(__file__)+'\_model\_sindonews_'+a_time+'.pickle'

        f = open(file_name, 'wb')
        pickle.dump(classifier, f)
        f.close()

        Cobamodel.objects.create(
            media_type = "sindonews",
            num_of_data = length_sentence,
            path_model = file_name
        ).save()
        return (result)