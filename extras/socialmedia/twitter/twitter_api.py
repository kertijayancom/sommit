from twitter import *
import json
import time
import os
from django.contrib.staticfiles.storage import staticfiles_storage
from django.conf import settings

#-----------------------------------------------------------------------
# create twitter API object
#-----------------------------------------------------------------------
class TwitterApi(object):

    def __init__(self,id):
        self.id = id

    def get_tweets(self,keywords,num_of_post,num_of_city, initial_date):

        # https://dev.twitter.com/rest/reference/get/search/tweets
        twitter = Twitter(
                    auth = OAuth("220014659-4zpJkYJu1dlQTnHjRznBerJpS96oopjcVkrZlief",
                                 "OFXOSMRJxMs8oEiKvZGMsuwTDsr2oTAyiBJjjWg4YEj2N",
                                 "06GAwlT1hqND4OcS0dOFQz510",
                                 "yqr8uLrDMgiFlJ1xSDbBu4nBoxApyFjvXMFJSH90urXjqQ36Hc"))

        with open(settings.STATIC_ROOT+'geocode_id.json') as json_data:
        # with open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'twitter/geocode_id.json')) as json_data:
            d = json.load(json_data)
            response_json = {}
            response_json["name"] = "result twitter API"
            response_json['keywords'] = keywords
            tweets = []

            # set time
            start_time = time.time()
            limit = 0
            num_id = 1
            limit_post = int(num_of_city)*int(num_of_post)
            print(limit_post)
            #get tweet for each cities
            for city in d["city"]:
                if (limit < limit_post):
                    kota = city['kota']
                    print("load for ", kota)

                    query = twitter.search.tweets(q=keywords,
                                                  count=num_of_post,
                                                  lang="id",
                                                  lat=city["latitude"],
                                                  long=city["longitude"],
                                                  result_type="recent",
                                                  until=initial_date)

                    for item in query["statuses"]:
                        tweets.append({
                            "created_at" : item["created_at"],
                            "text" : item["text"],
                            "name": item["user"]["screen_name"],
                            "location" : item["user"]["location"],
                            "points" : kota,
                            "lang" : item["lang"],
                            "tweet_id": str(num_id) + keywords + item["user"]["screen_name"],
                            "retweet_count" : item['retweet_count'],
                            "favorite_count" : item['favorite_count'],
                            "user_name" : item['user']['name'],
                            "user_screen_name" : item['user']['screen_name'],
                            "user_id" : item['user']['id'],
                            "profile_image_url" : item['user']['profile_image_url'],
                            "id_str" : item['id_str']
                        })
                        limit +=1
                    num_id+=1

            response_json["time_process"] = (time.time() - start_time)
            response_json["tweets"] = tweets
            return(response_json)

