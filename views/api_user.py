from rest_framework import generics
from ..serializer.user_serializers import UserSerializer
from django.contrib.auth.models import User


class CreateView(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def perform_create(self, serializer):
        serializer.save()


class DetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class SearchUserView(generics.ListAPIView):
    serializer_class = UserSerializer

    def get_queryset(self):

        username = self.request.POST.get(key='username', default=None)
        password = self.request.POST.get(key='password', default=None)
        return User.objects.filter(username = username)

