from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
import datetime


class Splitdetikcontent(models.Model):
    file = models.CharField(max_length=300)
    source = models.CharField(max_length=300)
    title = models.CharField(max_length=300)
    location = models.CharField(max_length=300)
    detik_id = models.CharField(max_length=300)
    result_sentence = models.TextField()
    old_sentence = models.TextField()
    split_sentence = JSONField()

class ProjectSentiment(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    name = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )

    user = models.ForeignKey(
        User,
        null=True,
        blank=True
    )

    status_project = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )

class SearchHistory(models.Model):
    date_created = models.DateTimeField(
        default= datetime.datetime.now,
        null=True,
        blank=True
    )

    query = models.CharField(
        max_length= 200,
        null=True,
        blank=True
    )

    user = models.ForeignKey(
        User,
        null=True,
        blank=True
    )

    category = models.CharField(
        max_length= 200,
        null=True,
        blank=True
    )

    project = models.ForeignKey(
        ProjectSentiment,
        null=True,
        blank=True
    )


class ResultNewsDetik(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    link = models.CharField(max_length=300)
    history = models.ForeignKey(
        SearchHistory,
        null=True,
        blank=True
    )
    news_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    date_news_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    urls = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    img = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    is_edited = models.BooleanField(
        default=False,
        blank=True
    )
    total_sentiment_positive = models.IntegerField(
        default=0,
        blank=True
    )
    total_sentiment_neutral = models.IntegerField(
        default=0,
        blank=True
    )
    total_sentiment_negative = models.IntegerField(
        default=0,
        blank=True
    )
    opinion = models.CharField(
        default="neutral",
        max_length=50)

class ResultDetik(models.Model):
    date_created = models.DateTimeField(
        default= datetime.datetime.now,
        null=True,
        blank=True
    )
    opinion = models.CharField(max_length=50)
    sentence = models.TextField()
    result_news_detik = models.ForeignKey(
        ResultNewsDetik,
        null=True,
        blank=True
    )
    is_edited = models.BooleanField(
        default=False,
        blank=True
    )

class RelationNewsDetik(models.Model):
    news_detik = models.ForeignKey(
        ResultNewsDetik,
        null=True,
        blank=True
    )
    sentence_detik = models.ForeignKey(
        ResultDetik,
        null=True,
        blank=True
    )


class ResultOtherNews(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    title = models.CharField(max_length=300,
                             null=True,
                             blank=True)
    history = models.ForeignKey(
        SearchHistory,
        null=True,
        blank=True
    )
    news_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    date_news_created = models.DateTimeField(
        null=True,
        blank=True
    )
    urls = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    img = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    is_edited = models.BooleanField(
        default=False,
        blank=True
    )
    sentence = models.TextField(
        null=True,
        blank=True
    )
    situs = models.TextField(
        null=True,
        blank=True
    )
    subsitus = models.TextField(
        null=True,
        blank=True
    )
    opinion = models.CharField(max_length=50,
                               blank=True,
                               null=True)


class LevelCSE(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    tipe = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    cakupan = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    nama = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )

class CSEGoogle(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    situs_utama = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    situs = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    link = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    cse_id = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    public_url = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    test = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )

    is_valid = models.BooleanField(
        default=False,
        blank=True
    )
    total_post = models.IntegerField(
        default=0,
        blank=True
    )

    level = models.ForeignKey(
        LevelCSE,
        null=True,
        blank=True
    )




class CountSentimentNews(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now,
        null=True,
        blank=True
    )
    situs_utama = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    total_post = models.IntegerField(
        default=0,
        blank=True
    )
    total_sentiment_positive = models.IntegerField(
        default=0,
        blank=True
    )
    total_sentiment_neutral = models.IntegerField(
        default=0,
        blank=True
    )
    total_sentiment_negative = models.IntegerField(
        default=0,
        blank=True
    )
    history = models.ForeignKey(
        SearchHistory,
        null=True,
        blank=True
    )

class BondowosoNews(models.Model):
    date_created = models.DateTimeField(
        default=datetime.datetime.now
    )
    nama_situs = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    url = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    nama_wartawan = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    is_scrape_able = models.BooleanField(
        default=False,
        blank=True
    )
    note = models.TextField(
        null=True,
        blank=True
    )


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
