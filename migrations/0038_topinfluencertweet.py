# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-01-30 03:48
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0037_resultnewsdetik_opinion'),
    ]

    operations = [
        migrations.CreateModel(
            name='TopInfluencerTweet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True)),
                ('user_screen_name', models.CharField(blank=True, max_length=100, null=True)),
                ('user_twitter_link', models.CharField(blank=True, max_length=100, null=True)),
                ('user_photo_link', models.CharField(blank=True, max_length=100, null=True)),
                ('total_post', models.IntegerField(default=0, null=True)),
                ('is_verified', models.BooleanField(default=False)),
                ('history', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.SearchHistory')),
            ],
        ),
    ]
