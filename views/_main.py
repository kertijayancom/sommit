from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseNotFound, Http404
from ..models import *
from openpyxl import *
import datetime, json, os
# index
def index(request):
    return render(request, "api/index.html",{})

def list_user(request):
    users = User.objects.all()
    list_raw_path = Rawpathjson.objects.all()
    list_split_path = Splitpathjson.objects.all()
    db_twitter_post = Splittwitterpost.objects.all()
    db_instagram_post = Splitinstagrampost.objects.all()
    db_facebook_post = Splitfacebookpost.objects.all()

    return render(request, 'api/list_user.html', {
        'users' : users,
        'raw_files' : list_raw_path,
        'split_files' : list_split_path,
        'twitter_post' : db_twitter_post,
        'instagram_post': db_instagram_post,
        'facebook_post' : db_facebook_post})

