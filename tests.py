# from django.test import TestCase
# from django.contrib.auth.models import User
# from rest_framework.authtoken.models import Token
# # from rest_framework.test import APIClient
# from rest_framework import status
# # from rest_framework.authtoken.models import Token
# # from django.core.urlresolvers import reverse
# # from django.test.client import Client
# # from rest_framework.test import force_authenticate
# # import base64
#
#
# from django.test import Client
#
# # # USER API TEST
# class BasicUserTestCase(TestCase):
#     def setUp(self):
#         self.new_user = User.objects.create_superuser(username="zhq",email="zhq@z.com",password="superoke1234yes")
#         self.token_superuser = Token.objects.create(user=self.new_user)
#         self.token = "a95e94c59b90b452d42713cc0c145b9f9b86416b"
#         self.auth_headers ={
#             'HTTP_AUTHORIZATION': 'Token '+ self.token
#         }
#         self.c = Client()
#         self.response = self.c.post('/auth_token/',{'username' : "okebro",
#                                                    'password' : "superoke123yes"},
#                                     **self.auth_headers)
#
#     def test_can_read_auth(self):
#         self.assertEqual(self.response.status_code, status.HTTP_200_OK)
#
#




# # USER API TEST
# class BasicUserTestCase(TestCase):
#
#     # set model
#     def setUp(self):
#         # set super user
#         self.password = 'sommit123'
#         self.newSuperUser = User.objects.create_superuser('zhq', 'zhq@sommit.com', self.password)
#
#         # set basic auth credentials
#         self.client.defaults['HTTP_AUTHORIZATION'] = base64.b64encode(b'zhq:sommit123')
#
#         # create new user
#         self.user_data = {'username' : "ariq", 'password' : "okebro123yes"}
#         self.response = self.client.post(
#             reverse('create_user'),
#             self.user_data,
#             format="json")
#
#     def test_api_can_create_a_user(self):
#
#         self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#     def test_api_can_get_user(self):
#
#         # get token
#         self.client.defaults['HTTP_AUTHORIZATION'] = base64.b64encode(b'zhq:sommit123')
#         self.token = self.client.post(
#             reverse('get_token_user'),
#             self.user_data,
#             format="json")
#
#         user = User.objects.filter(username= self.user_data.username).first()
#         token = Token.objects.create(user = user)
#
#         response = self.client.get(
#             reverse('detail_user', kwargs={'pk': user.id}),
#             format="json")
#         self.assertEqual(self.response.content, "ok")
# #
# #     # error ketika melakukan update
# #     def test_api_can_update_username(self):
# #         user = User.objects.get()
# #         change_user_data = {
# #             'username' : 'budistyawan'
# #         }
# #         response = self.client.put(
# #             reverse('detail_user', kwargs={'pk': user.id}),
# #             change_user_data,
# #             format="json")
# #         self.assertEqual(response.status_code, status.HTTP_200_OK)
# #
# #     def test_api_can_delete_user(self):
# #         user = User.objects.get()
# #         response = self.client.delete(
# #             reverse('detail_user', kwargs={'pk': user.id}),
# #             format="json",
# #             follow=True
# #         )
# #         self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
# #
# #
# #
# #     # def test_user_get_token(self):
# #     #     self.assertEqual(0, 0)
# #
# # class AuthUserTestCase(TestCase):
# #
# #     # set model
# #     def setUp(self):
# #         self.client = APIClient()
# #         self.user_data = {'username' : "ariqcw", 'password' : "okebro123yes"}
# #         self.response = self.client.post(
# #             reverse('create_user'),
# #             self.user_data,
# #             format="json")
# #
# #     def test_api_can_get_token(self):
# #         user = User.objects.get()
# #         user_data = {'username': user.username, 'password': user.password}
# #         response = self.client.post(
# #             reverse('get_token_user'),
# #             user_data,
# #             format="json")
# #         self.assertEqual(response.content, status.HTTP_200_OK)
# #
# #     def test_api_cannot_get_token_because_credentials(self):
# #         user = User.objects.get()
# #         user_data = self.user_data
# #         response = self.client.post(
# #             reverse('get_token_user'),
# #             user_data,
# #             format="json"
# #         )
# #         self.assertTrue("Unable to log in with provided credentials" in response.content)
# #
# #     def test_api_cannot_get_token_because_invalid_username_or_password(self):
# #         user = User.objects.get()
# #         user_data = self.user_data
# #         response = self.client.post(
# #             reverse('get_token_user'),
# #             user_data,
# #             format="json"
# #         )
# #         self.assertTrue("Unable to log in with provided credentials" in response.content)
# #
# #


# SKENARIO MEMBUAT USER
# 1. gunakan token superuser :
#       token = a95e94c59b90b452d42713cc0c145b9f9b86416b
# 2. akses api/users/ dengan METHOD POST
#       set Header -> Authorization : Token a95e94c59b90b452d42713cc0c145b9f9b86416b
#   variabel =
#       username = "sodara11"
#       password = "sodara123yes"
#       email    = "admin@sodara.com"
# 3. output berupa JSON hasil akhir:
#   {
#     "id": 15,
#     "username": "sodara11",
#     "password": "sodara123yes",
#     "first_name": "",
#     "last_name": "",
#     "email": "admin@sodara.com",
#     "is_staff": false,
#     "last_login": null,
#     "date_joined": "2017-11-07T06:43:32.114216Z"
#   }


# SKENARIO MENGAMBIL TOKEN USER
# 1. gunakan token superuser :
#       token = a95e94c59b90b452d42713cc0c145b9f9b86416b
# 2. akses api/token/ dengan METHOD POST
#   set Header -> Authorization : Token a95e94c59b90b452d42713cc0c145b9f9b86416b
#       variabel = {
#           username = "sodara11"
#           password = "sodara123yes"
#       }
# 3. output berupa JSON hasil akhir :
#  {"token":"491c935501339645cc33b0c77a2f431d0e4acf3f"}


# SKENARIO MENGAMBIL INFORMASI USER DARI TOKEN
# 1. gunakan token user :
#       token = 491c935501339645cc33b0c77a2f431d0e4acf3f
# 2. akses api/token/info dengan METHOD GET
#       set Header -> Authorization : Token 491c935501339645cc33b0c77a2f431d0e4acf3f
# 3. output berupa JSON hasil akhir :
#         {
#             "user": {
#                 "id": 15,
#                 "username": "sodara11",
#                 "email": "admin@sodara.com"
#             },
#             "token": "491c935501339645cc33b0c77a2f431d0e4acf3f"
#         }


# SKENARIO MELAKUKAN SEARCH
# 1. gunakan token user :
#       token = 491c935501339645cc33b0c77a2f431d0e4acf3f
# 2. akses api/search/ dengan METHOD POST
#       set Header -> Authorization : Token 491c935501339645cc33b0c77a2f431d0e4acf3f
#       variabel = {
#           query = "lagi macet perjalanan jauh adem kalo dengarkan ceramah ustad somad di Youtube"       ## Berupa kata kunci maupun kalimat max 200 karakter
#       }
# 3. menangkap informasi search dari token :
#   {
#     "id": 3,
#     "query": "lagi macet perjalanan jauh adem kalo dengarkan ceramah ustad somad di Youtube",
#     "user": 15,
#     "date_created": "2017-11-07T14:53:19.825830Z"
#   }


# SKENARIO PENCARIAN DI DETIK
# 1. gunakan token user :
#       token = 491c935501339645cc33b0c77a2f431d0e4acf3f
# 2. akses api/detik dengan METHOD POST
#       set Header -> Authorization : Token 491c935501339645cc33b0c77a2f431d0e4acf3f
#       variabel = {
#           query = "lagi macet perjalanan jauh adem kalo dengarkan ceramah ustad somad di Youtube"       ## Berupa kata kunci maupun kalimat max 200 karakter
#           search_id = 'search_history_id'
#       }
# 3. menangkap informasi search dari token :
#   {
#     "id": 3,
#     "query": "lagi macet perjalanan jauh adem kalo dengarkan ceramah ustad somad di Youtube",
#     "user": 15,
#     "date_created": "2017-11-07T14:53:19.825830Z"
#   }
