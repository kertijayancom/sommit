from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseNotFound, Http404
from ..models import *
from openpyxl import *
import datetime, json, os, nltk

from ..extras.news.detik import detik_api
from ..extras.news.liputan6 import liputan6_api
from ..extras.tokenizer import tokenizer_news
from ..extras.algorithm import naivebayes
from django.contrib.auth.models import User

current_user = 1#User.objects.get(id=1)

def get_detik(request,q):
    # get date
    a_time = str(datetime.datetime.now())
    a_time = a_time.replace(" ", "__")
    a_time = a_time.replace(":", "_")
    a_time = a_time.replace("-", "_")

    # create object twitter api
    new_obj = detik_api.DetikApi(q)
    a = new_obj.get_news(q)

    query = str(q)
    query = query.replace(" ", "_")

    file_name = '_' + a_time + "_" + query + '_detik.json'
    file_name1 = '..\extras\_result\\' + file_name
    path_file = os.path.join(os.path.dirname(__file__), file_name1)

    # save to _file json
    with open(path_file, "w") as outfile:
        json.dump(a, outfile)

    new_path = Rawpathjson.objects.create(
        path_name=file_name,
        user_id=current_user,
        media_type = "detik"
    ).save()

    page_result = {}
    page_result["user_id"] = current_user.id
    page_result["path_id"] = Rawpathjson.objects.get(path_name=file_name).id

    return HttpResponse(json.dumps(page_result), content_type="applications/json")

# # get tokenizer detik from _file or database, need path
def tokenizer_detik(request, path_id, user_id):
    new_path = Rawpathjson.objects.get(id = path_id)
    if (new_path.user_id.id == User.objects.get(id=user_id).id):
        pathfile = new_path.path_name
        path_file = os.path.join(os.path.dirname(__file__), '..\extras\_result\\'+pathfile)

        a = tokenizer_news.NewsTokenizer(path_file)
        response_data = a.clean_detik(path_file)

        # save to _file tokenize json
        file_name = '..\extras\_result_tokenize\\'+pathfile
        path_file_tokenize = os.path.join(os.path.dirname(__file__), file_name)
        with open(path_file_tokenize, "w") as outfile:
            json.dump(response_data, outfile)

        # delete raw data json
        # os.remove(path_file)
        new_path_split = Splitpathjson.objects.create(
            path_name = pathfile,
            user_id = User.objects.get(id=user_id),
            media_type = "detik"
        ).save()

        page_result = {}
        page_result["user_id"] = User.objects.get(id=user_id).id
        page_result["path_file"] = Splitpathjson.objects.get(path_name = pathfile).id

        return HttpResponse(json.dumps(page_result), content_type="applications/json")

    else:
        return HttpResponse(status=500)

def detikjson_to_db(request, path_id, user_id):
    get_path = Splitpathjson.objects.get(id = path_id)
    if(get_path.media_type == "detik"):
        # read json
        path_file = get_path.path_name
        file_name = '..\extras\_result_tokenize\\' + path_file
        path_file_tokenize = os.path.join(os.path.dirname(__file__), file_name)
        post = 0
        with open(path_file_tokenize) as data_file:
            data = json.load(data_file)
            for item in data['feed']:
                for sentence in item['result_content']:
                    Splitdetikcontent.objects.create(
                        file=data['_file'],
                        source=data['source'],
                        title=item['title'],
                        location=item['location'],
                        detik_id=sentence['id'],
                        result_sentence=sentence['resultSentence'],
                        old_sentence=sentence['oldSentence'],
                        split_sentence={
                            "split_sentence": sentence['splitSentence']
                        }
                    ).save()
                post +=1
        return HttpResponse(str(post))
    else:
        return HttpResponse(status=500)

def get_liputan6(request,q):
    # get date
    a_time = str(datetime.datetime.now())
    a_time = a_time.replace(" ", "__")
    a_time = a_time.replace(":", "_")
    a_time = a_time.replace("-", "_")

    # create object twitter api
    new_obj = liputan6_api.Liputan6Api(q)
    a = new_obj.get_news(q)

    query = str(q)
    query = query.replace(" ", "_")

    file_name = '_' + a_time + "_" + query + '_liputan6.json'
    file_name1 = '..\extras\_result\\' + file_name
    path_file = os.path.join(os.path.dirname(__file__), file_name1)

    # save to _file json
    with open(path_file, "w") as outfile:
        json.dump(a, outfile)

    new_path = Rawpathjson.objects.create(
        path_name=file_name,
        user_id=current_user,
        media_type = "liputan6"
    ).save()

    page_result = {}
    page_result["user_id"] = current_user.id
    page_result["path_id"] = Rawpathjson.objects.get(path_name=file_name).id

    return HttpResponse(json.dumps(page_result), content_type="applications/json")
# # get tokenizer detik from _file or database, need path
def tokenizer_liputan6(request, path_id, user_id):
    new_path = Rawpathjson.objects.get(id = path_id)
    if (new_path.user_id.id == User.objects.get(id=user_id).id):
        pathfile = new_path.path_name
        path_file = os.path.join(os.path.dirname(__file__), '..\extras\_result\\'+pathfile)

        a = tokenizer_news.NewsTokenizer(path_file)
        response_data = a.clean_liputan6(path_file)

        # save to _file tokenize json
        file_name = '..\extras\_result_tokenize\\'+pathfile
        path_file_tokenize = os.path.join(os.path.dirname(__file__), file_name)
        with open(path_file_tokenize, "w") as outfile:
            json.dump(response_data, outfile)

        # delete raw data json
        # os.remove(path_file)
        new_path_split = Splitpathjson.objects.create(
            path_name = pathfile,
            user_id = User.objects.get(id=user_id),
            media_type = "liputan6"
        ).save()

        page_result = {}
        page_result["user_id"] = User.objects.get(id=user_id).id
        page_result["path_file"] = Splitpathjson.objects.get(path_name = pathfile).id

        return HttpResponse(json.dumps(page_result), content_type="applications/json")

    else:
        return HttpResponse(status=500)

def readcsv(request):
    pathfile = "news.xlsx"
    path_file = os.path.join(os.path.dirname(__file__), '..\extras\_file\\' + pathfile)
    obj_tokenizer = tokenizer_news.NewsTokenizer("oke")

    file_excel = load_workbook(path_file)
    # sindonews
    wb_liputan6 = file_excel['detik']
    a = {}
    max_row = 902
    for position in range(2, max_row):
        a = {"split_message" : obj_tokenizer.news_tokenize_sentence(str(wb_liputan6['E'+str(position)].value))}
        Datatrain.objects.create(
            name = "oki",
            message_id = wb_liputan6['C'+str(position)].value,
            result_message = wb_liputan6['D'+str(position)].value,
            old_message = wb_liputan6['E'+str(position)].value,
            split_message = a,
            media_type = "detik",
            opinion = str(wb_liputan6['F'+str(position)].value)
        )
    print(a)
    return HttpResponse(a['split_message'])


def algorithm(request):

    run_algorithm = naivebayes.NaiveBayes(80,20)
    run_algorithm.sentiment_twitter_train()
    # run_algorithm.save_model("twitter")
    return HttpResponse(json.dumps(result), content_type="applications/json")
    return render(request, 'api/algorithm.html',{
        'result_detik': run_algorithm.sentiment_detik_train(),
        # 'result_liputan6': run_algorithm.sentiment_liputan6_train(),
        # 'result_sindonews': run_algorithm.sentiment_sindonews_train(),
    })


def test_detik_query(request, query):
    result = naivebayes.NaiveBayes(80,20).sentiment_test_detik(query=query)
    return HttpResponse(json.dumps(result), content_type="applications/json")
    # return render(request, 'api/test_detik_query.html',{'result' : result })


def test_detik(request):
    run_algorithm = naivebayes.NaiveBayes(80,20)
    detik_result =  [
        run_algorithm.sentiment_test_detik(query="lagi lagi pendidikan digugat"),
        run_algorithm.sentiment_test_detik(query="prabowo di kediaman sby"),
        run_algorithm.sentiment_test_detik(query="syarat minimum jalannya pemerintahan")
    ]
    return render(request, 'api/test_detik.html',{
        'result' : detik_result
    })