from rest_framework import serializers
from django.contrib.auth.models import User,Group
from  .._models.news import ResultDetik

class DetikSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""
    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = ResultDetik
        fields = ('id','opinion','sentence','date_created')
        read_only_fields = ('id','date_created')