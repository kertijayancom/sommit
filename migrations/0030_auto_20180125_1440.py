# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-01-25 14:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0029_projectsentiment_status_project'),
    ]

    operations = [
        migrations.AddField(
            model_name='resultothernews',
            name='situs',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='resultothernews',
            name='subsitus',
            field=models.TextField(blank=True, null=True),
        ),
    ]
