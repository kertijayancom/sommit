from rest_framework import generics
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User, Group
from ..serializer.search_serializers import SearchSerializer
from .._models.news import SearchHistory, ProjectSentiment


# ============================== SEARCH API ==============================
class CreateView(generics.ListCreateAPIView):
    queryset = SearchHistory.objects.all()
    serializer_class = SearchSerializer

    def perform_create(self, serializer):
        token = self.request.META.get('HTTP_AUTHORIZATION').split()[1]
        current_project = ProjectSentiment.objects.filter(id = int(self.request.data['id_project'])).first()
        serializer.save(user = Token.objects.filter(key = token).first().user, project = current_project)

# ============================== SEARCH API ==============================
class DetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = SearchHistory.objects.all()
    serializer_class = SearchSerializer
